# encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_printogo'
  s.version     = '1.1.5'
  s.summary     = 'Extension to manage Print Shop'
  s.required_ruby_version = '>= 1.8.7'

  s.author            = 'Diginess Snc'
  s.email             = 'info@diginess.com'

  s.files        = Dir['CHANGELOG', 'README.md', 'LICENSE', 'lib/**/*', 'app/**/*', 'db/**/*', 'config/**/*']
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree_core', '>= 1.1.5'
  s.add_dependency 'spree_banner', '~> 1.1.6'
  s.add_dependency 'spree_multi_slideshow', '~> 1.1.7'
  s.add_dependency 'spree_payment_calculator', '~> 1.1.0'
  s.add_dependency 'spree_invoice', '~> 1.2.3'
  
  s.add_dependency 'bootstrap-sass', '2.3.0.1'
  s.add_dependency 'remotipart', '~> 1.2'
  s.add_dependency 'jquery-fileupload-rails', '0.4.4'
  s.add_dependency 'font-awesome-sass-rails', ['~> 3.0', '>= 3.0.0.1']
	s.add_dependency 'easy_as_pie'
  s.add_dependency 'ckeditor', '>= 3.7.3'
  s.add_dependency 'business_time', '0.6.2'
  # s.add_dependency 'seedbank'
end
