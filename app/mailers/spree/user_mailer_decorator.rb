Spree::UserMailer.class_eval do

	def reset_password_instructions(user)
		# @edit_password_reset_url = spree.edit_user_password_url(:reset_password_token => user.reset_password_token)
		@edit_password_reset_url = "#{Spree::Config[:site_url]}/user/password/edit?reset_password_token=#{user.reset_password_token}"

		mail(:to => user.email,
		:subject => Spree::Config[:site_name] + ' ' + I18n.t(:password_reset_instructions))
	end

	def bill_change_email(address, user, resend = false)
		@user = user
		@address = address
		subject = "#{Spree::Config[:site_name]} #{t('user_mailer.bill_change_email.subject')}"
		mail(:to => Spree::Config[:notify_email], :subject => subject)
	end
end
