Spree::OrderMailer.class_eval do

  def confirm_email_store(order, resend = false)
    @order = order
    subject = (resend ? "[#{t(:resend).upcase}] " : '')
    subject += "#{Spree::Config[:site_name]} #{t('order_mailer.confirm_email.subject')} ##{order.number}"
    mail(:to => Spree::MailMethod.current.preferred_mails_from,
         :subject => subject)
  end
end
