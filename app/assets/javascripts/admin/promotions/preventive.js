/**
 *  Qui è implementata la funzione che si occupa di recuperare tutti i dati e inviarli per richiedere un preventivo
 */
$(function () {
	$('#line_item_promotion_paper').change(function() {
		setPaperWeight($('#line_item_promotion_product_id').val(), $(this).val(), $('#line_item_promotion_format').val());
	});
	
	$('#line_item_promotion_paper_cover').change(function() {
		setPaperWeightCover($('#line_item_promotion_product_id').val(), $(this).val(), $('#line_item_promotion_format').val());
	});
	
	$('#line_item_promotion_format').change(function() {
		if ($(this).val() == 0 || $(this).text() == "Personalizzato") {
			$('#width').show();
			$('#height').show();
		} else {
			$('#width').hide();
			$('#height').hide();
			if ($('#line_item_promotion_paper').children().length == 1) {
				setPaper($('#line_item_promotion_product_id').val(), $(this).val());
			}
			setPaperCover($('#line_item_promotion_product_id').val(), $(this).val());
			//setFoldingFormat($('#line_item_promotion_product_id').val(), $(this).val());
		}
	});
});

function retrive_folding_format(prod_id, format_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/'+ prod_id +'/retrive_folding_format/'+ format_id +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function retrive_paper_weight(prod_id, paper_id, format_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/'+ prod_id +'/retrive_paper_weight/'+ paper_id +'/'+ format_id +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function retrive_paper(prod_id, format_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/'+ prod_id +'/retrive_paper/'+ format_id +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function retrive_paper_weight_cover(prod_id, paper_id, format_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/'+ prod_id +'/retrive_paper_weight_cover/'+ paper_id +'/'+ format_id +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function retrive_paper_cover(prod_id, format_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/'+ prod_id +'/retrive_paper_cover/'+ format_id +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function setFoldingFormat(prod_id, format_id) {
	var a = retrive_folding_format(prod_id, format_id);
	var init = $('#line_item_promotion_folding').children()[0];
	$('#line_item_promotion_folding').removeAttr("disabled").html(init);
	if (a) {
		for (i = 0; i < a.length; i++) {
			$('#line_item_promotion_folding').append($("<option></option>").
				attr('value', a[i].id).
				text(a[i].name));
		}
	} else {
		$('#line_item_promotion_folding').attr("disabled", true);
	}
}

function setPaperWeight(prod_id, paper_id, format_id) {
	var a = retrive_paper_weight(prod_id, paper_id, format_id);
	var init = $('#line_item_promotion_weight').children()[0];
	$('#line_item_promotion_weight').removeAttr("disabled").html(init);
	if (a) {
		for (i = 0; i < a.length; i++) {
			$('#line_item_promotion_weight').append($("<option></option>").
				attr('value', a[i]).
				text(a[i]));
		}
	} else {
		$('#line_item_promotion_weight').attr("disabled", true);
	}
}

function setPaper(prod_id, format_id) {
	var a = retrive_paper(prod_id, format_id);
	var init = $('#line_item_promotion_paper').children()[0];
	$('#line_item_promotion_paper').removeAttr("disabled").html(init);
	if (a) {
		for (i = 0; i < a.length; i++) {
			$('#line_item_promotion_paper').append($("<option></option>").
				attr('value', a[i].id).
				text(a[i].presentation));
		}
	} else {
		$('#line_item_promotion_paper').attr("disabled", true);
	}
}

function setPaperWeightCover(prod_id, paper_id, format_id) {
	var a = retrive_paper_weight_cover(prod_id, paper_id, format_id);
	var init = $('#line_item_promotion_paper_cover_weight').children()[0];
	$('#line_item_promotion_paper_cover_weight').removeAttr("disabled").html(init);
	if (a) {
		for (i = 0; i < a.length; i++) {
			$('#line_item_promotion_paper_cover_weight').append($("<option></option>").
				attr('value', a[i]).
				text(a[i]));
		}
	} else {
		$('#line_item_promotion_paper_cover_weight').attr("disabled", true);
	}
}

function setPaperCover(prod_id, format_id) {
	var a = retrive_paper_cover(prod_id, format_id);
	var init = []
	$('#line_item_promotion_paper_cover').children().each(function() {
		if ($(this).val() == "" || $(this).val() == "-2" || $(this).val() == "-1") {
			init.push($(this)[0]);
		}
	});
	
	$('#line_item_promotion_paper_cover').removeAttr("disabled").html(init);
	if (a) {
		for (i = 0; i < a.length; i++) {
			$('#line_item_promotion_paper_cover').append($("<option></option>").
				attr('value', a[i].id).
				text(a[i].presentation));
		}
	} else {
		$('#line_item_promotion_paper_cover').attr("disabled", true);
	}
}

function preCalculate() {
	if ($('#new_line_item_promotion').length > 0) {
		data = $('#new_line_item_promotion').serializeArray();
	} else {
		data = $('form[id*="edit_line_item_promotion"]').serializeArray();
	}
	
	$.ajax({
        type: "POST",
        url: '/admin/precalculate',
        data: data
    });
}
