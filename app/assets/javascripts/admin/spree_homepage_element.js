function update_disabled_homepage(c){
    var a = ['.url_text_field_homepage','.taxon_select_homepage','.product_select_homepage','.promotion_select_homepage'];
    for( i = 0; i < a.length; i++){
        $(a[i]).attr('disabled', true);
    }
    $(c).attr('disabled', false);
}

$(function(){

  $("#url_type_insert_new_url_homepage").click(function(){
      update_disabled_homepage('.url_text_field_homepage');
      document.getElementById("advertisement").innerHTML="Ricordati di compilare i campi sottostanti";
  });
  $("#url_type_select_from_taxon_homepage").click(function(){
      update_disabled_homepage('.taxon_select_homepage');
      document.getElementById("advertisement").innerHTML="I dati sottostanti sostituiranno i valori della categoria selezionata";
  });
  $("#url_type_select_from_product_homepage").click(function(){
      update_disabled_homepage('.product_select_homepage');
      document.getElementById("advertisement").innerHTML="I dati sottostanti sostituiranno i valori del prodotto selezionato";
  });
  $("#url_type_select_from_promotion_homepage").click(function(){
      update_disabled_homepage('.promotion_select_homepage');
      document.getElementById("advertisement").innerHTML="Ricordati di compilare e/o personalizzare i campi sottostanti";
  });
  $(".taxon_select_homepage").change(function(){
      $("#homepage_element_title").val("");
      $("#homepage_element_description").val("");
  });
  $(".product_select_homepage").change(function(){
      $("#homepage_element_title").val("");
      $("#homepage_element_description").val("");
  });
  $(".promotion_select_homepage").change(function(){
      $("#homepage_element_title").val($(this).find('option:selected').text());
      $("#homepage_element_description").val($(this).find('option:selected').text()); 
  });
});