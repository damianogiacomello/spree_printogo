function getPrinterFormat(elem) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/retrieve_printer_format/'+ elem.val() +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function setPrinterFormat(elem) {
	var a = getPrinterFormat(elem);
	var default_val = $(elem).data('default');
	$("select[name='" + $(elem).data('format') + "']").html('');
	for (i = 0; i < a.length; i++) {
		$("select[name='" + $(elem).data('format') + "']").append($("<option></option>").
			attr('value', a[i].id).
			attr('selected', default_val == a[i].id).
			text(a[i].presentation));
	}
}