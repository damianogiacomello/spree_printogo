$(function () {	
    if ($('#variant_paper_weight_').length > 0) {
        var tag_name = '#variant_paper_weight_';
        var tag_prefix = 'variant';
        init_pg(tag_name, tag_prefix, paper_weight_hash, paper_products_selected, paper_products);
    }

    if ($('#cover_variant_paper_weight_').length > 0) {
        var tag_name = '#cover_variant_paper_weight_';
        var tag_prefix = 'cover_variant';
        init_pg(tag_name, tag_prefix, cover_paper_weight_hash, cover_paper_products_selected, cover_paper_products);
    }

    $("#slider-range-min-bw").slider({
        range:"min",
        value:$("#variant_limit_quantity_bw").val(),
        min:1,
        max:5000,
        slide:function (event, ui) {
            $("#amount-bw").html(ui.value);
            $("#variant_limit_quantity_bw").val(ui.value);
            $("." + $("#variant_limit_quantity_bw").data('ref')).html(ui.value);
            $("#slider-quarter-range-min-bw").slider({min:ui.value});
            $("#amount-quarter-bw").html(ui.value);
        }
    });
    $("#amount-bw").html($("#slider-range-min-bw").slider("value"));
    $("#variant_limit_quantity_bw").val($("#slider-range-min-bw").slider("value"));
    $("." + $("#variant_limit_quantity_bw").data('ref')).html($("#slider-range-min-bw").slider("value"));

    $("#slider-range-min-c").slider({
        range:"min",
        value:$("#variant_limit_quantity_c").val(),
        min:1,
        max:5000,
        slide:function (event, ui) {
            $("#amount-c").html(ui.value);
            $("#variant_limit_quantity_c").val(ui.value);
            $("." + $("#variant_limit_quantity_c").data('ref')).html(ui.value);
            $("#slider-quarter-range-min-c").slider({min:ui.value});
            $("#amount-quarter-c").html(ui.value);
        }
    });
    $("#amount-c").html($("#slider-range-min-c").slider("value"));
    $("#variant_limit_quantity_c").val($("#slider-range-min-c").slider("value"));
    $("." + $("#variant_limit_quantity_c").data('ref')).html($("#slider-range-min-c").slider("value"));

    // ----------------------- Slider for Quarter ---------------------- //
    $("#slider-quarter-range-min-bw").slider({
        range:"min",
        value:$("#variant_limit_quarter_bw").val(),
        min:1,
        max:10000,
        slide:function (event, ui) {
            $("#amount-quarter-bw").html(ui.value);
            $("#variant_limit_quarter_bw").val(ui.value);
        }
    });
    $("#amount-quarter-bw").html($("#slider-quarter-range-min-bw").slider("value"));
    $("#variant_limit_quarter_bw").val($("#slider-quarter-range-min-bw").slider("value"));

    $("#slider-quarter-range-min-c").slider({
        range:"min",
        value:$("#variant_limit_quarter_c").val(),
        min:1,
        max:10000,
        slide:function (event, ui) {
            $("#amount-quarter-c").html(ui.value);
            $("#variant_limit_quarter_c").val(ui.value);
        }
    });
    $("#amount-quarter-c").html($("#slider-quarter-range-min-c").slider("value"));
    $("#variant_limit_quarter_c").val($("#slider-quarter-range-min-c").slider("value"));
    // ----------------------- Slider for Quarter ---------------------- //

    // ----------------------- Slider for Cover ----------------------- //
    $("#cover-slider-range-min-bw").slider({
        range:"min",
        value:$("#cover_variant_limit_quantity_bw").val(),
        min:1,
        max:100000,
        slide:function (event, ui) {
            $("#cover-amount-bw").html(ui.value);
            $("#cover_variant_limit_quantity_bw").val(ui.value);
            $("." + $("#cover_variant_limit_quantity_bw").data('ref')).html(ui.value);
            $("#cover_cover-slider-quarter-range-min-bw").slider({min:ui.value});
            $("#cover_cover-amount-quarter-bw").html(ui.value);
        },
		change:function (event, ui) {
            $("#cover-amount-bw").html(ui.value);
            $("#cover_variant_limit_quantity_bw").val(ui.value);
            $("." + $("#cover_variant_limit_quantity_bw").data('ref')).html(ui.value);
            $("#cover_cover-slider-quarter-range-min-bw").slider({min:ui.value});
            $("#cover_cover-amount-quarter-bw").html(ui.value);
        }
    });
    $("#cover-amount-bw").html($("#cover-slider-range-min-bw").slider("value"));
    $("#cover_variant_limit_quantity_bw").val($("#cover-slider-range-min-bw").slider("value"));
    $("." + $("#cover_variant_limit_quantity_bw").data('ref')).html($("#cover-slider-range-min-bw").slider("value"));

    $("#cover-slider-range-min-c").slider({
        range:"min",
        value:$("#cover_variant_limit_quantity_c").val(),
        min:1,
        max:100000,
        slide:function (event, ui) {
            $("#cover-amount-c").html(ui.value);
            $("#cover_variant_limit_quantity_c").val(ui.value);
            $("." + $("#cover_variant_limit_quantity_c").data('ref')).html(ui.value);
            $("#cover-slider-quarter-range-min-c").slider({min:ui.value});
            $("#cover-amount-quarter-c").html(ui.value);
        },
		change:function (event, ui) {
            $("#cover-amount-c").html(ui.value);
            $("#cover_variant_limit_quantity_c").val(ui.value);
            $("." + $("#cover_variant_limit_quantity_c").data('ref')).html(ui.value);
            $("#cover-slider-quarter-range-min-c").slider({min:ui.value});
            $("#cover-amount-quarter-c").html(ui.value);
        }
    });
    $("#cover-amount-c").html($("#cover-slider-range-min-c").slider("value"));
    $("#cover_variant_limit_quantity_c").val($("#cover-slider-range-min-c").slider("value"));
    $("." + $("#cover_variant_limit_quantity_c").data('ref')).html($("#cover-slider-range-min-c").slider("value"));
    // ----------------------- Slider for Cover ----------------------- //

//    $('select[name*="variant[limit][printer"]').change();
//    $('select[name*="variant[limit][printer"]').change(function () {
//        onChangePrinter(this, printer_format_hash);
//    });
//
//    $('select[name*="cover_variant[limit][printer"]').change();
//    $('select[name*="cover_variant[limit][printer"]').change(function () {
//        onChangePrinter(this, printer_format_hash);
//    });
  // ----------------------- Slider for Bindery ----------------------- //
  	$("#bindery-slider-range").slider({
  		range: "min",
  	    value: $("#variant_limit_bindery_bw").val(),
  	    min: 1,
  	    max: 5000,
  	    slide: function( event, ui ) {
  	    	$("#bindery-amount").html(ui.value);
  		    $("#variant_limit_bindery_bw").val(ui.value);
  			$("#variant_limit_bindery_c").val(ui.value);
  		    $("." + $("#variant_limit_bindery_bw").data('ref')).html(ui.value);
  	    }
  	});
  	$("#bindery-amount").html($("#bindery-slider-range").slider("value"));
  	$("#variant_limit_bindery_bw").val($("#bindery-slider-range").slider("value"));
  	$("#variant_limit_bindery_c").val($("#bindery-slider-range").slider("value"));
  	$("." + $("#variant_limit_bindery_bw").data('ref')).html($("#bindery-slider-range").slider("value"));

  	$("#variant_limit_bindery_before_min_bw_id").change(function() {
  		$("#variant_limit_bindery_before_min_c_id").val(this.val());
  	});
  	$("#variant_limit_bindery_before_max_bw_id").change(function() {
  		$("#variant_limit_bindery_before_max_c_id").val(this.val());
  	});
  	$("#variant_limit_bindery_after_bw_id").change(function() {
  		$("#variant_limit_bindery_after_c_id").val(this.val());
  	});
  	// ----------------------- Slider for Bindery ----------------------- //

  	$('select[name*="variant[limit][printer"]').change();
});

function setConvenienceCover() {
	// Imposto il punto per bianco e nero
	var pose_bw_b = $('select[name="cover_variant[limit][pose_before_bw_id]"]').val();
	var printer_bw_b = $('select[name="cover_variant[limit][printer_before_bw_id]"]').val();
	var pose_bw_a = $('select[name="cover_variant[limit][pose_after_bw_id]"]').val();
	var printer_bw_a = $('select[name="cover_variant[limit][printer_after_bw_id]"]').val();
	var json_bw = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/calculate_convenience.json?bw=true&printer_b='+printer_bw_b+'&pose_b='+pose_bw_b+'&printer_a='+printer_bw_a+'&pose_a='+pose_bw_a,
		'dataType': "json",
		'success': function (data) { json_bw = data; }
	});
	
	$("#cover-slider-range-min-bw").slider( "option", "value", json_bw );
	
	// Imposto il punto per colori
	var pose_c_b = $('select[name="cover_variant[limit][pose_before_c_id]"]').val();
	var printer_c_b = $('select[name="cover_variant[limit][printer_before_c_id]"]').val();
	var pose_c_a = $('select[name="cover_variant[limit][pose_after_c_id]"]').val();
	var printer_c_a = $('select[name="cover_variant[limit][printer_after_c_id]"]').val();
	var json_c = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/calculate_convenience.json?bw=false&printer_b='+printer_c_b+'&pose_b='+pose_c_b+'&printer_a='+printer_c_a+'&pose_a='+pose_c_a,
		'dataType': "json",
		'success': function (data) { json_c = data; }
	});
	
	$("#cover-slider-range-min-c").slider( "option", "value", json_c );
}

