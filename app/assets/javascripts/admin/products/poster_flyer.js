$(function() {
	if ($('#variant_paper_weight_').length > 0) {
		var tag_name = '#variant_paper_weight_';
		var tag_prefix = 'variant';
		init_pg(tag_name, tag_prefix, paper_weight_hash, paper_products_selected, paper_products);
	}

	$("#slider-range-min-bw").slider({
		range: "min",
		value: $("#variant_limit_quantity_bw").val(),
		min: 1,
		max: 100000,
		slide: function( event, ui ) {
			$("#amount-bw").html(ui.value);
			$("#variant_limit_quantity_bw").val(ui.value);
			$("." + $("#variant_limit_quantity_bw").data('ref')).html(ui.value);
		},
		change: function( event, ui ) {
			$("#amount-bw").html(ui.value);
			$("#variant_limit_quantity_bw").val(ui.value);
			$("." + $("#variant_limit_quantity_bw").data('ref')).html(ui.value);
		}
	});
	$("#amount-bw").html($("#slider-range-min-bw").slider("value"));
	$("#variant_limit_quantity_bw").val($("#slider-range-min-bw").slider("value"));
	$("." + $("#variant_limit_quantity_bw").data('ref')).html($("#slider-range-min-bw").slider("value"));

	$("#slider-range-min-c").slider({
		range: "min",
		value: $("#variant_limit_quantity_c").val(),
		min: 1,
		max: 100000,
		slide: function( event, ui ) {
			$("#amount-c").html(ui.value);
			$("#variant_limit_quantity_c").val(ui.value);
			$("." + $("#variant_limit_quantity_c").data('ref')).html(ui.value);
		},
		change: function( event, ui ) {
			$("#amount-c").html(ui.value);
			$("#variant_limit_quantity_c").val(ui.value);
			$("." + $("#variant_limit_quantity_c").data('ref')).html(ui.value);
		}
	});
	$("#amount-c").html($("#slider-range-min-c").slider("value"));
	$("#variant_limit_quantity_c").val($("#slider-range-min-c").slider("value"));
	$("." + $("#variant_limit_quantity_c").data('ref')).html($("#slider-range-min-c").slider("value"));

	$('select[name*="variant[limit][printer"]').change();
});

function setConvenience() {
	// Imposto il punto per bianco e nero
	var pose_bw_b = $('select[name="variant[limit][pose_before_bw_id]"]').val();
	var printer_bw_b = $('select[name="variant[limit][printer_before_bw_id]"]').val();
	var pose_bw_a = $('select[name="variant[limit][pose_after_bw_id]"]').val();
	var printer_bw_a = $('select[name="variant[limit][printer_after_bw_id]"]').val();
	var json_bw = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/calculate_convenience.json?bw=true&printer_b='+printer_bw_b+'&pose_b='+pose_bw_b+'&printer_a='+printer_bw_a+'&pose_a='+pose_bw_a,
		'dataType': "json",
		'success': function (data) { json_bw = data; }
	});
	
	$("#slider-range-min-bw").slider( "option", "value", json_bw );
	
	// Imposto il punto per colori
	var pose_c_b = $('select[name="variant[limit][pose_before_c_id]"]').val();
	var printer_c_b = $('select[name="variant[limit][printer_before_c_id]"]').val();
	var pose_c_a = $('select[name="variant[limit][pose_after_c_id]"]').val();
	var printer_c_a = $('select[name="variant[limit][printer_after_c_id]"]').val();
	var json_c = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/admin/calculate_convenience.json?bw=false&printer_b='+printer_c_b+'&pose_b='+pose_c_b+'&printer_a='+printer_c_a+'&pose_a='+pose_c_a,
		'dataType': "json",
		'success': function (data) { json_c = data; }
	});
	
	$("#slider-range-min-c").slider( "option", "value", json_c );
}
