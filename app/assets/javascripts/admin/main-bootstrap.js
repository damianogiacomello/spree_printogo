handle_date_picker_fields = function(){
  $('.datepicker_bootstrap').datepicker({
    dateFormat: Spree.translations.date_picker,
    dayNames: Spree.translations.abbr_day_names,
    dayNamesMin: Spree.translations.abbr_day_names,
    monthNames: Spree.translations.month_names
  });
}

$(function() {
	$(".dropdown-toggle").dropdown();
	$(".alert, .flash").alert();
	$('.date_picker').datepicker({ dateFormat: "dd/mm/yy" });
	$('*[rel=tooltip-top]').tooltip({placement:'top', delay:{ show:0, hide:100 }});
	$('*[rel=popover-right]').popover({placement:'right', delay:{ show:0, hide:100 }, trigger: 'hover'});

	$('.pie-chart').easyPieChart({size: 150, animate: 2000, lineWidth: 6, scaleColor: false, trackColor: "#FFFFFF"});
});