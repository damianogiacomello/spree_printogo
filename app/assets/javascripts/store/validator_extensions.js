/*
 In questo file ci sono tutte le estensioni per il jquery validate
 */

/*
 Validatore personalizzato che consente di verificare se il valore inserito è un multiplo di quattro oppure no
 */
$.validator.addMethod("multiplesOfTwo", function (value, element) {
    return (parseInt(value) % 2) == 0;
}, "Deve essere un multiplo di 2.");
$.validator.addMethod("multiplesOfFour", function (value, element) {
    return (parseInt(value) % 4) == 0;
}, "Deve essere un multiplo di 4.");

$.validator.addMethod("multiplesOfEight", function (value, element) {
    return (parseInt(value) % 8) == 0;
}, "Deve essere un multiplo di 8.");

/*
 Controlla se il valore passato come value (cioè l'oggetto che lo invoca) e params in posizione 0 moltiplicati insieme
 non siano più grandi del valore passato in params[1].
 NB params[0]() perché è un valore dinamico cioè la validazione viene invocata così:
 $("#weight").rules("add", {"checkMaxFacadesWeightCover": [ function(){ return $('#number_of_facades').val()},
 max_facades_weight_cover]});
 */
$.validator.addMethod("checkMaxFacadesWeightCover", function (value, element, params) {
    return ((parseInt(value) * parseInt(params[0]())) <= parseInt(params[1]));
}, "La grammatura troppo alta per il numero di facciate.");

/*
 Controlla che la grammatura "value" (che è la grammatura della copertina) sia maggiore o uguale a quello del
 params[0] che sarebbe quello della carta interna, solo nel caso che per la copertina sia stata scelta una carta
 differente rispetto a quella delle pagine interne
 */
$.validator.addMethod("checkWeightCoverWeightPaper", function (value, element, params) {
    if (params[1]() == "-1") {
        return true;
    } else {
        var weightPaper = params[0]();
        return ((parseInt(value)) >= parseInt(weightPaper));
    }
}, "La grammatura della copertina deve essere maggiore o uguale a quella dell'interno.");


$(function () {
	jQuery.validator.classRuleSettings.multiplesOfTwo = { multiplesOfTwo:true };
    jQuery.validator.classRuleSettings.multiplesOfFour = { multiplesOfFour:true };
    jQuery.validator.classRuleSettings.multiplesOfEight = { multiplesOfEight:true };
    jQuery.validator.classRuleSettings.checkMaxFacadesWeightCover = { checkMaxFacadesWeightCover:true };
});