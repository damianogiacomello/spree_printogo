//paper_products_selected
var availableGr = retrieve_paper_weight();

$(function () {
	if ($('select[name="order[format]"]').length > 0) {
		
		$('select[name="order[format]"] option:first').attr('selected','selected');
		
		var papers = updatePaper($('input[name="order[format]"]').val());
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var gr = fetchGr($('input[name="order[format]"]:checked').val(), $('select[name="order[paper]"] > option:selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');

		$('select[name="order[format]"]').change(function () {
			var papers = updatePaper($(this).val());
			appendToOption('select[name="order[paper]"]', papers.available_paper);
			$('select[name="order[paper]"]').change();

		});

		$('select[name="order[paper]"]').change(function () {
			var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $(this).find("option:selected").text());
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		});

		$('select[name="order[format]"]').change();
		$('select[name="order[format]"] option:first').click();
		
	} else {
		$('input[name="order[format]"]').first().attr('checked', 'checked');
		var papers = updatePaper($('input[name="order[format]"]').val());
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var gr = fetchGr($('input[name="order[format]"]:checked').val(), $('select[name="order[paper]"] > option:selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');

		$('input[name="order[format]"]').change(function () {
			var papers = updatePaper($(this).val());
			appendToOption('select[name="order[paper]"]', papers.available_paper);
			$('select[name="order[paper]"]').change();

		});

		$('select[name="order[paper]"]').change(function () {
			var gr = fetchGr($('input[name="order[format]"]:checked').val(), $(this).find("option:selected").text());
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		});

		$('input[name="order[format]"]').change();
		$('input[name="order[format]"]:first').click();
	}

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxCustom('[name="order[number_of_copy]"]') && checkAndCallAjax('select[name="order[weight]"]') && checkAndCallAjax('[name="order[plasticization]"]') && checkAndCallAjax('[name="order[instructions]"]') && checkAndCallAjax('[name="order[print_color]"]') && checkAndCallAjax('[name="order[file_check]"]') && checkAndCallAjax('[id*="order_calculator_data_"]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[orientation]": $('input[name="order[orientation]"]:checked').val(),
		"order[instructions]": $('select[name="order[instructions]"] option:selected').val(),
		"order[paper]": $('select[name="order[paper]"] option:selected').val(),
		"order[weight]": $('select[name="order[weight]"] option:selected').val(),
		"order[plasticization]": $('select[name="order[plasticization]"] option:selected').val(),
		"order[print_color]": $('select[name="order[print_color]"] option:selected').val(),
		"order_calculator[data]": $('input[name="order_calculator[data]"]:checked').val(),
		"poster_flyer_product_id": $('#poster_flyer_product_id').val()
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function fetchGr(variant_id, paper_name) {
    var available_gr = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == parseInt(variant_id)) {
            $.each(v.values, function (i1, v1) {
				if (v1.weight.paper.name == paper_name) {
					available_gr.push(v1.weight.quantity);
				}
            });
        }
    });

    return available_gr;
}

function updatePaper(variant_id) {
    var available_paper = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}