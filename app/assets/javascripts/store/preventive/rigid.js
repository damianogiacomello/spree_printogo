$(function () {
	$(".format-check input[type=radio]").change(function () {
		if ($(this).data('custom') == true) {
			if ($('.custom-size input').attr('disabled') == 'disabled') {
				$('.custom-size input').attr('disabled', false);
			}
		} else {
			$('.custom-size input').attr('disabled', true);
		}
		$('select[name="order[cutting_coil]"]').prop("selectedIndex",0);
		if ($(this).data('cutting-coil') == false) {
			$('select[name="order[cutting_coil]"]').attr('disabled', true);
		} else {
			$('select[name="order[cutting_coil]"]').attr('disabled', false);
		}
		if ($(this).data('eyelet') == false) {
			$('select[name="order[eyelet_accessory]"]').attr('disabled', true);
			$('input[name="order[eyelet_quantity]"]').attr('disabled', true);
			$('select[name="order[eyelet]"]').attr('disabled', true);
		} else {
			$('select[name="order[eyelet_accessory]"]').attr('disabled', false);
			$('input[name="order[eyelet_quantity]"]').attr('disabled', false);
			$('select[name="order[eyelet]"]').attr('disabled', false);
		}
		if ($(this).data('sided') == false) {
			$('select[name="order[sided]"]').attr('disabled', true);
		} else {
			$('select[name="order[sided]"]').attr('disabled', false);
		}
		$('select[name="order[sided]"]').prop("selectedIndex",0);
	});
	$('.custom-size input').attr('disabled', true);
	$(".format-check input[type=radio]:first").click();
	$(".format-check input[type=radio]:checked").change();

	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});

	var eyeS = $('select[name="order[eyelet]"]').val();
	updateEyeletAC(papers, eyeS, $('select[name="order[paper]"]').val());
	$('select[name="order[eyelet]"]').change(function() {
		updateEyeletAC(papers, $(this).val(), $('select[name="order[paper]"]').val());
	});
	$('select[name="order[eyelet_accessory]"]').change(function() {
		updateEyeletQT($(this).attr("data-limit"));
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValid('[name="order[format]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[custom][width]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[custom][height]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[number_of_copy]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[sided]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[eyelet]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[eyelet_accessory]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('input[name="order[eyelet_quantity]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[lamination]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[cutting_coil]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[paper]": $('[name="order[paper]"]').val(),
		"order[print_color]": $('[name="order[print_color]"]').val(),
		"order[sided]": $('[name="order[sided]"] option:selected').val(),
		"order[eyelet]": $('[name="order[eyelet]"]').val(),
		"order[eyelet_accessory]": $('[name="order[eyelet_accessory]"]').val(),
		"order[eyelet_quantity]": $('input[name="order[eyelet_quantity]"]').val(),
		"order[lamination]": $('[name="order[lamination]"]').val(),
		"order[cutting_coil]": $('[name="order[cutting_coil]"]').val(),
		"order_calculator[data]": $('input[name="order_calculator[data]"]:checked').val(),
		"rigid_product_id": $('#rigid_product_id').val()
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('[name="order[custom][height]"]').val() != "" && $('[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			// Lamination
			$.each($('select[name="order[lamination]"]').find("option"), function(k, le) {
				if ($.inArray(Math.round($(le).val()), pv.laminations) < 0) {
					$(le).attr("disabled", "disabled");
				} else {
					$(le).removeAttr("disabled");
				}
			});
			$('select[name="order[lamination]"]').find("option:first").removeAttr("disabled");
			// CuttingCoil
			$.each($('select[name="order[cutting_coil]"]').find("option"), function(k, cce) {
				if ($.inArray(Math.round($(cce).val()), pv.cutting_coils) < 0) {
					$(cce).attr("disabled", "disabled");
				} else {
					$(cce).removeAttr("disabled");
				}
			});
			$('select[name="order[cutting_coil]"]').find("option:first").removeAttr("disabled");
			$('select[name="order[cutting_coil]"]').prop("selectedIndex",0);
			
			// Eyelet
			appendToOptionWithOneDefault('select[name="order[eyelet]"]', pv.eyelets);
			appendToOption('select[name="order[print_color]"]', pv.plotters);
			if (pv.sided == true) {
				$('select[name="order[sided]"]').removeAttr("disabled");
			} else {
				$('select[name="order[sided]"]').attr("disabled", "disabled");
			}
			$('select[name="order[sided]"]').prop("selectedIndex",0);
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
			$('.paper-dimension').html(pv.dimension);
		}
	});
}

function appendToOptionWithOneDefault(tag, elements) {
    var def1 = $(tag).children()[0];
	$(tag).html("").append(def1);
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

function appendToOption(tag, elements) {
	$(tag).html("");
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

function updateEyeletAC(papers, id, current) {
	var limit = 0
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			
			$.each(pv.eyelets, function(k, av) {
				
				if (av.id == id) {
					limit = av.limit;
					$('select[name="order[eyelet_accessory]"]').attr("disabled", "disabled");
					$.each($('select[name="order[eyelet_accessory]"]').find("option"), function(k, ve) {
						
						if ($.inArray($(ve).val(), av.disposition) < 0) {
							$(ve).attr("disabled", "disabled");
						} else {
							$(ve).removeAttr("disabled");
						}
					});
				} else {
					$.each($('select[name="order[eyelet_accessory]"]').find("option"), function(k, ve) {
						$(ve).attr("disabled", "disabled");
					});
				}
				$('select[name="order[eyelet_accessory]"]').find("option:not(:disabled)").first().attr("selected", true);
			});
		}
	});
	$('select[name="order[eyelet_accessory]"]').attr("data-limit", limit);
	updateEyeletQT(limit);
}

function updateEyeletQT(lim) {
	var width = $('input[name="order[custom][width]"]').val();
	var height = $('input[name="order[custom][height]"]').val();
	var disposition = $('select[name="order[eyelet_accessory]"]').val();
	var tot = 0;
	
	switch(disposition) {
		case "all_perimeter":
			tot = Math.ceil(((width * 2) + (height * 2))/lim)
			break;
		case "top_side":
			tot = Math.ceil((width)/lim)
			break;
		case "bottom_side":
			tot = Math.ceil((width)/lim)
			break;
		case "right_side":
			tot = Math.ceil((height)/lim)
			break;
		case "left_side":
			tot = Math.ceil((height)/lim)
			break;
		case "side_upper_lower":
			tot = Math.ceil((width * 2)/lim)
			break;
		case "right_left_side":
			tot = Math.ceil((height * 2)/lim)
			break;
	};

	$('input[name="order[eyelet_quantity]"]').attr("min", tot);
	$('input[name="order[eyelet_quantity]"]').val(tot);
	
	// aggiungo ajaxLoading = false; altrimenti ajaxCall non si accorge della modifica dell' eyelet_accessory
	// visualizzando il preventivo con un numero di occhielli relativo alla configurazione precedente
	ajaxLoading = false;
	// ajaxCall();
}
