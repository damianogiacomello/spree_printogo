var availableGr = retrieve_paper_weight();

$(function () {
	// Gestico la validazione
	$('#add_spiral').validate({
		errorPlacement:function (error, element) {
			$("div.error:first").remove();
			var error_message = error.html();
			error = "<label class='error'><div class='error-arrow'></div><div class='error-inner'><div class='error-content'>" + error_message + "</div></div></label>";
			element.after(error);
		}
	});
		
	$("#paper_cover_weight").rules("add", {"checkWeightCoverWeightPaper":[ function () {
		return $('#weight option:selected').val()
	}, function () {
		return $('#paper_cover').val()
	}]});

	// Disabilito la grammatura per la copertina
	$('div.cover_bw').hide();
	$('div.paper_cover_weight').hide();
	$('select[name="order[paper_cover_weight]"]').attr('disabled', true).removeClass("required");
	$('select[name="order[plasticization]"]').attr('disabled', true);

	$('select[name="order[paper_cover]"]').change(function () {
		onPaperCoverChange($(this));
	});

	$('select[name="order[plasticization_option]"]').change(function () {
		onPlasticizationOptionChange($(this));
	});

	if ($('select[name="order[format]"]').length > 0) {
		// Operazioni di inizializzazione
		$('select[name="order[format]"] option:first').attr('selected','selected');
		var papers = updatePaper($('select[name="order[format]"] option:selected').val());
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));
        
		appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper);
		var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $('select[name="order[paper]"] > option:selected').text(), paper_products_selected);
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
        
		$('select[name="order[format]"]').change(function () {
			var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));
			var papers = updatePaper($(this).val());
			appendToOption('select[name="order[paper]"]', papers.available_paper);
			appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper);
			onPaperCoverChange($('select[name="order[paper_cover]"]'));
			$('select[name="order[paper]"]').change();
		});

		$('select[name="order[paper]"]').change(function () {
			var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $(this).find("option:selected").text(), paper_products_selected);
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
			ajaxLoading = false;
		});
	} else {
		// Operazioni di inizializzazione
		$('input[name="order[format]"]').first().attr('checked', 'checked');
		var papers = updatePaper($('input[name="order[format]"]').val());
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var cover_paper = updateCoverPaper($('input[name="order[format]"]:checked').data('cover'));
        
		appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper);
		var gr = fetchGr($('input[name="order[format]"]:checked').val(), $('select[name="order[paper]"] > option:selected').text(), paper_products_selected);
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
        
		$('input[name="order[format]"]').change(function () {
			var cover_paper = updateCoverPaper($('input[name="order[format]"]:checked').data('cover'));
			var papers = updatePaper($(this).val());
			appendToOption('select[name="order[paper]"]', papers.available_paper);
			appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper);
			onPaperCoverChange($('select[name="order[paper_cover]"]'));
			$('select[name="order[paper]"]').change();
		});

		$('select[name="order[paper]"]').change(function () {
			var gr = fetchGr($('input[name="order[format]"]:checked').val(), $(this).find("option:selected").text(), paper_products_selected);
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
			ajaxLoading = false;
		});
	}

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValid('[name="order[format]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[number_of_copy]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[number_of_facades]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[weight]"]', 'form[id*=add_]') && checkAndCallAjaxIfEnableElementAndIsValid('[name="order[paper_cover]"]', '[name="order[paper_cover_weight]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[paper_cover_weight]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[plasticization_option]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[plasticization]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[cover_bw]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[spiraling]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[instructions]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[default_cover]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"spiral_product_id": $('#spiral_product_id').val(),
		"order[default_cover]": $('[name="order[default_cover]"]').val(),
		"order[instructions]": $('[name="order[instructions]"]').val(),
		"order[spiraling]": $('[name="order[spiraling]"]').val(),
		"order[print_color]": $('[name="order[print_color]"] option:selected').val(),
		"order_calculator[data]": $('[name="order_calculator[data]"]:checked').val(),
		"order[number_of_facades]": $('input[name="order[number_of_facades]"]').val(),
		"order[paper]": $('[name="order[paper]"] option:selected').val(),
		"order[weight]": $('[name="order[weight]"] option:selected').val(),
		"order[paper_cover]": $('[name="order[paper_cover]"] option:selected').val(),
		"order[paper_cover_weight]": $('[name="order[paper_cover_weight]"] option:selected').val(),
		"order[plasticization_option]": $('[name="order[plasticization_option]"]').val(),
		"order[plasticization]": $('[name="order[plasticization]"]').val(),
		"order[cover_bw]": $('[name="order[cover_bw]"] option:selected').val(),
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function appendToOptionWithDefault(tag, available_paper) {
    var def1 = $(tag).children()[0];
	var def2 = $(tag).children()[1];
	$(tag).html("").append(def1).append(def2);
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
    $(tag).change();
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}
function fetchGr(variant_id, paper_name) {
    return fetchGr(variant_id, paper_name, paper_products_selected);
}

function fetchGr(variant_id, paper_name, pproducts_selected) {
    var available_gr = [];
    $.each(pproducts_selected, function (i, v) {
        if (v.variant_id == parseInt(variant_id)) {
            $.each(v.values, function (i1, v1) {
				if (v1.weight.paper.name == paper_name) {
                	available_gr.push(v1.weight.quantity);
				}
            });
        }
    });

    return available_gr;
}

function updatePaper(variant_id) {
    var available_paper = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}


function updateCoverPaper(variant_id) {
    var available_paper = [];
    $.each(paper_cover_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}


// 'select[name="order[paper_cover]"]'
function onPaperCoverChange(element) {
    var selected_element = element.find('option:selected');
    if ($(selected_element).text() == "Stessa grammatura dell'interno") {
		$('div.default_cover').hide();
		$('div.paper_cover_weight').show();
		$('div.cover_bw').show();
        $('select[name="order[paper_cover_weight]"]').attr('disabled', true);
    } else if ($(selected_element).text() == "Nessuna") {
		$('div.default_cover').show();
		$('div.paper_cover_weight').hide();
		$('div.cover_bw').hide();
		$('select[name="order[paper_cover_weight]"]').attr('disabled', true).removeClass("required");
	} else {
		$('div.default_cover').hide();
		$('div.paper_cover_weight').show();
		$('div.cover_bw').show();
        $('select[name="order[paper_cover_weight]"]').attr('disabled', false).addClass("required");
        if ($('select[name="order[format]"]').length > 0) {
            var gr = fetchGr($('select[name="order[format]"] option:selected').data('cover'), $(element).find("option:selected").text(), paper_cover_selected);
        } else {
            var gr = fetchGr($('input[name="order[format]"]:checked').data('cover'), $(element).find("option:selected").text(), paper_cover_selected);
        }
        appendToOptionAndDisabled(gr, 'select[name="order[paper_cover_weight]"]');
    }
}
