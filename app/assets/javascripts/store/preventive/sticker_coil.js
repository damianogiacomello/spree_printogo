$(function () {	
	$('.custom-size input').attr('disabled', true);
	//$(".format-check input[type=radio]:checked").change();
	
	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	updatePlotter(papers, paper);
	
	$(".format-check input[type=radio]").change(function () {
		if ($(this).data('custom') == true) {
			if ($('.custom-size input').attr('disabled') == 'disabled') {
				$('.custom-size input').attr('disabled', false);
			}
		} else {
			$('.custom-size input').attr('disabled', true);
		}
		$('select[name="order[cutting_coil]"]').prop("selectedIndex", 0);
		if ($(this).data('cutting-coil') == false) {
			$('select[name="order[cutting_coil]"]').attr('disabled', true);
		} else {
			$('select[name="order[cutting_coil]"]').attr('disabled', false);
		}
		
		updatePapers(papers, $('select[name="order[paper]"]').val());
		updatePlotter(papers, $('select[name="order[paper]"]').val());
    });
	
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});
	
	$('#order_number_of_copy').change(function() {
		showhideProcessign(papers, $('select[name="order[paper]"]').val());
		// $("#order_number_of_stickers").val($(this).val());
	});
	
	// aggiungo la descrizione dei tipi di stampa
	$('select[name="order[print_color]"]').change(function() {
		updatePlotter(papers, paper);
	});
	
	//aggiungo la descrizione delle singole lavorazioni plotter selezionate
	$('select[name^="order[processing_plotter_combination]"]').change(function() {
		updateProcessingPlotterPresentation(papers, paper, $(this).attr("processing_plotter_id"));
	});
	$(".format-check input[type=radio]:first").click();

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	});
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});

	// Gestico la validazione
	$('#add_stickers').validate({
		errorPlacement:function (error, element) {
			$("div.error:first").remove();
			var error_message = error.html();
			error = "<label class='error'><div class='error-arrow'></div><div class='error-inner'><div class='error-content'>"+error_message+"</div></div></label>";
			element.after(error);
		}
	});
});


function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValid('input[name="order[format]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('select[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('select[name^="order[processing_plotter_combination]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[cutting_coil]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			if($('select[name="order[number_of_copy]"]').length > 0){
				if(checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]')) {
					QuotePreload();
				}
			} else {
				if(checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]')) {
					QuotePreload();
				}
			}
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[paper]": $('select[name="order[paper]"] option:selected').val(),
		"order[print_color]": $('select[name="order[print_color]"] option:selected').val(),
		"order[processing_plotter_combination]": get_processing_plotter_types($('select[name="order[paper]"] option:selected').val()),
		"order[number_of_stickers]": $('input[name="order[number_of_stickers]"]').val(),
		"order[orientation_coil]": $('input[name="order[orientation_coil]"]:checked').val(),
		"order[cutting_coil]": $('select[name="order[cutting_coil]"] option:selected').val(),
		"order_calculator[data]": $('input[name="order_calculator[data]"]:checked').val(),
		"sticker_product_id": $('#sticker_product_id').val(),
	}
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function updatePapers(papers, current) {
	//nascondo e resetto tutti i div conteneti processing_plotter_combination
	$('select[name^="order[processing_plotter_combination]"]').prop("selectedIndex", 0);
	$('select[name^="order[processing_plotter_combination]"]').parent().hide();
	
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			appendToOption('select[name="order[print_color]"]', pv.plotters);
			$('select[name="order[print_color]"]').change();
			
			$.each(pv.processing_plotters, function(it, st) {
				if (st.min < $('#order_number_of_copy').val()) {
					var elem = '#order_processing_plotter_combination_'+ st.id +'';
					$(elem).parent().show();
				}
			});
			
			// CuttingCoil
			$.each($('select[name="order[cutting_coil]"]').find("option"), function(k, cce) {
				
				$.each(pv.cutting_coils_limit, function(i, ccl) {
					
					if ((Math.round($(cce).val()) != ccl.id) || (ccl.min > $('#order_number_of_copy').val())) {
						$(cce).attr("disabled", "disabled");
					} else if (ccl.min < $('#order_number_of_copy').val()) {
						$(cce).removeAttr("disabled");
					}
				});
			});
			$('select[name="order[cutting_coil]"]').find("option:first").removeAttr("disabled");
			$('select[name="order[cutting_coil]"]').prop("selectedIndex", 0);
			
			//aggiungo la descrizione del tipo di materiale selezionato
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}

function showhideProcessign(papers, current) {
	//nascondo e resetto tutti i div conteneti processing_plotter_combination
	$('select[name^="order[processing_plotter_combination]"]').prop("selectedIndex", 0);
	$('select[name^="order[processing_plotter_combination]"]').parent().hide();

	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			// appendToOption('select[name="order[print_color]"]', pv.plotters);
			
			$.each(pv.processing_plotters, function(it, st) {
				if (st.min < $('#order_number_of_copy').val()) {
					var elem = '#order_processing_plotter_combination_'+ st.id +'';
					$(elem).parent().show();
				}
			});

			// CuttingCoil
			$.each($('select[name="order[cutting_coil]"]').find("option"), function(k, cce) {
				
				$.each(pv.cutting_coils_limit, function(i, ccl) {
					
					if ((Math.round($(cce).val()) != ccl.id) || (ccl.min > $('#order_number_of_copy').val())) {
						$(cce).attr("disabled", "disabled");
					} else if (ccl.min < $('#order_number_of_copy').val()) {
						$(cce).removeAttr("disabled");
					}
				});
			});
			$('select[name="order[cutting_coil]"]').find("option:first").removeAttr("disabled");
			$('select[name="order[cutting_coil]"]').prop("selectedIndex", 0);
			
			//aggiungo la descrizione del tipo di materiale selezionato
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}

// aggiungo la descrizione dei tipi di stampa
function updatePlotter(papers, current){
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$.each(pv.plotters, function(i, pl) {
				if (pl.id == $('select[name="order[print_color]"]').val()){
					$('select[name="order[print_color]"]').parent().data("powertip", pl.presentation);
				};
			});
		}
	});
}

//aggiungo la descrizione delle singole lavorazioni plotter selezionate
function updateProcessingPlotterPresentation(papers, current, id){
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$.each(pv.processing_plotters, function(i, pp) {
				$.each(pp.processing_plotter_types, function(i, ppp) {
					if (ppp.id == $('#order_processing_plotter_combination_'+ id +'').val()){
						$('#order_processing_plotter_combination_'+ id +'').parent().data("powertip", ppp.presentation);
					};
				});
			});
		}
	});
}


function appendToOptionWithOneDefault(tag, elements) {
    var def1 = $(tag).children()[0];
	$(tag).html("").append(def1);
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

function appendToOption(tag, elements) {
	$(tag).html("");
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

//devo ritornare una stringa tipo
//{"24130"=>"", "24581"=>"", "22962"=>"24127", "22963"=>"22958", "26160"=>"", "26159"=>"", "26158"=>""}
//che indica gli id dei processing_plotter_type da applicare

function get_processing_plotter_types(paper_id) {
	var json = {};
	$.each(papers, function(k, pv) {
		if (pv.id == paper_id) {
			$.each(pv.processing_plotters, function(it, st) {
				var ppc = $('#order_processing_plotter_combination_'+ st.id +'').val();
				if (ppc != "") {
					json[st.id] = ppc;
				}
			});
		}
	});
	
	return json;
}
