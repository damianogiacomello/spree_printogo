$(function () {
	$(".format-check input[type=radio]").change(function () {
        if ($(this).data('custom') == true) {
            if ($('.custom-size input').attr('disabled') == 'disabled') {
                $('.custom-size input').attr('disabled', false);
            }
        } else {
            $('.custom-size input').attr('disabled', true);
        }
        $('select[name="order[cutting_coil]"]').prop("selectedIndex",0);
		if ($(this).data('cutting-coil') == false) {
			$('select[name="order[cutting_coil]"]').attr('disabled', true);
		} else {
			$('select[name="order[cutting_coil]"]').attr('disabled', false);
		}
		if ($(this).data('eyelet') == false) {
			$('select[name="order[eyelet_accessory]"]').attr('disabled', true);
			$('input[name="order[eyelet_quantity]"]').attr('disabled', true);
			$('select[name="order[eyelet]"]').attr('disabled', true);
		} else {
			$('select[name="order[eyelet_accessory]"]').attr('disabled', false);
			$('input[name="order[eyelet_quantity]"]').attr('disabled', false);
			$('select[name="order[eyelet]"]').attr('disabled', false);
		}
		if ($(this).data('sided') == false) {
			$('select[name="order[sided]"]').attr('disabled', true);
		} else {
			$('select[name="order[sided]"]').attr('disabled', false);
		}
		$('select[name="order[sided]"]').prop("selectedIndex",0);
    });
    $('.custom-size input').attr('disabled', true);
	$(".format-check input[type=radio]:first").click();
    $(".format-check input[type=radio]:checked").change();
	
	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});
	
	var eyeS = $('select[name="order[eyelet]"]').val();
	updateEyeletAC(papers, eyeS, $('select[name="order[paper]"]').val());
	$('select[name="order[eyelet]"]').change(function() {
		updateEyeletAC(papers, $(this).val(), $('select[name="order[paper]"]').val());
	});
	$('select[name="order[eyelet_accessory]"]').change(function() {
		updateEyeletQT($(this).attr("data-limit"));
	});
});

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			// Lamination
			$.each($('select[name="order[lamination]"]').find("option"), function(k, le) {
				if ($.inArray(Math.round($(le).val()), pv.laminations) < 0) {
					$(le).attr("disabled", "disabled");
				} else {
					$(le).removeAttr("disabled");
				}
			});
			$('select[name="order[lamination]"]').find("option:first").removeAttr("disabled");
			// CuttingCoil
			$.each($('select[name="order[cutting_coil]"]').find("option"), function(k, cce) {
				if ($.inArray(Math.round($(cce).val()), pv.cutting_coils) < 0) {
					$(cce).attr("disabled", "disabled");
				} else {
					$(cce).removeAttr("disabled");
				}
			});
			$('select[name="order[cutting_coil]"]').find("option:first").removeAttr("disabled");
			$('select[name="order[cutting_coil]"]').prop("selectedIndex",0);
			
			// Eyelet
			appendToOptionWithOneDefault('select[name="order[eyelet]"]', pv.eyelets);
			appendToOption('select[name="order[print_color]"]', pv.plotters);
			if (pv.sided == true) {
				$('select[name="order[sided]"]').removeAttr("disabled");
			} else {
				$('select[name="order[sided]"]').attr("disabled", "disabled");
			}
			$('select[name="order[sided]"]').prop("selectedIndex",0);
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
			$('.paper-dimension').html(pv.dimension);
		}
	});
}

function appendToOptionWithOneDefault(tag, elements) {
    var def1 = $(tag).children()[0];
	$(tag).html("").append(def1);
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

function appendToOption(tag, elements) {
	$(tag).html("");
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

function updateEyeletAC(papers, id, current) {
	var limit = 0
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			
			$.each(pv.eyelets, function(k, av) {
				
				if (av.id == id) {
					limit = av.limit;
					$('select[name="order[eyelet_accessory]"]').attr("disabled", "disabled");
					$.each($('select[name="order[eyelet_accessory]"]').find("option"), function(k, ve) {
						
						if ($.inArray($(ve).val(), av.disposition) < 0) {
							$(ve).attr("disabled", "disabled");
						} else {
							$(ve).removeAttr("disabled");
						}
					});
				} else {
					$.each($('select[name="order[eyelet_accessory]"]').find("option"), function(k, ve) {
						$(ve).attr("disabled", "disabled");
					});
				}
				$('select[name="order[eyelet_accessory]"]').find("option:not(:disabled)").first().attr("selected", true);
			});
		}
	});
	$('select[name="order[eyelet_accessory]"]').attr("data-limit", limit);
	updateEyeletQT(limit);
}

function updateEyeletQT(lim) {
	var width = $('input[name="order[custom][width]"]').val();
	var height = $('input[name="order[custom][height]"]').val();
	var disposition = $('select[name="order[eyelet_accessory]"]').val();
	var tot = 0;
	
	switch(disposition) {
		case "all_perimeter":
			tot = Math.ceil(((width * 2) + (height * 2))/lim)
			break;
		case "top_side":
			tot = Math.ceil((width)/lim)
			break;
		case "bottom_side":
			tot = Math.ceil((width)/lim)
			break;
		case "right_side":
			tot = Math.ceil((height)/lim)
			break;
		case "left_side":
			tot = Math.ceil((height)/lim)
			break;
		case "side_upper_lower":
			tot = Math.ceil((width * 2)/lim)
			break;
		case "right_left_side":
			tot = Math.ceil((height * 2)/lim)
			break;
	};

	$('input[name="order[eyelet_quantity]"]').attr("min", tot);
	$('input[name="order[eyelet_quantity]"]').val(tot);
	
	// aggiungo ajaxLoading = false; altrimenti ajaxCall non si accorge della modifica dell' eyelet_accessory
	// visualizzando il preventivo con un numero di occhielli relativo alla configurazione precedente
	ajaxLoading = false;
	ajaxCall();
}
