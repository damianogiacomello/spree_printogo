var availableGr = retrieve_paper_weight();

$(function () {
    if ($('select[name="order[format]"]').length > 0) {
        var papers = updatePaper($('select[name="order[format]"] option:selected').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));   
    } else {
        var papers = updatePaper($('input[name="order[format]"]:checked').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        var cover_paper = updateCoverPaper($('input[name="order[format]"]:checked').data('cover'));
    }

    appendToOption('select[name="order[paper_cover]"]', cover_paper.available_paper);
    onPaperCoverChange('select[name="order[paper_cover]"] :selected');
    $('select[name="order[paper]"]').change();
	
    // Gestico la validazione
    $('#add_paperback').validate({
        errorPlacement:function (error, element) {
            $("div.error:first").remove();
            var error_message = error.html();
            error = "<label class='error'><div class='error-arrow'></div><div class='error-inner'><div class='error-content'>" + error_message + "</div></div></label>";
            element.after(error);
        }
    });

    $('select[name="order[paper_cover]"]').change(function () {
        onPaperCoverChange($('select[name="order[paper_cover]"] :selected'));
    });

    $('select[name="order[plasticization_option]"]').change(function () {
        onPlasticizationOptionChange($(this));
    });

    // $('select[name="order[print_color]"]').change(function () {
        // onPrintColorChange($(this));
    // });

    if ($('select[name="order[format]"]').length > 0) {
        // Operazioni di inizializzazione
        $('select[name="order[format]"] option:first').attr('selected','selected');
        var papers = updatePaper($('select[name="order[format]"] option:selected').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        // Aggiornameto parametri carta contenuto punto metallico
        var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $('select[name="order[paper]"] > option:selected').text(), paper_products_selected);
        appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
        // Aggiornamento parametri carta copertina
        var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));
        appendToOption('select[name="order[paper_cover]"]', cover_paper.available_paper);
        var gr_cover = fetchGr($('select[name="order[format]"] option:selected').data('cover'), $('select[name="order[paper_cover]"] > option:selected').text(), paper_cover_selected);
        appendToOptionAndDisabled(gr_cover, 'select[name="order[paper_cover_weight]"]');
		
		$('select[name="order[paper_cover]"]').change(function() {
			$('select[name="order[paper_cover_weight]"]').change();
		});
		
        $('select[name="order[format]"]').change(function () {
            var papers = updatePaper($('select[name="order[format]"] option:selected').val());
            appendToOption('select[name="order[paper]"]', papers.available_paper);
            var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));
            appendToOption('select[name="order[paper_cover]"]', cover_paper.available_paper);;
            onPaperCoverChange('select[name="order[paper_cover]"] :selected');
            $('select[name="order[paper]"]').change();
        });

        $('select[name="order[paper]"]').change(function () {
            var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $(this).find("option:selected").text(), paper_products_selected);
            appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
            $('select[name="order[weight]"]').change();
        });
        
        ajaxCall();
    } else {
        // Operazioni di inizializzazione
        $('input[name="order[format]"]').first().attr('checked', 'checked');
        var papers = updatePaper($('input[name="order[format]"]').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        // Aggiornameto parametri carta contenuto punto metallico
        var gr = fetchGr($('input[name="order[format]"]:checked').val(), $('select[name="order[paper]"] > option:selected').text(), paper_products_selected);
        appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
        // Aggiornamento parametri carta copertina
        var cover_paper = updateCoverPaper($('input[name="order[format]"]:checked').data('cover'));
        appendToOption('select[name="order[paper_cover]"]', cover_paper.available_paper);
        var gr_cover = fetchGr($('input[name="order[format]"]:checked').data('cover'), $('select[name="order[paper_cover]"] > option:selected').text(), paper_cover_selected);
        appendToOptionAndDisabled(gr_cover, 'select[name="order[paper_cover_weight]"]');
		
		$('select[name="order[paper_cover]"]').change(function() {
			$('select[name="order[paper_cover_weight]"]').change();
		});

        $('input[name="order[format]"]').change(function () {
            var papers = updatePaper($(this).val());
            appendToOption('select[name="order[paper]"]', papers.available_paper);
            var cover_paper = updateCoverPaper($(this).data('cover'));
            appendToOption('select[name="order[paper_cover]"]', cover_paper.available_paper);
            onPaperCoverChange('select[name="order[paper_cover]"] :selected');
            $('select[name="order[paper]"]').change();
        });

        $('select[name="order[paper]"]').change(function () {
            var gr = fetchGr($('input[name="order[format]"]:checked').val(), $(this).find("option:selected").text(), paper_products_selected);
            appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
            $('select[name="order[weight]"]').change();
        });

        ajaxCall();
    }
});

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function fetchGr(variant_id, paper_name, pproducts_selected) {
    var available_gr = [];
    $.each(pproducts_selected, function (i, v) {
        if (v.variant_id == parseInt(variant_id)) {
            $.each(v.values, function (i1, v1) {
				if (v1.weight.paper.name == paper_name) {
                	available_gr.push(v1.weight.quantity);
				}
            });
        }
    });

    return available_gr;
}

//function fetchGr(variant_id, paper_name) {
//    var available_gr = [];
//    $.each(paper_products_selected, function (i, v) {
//        if (v.variant_id == parseInt(variant_id)) {
//            $.each(v.values, function (i1, v1) {
//                available_gr.push(v1.weight.quantity);
//            });
//        }
//    });
//
//    return available_gr;
//}

function updatePaper(variant_id) {
    var available_paper = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}

// 'select[name="order[paper_cover]"]'
function onPaperCoverChange(element) {
    if ($('select[name="order[format]"]').length > 0) {
        var gr = fetchGr($('select[name="order[format]"] option:selected').data('cover'), $(element).text(), paper_cover_selected);
    } else {
        var gr = fetchGr($('input[name="order[format]"]:checked').data('cover'), $(element).text(), paper_cover_selected); 
    }
    appendToOptionAndDisabled(gr, 'select[name="order[paper_cover_weight]"]');
}

function updateCoverPaper(variant_id) {
    var available_paper = [];
    $.each(paper_cover_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}