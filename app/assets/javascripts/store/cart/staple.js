var availableGr = retrieve_paper_weight();


$(function () {

    // Gestico la validazione
    $('#add_staple').validate({
        errorPlacement:function (error, element) {
			$("div.error:first").remove();
            var error_message = error.html();
			error = "<label class='error'><div class='error-arrow'></div><div class='error-inner'><div class='error-content'>"+error_message+"</div></div></label>";
			element.after(error);
        }
    });

    $("#number_of_facades").rules("add", {"checkMaxFacadesWeightCover":[ function () {
        return $('#weight option:selected').val();
    }, max_facades_weight_cover]});
    $("#weight").rules("add", {"checkMaxFacadesWeightCover":[ function () {
        return $('#number_of_facades').val()
    }, max_facades_weight_cover]});
    $("#paper_cover_weight").rules("add", {"checkWeightCoverWeightPaper":[ function () {
        return $('#weight option:selected').val()
    }, function () {
        return $('#paper_cover').val()
    }]});


    // Disabilito la grammatura per la copertina
    $('select[name="order[paper_cover_weight]"]').attr('disabled', true);
    $('select[name="order[plasticization]"]').attr('disabled', true);


    $('select[name="order[paper_cover]"]').change(function () {
        onPaperCoverChange($(this));
    });

    $('select[name="order[plasticization_option]"]').change(function () {
        onPlasticizationOptionChange($(this));
    });

    // $('select[name="order[print_color]"]').change(function () {
        // onPrintColorChange($(this));
    // });

    if ($('select[name="order[format]"]').length > 0) {

        // Operazioni di inizializzazione
        $('select[name="order[format]"] option:first').attr('selected','selected');
        var papers = updatePaper($('select[name="order[format]"] option:selected').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));

        appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper, "Stessa grammatura dell'interno");
        var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $('select[name="order[paper]"] > option:selected').text(), paper_products_selected);
        appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');


        $('select[name="order[format]"]').change(function () {
            var cover_paper = updateCoverPaper($('select[name="order[format]"] option:selected').data('cover'));
            var papers = updatePaper($(this).val());
            appendToOption('select[name="order[paper]"]', papers.available_paper);
            appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper, "Stessa grammatura dell'interno");
            onPaperCoverChange($('select[name="order[paper_cover]"]'));
            $('select[name="order[paper]"]').change();
        });

        $('select[name="order[paper]"]').change(function () {
            var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $(this).find("option:selected").text(), paper_products_selected);
            appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
            ajaxCall();
        });

        $('select[name="order[format]"]').change();        

    } else {

        // Operazioni di inizializzazione
        $('input[name="order[format]"]').first().attr('checked', 'checked');
        var papers = updatePaper($('input[name="order[format]"]').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        var cover_paper = updateCoverPaper($('input[name="order[format]"]:checked').data('cover'));

        appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper, "Stessa grammatura dell'interno");
        var gr = fetchGr($('input[name="order[format]"]:checked').val(), $('select[name="order[paper]"] > option:selected').text(), paper_products_selected);
        appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');


        $('input[name="order[format]"]').change(function () {
            var cover_paper = updateCoverPaper($('input[name="order[format]"]:checked').data('cover'));
            var papers = updatePaper($(this).val());
            appendToOption('select[name="order[paper]"]', papers.available_paper);
            appendToOptionWithDefault('select[name="order[paper_cover]"]', cover_paper.available_paper, "Stessa grammatura dell'interno");
            onPaperCoverChange($('select[name="order[paper_cover]"]'));
            $('select[name="order[paper]"]').change();
        });

        $('select[name="order[paper]"]').change(function () {
            var gr = fetchGr($('input[name="order[format]"]:checked').val(), $(this).find("option:selected").text(), paper_products_selected);
            appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
            ajaxCall();
        });

        $('input[name="order[format]"]').change();
    }
});

function appendToOptionWithDefault(tag, available_paper, message) {
    $(tag).html("");
    $(tag).append(
        $("<option></option>").val(-1).text(message)
    );
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
    $(tag).change();
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}
function fetchGr(variant_id, paper_name) {
    return fetchGr(variant_id, paper_name, paper_products_selected);
}

function fetchGr(variant_id, paper_name, pproducts_selected) {
    var available_gr = [];
    $.each(pproducts_selected, function (i, v) {
        if (v.variant_id == parseInt(variant_id)) {
            $.each(v.values, function (i1, v1) {
				if (v1.weight.paper.name == paper_name) {
					available_gr.push(v1.weight.quantity);
				}
            });
        }
    });

    return available_gr;
}

function updatePaper(variant_id) {
    var available_paper = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}


function updateCoverPaper(variant_id) {
    var available_paper = [];
    $.each(paper_cover_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
    return {available_paper:available_paper };
}


// 'select[name="order[paper_cover]"]'
function onPaperCoverChange(element) {
    var selected_element = element.find('option:selected');
    if ($(selected_element).text() == "Stessa grammatura dell'interno") {
        $('select[name="order[paper_cover_weight]"]').attr('disabled', true);
    } else {
        $('select[name="order[paper_cover_weight]"]').attr('disabled', false);
        if ($('select[name="order[format]"]').length > 0) {
            var gr = fetchGr($('select[name="order[format]"] option:selected').data('cover'), $(element).find("option:selected").text(), paper_cover_selected);
        } else {
            var gr = fetchGr($('input[name="order[format]"]:checked').data('cover'), $(element).find("option:selected").text(), paper_cover_selected);
        }
        appendToOptionAndDisabled(gr, 'select[name="order[paper_cover_weight]"]');
    }
}
