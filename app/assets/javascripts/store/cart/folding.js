//paper_products_selected
var availableGr = retrieve_paper_weight();

$(function () {
	// verifico cosa e` stato selezionato e mi prendo carte e grammature associate
	var fold = $('input[name="order[folding]"]:checked');
	var format = $('input[name="order[format]"]:checked');
	if ($.inArray(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.attr("data-folding"))) == 0) {
		var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
		var papers = updatePaper(selectedPapers);
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		$('select[name="order[weight]"]').change();
		$('input[name="order[folding]"]').change();
	} else {
		var format_fold = jQuery.parseJSON(fold.attr("data-format"))[0]
		$('#radio_format_'+format_fold).attr("checked", "checked");
		$('input[name="order[format]"]').change();
	}
	
	if (fold.attr("data-plasticization") == "0") {
		$.each($('select[name="order[plasticization]"]'), function(k, v) {
			$(v).attr("disabled", "disabled");
		});
	} else {
		$.each($('select[name="order[plasticization]"]'), function(k, v) {
			$(v).removeAttr("disabled");
		});
	}
	
	if (fold.attr("data-orientation") == "1") {
		$.each($('input[name="order[orientation]"]'), function(k, v) {
			if ($(v).attr("data-orientation") == "verticale") {
				$(v).attr("disabled", "disabled");
			}
		});
	} else {
		$.each($('input[name="order[orientation]"]'), function(k, v) {
			$(v).removeAttr("disabled");
		});
	}
	
	// se cambio piega devo:
	// disattivare i formati non presenti nella piega
	// selezionare il primo formato disponibile se e` stato disattivato
	// recuperare carte e grammature
	$('input[name="order[folding]"]').on("change", function () {
		var fold = $(this);
		// ciclo sui formati per disattivare nel caso
		$.each($('input[name="order[format]"]'), function(k, v) {
			if ($.inArray(jQuery.parseJSON($(v).val()), jQuery.parseJSON(fold.attr("data-format"))) < 0) {
				$(v).attr("disabled", "disabled");
			} else {
				$(v).removeAttr("disabled");
			}
		});
		// setto il nuovo formato se e` stato disattivato
		if ($('input[name="order[format]"]:checked').is(":disabled")) {
			var format_fold = jQuery.parseJSON(fold.attr("data-format"))[0];
			$('#radio_format_'+format_fold).attr("checked", "checked");
			$('input[name="order[format]"]').change();
		} else {
			// recupero carte e grammature
			var format = $('input[name="order[format]"]:checked');
			var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
			var papers = updatePaper(selectedPapers);
			appendToOption('select[name="order[paper]"]', papers.available_paper);
			var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
			$('select[name="order[weight]"]').change();
		}
		if (fold.attr("data-plasticization") == "0") {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).attr("disabled", "disabled");
			});
		} else {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
		if (fold.attr("data-orientation") == "1") {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				if ($(v).attr("data-orientation") == "verticale") {
					$(v).attr("disabled", "disabled");
				}
			});
		} else {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
	});
	
	// se cambio formato devo:
	// disattivare i le pieghe non presenti nel formato
	// selezionare la prima piegha disponibile se disattivata
	// recuperare carte e grammature
    $('input[name="order[format]"]').on("change", function () {
		var format = $(this);
		var fold = $('input[name="order[folding]"]:checked');
		// recupero carte e grammature
		var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
		var papers = updatePaper(selectedPapers);
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		$('select[name="order[weight]"]').change();
		if (fold.attr("data-plasticization") == "0") {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).attr("disabled", "disabled");
			});
		} else {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
		if (fold.attr("data-orientation") == "1") {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				if ($(v).attr("data-orientation") == "verticale") {
					$(v).attr("disabled", "disabled");
				}
			});
		} else {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
    });
	
		$('select[name="order[paper]"]').on("change", function () {
			var format = $('input[name="order[format]"]:checked');
			var fold = $('input[name="order[folding]"]:checked');
			var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
			var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
			$('select[name="order[weight]"]').change();
		});

});

function retrieve_folding_paper_weight(fold_id, form_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/'+fold_id+'/retrieve_folding_paper_weight/'+form_id+'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function fetchGr(paper_weight, paper_name) {
    var available_gr = [];
    $.each(paper_weight, function (i, v) {
        //if (v.variant_id == parseInt(variant_id)) {
            $.each(v.values, function (i1, v1) {
				if (v1.weight.paper.name == paper_name) {
					available_gr.push(v1.weight.quantity);
				}
            });
        //}
    });

    return available_gr;
}

function updatePaper(paper_weight) {
    var available_paper = [];
    $.each(paper_weight, function (i, v) {
        //if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        //}
    });
    return {available_paper:available_paper };
}