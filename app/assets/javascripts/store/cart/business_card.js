$(function () {
    var availablePaper = getAvailablePaper();
    var availableGr = retrieve_paper_weight();
	
    populateSelect(availablePaper, 'paper', false);
	
    var availableWeight = getAvailableWeightFromPaper(availablePaper, $('#paper option:selected').first().text());
    populateSelectWithDisabled(availableWeight, 'weight', true, availableGr);
	
    $('#paper').change(function () {
        var availableWeight = getAvailableWeightFromPaper(availablePaper, $('#paper option:selected').first().text());
        populateSelectWithDisabled(availableWeight, 'weight', true, availableGr);
    });

    var number_of_copy_id = $('#order_number_of_copy option:selected').val();
	
    $('#order_number_of_copy').change(function () {
        var number_of_copy_id = $(this).find(":selected").val();
    });
	
    $('input[id*=radio_format_]').change(function(){
        for(var i = 0; i < printer_format_users.printer_format_cards.length; i++){
            if(printer_format_users.printer_format_cards[i].id == $(this).val()){
                $('#download_template').attr('href', printer_format_users.printer_format_cards[i].template_url);
            }
        }
    });

    $('select[name="order[format]"]').change(function(){
        for(var i = 0; i < printer_format_users.printer_format_cards.length; i++){
            if(printer_format_users.printer_format_cards[i].id == $(this).val()){
                $('#download_template').attr('href', printer_format_users.printer_format_cards[i].template_url);
            }
        }
    });

	// ajaxCall();
});

function appendOption(selectId, optTxet, optValue, disabled) {
    if (disabled) {
        $('#' + selectId).append($("<option></option>").
            attr('value', optValue).
            text(optTxet));
    } else {
        $('#' + selectId).append($("<option></option>").
            attr('value', optValue).
            attr('disabled', 'disabled').
            text(optTxet));
    }
}

function findVariantFromOptionValueAndOptionType(option_value, option_type) {
    var r = null;
    jQuery.each(variants, function (index, itemData) {
        $.each(itemData, function (k, v) {
            $.each(v.variant.option_values, function (k1, v1) {
                if (v1.name == option_value && v1.option_type_name == option_type) {
                    r = v;
                }
            })
        })
    });
    return r;
}

function getAvailablePaper() {
    var h = new Array();
    $.each(paper_products_selected, function (i, v) {
        var v1 = v.weight;
        if (!find_paper_in_array(h, v1.paper.id)) {
            var gr = getAvailablePaperType(v1.paper.id);
            var t = {'name':v1.paper.name, 'presentation':v1.paper.presentation, 'id':v1.paper.id, 'gr':gr };
            h.push(t);
        }
    });
    return h;
}

function find_paper_in_array(array, paper_id) {
    var result = false;
    $.each(array, function (i, v) {

        if (v.id == paper_id) {
            result = true;
        }
    });
    return result;
}


function getAvailableWeightFromPaper(availablePaper, paperSelect) {
    var tmp = [];
    $.each(availablePaper, function (i, v) {
        if (v.presentation == paperSelect) {
            $.each(v.gr, function (i1, v1) {
                tmp.push(v1);
            });
        }
    });
    return tmp;
}

function getAvailablePaperFromWeight(availablePaper, weightSelected) {
    var tmp = [];
    $.each(availablePaper, function (i, v) {
        $.each(v.gr, function (i1, v1) {
            if (v1.name == weightSelected && $.inArray(v, tmp) < 0) {
                tmp.push(v);
            }

        });
    });
    return tmp;
}

function getAvailablePaperType(paper_id) {
    var h = new Array();
    $.each(paper_products_selected, function (i, v) {
        v1 = v.weight;
        if (v1.paper.id == paper_id) {
            h.push({ name:v1.quantity, presentation:v1.value, id:v1.id});
        }
    });
    return h;
}

/**
 * Popola la select identifcata dal select_id (senza #) con i valori dell'array passato.
 * Come campo value viene messo l'id, mentre come campo text viene messo presentation.
 * @param array array contente hash con id e presentation
 * @param select_id id della select
 */
function populateSelect(array, select_id, inverse) {
    var tmp = [];
    $("#" + select_id).html('');
    $.each(array, function (i, v) {
        if ($.inArray(v.name, tmp) < 0) {
            tmp.push(v.name);

            if (inverse) {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v.id).
                    text(v.name));
            } else {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v.id).
                    text(v.presentation));
            }
        }
    });
}

function populateSelectWithDisabled(array, select_id, inverse, availableGr) {
    var tmp = [];
    $("#" + select_id).html('');
    $.each(array, function (i, v) {
        if ($.inArray(v.name, tmp) < 0) {
            tmp.push(v.name);
        }
    });

    $.each(availableGr, function (i, v) {
        if ($.inArray(v, tmp) < 0) {
            if (inverse) {
                $("#" + select_id).append($("<option disabled='disabled'></option>").
                    attr('value', v).
                    text(v));
            } else {
                $("#" + select_id).append($("<option disabled='disabled'></option>").
                    attr('value', v).
                    text(v));
            }
        } else {
            if (inverse) {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v).
                    text(v));
            } else {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v).
                    text(v));
            }
        }
		$("#" + select_id).find("option:not(:disabled)").first().attr("selected", true);
    });
}


