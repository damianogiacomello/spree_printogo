$(function () {
	$('input[name="order[orientation]"]:checked').change();

	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});
	
});

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}