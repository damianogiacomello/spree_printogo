$(function () {
	var model = $('select[name="order[model]"]').val();
	updateFormat(model);
	$('select[name="order[model]"]').change(function() {
		updateFormat($(this).val());
		ajaxCallValidFieldSimpleProduct();
	});
	
	ajaxCall();
});

function updateFormat(model) {
	$('#order_number_of_copy').remove();
    $.each(model_copies, function (i, v) {
        if(v.id == parseInt(model)) {
			if (v.quantity[0].from == "1") {
				$('.copies').append(
					$('<input id="order_number_of_copy" name="order[number_of_copy]" type="text" min="1"></input>').val(1)
				);
			} else {
				$('.copies').append(
					$('<select id="order_number_of_copy" name="order[number_of_copy]"></select>').val(1)
				);
				$.each(v.quantity, function(i, v) {
					$('#order_number_of_copy').append(
						$("<option></option>").val(v.to).text(v.to)
					);
				});
			}
		}
    });
}