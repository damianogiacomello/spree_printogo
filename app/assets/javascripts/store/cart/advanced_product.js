$(function () {
	var model = $('select[name="order[model]"]').val();
	ModelOptions(variant_options, model);
	ajaxLoading = false;
	$('.loader-box').hide();
	//VariantOptions(attribute_options, variant_options, $('select[name*="attributes"]').first());
	
	$('select[name*="attributes"]').change(function(e) {
		VariantOptions(variant_options, $('select[name="order[model]"]').val(), $(this));
	});
	
	$('select[name="order[model]"]').change(function() {
		ModelOptions(variant_options, $(this).val());
	});
	
	//ajaxCallValidFieldAdvancedProduct();
	//$('select[name="order[model]"]').change();
});

function ModelOptions(options, val) {
	$.each(options, function(oi, ov) {
		if(ov.id == parseInt(val)) {
			var attr = ov.attributes[0];
			var elem = $('select[name="attributes[attribute_'+attr.id+']"]');
			$.each($('select[name*="attributes"]'), function(sai, sav) {
				
				var pos = parseInt($(sav).attr("data-position"));
				$(sav).html("<option value=''>"+Spree.translations.select+" "+ov.attributes[pos].name+"</option>").attr("disabled", true);
			});
			//elem.html("<option value=''>Seleziona "+attr.name+"</option>");
			//var ent = [];
			$.each(attr.entities, function(ei, ev) {
				
				//ent[ei] = ev.id;
				if (elem.find('option[value="'+ev.value_id+'"]').length == 0) {
					elem.append(
						$('<option/>').val(ev.value_id).text(ev.value_presentation)
					);
				}
				var dat = new Array(elem.find('option[value="'+ev.value_id+'"]').attr("data-entities"));
				dat.push(ev.id);
				elem.find('option[value="'+ev.value_id+'"]').attr("data-entities", dat.filter(function(e){return e}).join(","));
			});
			elem.attr("disabled", false);
		}
	});
}

function VariantOptions(options, model, self) {
	$.each(options, function(oi, ov) {
		if(ov.id == parseInt(model)) {
			if ((parseInt(self.attr("data-position")) + 1) < ov.attributes.length) {
				var next_attr = ov.attributes[parseInt(self.attr("data-position")) + 1];
				$.each($('select[name*="attributes"]'), function(sai, sav) {
					
					var pos = parseInt($(sav).attr("data-position"));
					if (pos > parseInt(self.attr("data-position"))) {
						$(sav).html("<option value=''>"+Spree.translations.select+" "+ov.attributes[pos].name+"</option>").attr("disabled", true);
					}
				});
				var ent = [];
				var old_ent = self.find('option:selected').attr("data-entities").split(",");
				var elem = $('select[name="attributes[attribute_'+next_attr.id+']"]');
				$.each(next_attr.entities, function(ei, ev) {
					
					if (jQuery.inArray(""+ev.id+"", old_ent) != -1) {
						ent[ei] = ev.id;
						if (elem.find('option[value="'+ev.value_id+'"]').length == 0) {
							elem.append(
								$('<option/>').val(ev.value_id).text(ev.value_presentation)
							);
						}
						var dat = new Array(elem.find('option[value="'+ev.value_id+'"]').attr("data-entities"));
						dat.push(ev.id);
						elem.find('option[value="'+ev.value_id+'"]').attr("data-entities", dat.filter(function(e){return e}).join(","));
					}
				});
				elem.attr("disabled", false);
			}
			if ((parseInt(self.attr("data-position")) + 1) == ov.attributes.length) {
				$('#entity_id').val(self.find('option:selected').attr("data-entities"));
				ajaxCall();
			}
		}
	});
}
