$(function () {
	$(".format-check input[type=radio]").on("change", function () {
        if ($(this).data('custom') == true) {
            if ($('.custom-size input').attr('disabled') == 'disabled') {
                $('.custom-size input').attr('disabled', false);
            }
        } else {
            $('.custom-size input').attr('disabled', true);
        }
    });
    $('.custom-size input').attr('disabled', true);
    $(".format-check input[type=radio]:checked").change();

    if ($('#add_posters').is('*')) {
        $('#add_to_cart_js').click(function () {
            $('#add_posters').submit();
        });
    }
	
    var paper = $('select[name="order[paper]"]').val();
    updatePapers(papers, paper);
    $('select[name="order[paper]"]').change(function() {
        updatePapers(papers, $(this).val());
    });

	ajaxCall();
});

function updatePapers(papers, current) {
    $.each(papers, function(k, pv) {
        if (pv.id == current) {
            $('select[name="order[paper]"]').parent().data("powertip", pv.description);
        }
    });
}