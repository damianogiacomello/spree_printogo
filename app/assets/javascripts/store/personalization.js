$(document).ajaxComplete(function(event, request, settings) {
	setTimeout(function() {
		if($.active == 0) {
			$('.loader-box').hide();
		}
	}, 300);
});

$(function () {
	$('[powertip="true"]').powerTip({
	    fadeInTime: 50,
	    fadeOutTime: 50,
		placement: 'e',
		mouseOnToPopup: true
	});
	
	$('.box > a').hoverZoom({
		overlay: true, // false to turn off (default true)
		overlayColor: '#000000', // overlay background color
		zoom: 0, // amount to zoom (px)
	});
	$('.box-twocolumns > a').hoverZoom({
		overlay: true, // false to turn off (default true)
		overlayColor: '#000000', // overlay background color
		zoom: 0, // amount to zoom (px)
	});

	if ($('.aside').length > 0 && $('#fixed').length > 0) {
		if($('#twocolumns #content').length > 0) {
			$('.aside').css('position', 'relative');
			$('.aside').css('height', $('#twocolumns #content').css('height'));
			$('#fixed').stickySidebar({speed:200, constrain:true});
		} else {
			$('.aside').css('position', 'relative');
			$('.aside').css('height', $('#twocolumns').css('height'));
			$('#fixed').stickySidebar({speed:200, constrain:true});
		}
	}

	if ($("#first-step").length > 0) {
		$("#first-step").validate();
	}

	if($('[name="order[format]"]').length > 0) {
		$('[name="order[format]"]').change(function() {
			if($('select[name="order[format]"]').length > 0) {
				var tId = $('select[name="order[format]"] option:selected').val();
			} else {
				var tId = $('input[name="order[format]"]:checked').val();
			}
			$('#download_template').attr("href", '/download/template/'+tId+'');
		});
		if($('select[name="order[format]"]').length > 0) {
			var tId = $('select[name="order[format]"] option:selected').val();
		} else {
			var tId = $('input[name="order[format]"]:checked').val();
		}
		$('#download_template').attr("href", '/download/template/'+tId+'');
	}

	$('.fileupload').fileupload({
		dataType: 'json',
		add: function (e, data) {
			// $(this).val(data.files[0].name);
			$('.fileupload').parent().find("button").remove();
			data.context = $('<button/>').text(Spree.translations.upload)
			.appendTo('#ali_'+$(this).data("id")+' .controls')
			.click(function () {
				$(this).replaceWith($('<p/>').addClass("upload").html('Upload <img src="/assets/store/uploader.gif"/></p><p>'+Spree.translations.notice_ie_upload));
				data.submit();
			});
		},
		done: function (e, data) {
			//$('#add-to-cart-button').removeAttr("disabled");
			var id_f = $(this).data("id")
			$('#ali_'+id_f+' .progress').hide();
			$('#ali_'+id_f+' p.upload').remove();
			$('#ali_'+id_f).toggle(false);
			$('#ali_aselect_'+id_f).after('<img src="/assets/store/checked.png"/>');
			$('.remember-upload').remove();
			$('.form-buttons').before("<p class='remember-upload'><strong>"+Spree.translations.upload_success_description+"</strong></p>");
			$('#add-to-cart-button').removeAttr("disabled");
			$('body').append("<div class='notification_container'><div id='basic-template'><a class='ui-notify-cross ui-notify-close' href='#'>x</a><h1>\#{title}</h1><p>\#{text}</p></div></div>");
			$(".notification_container").notify();
			$('.notification_container').notify('create', { title: Spree.translations.upload_success_title, text: Spree.translations.upload_success_description});
		},
		progressall: function (e, data) {
			var id_f = $(this).data("id")
			$('#ali_'+id_f+' .progress').show();
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#ali_'+id_f+' .progress .bar').css(
				'width',
				progress + '%'
			);
			//per firefox
			if (progress == 99) {
				setTimeout(function() {
					$('#ali_'+id_f+' .progress').hide();
					$('#ali_'+id_f+' p.upload').remove();
					$('#ali_'+id_f).toggle(false);
				}, 1000);
			}
			//per chrome e Safari
			if (progress == 100) {
				setTimeout(function() {
					$('#ali_'+id_f+' .progress').hide();
					$('#ali_'+id_f+' p.upload').remove();
					$('#ali_'+id_f).toggle(false);
					$('#ali_aselect_'+id_f).after('<img src="/assets/store/checked.png"/>');
				}, 1000);
			}
		}
	});
});

function selectMethod(elem) {
	$('#'+elem).toggle('slow');
}

function checkAndCallAjaxIfValid(field, form) {
	if($(field).length > 0) {
		if ($(field).is("*")) {
			if ($(form).valid()) {
				if ($(field).attr("id") == undefined || !$(field).attr("id").match(/^promotion_id_/)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function checkAndCallAjax(field) {
	if($(field).length > 0) {
		if ($(field).is("*")) {
			if ($(field).attr("id") == undefined || !$(field).attr("id").match(/^promotion_id_/)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function checkAndCallAjaxIfEnableElementAndIsValid(field, field_to_check, form) {
	if($(field).length > 0) {
		if ($(field).is("*")) {
			if ($(form).valid()) {
				if (!$(field_to_check).attr('disabled')) {
					if ($(field).attr("id") == undefined || !$(field).attr("id").match(/^promotion_id_/)) {
						return true;
					} else {
						return false;
					}
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function checkAndCallAjaxCustom(field) {
	if($(field).length > 0) {
		if ($(field).is("*")) {
			if ($(field).attr("id") == undefined || !$(field).attr("id").match(/^promotion_id_/)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function checkAndCallAjaxIfValidCustom(field, form) {
	if($(field).length > 0) {
		if ($(field).is("*")) {
			if ($(form).valid()) {
				if ($(field).attr("id") == undefined || !$(field).attr("id").match(/^promotion_id_/)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function openPopup(o_id, email) {
	var msg = Spree.translations.we_transfer_description_first+o_id+Spree.translations.we_transfer_description_second
	var stile = "top=10, left=10, width=420, height=600, status=no, menubar=no, toolbar=no , scrollbars=no, resizable=0";
	window.open("https://www.wetransfer.com/?to="+email+"&msg="+msg, "Upload File", stile);
	$('#add-to-cart-button').removeAttr("disabled");
}

function retrieve_paper_weight() {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/retrieve_paper_weight.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

// Quando il tipo di stampa, che può essere "Bianco e nero" o "Colori", cambia, viene aggiornato in modo adeguato il
// campo che indica se stampare anche copertina in Bianco e nero oppure no.
function onPrintColorChange(element) {
    if ($(element).find("option:selected").val() == "false") {
        $('select[name="order[cover_bw]"]').attr('disabled', false);
    } else {
        $('select[name="order[cover_bw]"]').attr('disabled', true);
    }
}

function onPlasticizationOptionChange(element) {
    if ($(element).find("option:selected").val() != "Nessuna") {
        $('select[name="order[plasticization]"]').attr('disabled', false);
    } else {
        $('select[name="order[plasticization]"]').attr('disabled', true);
    }
}

function PopulateCart() {
	$("form.buy-form").submit();
	return false;
}

function SetRadioPromotion(code) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/get_promotion_by_date/'+ code +'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	
	run_preventive = false;
	SetOptionPromotion(json);
}

function SetOptionPromotion(promo_json) {
	if (promo_json["frame"] != null) {
		$('select[name="order[frame]"]').val(promo_json["frame"]);
		updateFormat(promo_json["frame"]);
	}
	if (promo_json["folding"] != null) {
		$('#radio_folding_'+promo_json["folding"]).attr("checked", true);
	}
	
	if (promo_json["format"] != null && $('input[name="order[format]"]').length > 0) {
		$('#radio_format_'+promo_json["format"]).click();
	} else if (promo_json["format"] != null && $('select[name="order[format]"]').length > 0) {
		$('select[name="order[format]"]').val(promo_json["format"]);
	} else {
		if ($('input[name="order[custom][width]"]').hasClass("required") && $('input[name="order[custom][height]"]').hasClass("required")) {
			$('input[name="order[custom][width]"]').val(promo_json["custom_width"]);
			$('input[name="order[custom][height]"]').val(promo_json["custom_height"]);
		} else {
			$('select[name="order[format][width]"]').val(promo_json["custom_width"]);
			$('select[name="order[format][height]"]').val(promo_json["custom_height"]);
		}
	}
	
	$('[id*="radio_orientation"]').attr("checked", false);
	$('#radio_orientation_'+promo_json["orientation"]).click();
	
	if ($('select[name="order[number_of_copy]"]').length > 0) {
		$('select[name="order[number_of_copy]"]').val(promo_json["number_of_copy"]);
	} else {
		$('input[name="order[number_of_copy]"]').val(promo_json["number_of_copy"]);
	}
	$('select[name="order[instructions]"]').val(promo_json["front_back"]);
	if (promo_json["frame"] != null) {
		$('select[name="order[frame]"]').val(promo_json["frame"]);
	}
	
	if (promo_json["hanger_accessory"] != null) {
		$('select[name="order[hanger_accessory]"]').val(promo_json["hanger_accessory"]);
	}
	if (promo_json["eyelet"] != null) {
		$('select[name="order[eyelet]"]').val(promo_json["eyelet"]);
	}
	if (promo_json["eyelet_accessory_disposition"] != null) {
		$('select[name="order[eyelet_accessory]"]').val(promo_json["eyelet_accessory_disposition"]);
	}
	if (promo_json["eyelet_accessory_quantity"] != null) {
		$('input[name="order[eyelet_quantity]"]').val(promo_json["eyelet_accessory_quantity"]);
	}
	if (promo_json["buttonhole_accessory_disposition"] != null) {
		$('select[name="order[buttonhole_accessory]"]').val(promo_json["buttonhole_accessory_disposition"]);
	}
	if (promo_json["pocket_disposition"] != null) {
		$('select[name="order[pocket]"]').val(promo_json["pocket_disposition"]);
	}
	if (promo_json["reinforcement_perimeter"] != null) {
		$('select[name="order[reinforcement_perimeter]"]').val(promo_json["reinforcement_perimeter"]);
	}
	if (promo_json["number_of_facades"] != null) {
		$('input[name="order[number_of_facades]"]').val(promo_json["number_of_facades"]);
	}
	
	$('select[name="order[paper]"]').val(promo_json["paper"]);
	$('select[name="order[paper]"]').change();
	
	$('select[name="order[weight]"]').val(promo_json["weight"]);
	$('select[name="order[weight]"]').change();
	
	$('select[name="order[print_color]"]').val(promo_json["print_color"]);
	$('select[name="order[print_color]"]').change();
	
	if (promo_json["plasticization"] != null) {
		$('select[name="order[plasticization]"]').val(promo_json["plasticization"]);
	} else {
		$('select[name="order[plasticization]"]').val("");
	}
	
	// COVER
	if (promo_json["paper_cover"] != null) {
		$('select[name="order[paper_cover]"]').val(promo_json["paper_cover"]);
	}
	if (promo_json["paper_cover_weight"] != null) {
		$('select[name="order[paper_cover_weight]"]').val(promo_json["paper_cover_weight"]);
	}
	if (promo_json["sleeve"] != null) {
		$('select[name="order[sleeve]"]').val(promo_json["sleeve"]);
	}
	
	$('#promotion_id_'+promo_json["id"]).attr("checked", true);
	
	run_preventive = true;	
	ajaxCall();
}

function addJavascript(jsname,pos) {
	var th = document.getElementsByTagName(pos)[0];
	var s = document.createElement('script');
	s.setAttribute('type','text/javascript');
	s.setAttribute('src',jsname);
	th.appendChild(s);
}

