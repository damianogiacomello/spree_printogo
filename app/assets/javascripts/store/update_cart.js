$(function () {
    $('#cart-empty').click(function () {
        $("input[id*='quantity']").val(0);
        $('#update-cart').submit();
        return false;
    });

    $('.accordion-content').each(function (i) {
        if (i > 0) {
            toggleHide(this, false);
        }
    });

    $('.collapse').click(function () {
        $('.accordion-content').each(function (i) {
            toggleHide(this, true);
        });
        if ($(this).parents('.product').first().find('.accordion-content').first().css('display') == 'none') {
            $(this).parents('.product').first().find('.accordion-content').first().delay(1100).show(1000);
            $(this).html("<span> - </span>");
        }
    });

    $('span[data-id*="delete_line_item_"]').each(function () {
        $(this).click(function (e) {
            $(this).parents('.product').first().find('input[id*="_quantity"]').first().val("0");
            $(this).parents('form').first().submit();
            return false;
        });
    });

    $('#cart-empty').click(function () {
        $(this).parents('form').first().submit();
    });


});

function toggleHide(element, effect) {
    if (effect) {
        if ($(element).css('display') != 'none') {
            $(element).hide(1000);
        }
    } else {
        $(element).hide();
    }
    $(element).parents('.product').first().find('.product-name').first().find('.collapse').html("<span> + </span>");
}