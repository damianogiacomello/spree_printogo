var run_preventive = true;
/**
*  Qui è implementata la funzione che si occupa di recuperare tutti i dati e inviarli per richiedere un preventivo
*/
$(function () {
	ajaxLoading = false;
    
	if ($('#add_business_card').length > 0 || $('#add_posters').length > 0 || $('#add_poster_high_qualities').length > 0) {
		ajaxCallValidFieldBusinessPoster();
		
		if ($('#add_posters').length > 0 || $('#add_poster_high_qualities').length > 0) {
			
			// Controllo se l'utente deve inserire le dimensioni personalizzate non lancio il preventivo
			$('input[name="order[format]"]').click(function () {
				if ($(this).data('custom') != true) {
					ajaxCall();
				}
			});

			$('input[name="order[custom][width]"]').change(function () {
				if ($('input[name="order[custom][height]"]').val() != "") {
					ajaxLoading = false;
					ajaxCall();
					$('.loader-box').show();
				}
			});
			
			$('input[name="order[custom][height]"]').change(function () {
				if ($('input[name="order[custom][width]"]').val() != "") {	
					ajaxLoading = false;
					ajaxCall();
					$('.loader-box').show();
				}
			});
		}
		
	} else {
		if ($('#add_flayer').length > 0 || $('#add_poster_flyer').length > 0 || $('#add_letterhead').length > 0 || $('#add_postcard').length > 0 || $('#add_playbill').length > 0) {
			ajaxCallValidFieldFlayer();
		}
        
		if ($('#add_paperback').length > 0 || $('#add_staple').length > 0) {
			ajaxCallValidField();
		}
        
		if ($('#add_folding').length > 0) {
			ajaxCallValidFieldFolding();
		}
        
		if ($('#add_banners').length > 0) {
			ajaxCallValidFieldBanner();
		}
        
		if ($('#add_pvc_stickers').length > 0) {
			ajaxCallValidFieldPvc();
		}
        
		if ($('#add_spiral').length > 0) {
			ajaxCallValidFieldSpiral();
		}
        
		if ($('#add_canvas').length > 0) {
			ajaxCallValidFieldCanvas();
		}
        
		if ($('#add_simple_product').length > 0) {
			ajaxCallValidFieldSimpleProduct();
		}
        
		if ($('#add_advanced_product').length > 0) {
			ajaxCallValidFieldAdvancedProduct();
		}

		if ($('#add_rigids').length > 0) {
			ajaxCallValidFieldRigid();
		}

		if ($('#add_stickers').length > 0) {
			ajaxCallValidFieldSticker();
		}
	}

});

function ajaxCallValidFieldBusinessPoster() {
    if ($('select[name="order[number_of_copy]"]').length > 0) {
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }

    if ($('select[name="order[format]"]').length > 0) {
        checkAndCallAjaxIfValid('select[name="order[format]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValid('input[name="order[format]"]', 'form[id*=add_]');
    }
    
    checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[plasticization]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[instructions]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

// Qui ci sono tutti i campi, che, se presenti, devono essere validati prima di fare la chiamata ajax
function ajaxCallValidField() {
    
    if ($('select[name="order[format]"]').length > 0) {
        checkAndCallAjaxIfValid('select[name="order[format]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValid('input[name="order[format]"]', 'form[id*=add_]');
    }

    if ($('select[name="order[number_of_copy]"]').length > 0) {
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }
    
    if ($('select[name="order[sleeve]"]').length > 0) {
        checkAndCallAjaxIfValid('select[name="order[sleeve]"]', 'form[id*=add_]');
    }
    checkAndCallAjaxIfValidCustom('input[name="order[number_of_facades]"]', 'form[id*=add_]');
    //checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[weight]"]', 'form[id*=add_]');
    //checkAndCallAjaxIfEnableElementAndIsValid('select[name="order[paper_cover]"]', 'select[name="order[paper_cover_weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfEnableElementAndIsValid('select[name="order[paper_cover_weight]"]', 'select[name="order[cover_weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[plasticization_option]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[plasticization]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[cover_bw]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

function ajaxCallValidFieldFlayer() {
    //checkAndCallAjax('input[name="order[format]"]');
    if ($('select[name="order[number_of_copy]"]').length > 0) {
        checkAndCallAjaxCustom('select[name="order[number_of_copy]"]');
    } else {
        checkAndCallAjaxCustom('input[name="order[number_of_copy]"]');
    }
    //checkAndCallAjax('select[name="order[paper]"]');
    checkAndCallAjax('select[name="order[weight]"]');
    
    checkAndCallAjax('select[name="order[plasticization]"]');
    checkAndCallAjax('select[name="order[instructions]"]');
    checkAndCallAjax('select[name="order[print_color]"]');
    
    checkAndCallAjax('select[name="order[file_check]"]');
    checkAndCallAjax('[id*="order_calculator_data_"]');
}

function ajaxCallValidFieldFolding() {
    checkAndCallAjax('input[name="order[folding]"]');
    checkAndCallAjax('input[name="order[format]"]');
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxCustom('select[name="order[number_of_copy]"]');
    } else {
        checkAndCallAjaxCustom('input[name="order[number_of_copy]"]');
    }
    //checkAndCallAjax('select[name="order[paper]"]');
    //checkAndCallAjaxIfEnableElementAndIsValid('select[name="order[paper]"]', 'select[name="order[weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfEnableElementAndIsValid('select[name="order[weight]"]', 'select[name="order[paper]"]', 'form[id*=add_]');
    //checkAndCallAjax('select[name="order[weight]"]');
    
    checkAndCallAjax('select[name="order[plasticization]"]');
    checkAndCallAjax('select[name="order[instructions]"]');
    checkAndCallAjax('select[name="order[print_color]"]');
    
    checkAndCallAjax('select[name="order[file_check]"]');
    checkAndCallAjax('[id*="order_calculator_data_"]');
}

function ajaxCallValidFieldBanner() {
    checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]');
	
    checkAndCallAjaxIfValid('input[name="order[orientation]"]', 'form[id*=add_]');
        
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }
	
    checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]');
	
    checkAndCallAjaxIfValidCustom('select[name="order[eyelet]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('select[name="order[eyelet_accessory]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('input[name="order[eyelet_quantity]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('select[name="order[pocket]"]', 'form[id*=add_]');
	
    checkAndCallAjaxIfValid('select[name="order[reinforcement_perimeter]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

function ajaxCallValidFieldPvc() {
    checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('input[name="order[orientation]"]', 'form[id*=add_]');
        
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }
    checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[lamination]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[cutting_coil]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

function ajaxCallValidFieldCanvas() {
    if ($('input[name="order[custom][height]"]').hasClass("required") && $('input[name="order[custom][width]"]').hasClass("required")) {
        checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]');
        checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]');
    }
    if ($('select[name="order[format][width]"]').hasClass("required") && $('select[name="order[format][height]"]').hasClass("required")) {
        checkAndCallAjaxIfValidCustom('select[name="order[format][width]"]', 'form[id*=add_]');
        checkAndCallAjaxIfValidCustom('select[name="order[format][height]"]', 'form[id*=add_]');
    }
        
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxCustom('select[name="order[number_of_copy]"]');
    } else {
        checkAndCallAjaxCustom('input[name="order[number_of_copy]"]');
    }
    checkAndCallAjax('select[name="order[paper]"]');
    checkAndCallAjax('select[name="order[print_color]"]');
    
    $('select[name="order[frame]"]').change(function() {
        ajaxCallValidFieldCanvas();
        ajaxCall();
    });
    checkAndCallAjax('select[name="order[hanger_accessory]"]');
    
    checkAndCallAjax('select[name="order[file_check]"]');
    checkAndCallAjax('[id*="order_calculator_data_"]');
}

function ajaxCallValidFieldSpiral() {
    if ($('select[name="order[format]"]').length > 0){
        checkAndCallAjaxIfValid('select[name="order[format]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValid('input[name="order[format]"]', 'form[id*=add_]');
    }

    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }
    
    checkAndCallAjaxIfValid('input[name="order[number_of_facades]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfEnableElementAndIsValid('select[name="order[paper_cover]"]', 'select[name="order[paper_cover_weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[paper_cover_weight]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[plasticization_option]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[plasticization]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[cover_bw]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[spiraling]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[instructions]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[default_cover]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

function ajaxCallValidFieldSimpleProduct() {
    checkAndCallAjax('select[name="order[model]"]');
    
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjax('select[name="order[number_of_copy]"]');
    } else {
        checkAndCallAjax('input[name="order[number_of_copy]"]');
    }
    
    checkAndCallAjax('[id*="order_calculator_data_"]');
    checkAndCallAjax('select[name="order[file_check]"]');
}

function ajaxCallValidFieldAdvancedProduct() {
    //checkAndCallAjax('select[name="order[model]"]');
    checkAndCallAjax('select[name="entity_id"]');
    
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjax('select[name="order[number_of_copy]"]');
    } else {
        checkAndCallAjax('input[name="order[number_of_copy]"]');
    }
    
    checkAndCallAjax('[id*="order_calculator_data_"]');
    checkAndCallAjax('select[name="order[file_check]"]');
    order[custom][width]
}

function ajaxCallValidFieldRigid() {
    checkAndCallAjaxIfValid('input[name="order[format]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]');
        
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }
    
    checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[sided]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[eyelet]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[eyelet_accessory]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('input[name="order[eyelet_quantity]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[lamination]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('select[name="order[cutting_coil]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

function ajaxCallValidFieldSticker() {
    checkAndCallAjaxIfValid('input[name="order[format]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]');
        
    if($('select[name="order[number_of_copy]"]').length > 0){
        checkAndCallAjaxIfValidCustom('select[name="order[number_of_copy]"]', 'form[id*=add_]');
    } else {
        checkAndCallAjaxIfValidCustom('input[name="order[number_of_copy]"]', 'form[id*=add_]');
    }
    
    //checkAndCallAjaxIfValidCustom('select[name="order[paper]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValidCustom('select[name="order[print_color]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValidCustom('select[name^="order[processing_plotter_combination]"]', 'form[id*=add_]');

    checkAndCallAjaxIfValid('select[name="order[cutting_coil]"]', 'form[id*=add_]');
    
    checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]');
    checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]');
}

function checkAndCallAjaxIfValid(field, form) {
    if ($(field).is("*")) {
        $(field).change(function () {
            if ($(form).valid()) {
				if (!$(field).attr("id").match(/^promotion_id_/)) {
                	ajaxCall();
				}
            }
        });
    }
}

function checkAndCallAjax(field) {
    if ($(field).is("*")) {
        $(field).change(function () {
			if (!$(field).attr("id").match(/^promotion_id_/)) {
            	ajaxCall();
			}
        });
    }
}

function checkAndCallAjaxIfEnableElementAndIsValid(field, field_to_check, form) {
    if ($(field).is("*")) {
        $(field).change(function () {
            if ($(form).valid()) {
                if (!$(field_to_check).attr('disabled')) {
					if (!$(field).attr("id").match(/^promotion_id_/)) {
                    	ajaxCall();
					}
                }
            }
        });
    }
}

function checkAndCallAjaxCustom(field) {
	if ($(field).is("*")) {
		$(field).change(function () {
			if (!$(field).attr("id").match(/^promotion_id_/)) {
				$('.loader-box').show();
				ajaxLoading = false;
				ajaxCall();
			}
		});
	}
}

function checkAndCallAjaxIfValidCustom(field, form) {
	if ($(field).is("*")) {
		$(field).change(function () {
			if ($(form).valid()) {
				if (!$(field).attr("id").match(/^promotion_id_/)) {
					$('.loader-box').show();
					ajaxLoading = false;
					ajaxCall();
				}
			}
		});
	}
}

function fetchPreventiveData() {
	var data = {};
	if ($('select[name="order[format]"]').length > 0) {
		data["order[format]"] = $('select[name="order[format]"] option:checked').val();
	} else {
		data["order[format]"] = $('input[name="order[format]"]:checked').val();
	}
	data["order[orientation]"] = $('input[name="order[orientation]"]:checked').val();
	data["order[instructions]"] = $('select[name="order[instructions]"] option:selected').val();
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[weight]"] = $('select[name="order[weight]"] option:selected').val();
	data["order[plasticization]"] = $('select[name="order[plasticization]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	return data;
}

function fetchPreventiveDataPoster() {
	var data = {};
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	data["order[format]"] = $('input[name="order[format]"]:checked').val();
	if($('select[name="order[number_of_copy]"]').length > 0) {
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
    
	if ($('#poster_high_quality_product_id').length > 0) {
		data["poster_high_quality_product_id"] = $('#poster_high_quality_product_id').val();
	} else {
		data["poster_product_id"] = $('#poster_product_id').val();
	}

	return data;
}

function fetchPreventiveDataCanvas() {
	var data = {};
	if ($('input[name="order[custom][height]"]').hasClass("required") && $('input[name="order[custom][width]"]').hasClass("required")) {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if ($('select[name="order[format][height]"]').hasClass("required") && $('select[name="order[format][width]"]').hasClass("required")) {
		data["order[format][height]"] = $('select[name="order[format][height]"]').val();
		data["order[format][width]"] = $('select[name="order[format][width]"]').val();
	}
    
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	data["order[frame]"] = $('select[name="order[frame]"] option:selected').val();
	data["order[hanger_accessory]"] = $('select[name="order[hanger_accessory]"] option:selected').val();
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["canvas_product_id"] = $('#canvas_product_id').val();

	return data;
}

function fetchPreventiveDataBanner() {
	var data = {};
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[orientation]"] = $('input[name="order[orientation]"]:checked').val();
	data["order[eyelet]"] = $('select[name="order[eyelet]"] option:selected').val();
	data["order[eyelet_accessory]"] = $('select[name="order[eyelet_accessory]"] option:selected').val();
	data["order[eyelet_quantity]"] = $('input[name="order[eyelet_quantity]"]').val();
	data["order[pocket]"] = $('select[name="order[pocket]"] option:selected').val();
	data["order[pocket_quantity]"] = $('input[name="order[pocket_quantity]"]').val();
	data["order[reinforcement_perimeter]"] = $('select[name="order[reinforcement_perimeter]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["banner_product_id"] = $('#banner_product_id').val();

	return data;
}

function fetchPreventiveDataPvc() {
	var data = {};
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[orientation]"] = $('input[name="order[orientation]"]:checked').val();
	data["order[lamination]"] = $('select[name="order[lamination]"] option:selected').val();
	data["order[cutting_coil]"] = $('select[name="order[cutting_coil]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["pvc_product_id"] = $('#pvc_product_id').val();

	return data;
}

function fetchPreventiveDataRigid() {
	var data = {};
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	data["order[format]"] = $('input[name="order[format]"]:checked').val();
    
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
    
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	data["order[sided]"] = $('select[name="order[sided]"] option:selected').val();
    
	data["order[eyelet]"] = $('select[name="order[eyelet]"] option:selected').val();
	data["order[eyelet_accessory]"] = $('select[name="order[eyelet_accessory]"] option:selected').val();
	data["order[eyelet_quantity]"] = $('input[name="order[eyelet_quantity]"]').val();
    
	data["order[lamination]"] = $('select[name="order[lamination]"] option:selected').val();
	data["order[cutting_coil]"] = $('select[name="order[cutting_coil]"] option:selected').val();
    
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["rigid_product_id"] = $('#rigid_product_id').val();

	return data;
}

function fetchPreventiveDataSticker() {
	var data = {};
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	data["order[format]"] = $('input[name="order[format]"]:checked').val();
    
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
    
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
    
	data["order[processing_plotter_combination]"] = get_processing_plotter_types(data["order[paper]"]);
	
	data["order[number_of_stickers]"] = $('input[name="order[number_of_stickers]"]').val();
	data["order[orientation_coil]"] = $('input[name="order[orientation_coil]"]:checked').val();
	data["order[cutting_coil]"] = $('select[name="order[cutting_coil]"] option:selected').val();
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["sticker_product_id"] = $('#sticker_product_id').val();
	return data;
}

function fetchPreventiveDataStapleAndPaperback() {
	var data = {};
	if ($('select[name="order[format]"]').length > 0) {
		data["order[format]"] = $('select[name="order[format]"] option:checked').val();
	} else {
		data["order[format]"] = $('input[name="order[format]"]:checked').val();
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order[number_of_facades]"] = $('input[name="order[number_of_facades]"]').val();
	data["order[paper]"] = $('select[name="order[paper]"] option:selected').val();
	data["order[weight]"] = $('select[name="order[weight]"] option:selected').val();
	data["order[paper_cover]"] = $('select[name="order[paper_cover]"] option:selected').val();
	data["order[paper_cover_weight]"] = $('select[name="order[paper_cover_weight]"] option:selected').val();
	data["order[plasticization_option]"] = $('select[name="order[plasticization_option]"] option:selected').val();
	data["order[plasticization]"] = $('select[name="order[plasticization]"] option:selected').val();
	data["order[print_color]"] = $('select[name="order[print_color]"] option:selected').val();
	data["order[cover_bw]"] = $('select[name="order[cover_bw]"] option:selected').val();
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	return data;
}

function fetchPreventiveDataSimpleProduct() {
	var data = {};
	data["order[model]"] = $('select[name="order[model]"]').val();
    
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["simple_product_id"] = $('#simple_product_id').val();
	return data;    
}

function fetchPreventiveDataAdvancedProduct() {
	var data = {};
	data["order[model]"] = $('select[name="order[model]"]').val();
    
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"]').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
    
	data["order_calculator[data]"] = $('input[name="order_calculator[data]"]:checked').val();
	data["advanced_product_id"] = $('#advanced_product_id').val();
	data["entity_id"] = $('#entity_id').val();
	return data;
}


function ajaxCall() {
	if (xhr != undefined) {
		xhr.abort();
	}
	var xhr;
	if (run_preventive) {
		if (!ajaxLoading) {
			ajaxLoading = false;
			var data = undefined;
			if ($('#add_posters').length > 0 || $('#add_poster_high_qualities').length > 0) {
				data = fetchPreventiveDataPoster();
			} else if ($('#add_rigids').length > 0) {
				data = fetchPreventiveDataRigid();
			} else if ($('#add_stickers').length > 0) {
				data = fetchPreventiveDataSticker();
			} else if ($('#add_banners').length > 0) {
				data = fetchPreventiveDataBanner();
			} else if ($('#add_pvc_stickers').length > 0) {
				data = fetchPreventiveDataPvc();
			} else if ($('#add_canvas').length > 0) {
				data = fetchPreventiveDataCanvas();
			} else if ($('#add_simple_product').length > 0) {
				data = fetchPreventiveDataSimpleProduct();
			} else if ($('#add_advanced_product').length > 0) {
				data = fetchPreventiveDataAdvancedProduct();
			} else {
				if (($('#add_business_card').length > 0) || ($('#add_flayer').length > 0) || ($('#add_paperback').length > 0) || ($('#add_staple').length > 0) || ($('#add_folding').length > 0) || ($('#add_spiral').length > 0) || ($('#add_poster_flyer').length > 0) || ($('#add_letterhead').length > 0) || ($('#add_postcard').length > 0) || ($('#add_playbill').length > 0)) {
					data = fetchPreventiveData();
					if ($('#add_business_card').length > 0) {
						data["business_card_product_id"] = $('#business_card_product_id').val();
					}
					if ($('#add_flayer').length > 0) {
						data["flayer_product_id"] = $('#flayer_product_id').val();
					}
					if ($('#add_poster_flyer').length > 0) {
						data["poster_flyer_product_id"] = $('#poster_flyer_product_id').val();
					}
					if ($('#add_letterhead').length > 0) {
						data["letterhead_product_id"] = $('#letterhead_product_id').val();
					}
					if ($('#add_postcard').length > 0) {
						data["postcard_product_id"] = $('#postcard_product_id').val();
					}
					if ($('#add_playbill').length > 0) {
						data["playbill_product_id"] = $('#playbill_product_id').val();
					}
					if ($('#add_paperback').length > 0) {
						data = fetchPreventiveDataStapleAndPaperback();
						data["paperback_product_id"] = $('#paperback_product_id').val();
						data["order[sleeve]"] = $('select[name="order[sleeve]"] option:selected').val();
					}
					if ($('#add_staple').length > 0) {
						data = fetchPreventiveDataStapleAndPaperback();
						data["staple_product_id"] = $('#staple_product_id').val();
					}
					if ($('#add_folding').length > 0) {
						data["folding_product_id"] = $('#folding_product_id').val();
						data["order[folding]"] = $('input[name="order[folding]"]:checked').val();
					}
					if ($('#add_spiral').length > 0) {
						data = fetchPreventiveDataStapleAndPaperback();
						data["spiral_product_id"] = $('#spiral_product_id').val();
						data["order[default_cover]"] = $('select[name="order[default_cover]"] option:selected').val();
						data["order[instructions]"] = $('select[name="order[instructions]"] option:selected').val();
						data["order[spiraling]"] = $('select[name="order[spiraling]"] option:selected').val();
					}
				}
			}
			
			if ($('select[name="order[file_check]"]').length > 0) {
				data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
			}
			
			$('select').each(function(s) {
				if($(this).find("option:not(:disabled)").length == 0) {
					$(this).attr("disabled", "disabled");
				} else {
					$(this).removeAttr("disabled");
				}
			});
					
			$('.loader-box').show();
			var xhr = $.ajax({
				type: "POST",
				url: preventiveUrl,
				data: data
			});
		}
	}
}