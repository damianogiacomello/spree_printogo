# PIEGHEVOLI
module Spree
  class FoldingProduct < Product
    #after_initialize :add_option_type
    before_create :add_option_type
    has_many :folding_variants, :class_name => Spree::FoldingVariant, :foreign_key => "product_id", :dependent => :destroy
    attr_accessible :folding_variants, :folding_variants_attributes, :seo_title

    def initialize(val = nil)
      super(val)
      self.name = "Pieghevole" if self.name.empty?
      #self.available_on = Time.now if self.available_on.nil?
      if self.master.present?
        self.master.price = 0 if self.master.price.nil?
        self.master.on_hand = 1
      else
        self.master = Spree::Variant.new
        self.master.price = 0
        self.master.on_hand = 1
      end
    end

    def printer_type_option_type
      self.option_types.where("name = 'printer'").first
    end

    def directing_option_type
      self.option_types.where("name = 'printer_orientation'").first
    end

    def cut
      self.option_types.where("name = 'cut'").first
    end
    
    def folding_formats_variants
      tmp = []
      res = []
      self.folding_variants.each do |fv|
        res += fv.folding_formats.active
      end
      res.uniq_by {|ff| ff.option_value_id}
    end

    private
    def add_option_type
      if self.new_record?
        #plastification = Spree::OptionType.find_by_name 'plastification'
        print_color_ot = Spree::OptionType.find_by_name 'printer_color'
        printer_orientation_ot = Spree::OptionType.find_by_name 'printer_orientation'
        cut = Spree::OptionType.find_by_name 'cut'
        plasticization = Spree::OptionType.find_by_name 'plasticization'
        printer_ot = Spree::OptionType.find_by_name 'printer'
        folding_format_ot = Spree::OptionType.find_by_name('folding_format')
        
        #self.option_types << plastification
        self.option_types << print_color_ot
        self.option_types << printer_orientation_ot
        self.option_types << cut
        self.option_types << plasticization
        self.option_types << printer_ot
        self.option_types << folding_format_ot
      end
    end
  end
end
