Spree::Shipment.class_eval do
  def cost
    self.order.total_shipping
  end
  
  def ensure_correct_adjustment
    if adjustment
      adjustment.delete
    else
      reload #ensure adjustment is present on later saves
    end
  end
end