module Spree
  class MasterPrice < OptionValue
    belongs_to :plasticization, :class_name => "Spree::Plasticization", :foreign_key => "option_value_id"

    def plasticization_name
      name
    end

    def master_price
      presentation
    end

  end
end