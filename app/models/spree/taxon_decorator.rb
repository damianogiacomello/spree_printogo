Spree::Taxon.class_eval do
	after_create :set_slideshow
	before_update :update_slideshow
	before_destroy :disable_related_element

	has_attached_file :icon,
		:styles => { :mini => '32x32>', :normal => '128x128>', :box => '160x142#' },
		:default_style => :mini,
		:url => '/spree/taxons/:id/:style/:basename.:extension',
		:path => ':rails_root/public/spree/taxons/:id/:style/:basename.:extension',
		:convert_options => { :all => '-strip -auto-orient' }

	def set_slideshow
		Spree::SlideshowType.create(:category => self.permalink)
	end

	def update_slideshow
		if slideshow = Spree::SlideshowType.find_by_category(self.permalink_was)
			slideshow.update_attribute(:category, self.permalink)
		end
	end
	
	def disable_related_element
		sl = Spree::SlideshowType.find_by_category(self.permalink)
		sl.destroy if !sl.blank?
	
		Spree::HomepageElement.find_all_by_homepageble_id(self.id).each do | h |
			if (!h.blank? && h.homepageble_type == "Spree::Taxon")
				h.destroy
			end
		end

		Spree::Menu.find_all_by_linkable_id(self.id).each do |m|
			if (!m.blank? && m.linkable_type == "Spree::Taxon")
				m.destroy
			end
		end
	end

	def logo_image
		self.icon.url(:normal) if !self.icon.present?
  	end

  	def logo_image_box
  		if !self.icon.present?
      		self.icon.url(:box)
    	end
  	end

end
