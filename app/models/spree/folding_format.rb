# TIPI DI PIEGHE PER I FORMATI
module Spree
  class FoldingFormat < OptionValue
    after_save :associate_option_type
    belongs_to :folding, :class_name => "Spree::Folding", :foreign_key => "option_value_id"
    belongs_to :folding_variant, :class_name => "Spree::FoldingVariant", :foreign_key => "variant_id"
    has_many :paper_products, :class_name => "Spree::PaperProduct", :foreign_key => "folding_format_id", :dependent => :delete_all
    
    attr_accessible :active, :variant_id, :option_value_id, :one_orientation
    
    scope :active, lambda { where("active IS NOT NULL AND active = 1") }
    
    def one_orientation
      self.is_cut
    end
    
    def one_orientation=value
      self.is_cut = value
    end
    
    def formats
      self.folding.folding_formats.active.collect {|ff| ff.folding_variant.id}.to_json
    end
    
    def papers
      self.folding.folding_formats.active.collect {|ff| ff.paper_products.uniq_by {|ff| ff.paper_id}.collect{|pp| pp.paper.id}}.to_json
    end
    
    def get_paper_weight
      a = []
      t = []
      self.paper_products.each do |pp|
        tmp = t.select { |z| z[:weight][:quantity] == pp.paper_weight.weight if z[:weight] }
        if !tmp.empty?
          tmp.first[:weight][:paper] << {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}
        else
          t << {'weight' => {:quantity => pp.paper_weight.weight,
                             :id => pp.paper_weight.id,
                             :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
        end
        a << {:folding_format_id => self.id, :values => t}
      end
      a
    end
    
    def generate_paper_products
      fw = self.folding.folding_weights.order("start_up_cost_bw desc").first.weight_limit
      Spree::Paper.masters.each do |p|
        p.paper_weights.where("name <= #{fw}").each do |pw|
          self.paper_products.create(:paper_id => p.id, :weight_id => pw.id, :product_id => self.folding_variant.id)
        end
      end
    end
    
    def update_paper_products
      fw = self.folding.folding_weights.order("start_up_cost_bw desc").first.weight_limit
      self.paper_products.delete_all
      Spree::Paper.masters.each do |p|
        p.paper_weights.where("name <= #{fw}").each do |pw|
          self.paper_products.create(:paper_id => p.id, :weight_id => pw.id, :product_id => self.folding_variant.id)
        end
      end
    end
    
    def selected_weight_uniq
      tmp = []
      res = []
      self.paper_products.each do |pp|
        if tmp.empty?
          tmp << pp.paper_weight.weight
          res << pp.paper_weight.weight
        elsif !tmp.include?(pp.paper_weight.weight)
          tmp << pp.paper_weight.weight
          res << pp.paper_weight.weight
        end
      end
      res
    end
    
    def weight_uniq
      tmp = []
      res = []
      fw = self.folding.folding_weights.order("start_up_cost_bw desc").last.weight_limit
      Spree::PaperWeight.where("name <= #{fw.to_i}").each do |pw|
        if tmp.empty?
          tmp << pw.weight
          res << [pw.weight, pw.weight]
        elsif !tmp.include?(pw.weight)
          tmp << pw.weight
          res << [pw.weight, pw.weight]
        end
      end
      res
    end
    
    def selected_paper_uniq
      tmp = []
      res = []
      self.paper_products.each do |pp|
        if tmp.empty?
          tmp << pp.paper.presentation
          res << pp.paper.id
        elsif !tmp.include?(pp.paper.presentation)
          tmp << pp.paper.presentation
          res << pp.paper.id
        end
      end
      res
    end
    
    def paper_uniq
      tmp = []
      res = []
      fw = self.folding.folding_weights.order("start_up_cost_bw desc").last.weight_limit
      Spree::PaperWeight.where("name <= #{fw.to_i}").each do |pw|
        if tmp.empty? && !pw.paper.blank? && pw.paper.active
          tmp << pw.paper.id
          res << [pw.paper.presentation, pw.paper.id]
        elsif !pw.paper.blank? && pw.paper.active && !tmp.include?(pw.paper.id)
          tmp << pw.paper.presentation
          res << [pw.paper.presentation, pw.paper.id]
        end
      end
      res
    end
    
    def get_selected_values_json
      a = []
      self.paper_products.each do |pp|
        a << {'weight' => {:quantity => pp.paper_weight.weight, :id => pp.paper_weight.id, :value => pp.paper_weight.presentation, :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
      end
      a.to_json
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('folding_format').id) if self.option_type.nil?
    end
  end
end