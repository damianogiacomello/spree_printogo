# Finitura FORATURA
module Spree
  class Puncture < OptionType
    after_initialize :add_name_and_presentation

    attr_accessible :hourly_cost, :average_hourly_print, :start_time
    validates :hourly_cost, :average_hourly_print, :start_time, :presence => true

    def add_name_and_presentation
      if self.new_record?
        self.name = "puncture"
        self.presentation = "Foratura"
        option_values.new :name => 'puncture', :presentation => 'Foratura'
      end
    end

    def average_hourly_print
      self.average_hourly_work
    end

    def average_hourly_print= value
      self.average_hourly_work = value
    end
  end
end