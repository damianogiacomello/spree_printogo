module Spree
  class LetterheadVariant < FlayerVariant
    belongs_to :letterhead_product, :class_name => Spree::LetterheadProduct, :foreign_key => "product_id"
  end
end