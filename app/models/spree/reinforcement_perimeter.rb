# RINFORZO PERIMETRALE
module Spree
  class ReinforcementPerimeter < OptionValue
    after_save :associate_option_type
    attr_accessible :available_meter

    def available_meter
      self.start_up_cost_bw
    end
    
    def available_meter= value
      self.start_up_cost_bw = value
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('reinforcement_perimeter').id) if self.option_type_id.nil?
    end
  end
end