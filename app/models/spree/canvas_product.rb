# CANVAS [TELA]
module Spree
  class CanvasProduct < Product
    before_create :add_option_type
    attr_accessible :max_width, :incremental, :min_dimension, :max_height,
                    :canvas_variant, :canvas_variant_attributes, :seo_title
    has_one :canvas_variant, :class_name => Spree::CanvasVariant, :foreign_key => "product_id"
    
    accepts_nested_attributes_for :canvas_variant
    
    def incremental
      self.max_facades_weight_cover
    end
    
    def min_dimension
      self.max_facades_cover
    end
    
    def incremental=value
      self.max_facades_weight_cover = value
    end
    
    def min_dimension=value
      self.max_facades_cover = value
    end
    
    def initialize(val = nil)
      super(val)
      if self.new_record?
        self.name = "Canvas"
        #self.available_on = Time.now if self.available_on.nil?
        self.master.price = 0 if self.master && self.master.price.nil?
      end
    end

    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      self.canvas_variant.papers.map do |p|
        res << {
            :id => p.id,
            :name => p.name,
            :description => p.description.to_s,
        }
      end
      
      res.to_json
    end

    # Accesso agli option types
    def print_colors
      self.option_types.where(:name => "printer_color").first.option_values
    end

    def papers_option_type
      self.option_types.where(:name => "coil").first
    end

    def plotter_option_type
      self.option_types.where(:name => "plotter").first
    end
    
    private
    def add_option_type
      if self.new_record?
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        plotter_ot = Spree::OptionType.find_by_name('plotter')
        paper_ot = Spree::OptionType.find_by_name('coil')
        hanger_ot = Spree::OptionType.find_by_name('hanger')
        
        self.option_types << print_color_ot
        self.option_types << plotter_ot
        self.option_types << paper_ot
        self.option_types << hanger_ot
      end
    end
  end
end