# ASOLA ACCESSORIA (abbinata allo STRISCIONE)
module Spree
  class ButtonholeAccessory < OptionValue
    after_save :associate_option_type
    belongs_to :banner_product, :class_name => "Spree::BannerProduct", :foreign_key => "variant_id"
    belongs_to :buttonhole, :class_name => "Spree::Buttonhole", :foreign_key => "option_value_id"
    attr_accessible :available_meter, :buttonhole, :buttonhole_id, :disposition, :option_value_id
    
    def disposition
      self.presentation
    end
        
    def available_meter
      self.start_time_bw
    end
    
    def disposition=value
      self.presentation = value.compact.delete_if{|v| v == ""}.to_json
    end
    
    def available_meter=value
      self.start_time_bw = value
    end
    
    def disposition_enum
      ["Tutto il perimetro", "Lato Superiore", "Lato Inferiore", "Lato Destro", "Lato Sinistro", "Lato Superiore e Inferiore", "Lato Destro e Sinistro"]
    end
    
    def self.disposition_enum
      ["Tutto il perimetro", "Lato Superiore", "Lato Inferiore", "Lato Destro", "Lato Sinistro", "Lato Superiore e Inferiore", "Lato Destro e Sinistro"]
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('buttonhole_accessory').id) if self.option_type.nil?
    end
  end
end