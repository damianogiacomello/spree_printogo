# TELAIO
module Spree
  class Frame < OptionValue
    after_save :associate_option_type
    attr_accessible :copy_cost
    
    def self.frame
      Spree::OptionType.find_by_name('frame').option_values
    end
    
    def copy_cost
      self.start_time_bw
    end
    
    def copy_cost=value
      self.start_time_bw = value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('frame').id) if self.option_type.nil?
    end
  end
end