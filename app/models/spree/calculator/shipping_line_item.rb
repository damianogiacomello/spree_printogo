require_dependency 'spree/calculator'

module Spree
  class Calculator::ShippingLineItem < Calculator
    preference :amount, :decimal, :default => 0

    attr_accessible :preferred_amount

    def self.description
      "Aggiunge o toglie un valore per ogni item dell'ordine in base al metodo di spedizione"
    end

    def compute(object=nil)
      return 0 if object.nil?
      if object.is_a?(Spree::Order)
        self.preferred_amount * object.line_items.count
      elsif object.is_a?(Spree::LineItem) || object.is_a?(Spree::LineItemPromotion)
        # Calcolo la percentuale sul prezzo del line item
        # TODO da capire se il calcolo deve essere fatto sul prezzo solo del line_item o se include anche le spese di
        # spedizioni
        if object.is_a?(Spree::LineItem) && object.promo
          0
        else
          # Viene usato amount
          ((self.preferred_amount * object.amount)/100.to_f)*(-1)
        end
      end
    end

    # Returns all products that match this calculator, but only if the calculator
    # is attached to a promotion. If attached to a ShippingMethod, nil is returned.
    def matching_products
      # Regression check for #1596
      # Calculator::PerItem can be used in two cases.
      # The first is in a typical promotion, providing a discount per item of a particular item
      # The second is a ShippingMethod, where it applies to an entire order
      #
      # Shipping methods do not have promotions attached, but promotions do
      # Therefore we must check for promotions
      if self.calculable.respond_to?(:promotion)
        self.calculable.promotion.rules.map(&:products).flatten
      end
    end
  end
end