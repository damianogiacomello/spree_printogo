require_dependency 'spree/calculator'

module Spree
  class Calculator::Packaging < Calculator
    preference :amount, :decimal, :default => 0

    attr_accessible :preferred_amount

    def self.description
      "Aggiunge il prezzo del pacchetto"
    end

    def compute(object=nil)
      return 0 if object.nil?
      if object.is_a?(Spree::Order)
        self.preferred_amount
      else
        0
      end
    end

    def matching_products
      if self.calculable.respond_to?(:promotion)
        self.calculable.promotion.rules.map(&:products).flatten
      end
    end
  end
end