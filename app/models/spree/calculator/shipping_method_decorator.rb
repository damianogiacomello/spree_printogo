Spree::ShippingMethod.class_eval do
  attr_accessible :min_weight,:max_weight
  validates :min_weight, :max_weight, :presence => true
end