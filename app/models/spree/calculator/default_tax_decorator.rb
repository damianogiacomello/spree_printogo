Spree::Calculator::DefaultTax.class_eval do  
  def compute(computable)
    case computable
    when Spree::Order
      compute_order(computable)
    when Spree::LineItem
      compute_line_item(computable)
    when Spree::LineItemPromotion
      compute_line_item(computable)
    end
  end

  # Calcolo dell'iva con la proporzione 100 : 22 = total : x
  # essendo rate.amount = 0.22 basta moltiplicarlo per total
  def deduced_total_by_rate(total, rate)
    total * rate.amount
  end

  #def self.description
  #  "IVA: "
  #end

  def compute_line_item(line_item)
    if line_item.product.tax_category == rate.tax_category
      deduced_total_by_rate(line_item.total, rate)
    else
      0
    end
  end
  
  def compute_order(order)
    tax_total = order.item_total + order.total_shipping + order.total_presentation_pack
    deduced_total_by_rate(tax_total, rate)
  end
end