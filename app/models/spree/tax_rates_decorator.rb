Spree::TaxRate.class_eval do
  def adjust(order)
    label = "IVA: #{(amount*100).to_i}%"
    if included_in_price
      if Spree::Zone.default_tax && Spree::Zone.default_tax.contains?(order.tax_zone)
        order.adjustments.where(:originator_type => 'Spree::TaxRate').each { |ad| ad.destroy }
        order.line_items.each { |line_item| create_adjustment(label, line_item, line_item) }
      else
        order.adjustments.where(:originator_type => 'Spree::TaxRate').each { |ad| ad.destroy }
        amount =  calculator.compute(order)
        label =  label
        order.adjustments.create({ :amount => amount,
          :source => order,
          :originator => self,
          :locked => true,
          :label => label }, :without_protection => true)
      end
    else
      create_adjustment(label, order, order)
    end
  end
end