# Finitura PLASTIFICAZIONE
module Spree
  class Plasticization < OptionValue
    after_save :associate_option_type
    attr_accessible :quantity, :price_start_up, :max_quantity
    has_many :master_prices, :class_name => "Spree::MasterPrice", :foreign_key => "option_value_id"
    has_many :plasticization_prices, :class_name => "Spree::PlasticizationPrice", :foreign_key => "option_value_id", :dependent => :delete_all


		def self.plasticization_types
			[
				[I18n.t("printtogo.plasticization.polished_film"), "polished_film"],
				[I18n.t("printtogo.plasticization.matt_film"), "matt_film"]
			]
		end

    # Restituisce tutte le plasificazioni nei formati:
    #   - solo fronte
    #   - fronte e retro
    def self.plasticization_types_all
      all_format = []
      self.plasticization_types.each do |v|
        all_format << "#{v} - #{self.type_fb[0]}"
        all_format << "#{v} - #{self.type_fb[1]}"
      end
      all_format
    end

		def self.plasticization_cover
			[
				[I18n.t("printtogo.plasticization.no_plasticization"), "no_plasticization"],
				[I18n.t("printtogo.plasticization.front_cover_plasticization"), "front_cover_plasticization"],
				[I18n.t("printtogo.plasticization.front_back_cover_plasticization"), "front_back_cover_plasticization"],
				[I18n.t("printtogo.plasticization.all_plasticization"), "all_plasticization"]
			]
		end

		def self.type_fb
			[[I18n.t("printtogo.plasticization.front_only_plasticization_type"), "front_only_plasticization_type"], [I18n.t("printtogo.plasticization.front_back_plasticization_type"), "front_back_plasticization_type"]]
		end

		def self.standard
			[
				[I18n.t("printtogo.plasticization.standard.polished_film_front"), "polished_film_front"],
				[I18n.t("printtogo.plasticization.standard.polished_film_front_back"), "polished_film_front_back"],
				[I18n.t("printtogo.plasticization.standard.matt_film_front"), "matt_film_front"],
				[I18n.t("printtogo.plasticization.standard.matt_film_front_back"), "matt_film_front_back"]
			]
		end

    # Riceve il nome composto ossia: nome plastificazione - modo di plastificare (es.: Flim Lucido - solo fronte)
    # e restituisce un hash composto nel seguente modo:
    #   -  plasticization => option value identificato dalla prima parte del nome
    #   -  verse => nome del verso ossia o solo fronte o solo retro
    def self.retrive_from_string(composed_name)
      values = composed_name.split('-')
      {:plasticization =>
           Spree::OptionType.find_by_name("plastification").option_values.where(:name => values[0].strip).first,
       :verse => values[1].strip
      }
    end

    def self.plasticization_quantity
      ["0 - 1000", "1001 - 2000", "2001 - 3000", "oltre 3000"]
    end

    def price_start_up
      if self.price_front_bw
        self.price_front_bw.to_f
      else
        0
      end
    end

    def price_start_up= value
      self.price_front_bw = value
    end
    
    def max_quantity
      self.average_hourly_print_bw
    end
    
    def max_quantity= value
      self.average_hourly_print_bw = value
    end
    
    def format
      name
    end
    
    def self.get_plasticization_price_for_sheet(plasticization_type, number_of_sheet, printer_format)
      Spree::Plasticization.find_by_name(printer_format).plasticization_prices.each do |plasticization|
        if plasticization.name == plasticization_type
          if number_of_sheet.to_i > 3000 && plasticization.quantity.titleize == "Oltre 3000"
            return plasticization
          elsif number_of_sheet.to_i <= 1000 && plasticization.quantity == "0 - 1000"
            return plasticization
          elsif number_of_sheet > 1000 && number_of_sheet.to_i <= 2000 && plasticization.quantity == "1001 - 2000"
            return plasticization
          elsif number_of_sheet > 2000 && number_of_sheet.to_i <= 3000 && plasticization.quantity == "2001 - 3000"
            return plasticization
          end
        end
      end
      return 0
    end

    def self.get_plasticization_price_start_up(number_of_sheet)
      return 0 if self.first.max_quantity.nil?
      return self.first.price_start_up if number_of_sheet.to_i <= self.first.max_quantity
			return 0 if number_of_sheet.to_i > self.first.max_quantity
    end

    def generate_option_values
      self.option_values << Spree::OptionValue.new(:name => I18n.t("printtogo.plasticization.plasticizing_only_front"), :presentation => '0')
      self.option_values << Spree::OptionValue.new(:name => I18n.t("printtogo.plasticization.plasticizing_only_back"), :presentation => '0')
      self.option_values << Spree::OptionValue.new(:name => I18n.t("printtogo.plasticization.plasticizing_front_back"), :presentation => '0')
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('plasticization').id) if self.option_type_id.nil?
    end
  end
end