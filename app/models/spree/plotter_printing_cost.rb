module Spree
  class PlotterPrintingCost < OptionValue
    after_save :associate_option_type
    belongs_to :plotter, :class_name => "Spree::Plotter", :foreign_key => "option_value_id"
    
    attr_accessor :_destroy
    attr_accessible :cost_mq, :plant
    
    validates :name, :presentation, :cost_mq, :presence => true
    
    def cost_mq # Cost Mq
      self.start_up_cost_bw
    end
    
    def plant # Costo impianto
      self.price_front_bw
    end

    def cost_mq=(value)
      self.start_up_cost_bw=value
    end
    
    def plant=(value)
      self.price_front_bw = value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('plotter_printing_cost').id) if self.option_type.nil?
    end
  end
end