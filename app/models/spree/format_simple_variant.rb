# FORMATI PER I PRODOTTI SEMPLICI
module Spree
  class FormatSimpleVariant < OptionValue
    after_save :associate_option_type
    belongs_to :simple_variant, :class_name => "Spree::SimpleVariant", :foreign_key => "variant_id"
    
    default_scope order("waste_paper_bw, waste_paper_c ASC")
    
    attr_accessible :from, :to, :price
    attr_accessor :_destroy
    
    def from
      self.waste_paper_bw || 0
    end
    
    def to
      self.waste_paper_c || 0
    end
    
    def price
      self.start_time_bw
    end
    
    def from=value
      self.waste_paper_bw = value
    end
    
    def to=value
      self.waste_paper_c = value
    end
    
    def price=value
      self.start_time_bw = value
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('format_simple_variant').id) if self.option_type.nil?
    end
  end
end