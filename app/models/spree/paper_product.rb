module Spree
  class PaperProduct < ActiveRecord::Base
    attr_accessible :product_id, :paper_id, :weight_id, :option_value_id
    belongs_to :paper, :class_name => "Spree::Paper", :foreign_key => "paper_id"
    belongs_to :product, :class_name => "Spree::Product", :foreign_key => "product_id"
    belongs_to :paper_weight, :class_name => "Spree::PaperWeight", :foreign_key => "weight_id"
    belongs_to :variant, :class_name => "Spree::Variant", :foreign_key => "product_id"
    belongs_to :folding_format, :class_name => "Spree::FoldingFormat", :foreign_key => "folding_format_id"

		after_save :check_product_or_folding

		def check_product_or_folding
			if product_id.blank? && folding_format_id.blank?
				self.delete
			end
		end
  end
end