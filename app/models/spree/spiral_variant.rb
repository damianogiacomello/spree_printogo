module Spree
  class SpiralVariant < Variant
    attr_accessible :limit, :paper, :paper_weight, :format, :images, :default_cover_price
    #after_initialize :after_init
    before_create :after_init
    belongs_to :spiral_product, :class_name => Spree::SpiralProduct, :foreign_key => "product_id"
    has_one :limit, :class_name => "Spree::Limit", :foreign_key => "variant_id"
    has_many :paper_products, :class_name => "Spree::PaperProduct", :foreign_key => "product_id"

    # Serve dichiarare nuovamente l'associazione x associare il tipo corretto, altrimenti veniva usato Spree::Variant
    # che non poteva godere delle propietà di Spree::FlayerVariant
    has_many :children, :class_name => "Spree::CoverFlyerVariant", :foreign_key => "parent_id", :dependent => :destroy
		alias_attribute :format, :sku

    def get_selected_values_json
      a = []
      self.paper_products.each do |pp|
        a << {'weight' => {:quantity => pp.paper_weight.weight, :id => pp.paper_weight.id, :value => pp.paper_weight.presentation, :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
      end
      a.to_json
    end
    
    def get_paper_product_json
      h = []
      self.paper_products.each do |paper_page|
        h << {:paper_name => paper_page.paper.name, :paper_id => paper_page.paper.id, :weight => paper_page.paper_weight.weight}
      end
      h.to_json
    end

    def cover_variant
      self.children.first
    end

    private
    def after_init
      if self.new_record?
        self.limit = Spree::Limit.new if self.limit.nil?
        self.on_hand = 1
        if self.children.empty?
          # NB si da per scontato che esista un CoverFlayerProduct e che sia unico.
          # Altrimenti bisogna associare il CoverFlyerProduct al StapleProduct.
          child = self.children.new
          child.product = self.product.cover_product
        end
      end
    end

  end
end