# Finitura INTARCALATURA
module Spree
  class Intercalation < OptionType
    after_initialize :add_name_and_presentation
    #attr_accessible :hourly_cost, :average_hourly_print, :start_time
    #validates :hourly_cost, :average_hourly_print, :start_time, :presence => true

    has_many :intercalation_quantities, :class_name => "Spree::IntercalationQuantity", :foreign_key => "option_type_id", :dependent => :delete_all

    def add_name_and_presentation
      if self.new_record?
        self.name = "intercalation"
        self.presentation = "Intercalatura"
        option_values.new :name => 'intercalation', :presentation => 'Intercalatura'
      end
    end
  end
end