module Spree
  class Pose < OptionValue
    after_save :associate_option_type

    def self.poses
      Spree::OptionType.find_by_name('pose').option_values
    end

    def pose_number
      self.name
    end
    
    def cut_number
      self.presentation
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('pose').id) if self.option_type_id.nil?
    end
  end
end