# PVC Adesivo
module Spree
  class PvcProduct < BannerProduct
    before_create :add_option_type
    attr_accessible :pvc_variant, :pvc_variant_attributes, :seo_title
    has_one :pvc_variant, :class_name => Spree::PvcVariant, :foreign_key => "product_id"
    
    accepts_nested_attributes_for :pvc_variant
    
    def initialize(val = nil)
      super(val)
      if self.new_record?
        self.name = "PVC Adesivo"
        #self.available_on = Time.now if self.available_on.nil?
        self.master.price = 0 if self.master && self.master.price.nil?
      end
    end

    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      self.pvc_variant.papers.map do |p|
        res << {
            :id => p.id,
            :name => p.name,
            :description => p.description.to_s,
        }
      end
      
      res.to_json
    end
    
    private
    def add_option_type
      if self.new_record?
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        plotter_ot = Spree::OptionType.find_by_name('plotter')
        paper_ot = Spree::OptionType.find_by_name('coil')
        printer_instructions_ot = Spree::OptionType.find_by_name('printer_instructions')
        printer_orientation_ot = Spree::OptionType.find_by_name('printer_orientation')
        
        lamination_type_ot = Spree::OptionType.find_by_name('lamination_type')
        cutting_coil_type_ot = Spree::OptionType.find_by_name('cutting_coil_type')
        
        self.option_types << print_color_ot
        self.option_types << paper_ot
        self.option_types << printer_instructions_ot
        self.option_types << plotter_ot
        self.option_types << printer_orientation_ot
        
        self.option_types << lamination_type_ot
        self.option_types << cutting_coil_type_ot
      end
    end
  end
end