module Spree
  class Plotter < OptionValue
    after_save :associate_option_type
    has_many :plotter_printing_costs, :class_name => "Spree::PlotterPrintingCost", :foreign_key => "option_value_id", :dependent => :delete_all
    
    attr_accessible :start_up_cost_c, :plant_color_c, :price_front_and_back_c,
                    :start_up_cost_bw, :plant_color_bw, :price_front_and_back_bw,
                    :formats, :printer_hourly_cost_bw, :start_time_bw, :average_hourly_print_bw, :cost_ink_bw,
                    :printer_hourly_cost_c, :start_time_c, :average_hourly_print_c, :cost_ink_c, :cost_printing_bw,
                    :cost_printing_c, :plotter_printing_costs, :plotter_printing_costs_attributes
    
    accepts_nested_attributes_for :plotter_printing_costs, :allow_destroy => true
    
    def plotter_hourly_cost
      self.printer_hourly_cost_c
    end

    def plotter_hourly_cost=(value)
      self.printer_hourly_cost_c=value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('plotter').id) if self.option_type.nil?
    end
  end
end