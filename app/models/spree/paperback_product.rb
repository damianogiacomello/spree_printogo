# Brossura
module Spree
  class PaperbackProduct < Product
    #after_initialize :add_option_type
    before_create :add_option_type
    has_many :paperback_variants, :class_name => Spree::PaperbackVariant, :foreign_key => "product_id"
    has_many :children, :class_name => "Spree::CoverFlyerProduct", :foreign_key => "parent_id"
    attr_accessor :label_name
    attr_accessible :paperback_variants, :label_name, :finishing, :seo_title, :min_facades

    def min_facades
      self.max_facades_cover || 48
    end
    
    def min_facades=value
      self.max_facades_cover = value
    end

    def initialize(val = nil)
      super(val)
      self.name = "Brossura #{label_name}" if self.name.empty?
      
      #self.available_on = Time.now if self.available_on.nil?
      if self.master.present?
        self.master.price = 0 if self.master.price.nil?
        self.master.on_hand = 1
      else
        self.master = Spree::Variant.new
        self.master.price = 0
        self.master.on_hand = 1
      end
    end

    def printer_type_option_type
      self.option_types.where("name = 'printer'").first
    end

    # Ritorna un json di questo tipo:
    # supponendo p1 = carta di tipo 1,p2 = carta di tipo 2,p3 = carta di tipo 3
    # { 90 => {p1,p2}, 100 => {p2,p3} }
    def generate_weight_paper_json
      h = []
      Spree::Config[:paper_weight].split(",").collect { |w| w.to_i }.each do |weight|
        paper_weight = Spree::PaperWeight.find_all_by_name(weight.to_s)
        if !paper_weight.empty?
          t = []
          paper_weight.each do |pw|
            t << {:name => pw.paper.name, :id => pw.paper.id} if pw.paper && pw.paper.active
          end
          h << {'weight' => {:quantity => paper_weight.first.weight, :papers => t}}
        end
      end
      h.to_json
    end

    def get_selected_values_json
      a = []
      self.paperback_variants.each do |v|
        t = []
        v.paper_products.each do |pp|
          tmp = t.select { |z| z[:weight][:quantity] == pp.paper_weight.weight if z[:weight] }
          if !tmp.empty?
            tmp.first[:weight][:paper] << {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}
          else
            t << {'weight' => {:quantity => pp.paper_weight.weight,
                               :id => pp.paper_weight.id,
                               :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
          end
        end


        a << {:variant_id => v.id, :values => t}

      end
      a.to_json
    end

    def cover_product
      self.children.first
    end

    def get_cover_selected_values_json
      cover_product.get_selected_values_json
    end

    def cut
      self.option_types.where("name = 'cut'").first
    end

    def creasing
      self.option_types.where("name = 'creasing'").first
    end

    private
    def add_option_type
      if self.new_record?
        #plastification = Spree::OptionType.find_by_name 'plastification'
        print_color_ot = Spree::OptionType.find_by_name 'printer_color'
        plasticization = Spree::OptionType.find_by_name 'plasticization'
        printer_ot = Spree::OptionType.find_by_name 'printer'
        cut = Spree::OptionType.find_by_name 'cut'
        creasing = Spree::OptionType.find_by_name('creasing')

        #self.option_types << plastification
        self.option_types << print_color_ot
        self.option_types << plasticization
        self.option_types << printer_ot
        self.option_types << cut
        self.option_types << creasing
      end
    end
  end
end