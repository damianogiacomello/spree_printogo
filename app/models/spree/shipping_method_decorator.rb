Spree::ShippingMethod.class_eval do
  attr_accessible :min_weight, :max_weight

  default_scope order("name, min_weight ASC")
  
  def available_to_order?(order, display_on = nil)
    availability_check = available?(order,display_on)
    zone_check = zone && zone.include?(order.ship_address)
    # category_check = category_match?(order)
    availability_check && zone_check
  end
  
  def adjust(item, label = "")
    # Prima cancello gli eventuali altri calculator associati e poi lo aggiungo
    if item.is_a?(Spree::LineItem)
      item.adjustments.where(:originator_type => 'Spree::ShippingMethod').each { |ad| ad.destroy }
      w = item.weight
      label_name = "Spedizione #{item.variant.product.name} (#{w} kg)"
      item.adjustments.create({:amount => self.calculator.compute,
                                    :source => item,
                                    :originator => self,
                                    :locked => true,
                                    :label => label_name},
                                    :without_protection => true)
    else
      w = item.weight_item
      label_name = "#{self.name} ##{item.number} (Fino a #{label} kg)"
      item.adjustments.create({:amount => self.calculator.compute,
                                    :source => item,
                                    :originator => self,
                                    :locked => true,
                                    :label => label_name},
                                    :without_protection => true)
    end
    
    # Aggiungo le tasse come adjustment al line_item
    #tax_rate = Spree::TaxRate.first
    #tax_rate.adjust(item)
  end
end