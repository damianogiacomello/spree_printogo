Spree::PaymentMethod.class_eval do
  attr_accessible :icon
  has_attached_file :icon,
        :styles => { :mini => '32x32>', :normal => '128x128>' },
        :default_style => :mini,
        :url => '/spree/payment_method/:id/:style/:basename.:extension',
        :path => ':rails_root/public/spree/payment_method/:id/:style/:basename.:extension',
        :default_url => '/assets/default_payment_method.png'
end