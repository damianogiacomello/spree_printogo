# Entita`
module Spree
  class Entity < ActiveRecord::Base
    belongs_to :advanced_variant, :class_name => "Spree::AdvancedVariant", :foreign_key => "variant_id"
    has_many :entity_attribute_values, :class_name => "Spree::EntityAttributeValue", :foreign_key => "entity_id", :dependent => :delete_all
    
    attr_accessible :variant_id, :price, :weight, :entity_attribute_values, :entity_attribute_values_attributes
    
    validates :variant_id, :price, :weight, :presence => true
    
    accepts_nested_attributes_for :entity_attribute_values, :allow_destroy => true
  end
end