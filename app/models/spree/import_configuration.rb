module Spree
  class ImportConfiguration < ActiveRecord::Base
    validate :csv, :presence => true
    
    attr_accessible :csv, :error_log, :imported
    
    scope :imported, :conditions => {:used => true}
    
    has_attached_file :csv,
              :url  => "/spree/import_csv/:id/:csv_rename.csv",
              :path => ":rails_root/public/spree/import_csv/:id/:csv_rename.csv"

    Paperclip.interpolates :csv_rename do |attachment, style|
      attachment.instance.name
    end
    
    def configuration_csv
      if self.csv?
        
        data = FasterCSV.read(self.csv.path, {:col_sep => ",", :headers => true})
        self.imports_errors_log = "" if self.imports_errors_log.blank?

        data.each_with_index do |row, index|
          begin
            
          rescue Exception => e
            self.imports_errors_log += "#{e} \n"
          end
        end
        
        self.imported = true
        self.save
      end
    end
    
  end
end