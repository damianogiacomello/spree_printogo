require "custom_logger"
require_dependency "spree/order/order_calculate_function"
require_dependency "spree/order/quote_multipage"
require_dependency "spree/order/line_item_calculate"
require_dependency "spree/order/order_calculate"

include Spree::ShippingMethodsHelper
Spree::Order.class_eval do
  before_update :check_line_item_exists
  before_validation :clone_default_bill_address, :unless => :other_bill?
  attr_accessor :other_bill
  
  attr_accessible :presentation_pack_id, :contract_accept, :file_check, :other_bill
  belongs_to :presentation_pack, :class_name => "Spree::PresentationPack", :foreign_key => "presentation_pack_id"

  def add_variant(variant, params ={}, user = nil, quantity = 1, fake = false, promo = true)
    begin
      if !variant.is_a?(Spree::Product) && variant.product.is_a?(Spree::BusinessCardProduct)
        current_item = business_card_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::PosterProduct)
        current_item = poster_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::PosterHighQualityProduct)
        current_item = poster_high_quality_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::RigidProduct)
        current_item = rigid_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::StickerProduct)
        current_item = sticker_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::PosterFlyerVariant)
        current_item = poster_flyer_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::LetterheadVariant)
        current_item = letterhead_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::PlaybillVariant)
        current_item = playbill_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::FlayerVariant)
        current_item = flyer_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::StapleVariant)
        # Cambiato il file e la funzione di calcolo per il MultiPagina => quote_multipage
        current_item = quote_staple(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::PaperbackVariant)
        current_item = quote_paperback(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::FoldingVariant)
        current_item = folding_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::BannerVariant) && !variant.is_a?(Spree::PvcVariant)
        current_item = banner_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::PvcVariant)
        current_item = pvc_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::SpiralVariant)
        current_item = spiral_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::CanvasVariant)
        current_item = canvas_calculate(variant, params, quantity, promo, user)
      elsif variant.is_a?(Spree::AdvancedVariant)
        current_item = advanced_product_calculate(variant, params, quantity, user)
      else
        current_item = simple_product_calculate(variant, params, quantity, user)
      end

      if fake
        res = {:total => current_item.total, :price => current_item.price, :total_whitout_date => current_item.total_whitout_date,
              :shipping_method => current_item.shipping_method_id, :promo => current_item.promo,
              :weight_item => current_item.weight, :total_filecheck => current_item.total_filecheck,
              :number_of_copy => (current_item.number_of_copy rescue 0),
              :line_item_promotion => (current_item.line_item_promotion.present? ? current_item.line_item_promotion : nil)}
        
        self.line_items.last.delete
        res
      else
        current_item.save

        self.reload
        #ricalcolo il totale
        self.payment_total = payments.completed.map(&:amount).sum
        self.item_total = line_items.map(&:amount).sum
        self.adjustment_total = adjustments.eligible.map(&:amount).sum
        self.total = item_total + adjustment_total
        current_item
      end
    rescue Exception => ex
      self.line_items.last.destroy if fake && !self.line_items.empty?
      []
    end
  end
  
  def check_line_item_exists
    if self.line_items.blank?
      self.presentation_pack_id = nil
      self.shipping_method_id = nil
      self.adjustments.where(:originator_type => 'Spree::ShippingMethod').each { |sm| sm.destroy }
      self.adjustments.where(:originator_type => 'Spree::PresentationPack').each { |pp| pp.destroy }
      self.adjustments.where(:originator_type => 'Spree::TaxRate').each { |tr| tr.destroy }
      update!
    end
  end
  
  def weight_item
    self.line_items.collect {|li| li.weight}.sum
  end
  
  def checkout_steps
    if payment and payment.payment_method.payment_profiles_supported?
      %w(address upload_image payment confirm complete)
    else
      %w(address upload_image payment complete)
    end
  end
  
  # order state machine (see http://github.com/pluginaweek/state_machine/tree/master for details)
  StateMachine::Machine.ignore_method_conflicts = true
  Spree::Order.state_machines[:state] = StateMachine::Machine.new(Spree::Order, :initial => 'cart') do

    event :next do
      transition :from => 'cart', :to => 'address'
      transition :from => 'address', :to => 'upload_image'
      transition :from => 'upload_image', :to => 'payment'
      transition :from => 'payment', :to => 'complete'
      transition :from => 'confirm', :to => 'complete'

      # note: some payment methods will not support a confirm step
      transition :from => 'payment', :to => 'confirm',
                 :if => Proc.new { |order| order.payment_method && order.payment_method.payment_profiles_supported? }
    end
    
    event :cancel do
      transition :to => 'canceled', :if => :allow_cancel?
    end
    event :return do
      transition :to => 'returned', :from => 'awaiting_return'
    end
    event :resume do
      transition :to => 'resumed', :from => 'canceled', :if => :allow_resume?
    end
    event :authorize_return do
      transition :to => 'awaiting_return'
    end

    before_transition :to => 'complete' do |order|
      begin
        order.process_payments!
      rescue Core::GatewayError
        !!Spree::Config[:allow_checkout_on_gateway_error]
      end
    end

    before_transition :to => ['payment'] do |order|
      order.shipments.each { |s| s.destroy unless s.shipping_method.available_to_order?(order) }
    end

    after_transition :to => 'complete', :do => :finalize!
    #after_transition :to => 'delivery', :do => :create_tax_charge!
    #after_transition :to => 'payment', :do => :create_shipment!
    after_transition :to => 'resumed', :do => :after_resume
    after_transition :to => 'canceled', :do => :after_cancel
  end

	def allow_cancel?
		return false unless completed? and state != 'canceled'
		# %w{ready backorder pending}.include? shipment_state
		return true
	end

  # Creates new tax charges if there are any applicable rates. If prices already
  # include taxes then price adjustments are created instead.
  def create_tax_charge!
    # destroy any previous adjustments (eveything is recalculated from scratch)
    adjustments.tax.each(&:destroy)
    #price_adjustments.each(&:destroy)
    #
    #TaxRate.match(self).each { |rate| rate.adjust(self) }
  end
  
  def has_available_shipment
    true
    #return unless :address == state_name.to_sym
    #return unless ship_address && ship_address.valid?
    #errors.add(:base, :no_shipping_methods_available) if available_shipping_methods.empty?
  end

  def has_available_payment
    return unless :delivery == state_name.to_sym
    errors.add(:base, :no_payment_methods_available) if available_payment_methods.empty?
  end
  
  def deliver_order_confirmation_email
    begin
      Spree::OrderMailer.confirm_email(self).deliver
      Spree::OrderMailer.confirm_email_store(self).deliver
    rescue Exception => e
      logger.error("#{e.class.name}: #{e.message}")
      logger.error(e.backtrace * "\n")
    end
  end

  def total_vat
    adjustments.where("originator_type = 'Spree::TaxRate'").first.amount rescue 0
    #self.line_items.sum(&:total_vat)
  end

  def total_with_vat_and_shipping
    total_without_vat + adjustments.sum(&:amount)
  end

  def total_with_vat
    total_without_vat + total_vat
  end

  def total_without_vat
    self.line_items.sum(&:total)
  end
  
  def price_without_vat
    self.line_items.sum(&:price)
  end
  
  def file_check_total
    check_tot = 0
    self.line_items.each do |li|
      check_tot += li.adjustments.where("originator_type = ?", "Spree::FileCheck").sum {|adj| adj.amount } rescue 0
    end
    return check_tot
  end

  def total_shipping
    self.adjustments.where("originator_type = ?", "Spree::ShippingMethod").sum {|adj| adj.amount } rescue 0
  end
  
  def total_presentation_pack
    self.adjustments.where("originator_type = ?", "Spree::PresentationPack").sum {|adj| adj.amount } rescue 0
  end
  
  def clone_default_bill_address
		if self.state == "address"
			self.bill_address = self.user.bill_address.clone if self.bill_address && !self.bill_address.empty?
		end
		return true
  end
  
  def can_ship?
    self.complete? || self.resumed? || self.awaiting_return? || self.returned?
  end
  
  private
  def other_bill?
    @other_bill == true || @other_bill == "true" || @other_bill == "1"
  end
  
  def add_invoice
  	# Only create an invoice if the order is completed!
  	# And only create it if there is no invoice yet.
  	if Spree::Config[:invoice_on_order_complete]
      self.create_invoice(:user => user) if self.completed? && self.invoice.blank?
    end
  end
end