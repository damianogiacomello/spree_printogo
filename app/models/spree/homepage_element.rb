module Spree
  class HomepageElement < ActiveRecord::Base
    belongs_to :homepageble, :polymorphic => true

    attr_accessible :title, :description, :url, :attachment, :class_list, :position,
                    :homepageble, :homepageble_id, :homepageble_type, :enabled

    validate :title, :presence => true, :if => "homepageble_type == 'Spree::LineItemPromotion'"
    validate :description, :presence => true, :if =>"homepageble_type == 'Spree::LineItemPromotion'"

    has_attached_file :attachment,
            :styles => { :mini => '80x71#', :small => '160x142#', :normal => "160x142#", :large => '320x284#' },
            :default_style => :small,
            :url  => "/spree/homepage_elements/:id/:style_:basename.:extension",
            :path => ":rails_root/public/spree/homepage_elements/:id/:style_:basename.:extension",
            :convert_options => { :all => '-strip -auto-orient' }

    default_scope order('position ASC')
    scope :enabled, {:conditions => {:enabled => true}}

    def is_available?
      if (!homepageble.blank?)
        (homepageble_type != "Spree::LineItemPromotion") || (homepageble_type == "Spree::LineItemPromotion" && homepageble.is_available?)
      else
        true
      end
    end
    
    def initialize(*args)
      super(*args)
      last_homepage_element = HomepageElement.last
      self.position = last_homepage_element ? last_homepage_element.position + 1 : 0
      self.enabled = true
    end
  end
end
