Spree::Product.class_eval do 
	
	add_search_scope :taxons_id_eq do |taxon|
      select("DISTINCT(spree_products.id), spree_products.*").
      joins(:taxons).
      where(Spree::Taxon.table_name => { :id => taxon.id })
    end
	
end