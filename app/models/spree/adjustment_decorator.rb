Spree::Adjustment.class_eval do
  scope :user_promotion, lambda { where(:originator_type => 'Spree::UserPromotion', :adjustable_type => 'Spree::LineItem') }
end