# Entita` Attributo Valore
module Spree
  class EntityAttributeValue < ActiveRecord::Base
    belongs_to :entity, :class_name => "Spree::Entity", :foreign_key => "entity_id"
    belongs_to :simple_attribute, :class_name => "Spree::SimpleAttribute", :foreign_key => "attribute_id"
    belongs_to :simple_value, :class_name => "Spree::SimpleValue", :foreign_key => "value_id"
    
    attr_accessible :entity_id, :attribute_id, :value_id, :simple_value, :simple_attribute
  end
end