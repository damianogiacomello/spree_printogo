# Rigido
module Spree
  class RigidProduct < Product
    before_create :add_option_type
    has_many :rigid_variants, :class_name => Spree::RigidVariant, :foreign_key => "product_id"

		attr_accessor :label_name
    attr_accessible :min_width, :min_height, :rigid_variants, :rigid_variants_attributes, :seo_title, :label_name
    accepts_nested_attributes_for :rigid_variants

    def min_width
      self.max_weight_cover
    end
    
    def min_height
      self.max_facades_weight_cover
    end
    
    def min_width=value
      self.max_weight_cover = value
    end
    
    def min_height=value
      self.max_facades_weight_cover = value
    end
    
    def initialize(val = nil)
      super(val)
      if self.new_record?
        self.name = "Supporto Rigido" if self.name.empty?
        #self.available_on = Time.now if self.available_on.nil?
        self.master.price = 0 if self.master && self.master.price.nil?
      end
    end

    # Accesso agli option types
    def print_colors
      self.option_types.where(:name => "print_color").first.option_values
    end

    def papers_option_type
      self.option_types.where(:name => "rigid_support").first
    end

    def plotter_option_type
      self.option_types.where(:name => "plotter").first
    end

    def instructions_option_type
      self.option_types.where(:name => "printer_instructions").first
    end
    
    def directings
      self.option_types.where(:name => "printer_orientation").first
    end
    
    def rigid_printer_format_option_type
      self.option_types.where(:name => "rigid_printer_format").first
    end
    
    def rigid_printer_formats(id = nil)
      if !id.blank?
				begin
					self.master.option_values.find(id)
				rescue Exception => e
					logger.info "+=+=+=+=+=+=+=+=+=+=+=+=+=+= #{e} +=+=+=+=+=+=+=+=+=+=+=+=+=+="
					nil
				end
      else
        self.master.option_values.where(:option_type_id => self.rigid_printer_format_option_type.id)
      end
    end
    
    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      
      self.rigid_variants.map do |rv|
        rv.papers.map do |p|
          res << {
              :id => p.id,
              :name => p.name,
              :sided => p.sided,
              :dimension => "#{p.width} x #{p.height} cm",
              :description => p.description.to_s,
              :plotters => rv.plotter.plotter_printing_costs.map {|ppc| {:id => ppc.id, :name => ppc.name}},
              :eyelets => rv.eyelet_accessories.map {|ea| {:id => ea.eyelet.id, :name => ea.eyelet.name, :disposition => JSON.parse(ea.disposition), :limit => ea.available_meter}},
              :cutting_coils => rv.cutting_coils.map {|cc| cc.id},
              :laminations => rv.laminations.map {|l| l.id}
          }
        end
      end
      
      res.to_json
    end
    
    def papers_variant
      tmp = []
      res = []
      
      self.rigid_variants.map do |rv|
        rv.papers.map do |p|
          if tmp.empty?
            tmp << p
            res << p
          elsif !tmp.include?(p)
            tmp << p
            res << p
          end
        end
      end
      
      res
    end
    
    def cutting_coil_variants
      tmp = []
      res = []
      
      self.rigid_variants.map do |rv|
        rv.cutting_coils.map do |cc|
          if tmp.empty?
            tmp << cc
            res << cc
          elsif !tmp.include?(cc)
            tmp << cc
            res << cc
          end
        end
      end
      
      res
    end
    
    def lamination_variants
      tmp = []
      res = []
      
      self.rigid_variants.map do |rv|
        rv.laminations.map do |l|
          if tmp.empty?
            tmp << l
            res << l
          elsif !tmp.include?(l)
            tmp << l
            res << l
          end
        end
      end
      
      res
    end
    
    def plotters_variant
      tmp = []
      res = []
      
      plotters = self.rigid_variants.map {|rv| rv.plotter}
      
      plotters.map do |pl|
        pl.plotter_printing_costs.map do |ppc|
          if tmp.empty?
            tmp << ppc.name.downcase
            res << ppc
          elsif !tmp.include?(ppc.name.downcase)
            tmp << ppc.name.downcase
            res << ppc
          end
        end
      end
            
      res
    end
    
    def eyelet_accessories_variant
      tmp = []
      res = []
      
      self.rigid_variants.map do |rv|
        rv.eyelet_accessories.map do |ea|
          if tmp.empty?
            tmp << ea.eyelet
            res << ea
          elsif !tmp.include?(ea.eyelet)
            tmp << ea.eyelet
            res << ea
          end
        end
      end
      
      res
    end
    
    def find_rigids_variants(paper, print_color)
      self.rigid_variants.each do |rv|
        if rv.papers.include?(paper) && rv.plotter.plotter_printing_costs.include?(print_color)
          return rv
        end
      end
    end
    
    private
    def add_option_type
      if self.new_record?
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        plotter_ot = Spree::OptionType.find_by_name('plotter')
        paper_ot = Spree::OptionType.find_by_name('rigid_support')
        printer_instructions_ot = Spree::OptionType.find_by_name('printer_instructions')
        printer_orientation_ot = Spree::OptionType.find_by_name('printer_orientation')
        
        rigid_printer_format_ot = Spree::OptionType.find_by_name('rigid_printer_format')
        
        self.option_types << print_color_ot
        self.option_types << paper_ot
        self.option_types << printer_instructions_ot
        self.option_types << plotter_ot
        self.option_types << printer_orientation_ot
        
        self.option_types << rigid_printer_format_ot
        self.master.option_values << rigid_printer_format_ot.option_values
      end
    end
  end
end