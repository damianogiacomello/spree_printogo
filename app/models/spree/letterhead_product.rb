# CARTA INTESTATA
module Spree
  class LetterheadProduct < FlayerProduct
    has_many :letterhead_variants, :class_name => Spree::LetterheadVariant, :foreign_key => "product_id"
    attr_accessible :letterhead_variants, :label_name, :seo_title
  end
end