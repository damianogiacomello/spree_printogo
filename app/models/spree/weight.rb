# Numero di fogli da tagliare a seconda della grammatura
module Spree
  class Weight < OptionValue
    after_save :associate_option_type
    
    def weight
      self.name
    end
    
    def ream
      self.presentation
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('weight_cut_ream').id) if self.option_type_id.nil?
    end
  end
end