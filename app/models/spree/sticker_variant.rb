# Etichette
module Spree
  class StickerVariant < Variant
    before_create :after_init
    belongs_to :sticker_product, :class_name => "Spree::StickerProduct", :foreign_key => "product_id"
    has_many :processing_plotter_combinations, :class_name => "Spree::ProcessingPlotterCombination", :foreign_key => "variant_id", :dependent => :nullify
    attr_accessible :plotter, :papers, :cutting_coils, :format, 
                    :processing_plotter_combinations, :processing_plotter_combinations_attributes

    accepts_nested_attributes_for :processing_plotter_combinations, :allow_destroy => true                 

    validate :processing_plotter_combinations_count_within_bounds
		alias_attribute :format, :sku

    # Accesso agli option values
    def papers
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('coil').id)
    end

    def plotter
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('plotter').id).last
    end

    def cutting_coils
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('cutting_coil_type').id)
    end

    # Setto i tipi di carta che per questo prodotto sono le coil (BOBINE)
    def papers= values
      self.papers.each do |p|
        unless values.include?(p.id)
          self.option_values.delete(p)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.papers.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Setto la stampante che per questo prodotto e` il plotter
    def plotter= value
      self.option_values.delete(self.plotter) if !self.plotter.blank?
      self.option_values << Spree::Plotter.find(value)
    end
    
    # Setto i tipi di sagomatura
    def cutting_coils= values
      self.cutting_coils.each do |cc|
        unless values.include?(cc.id)
          self.option_values.delete(cc)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.cutting_coils.include?(ov)
            self.option_values << ov
          end
        end
      end
    end

    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end

    def processing_plotter_combinations_count_within_bounds
      return if processing_plotter_combinations.blank?
      errors.add("Too many processing_plotter_combinations") if processing_plotter_combinations.length > 5
    end
  end
end