# Cmbinazioni di lavorazione plotter
module Spree
  class ProcessingPlotterCombination < OptionValue
    after_save :associate_option_type
    belongs_to :sticker_variant, :class_name => "Spree::StickerVariant", :foreign_key => "variant_id"
    has_and_belongs_to_many :processing_plotter_types, :join_table => 'spree_processing_plotter_combination_processing_plotter_types'
    
    default_scope order("name ASC")
    
    attr_accessible :name, :presentation, :processing_plotter_types, :processing_plotter_type_ids,
                    :processing_plotter_types_attributes, :min_copies_active
    
    attr_accessor :_destroy
    
    def processing_plotter # Nome lavorazione
      self.presentation
    end
    
    def min_copies_active # Minimo copie per attivo
      self.waste_paper_bw || 0
    end
    
    def processing_plotter=value
      self.presentation = value.compact.delete_if{|v| v == ""}.to_json
    end
    
    def min_copies_active=value
      self.waste_paper_bw = value
    end
    
    def get_processing_plotter_json
      (JSON.parse(self.processing_plotter) rescue "")
    end

    def get_complete_processing_plotter_json
      @res = []
      (JSON.parse(self.processing_plotter)).each do |st|
        @res << { :name => Spree::OptionValue.find_by_id(st.to_i).name, :id => st.to_i }
      end
      @res.to_json
    end

    def get_complete_processing_plotter
      @res = []
      (JSON.parse(self.processing_plotter)).each do |st|
        @res << [Spree::OptionValue.find_by_id(st.to_i).name, st.to_i]
      end
      @res
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('processing_plotter_combination').id) if self.option_type.nil?
    end
  end
end
