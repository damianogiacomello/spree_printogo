# Quartini
module Spree
  class Quarter < OptionValue
    after_save :associate_option_type
    attr_accessible :number, :cut_number

    def self.quarters
      Spree::OptionType.find_by_name('quarter').option_values
    end

    def number
      self.name
    end
    
    def cut_number
      self.presentation
    end
    
    def number= value
      self.name=value
    end
    
    def cut_number= value
      self.presentation=value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('quarter').id) if self.option_type_id.nil?
    end
  end
end