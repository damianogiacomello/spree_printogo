module Spree
  class FoldingVariant < Variant
    attr_accessible :limit, :paper, :paper_weight, :format, :images, :limit_attributes,
                    :paper_attributes, :paper_weight_attributes, :available, :folding_formats, :folding_formats_attributes
    #after_initialize :after_init
    before_create :after_init
    after_create :set_folding_format
    belongs_to :folding_product, :class_name => Spree::FoldingProduct, :foreign_key => "product_id"
    has_one :limit, :class_name => "Spree::Limit", :foreign_key => "variant_id", :dependent => :destroy
    has_many :folding_formats, :class_name => "Spree::FoldingFormat", :foreign_key => "variant_id", :dependent => :destroy
    has_many :paper_products, :class_name => "Spree::PaperProduct", :foreign_key => "product_id", :dependent => :destroy

		alias_attribute :format, :sku
		alias_attribute :available, :active

    def foldings
      self.folding_formats.active.collect {|ff| ff.folding.id}.to_json
    end
    
    def get_selected_values_json
      a = []
      self.paper_products.each do |pp|
        a << {'weight' => {:quantity => pp.paper_weight.weight, :id => pp.paper_weight.id, :value => pp.paper_weight.presentation, :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
      end
      a.to_json
    end

    private
    def after_init
      if self.new_record?
        self.limit = Spree::Limit.new if self.limit.nil?
        self.on_hand = 1
      end
    end
    
    def set_folding_format
      if !self.is_master
        Spree::Folding.active.each do |f|
          ff = Spree::FoldingFormat.create(:option_value_id => f.id, :variant_id => self.id)
          ff.generate_paper_products
          self.option_values << ff
        end
      end
    end
  end
end