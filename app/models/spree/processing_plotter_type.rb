# TIPI ETICHETTATURA
module Spree
  class ProcessingPlotterType < OptionValue
    after_save :associate_option_type
    belongs_to :processing_plotter, :class_name => "Spree::ProcessingPlotter", :foreign_key => "option_value_id"
    has_and_belongs_to_many :processing_plotter_combinations, :join_table => 'spree_processing_plotter_combination_processing_plotter_types', :class_name => "Spree::ProcessingPlotterCombination"
    
    attr_accessible :available_meter, :processing_plotter, :processing_plotter_id, :option_value_id,
                    :hourly_cost, :start_time, :presentation, :plant
    
    default_scope order("name ASC")

    attr_accessor :_destroy

    def start_time # Tempo di avviamento
      self.start_time_bw
    end
    
    def hourly_cost # Costo orario
      self.average_hourly_print_bw
    end
    
    def available_meter # Prezzo al metro quadro
      self.plant_color_bw
    end
    
    def plant # Prezzo impianto
      self.price_front_bw || 0
    end

    def start_time=value
      self.start_time_bw = value
    end
    
    def hourly_cost=value
      self.average_hourly_print_bw = value
    end
    
    def available_meter=value
      self.plant_color_bw = value
    end
    
    def plant=value
      self.price_front_bw = value
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('processing_plotter_type').id) if self.option_type.nil?
    end
  end
end