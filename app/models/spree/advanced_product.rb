# Prodotto Avanzato
module Spree
  class AdvancedProduct < Product
	  has_many :advanced_variants, :class_name => Spree::AdvancedVariant, :foreign_key => "product_id", :dependent => :delete_all
	  has_many :simple_attributes, :class_name => "Spree::SimpleAttribute", :foreign_key => "variant_id", :dependent => :delete_all
    
    attr_accessible :marginality, :seo_title, :advanced_variants, :advanced_variants_attributes, :max_width,
                    :taxons, :taxon_ids, :simple_attributes, :simple_attributes_attributes
    
    after_create :setting_default_shipping_date
    
    accepts_nested_attributes_for :simple_attributes, :allow_destroy => true
    
    def accept_upload?
      self.max_width == 1
    end
    
    def ensure_master
      return unless new_record?
      self.master ||= Variant.new(:price => 0)
    end
    
    #def to_preventive
    #  self.advanced_variants.map{|av| av.entities.map{|e| ["id" => e.id, "model" => av.id, "attributes" => e.entity_attribute_values.map{|eav| Hash["id" => eav.attribute_id, 'pos' => eav.simple_attribute.position, "value" => Hash['id' => eav.value_id, 'pres' => eav.simple_value.presentation]]}.flatten]}}.flatten
    #end
    
    def to_preventive
      content = []
      self.advanced_variants.map do |av|
        acont = []
        self.simple_attributes.map do |sa|
          econt = []
          sa.entity_attribute_values.where("entity_id IN (#{av.entities.map{|e| e.id}.join(",")})").map do |eav|
            econt << {:id => eav.entity_id, :value_presentation => eav.simple_value.presentation, :value_id => eav.simple_value.id}
          end
          
          acont << {:id => sa.id, :position => sa.position, :name => sa.name, :entities => econt}
        end
        
        content << {:id => av.id, :attributes => acont}
      end
      
      content
    end
    
    def setting_default_shipping_date
      Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => self.id)
    end
	end
end