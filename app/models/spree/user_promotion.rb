module Spree
  class UserPromotion < ActiveRecord::Base
    belongs_to :product, :class_name => "Spree::Product"
    belongs_to :user, :class_name => "Spree::User"

    validates :amount, :presence => true, :numericality => true
    validates :user_id, :presence => true
    
    before_validation :setting_amount

    calculated_adjustments
    attr_accessible :amount, :product_id, :calculator, :product, :user, :user_id, :number_of_copy
    
    # Gets the array of UserPromotion appropriate for the specified order
    def self.match(order)
      return [] unless order.user
      all.select do |promo|
        promo.user == order.user || promo.user.contains?(order.user)
      end
    end

    # Creates necessary user_promotion adjustments for the order.
    def adjust(order)
      order.adjustments.where(:originator_type => 'Spree::UserPromotion').each { |ad| ad.destroy }
      label = create_label
      amount = -1 * calculator.compute(order)
      label = I18n.t(:discount) + label
      order.adjustments.create({ :amount => amount,
          :source => order,
          :originator => self,
          :locked => true,
          :label => label }, :without_protection => true)
    end
    
    def setting_amount
      c = Spree::Calculator::UserPromotionLineItem.new
      c.calculable = self
      c.preferred_amount = amount
      self.calculator = c
      self.amount = (self.amount/100) if self.amount
    end
    
    private
    def create_label
      " #{(amount * 100).to_i}%"
    end
  end
end
