module Spree
  class PresentationPack < ActiveRecord::Base
    has_many :orders, :class_name => "Spree::Order"

    after_initialize :create_calculator
    after_save :update_calculator_amount
    attr_accessible :orders, :icon, :name, :presentation, :price

    has_attached_file :icon,
                      :styles => {:mini => '32x32>', :normal => '63x53>'},
                      :default_style => :mini,
                      :url => '/spree/presentation_pack/:id/:style/:basename.:extension',
                      :path => ':rails_root/public/spree/presentation_pack/:id/:style/:basename.:extension',
                      :default_url => '/assets/default_presentation_pack.png'

    calculated_adjustments

    def adjust(order)
      # Prima cancello gli eventuali altri calculator associati e poi lo aggiungo
      order.adjustments.where(:originator_type => 'Spree::PresentationPack').each { |ad| ad.destroy }
      order.adjustments.create({:amount => amount(order),
        :source => order,
        :originator => self,
        :locked => true,
        :label => "Prezzo per il pacco: #{name}"}, :without_protection => true)
    end

    def amount(order)
      self.calculator.compute(order)
    end

    private
    def create_calculator
      if self.calculator.nil?
        c = Spree::Calculator::ShippingLineItem.new
        c.calculable = self
        c.preferred_amount = 0
        self.calculator = c
      end
    end

    def update_calculator_amount
      self.calculator.update_attribute :preferred_amount, self.price
    end
  end
end
