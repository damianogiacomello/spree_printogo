module Spree
  class PrinterFormatDefault < OptionValue
    after_save :associate_option_type
    before_save :set_name_and_presentation
    before_create :generate_plasticization
    before_destroy :remove_plasticization
    before_update :rename_plasticization
    
    has_and_belongs_to_many :printers, :class_name => 'Spree::Printer'
    belongs_to :master, :class_name => 'Spree::PrinterFormatDefault', :foreign_key => "option_value_id"
    
    attr_accessible :master, :quantity, :option_value_id, :option_value, :base, :height
    
    scope :masters, lambda {where("option_value_id IS NULL")}
    
    def quantity
      self.color_type || 1
    end
    
    def quantity= value
      self.color_type = value
    end
    
    def is_master
      !self.master.present?
    end
    
    def set_name_and_presentation
      self.name = "#{self.base}x#{self.height}"
      self.presentation = "#{self.base}x#{self.height}"
    end
    
    def rename_plasticization
      if p = Spree::Plasticization.find_by_name(self.name_was)
        p.name = self.name
        p.presentation = self.name
        p.save
      end
    end
    
    def generate_plasticization
      p = Spree::Plasticization.find_or_create_by_name(self.name)
      p.presentation = self.name
      p.save
      Spree::Plasticization.plasticization_quantity.each do |quantity|
        Spree::Plasticization.plasticization_types.each do |type|
          p.plasticization_prices.create(:name => type, :presentation => "0.0", :quantity => quantity)
        end
      end
    end
    
    def remove_plasticization
      p = Spree::Plasticization.find_by_name(self.name)
      p.destroy
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('printer_format_default').id) if self.option_type.nil?
    end
  end
end