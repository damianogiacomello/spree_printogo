# encoding: UTF-8

module Spree
  class FileCheck
    include ActiveModel::Validations
    include ActiveModel::Conversion
    extend ActiveModel::Naming
    
    attr_accessor :amount, :order, :order_id
    
    def initialize(attributes = {})
      attributes.each do |name, value|
        send("#{name}=", value)
      end
    end
    
    #calculated_adjustments

    # Creates necessary user_promotion adjustments for the order.
    def adjust(order)
      order.adjustments.where(:originator_type => 'Spree::FileCheck').each { |ad| ad.destroy }
      label = create_label
      label = I18n.t(:file_check) + label
      order.adjustments.create({:amount => self.amount,
          :source => order,
          :originator_type => 'Spree::FileCheck',
          :locked => true,
          :label => label }, :without_protection => true)
    end
    
    private
    def create_label
      " #{(amount).to_s} €"
    end
  end
end
