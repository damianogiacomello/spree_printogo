######################################################################################################
#                                           Other Function                                           #
######################################################################################################

# CREO IL MIO PARAMS PERSONALIZZATO
# line_params = {
#        :variant => Variante del prodotto
#        :current_item => Il nuovo current_item
#        :format => Formato selezionato
#        :orientation => L'orientamento (Verticale, Orizzontale)
#        :paper => Il tipo di carta (Tintoretto, Usomano, ...)
#        :front_back => Le istruzioni sulla stampa (Fronte, Retro, Fronte/Retro)
#        :print_color => La tipologia di colore (B/N, A colori)
#        :plastification => La plastificazione
#        :number_of_copy => Numero di copie
#        :weight => La grammatura
#        :number_of_facades => Numero di facciate
#        :printer => La stampante in base al numero di copie dato il punto di convenienza
#        :pose => Le pose in base al numero di copie dato il punto di convenienza
#        :paper_format => il formato di stampa impostato in base al numero di copie dato il punto di convenienza
#        :paper_cover => Il tipo di carta della copertina (Tintoretto, Usomano, ...)
#        :cover_weight => La grammatura della copertina
#        :print_color_cover => La tipologia di colore della copertina (B/N, A colori)
#        :front_back_cover => Le istruzioni sulla stampa della copertina (Fronte, Retro, Fronte/Retro)
#    }
#

module Spree
  class Order < ActiveRecord::Base
    
    # Setta il nuovo current_item con quello della promozione
    def set_promotion(line_params)
      line_params[:current_item].destroy
      line_params[:promo] = Spree::LineItemPromotion.find(line_params[:promo_id])
      line_params[:current_item] = line_params[:promo].clone
      line_params[:current_item].quantity = 1
      self.line_items << line_params[:current_item]
      line_params[:current_item].price = line_params[:promo].price
      #line_params[:current_item].shipping_date_adjustment = line_params[:promo].shipping_date_adjustment
      line_params[:current_item].line_item_promotion_id = line_params[:promo].id
      line_params[:current_item].promo = true
      line_params[:current_item].weight = line_params[:promo].weight_item
      line_params[:current_item].line_items_option_values = line_params[:promo].line_items_option_values.collect{|liov| liov.clone}
      # Aggiungo gli adjustment
      #line_params[:current_item].shipping_date_adjustment.adjust(line_params[:current_item])
      @ship_date = Spree::ShippingDateAdjustment.new({:name => "Promozione", :preferred_amount => 0.0, :product_id => line_params[:promo].product_id, :days => Date.today.business_days_until(line_params[:promo].shipping_date.to_date)})
      @ship_date.init_calculator
      @ship_date.adjust(line_params[:current_item])
      @ship_date.destroy
      line_params[:current_item].shipping_date = line_params[:promo].shipping_date
      #tax_rate = Spree::TaxRate.first
      #tax_rate.adjust(line_params[:current_item])
      if line_params[:file_check] && line_params[:file_check] != "0"
        Spree::FileCheck.new(:amount => Spree::Config[:file_check_cost].to_f).adjust(line_params[:current_item])
      end
      # Verifico se la promozione e` illimitata o ha una qunatity
      if !line_params[:promo].unlimited
        line_params[:promo].update_attribute(:quantity, line_params[:promo].quantity - 1)
      end
      
      line_params
    end
    
    # Verifica se esiste una promozione per il prodotto configurato e nel caso la seleziona
    def skip_promotion(line_params, variant)
      line_params[:skip] = nil
      # CUSTOM_LOGGER.info "SONO IN skip_promotion *********************************"
      if variant.line_item_promotions.active.available.present?
        promotion_params = variant.line_item_promotions.active.available.collect{|lip| lip.to_calculate}
        promotion_params.each do |pp|
          line_params[:skip] = true
          line_params[:promo_id] = pp[:id]
          # CUSTOM_LOGGER.info "line_params: #{line_params.to_json} *********************************"
          # CUSTOM_LOGGER.info "Entro nel ciclo promotion_params.each *********************************"
          # CUSTOM_LOGGER.info "promotion_params: #{pp.to_json} *********************************"
          pp.delete(:id)
          pp.each do |k, v|
            # CUSTOM_LOGGER.info "Entro nel ciclo pp.each *********************************"
            if k == :format
              if line_params[:current_item].format_promo != pp[:format]
                line_params[:skip] = false
                # CUSTOM_LOGGER.info "format_item: #{line_params[:current_item].format} *********************************"
                # CUSTOM_LOGGER.info "skip: #{pp[:format]} *********************************"
              end
            elsif k == :shipping_date
              if (line_params[k.to_sym].blank? && !pp[k.to_sym].blank?) || (!line_params[k.to_sym].blank? && v != line_params[k.to_sym].presentation)
                # CUSTOM_LOGGER.info "key: #{k} *********************************"
                # CUSTOM_LOGGER.info "skip: #{v} *********************************"
                line_params[:skip] = false
              end
            else
              if (line_params[k.to_sym].blank? && !pp[k.to_sym].blank?) || (!line_params[k.to_sym].blank? && v != line_params[k.to_sym].presentation.to_i && v != line_params[k.to_sym].id && v != line_params[k.to_sym].name && v != line_params[k.to_sym].name.to_i)
                # CUSTOM_LOGGER.info "key: #{k} *********************************"
                # CUSTOM_LOGGER.info "skip: #{v} *********************************"
                line_params[:skip] = false
              end
            end
          end
          if line_params[:skip]
            break
          end
        end
      else
        line_params[:skip] = false
      end
      line_params
    end

    # Ritorno i line_params per BusinessCard
    def fetch_values_business(params, variant, quantity)
      # CUSTOM_LOGGER.info "SONO IN fetch_values_business *********************************"
      line_params = Hash.new()
      line_params[:variant] = variant
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      # Recupero i parametri parziali:
      #   - format: tipo 8,5x5 cm
      #   - orientation: verticale o orizzontale
      #   - paper: Tintoretto, Uso mano...
      #   - weight: Grammatura
      #   - front_back
      # e li aggiungo come OptionValuesLineItem

      line_params[:format] = Spree::OptionValue.find(params[:order][:format])
      # CUSTOM_LOGGER.info "format: #{line_params[:format].name} *********************************"
      line_params[:orientation] = Spree::OptionValue.find(params[:order][:orientation])
      # CUSTOM_LOGGER.info "orientation: #{line_params[:orientation].name} *********************************"
      line_params[:paper] = Spree::Paper.find(params[:order][:paper])
      # CUSTOM_LOGGER.info "paper: #{line_params[:paper].name} *********************************"
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = '#{params[:order][:instructions].to_s}'").first
      # CUSTOM_LOGGER.info "front_back: #{line_params[:front_back].name} *********************************"
      line_params[:print_color_ot] = Spree::OptionType.find_by_name('printer_color')
      line_params[:print_color] = line_params[:print_color_ot].option_values.where("name = '#{params[:order][:print_color].to_s}'").first
      # CUSTOM_LOGGER.info "print_color: #{line_params[:print_color].name} *********************************"
      line_params[:plastification] = Spree::OptionValue.where(:name => params[:order][:plasticization], :option_type_id => Spree::OptionType.find_by_name("plasticization")).first if params[:order][:plasticization] != 'none'
      # CUSTOM_LOGGER.info "plastification: #{line_params[:plastification]} *********************************" if params[:order][:plasticization] != 'none'
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation} *********************************"
      # Per la grammatura ho solo il valore(name) e devo cercare option_value appropiato
      line_params[:weight] = line_params[:paper].paper_weights.where("name = '#{params[:order][:weight].to_s}'").first
      # CUSTOM_LOGGER.info "weight: #{line_params[:weight].name} *********************************"

      current_item.line_items_option_values.new :option_value => line_params[:format]
      current_item.line_items_option_values.new :option_value => line_params[:orientation]
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:weight]
      current_item.line_items_option_values.new :option_value => line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:plastification] if params[:order][:plasticization] != 'none'
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]

      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0

      # ricavo un nuovo hash per STAMPANTE, POSE e FORMATO DELLA CARTA
      line_params = get_printer_variant(line_params, line_params[:current_item].variant.limit)
      # Ricavo il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
      line_params[:paper_init_format] = get_start_format(line_params[:paper_format])
      # CUSTOM_LOGGER.info "orientation: #{line_params[:paper_init_format].to_json} *********************************"
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]

      return line_params
    end

    # Ritorno i line_params per FlyerCard
    def fetch_values_flyer(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      
      line_params[:orientation] = Spree::OptionValue.find(params[:order][:orientation])
      line_params[:paper] = Spree::Paper.find(params[:order][:paper])
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = '#{params[:order][:instructions].to_s}'").first
      line_params[:print_color_ot] = Spree::OptionType.find_by_name('printer_color')
      line_params[:print_color] = line_params[:print_color_ot].option_values.where("name = '#{params[:order][:print_color].to_s}'").first
      line_params[:plastification] = Spree::OptionValue.where(:name => params[:order][:plasticization], :option_type_id => Spree::OptionType.find_by_name("plasticization")).first if params[:order][:plasticization] != 'none'
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      # Per la grammatura ho solo il valore(name) e devo cercare option_value appropiato
      line_params[:weight] = line_params[:paper].paper_weights.where("name = '#{params[:order][:weight].to_s}'").first
      
      current_item.line_items_option_values.new :option_value => line_params[:orientation]
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:weight]
      current_item.line_items_option_values.new :option_value => line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:plastification] if params[:order][:plasticization] != 'none'
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]

      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0

      # ricavo un nuovo hash per STAMPANTE, POSE e FORMATO DELLA CARTA
      line_params = get_printer_variant(line_params, line_params[:current_item].variant.limit)
      # Ricavo il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
      line_params[:paper_init_format] = get_start_format(line_params[:paper_format])
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]

      return line_params
    end
    
    # Ritorno i line_params per Folding
    def fetch_values_folding(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant

      # format = Spree::OptionValue.find(params[:order][:format])
      line_params[:orientation] = Spree::OptionValue.find(params[:order][:orientation])
      line_params[:folding_format] = Spree::FoldingFormat.find_by_option_value_id_and_variant_id(params[:order][:folding], params[:order][:format])
      line_params[:folding] = line_params[:folding_format].folding
      line_params[:paper] = Spree::Paper.find(params[:order][:paper])
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = '#{params[:order][:instructions].to_s}'").first
      line_params[:print_color_ot] = Spree::OptionType.find_by_name('printer_color')
      line_params[:print_color] = line_params[:print_color_ot].option_values.where("name = '#{params[:order][:print_color].to_s}'").first
      line_params[:plastification] = Spree::OptionValue.where(:name => params[:order][:plasticization], :option_type_id => Spree::OptionType.find_by_name("plasticization")).first if params[:order][:plasticization] != 'none'
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      # Per la grammatura ho solo il valore(name) e devo cercare option_value appropiato
      line_params[:weight] = line_params[:paper].paper_weights.where("name = '#{params[:order][:weight].to_s}'").first
      
      current_item.line_items_option_values.new :option_value => line_params[:orientation]
      current_item.line_items_option_values.new :option_value => line_params[:folding_format]
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:weight]
      current_item.line_items_option_values.new :option_value => line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:plastification] if params[:order][:plasticization] != 'none'
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      
      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0

      # ricavo un nuovo hash per STAMPANTE, POSE e FORMATO DELLA CARTA
      line_params = get_printer_variant(line_params, line_params[:current_item].variant.limit)
      # Ricavo il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
      line_params[:paper_init_format] = get_start_format(line_params[:paper_format])
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end
    
    # Ritorno i line_params per Banner
    def fetch_values_banner(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      
      line_params[:orientation] = Spree::OptionValue.find(params[:order][:orientation])
      line_params[:custom_format] = params[:order][:custom]
      line_params[:paper] = Spree::CoilValue.find(params[:order][:paper])
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = 'front'").first
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      
      line_params[:eyelet] = line_params[:variant].eyelet_accessories.find(params[:order][:eyelet]) rescue false
      line_params[:eyelet_accessory_disposition] = Spree::OptionValue.create(:name => :eyelet_accessory_disposition, :presentation => params[:order][:eyelet_accessory]) if params[:order][:eyelet_accessory]
      line_params[:eyelet_accessory_quantity] = Spree::OptionValue.create(:name => :eyelet_accessory_quantity, :presentation => params[:order][:eyelet_quantity]) if params[:order][:eyelet_quantity]
      #line_params[:buttonhole_accessory_disposition] = Spree::OptionValue.create(:name => :buttonhole_accessory_disposition, :presentation => params[:order][:buttonhole_accessory]) if params[:order][:buttonhole_accessory]
      #line_params[:buttonhole] = line_params[:variant].buttonhole_accessory
      line_params[:pocket_disposition] = Spree::OptionValue.create(:name => :pocket_disposition, :presentation => params[:order][:pocket]) if !params[:order][:pocket].blank?
      line_params[:pocket] = line_params[:variant].pocket if !params[:order][:pocket].blank?
      line_params[:pocket_quantity] = Spree::OptionValue.create(:name => :pocket_quantity, :presentation => params[:order][:pocket_quantity]) if !params[:order][:pocket].blank?
      if params[:order][:reinforcement_perimeter] != "0"
        line_params[:reinforcement_perimeter] = line_params[:variant].reinforcement_perimeter
      else
        line_params[:reinforcement_perimeter] = false
      end
      line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => line_params[:custom_format][:width])
      line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => line_params[:custom_format][:height])
      
      current_item.line_items_option_values.new :option_value => line_params[:custom_width]
      current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      current_item.line_items_option_values.new :option_value => line_params[:orientation]
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:reinforcement_perimeter] if params[:order][:reinforcement_perimeter] != "0"
      current_item.line_items_option_values.new :option_value => line_params[:eyelet] if line_params[:eyelet]
      #current_item.line_items_option_values.new :option_value => line_params[:buttonhole]
      #current_item.line_items_option_values.new :option_value => line_params[:buttonhole_accessory_disposition] if line_params[:buttonhole_accessory_disposition]
      current_item.line_items_option_values.new :option_value => line_params[:pocket_disposition] if line_params[:pocket_disposition]
      current_item.line_items_option_values.new :option_value => line_params[:pocket] if line_params[:pocket_disposition]
      current_item.line_items_option_values.new :option_value => line_params[:pocket_quantity] if line_params[:pocket_disposition]
			current_item.line_items_option_values.new :option_value => line_params[:eyelet_accessory_quantity] if line_params[:eyelet_accessory_disposition]
      current_item.line_items_option_values.new :option_value => line_params[:eyelet_accessory_disposition] if line_params[:eyelet_accessory_disposition]
      
      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params[:area] = line_params[:custom_format][:width].to_f * line_params[:custom_format][:height].to_f
      line_params[:perimeter] = (line_params[:custom_format][:width].to_f * 2) + (line_params[:custom_format][:height].to_f * 2)
      
      line_params = calculate_heat_sealing(line_params)
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end
    
    def fetch_values_pvc(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      
      line_params[:orientation] = Spree::OptionValue.find(params[:order][:orientation])
      line_params[:custom_format] = params[:order][:custom]
      line_params[:paper] = Spree::CoilValue.find(params[:order][:paper])
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = 'front'").first
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      
      line_params[:lamination_type] = Spree::LaminationType.find(params[:order][:lamination]) rescue false
      line_params[:cutting_coil_type] = Spree::CuttingCoilType.find(params[:order][:cutting_coil]) rescue false
      
      line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => line_params[:custom_format][:width])
      line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => line_params[:custom_format][:height])
      
      current_item.line_items_option_values.new :option_value => line_params[:custom_width]
      current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      current_item.line_items_option_values.new :option_value => line_params[:orientation]
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:lamination_type] if line_params[:lamination_type]
      current_item.line_items_option_values.new :option_value => line_params[:cutting_coil_type] if line_params[:cutting_coil_type]
      
      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params[:area] = line_params[:custom_format][:width].to_f * line_params[:custom_format][:height].to_f
      line_params[:perimeter] = (line_params[:custom_format][:width].to_f * 2) + (line_params[:custom_format][:height].to_f * 2)
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end
    
    # Ritorno i line_params per Poster
    def fetch_values_poster(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant.master
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant.master
      
      line_params[:format] = variant.poster_printer_formats(Integer(params[:order][:format]))
      # da aggiungere il formato personalizzato
      #instructions = variant.instructions.select{ |v| v.name == params[:order][:instructions] }.first

      line_params[:paper] = Spree::OptionValue.find(params[:order][:paper])
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = 'front'").first
      #line_params[:print_color] = variant.print_colors.find(params[:order][:print_color])
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])

      # CUSTOM_LOGGER.info "Poster: format: #{line_params[:format]}"
      # CUSTOM_LOGGER.info "Poster: format.name: #{line_params[:format].name}"
      
      if line_params[:format].name == 'custom' && params[:order][:custom]
        # CUSTOM_LOGGER.info "Poster: format.custom: #{params[:order][:custom]}"
        
        line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:order][:custom][:width])
        line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:order][:custom][:height])
        current_item.line_items_option_values.new :option_value => line_params[:custom_width]
        current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      else
        current_item.line_items_option_values.new :option_value => line_params[:format]
      end
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:orientation] = Spree::OptionValue.find_by_name('horizontal') # metto l'orientamento orizzontale fisso
      
      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params
    end
    
    # Ricavo il costo di stampa e finitura
    def fetch_paper_poster(line_params)
      #gr = Integer(get_weight_from_name(line_params[:paper].name))
      gr = line_params[:paper].weight
      if line_params[:format].name == "custom"
        h = {
            :w => (line_params[:custom_width].presentation.to_f / 100.to_f),
            :h => (line_params[:custom_height].presentation.to_f / 100.to_f)
            }
      else
        h = {
            :w => (line_params[:format].width.to_f / 100.to_f),
            :h => (line_params[:format].height.to_f / 100.to_f)
            }
      end
      #quantity = line_item.line_items_option_values.select { |ov| ov.option_value.name == 'quantity' }.first.option_value.presentation
      if line_params[:paper].waste > 0
        if line_params[:paper].waste.to_f >= ((h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i)
          line_params[:total] += line_params[:paper].waste.to_f * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "totale dopo costo della carta con spreco: #{line_params[:total]} *********************************"
        else
          cost_for_one_copy = (Float(h[:w]) * Float(h[:h])) * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
          line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
        end
      else
        cost_for_one_copy = (Float(h[:w]) * Float(h[:h])) * line_params[:paper].price.to_f
        # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
        line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
      end
      # Calcolo il costo di avviamento del plotter
      line_params[:total] += Float(line_params[:variant].product.plotter.plotter_hourly_cost) * (Float(line_params[:variant].product.plotter.start_time_c)/60.to_f)
      
      # Calcolo il costo del plotter
      line_params[:total] += line_params[:print_color].cost_mq.to_f * (Float(h[:w]) * Float(h[:h])) * line_params[:number_of_copy].presentation.to_i
      
      line_params[:weight_line_item] = (Float(h[:w]) * Float(h[:h])) * (gr / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      
      line_params
    end
    
    # Ritorno i line_params per Poster
    def fetch_values_poster_high_quality(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant.master
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant.master
      
      line_params[:format] = variant.poster_high_quality_printer_formats(params[:order][:format].to_i)
      # da aggiungere il formato personalizzato
      #instructions = variant.instructions.select{ |v| v.name == params[:order][:instructions] }.first

      line_params[:paper] = Spree::OptionValue.find(params[:order][:paper])
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = 'front'").first
      #line_params[:print_color] = variant.print_colors.find(params[:order][:print_color])
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])

      # CUSTOM_LOGGER.info "Poster: format.name: #{line_params[:format]}"
      # CUSTOM_LOGGER.info "Poster: format.name: #{line_params[:format].name}"
      if line_params[:format].name == 'custom' && params[:order][:custom]
        line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:order][:custom][:width])
        line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:order][:custom][:height])
        current_item.line_items_option_values.new :option_value => line_params[:custom_width]
        current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      else
        current_item.line_items_option_values.new :option_value => line_params[:format]
      end
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:orientation] = Spree::OptionValue.find_by_name('horizontal') # metto l'orientamento orizzontale fisso
      
      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params
    end
    
    # Ricavo il costo di stampa e finitura (Occhielli e Asole)
    def fetch_paper_banner(line_params)
      # CUSTOM_LOGGER.info "SONO IN fetch_paper_banner *********************************"
      #gr = Integer(get_weight_from_name(line_params[:paper].name))
      gr = line_params[:paper].weight
      # CUSTOM_LOGGER.info "gr: #{gr} *********************************"
      if line_params[:heat_sealing_count] > 1
        # CUSTOM_LOGGER.info "heat_sealing_count: #{line_params[:heat_sealing_count]} *********************************"
        cost_for_one_copy = (Float(line_params[:copy_heat_sealing]) / 10000.to_f) * Float(line_params[:paper].price)
        # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
        line_params[:total] += cost_for_one_copy * Integer(line_params[:number_of_copy].presentation)
        # CUSTOM_LOGGER.info "totale dopo costo della carta: #{line_params[:total]} *********************************"
      else
        if line_params[:paper].waste.to_f > 0
          if line_params[:paper].waste.to_f >= ((line_params[:area] / 10000.to_f) * line_params[:number_of_copy].presentation.to_i)
            line_params[:total] += line_params[:paper].waste.to_f * line_params[:paper].price.to_f
            # CUSTOM_LOGGER.info "totale dopo costo della carta con spreco: #{line_params[:total]} *********************************"
          else
            cost_for_one_copy = (line_params[:area] / 10000.to_f) * line_params[:paper].price.to_f
            # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
            line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
          end
        else
          cost_for_one_copy = (line_params[:area] / 10000.to_f) * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
          line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
        end
        # CUSTOM_LOGGER.info "totale dopo costo della carta: #{line_params[:total]} *********************************"
      end
      # Calcolo il costo di avviamento del plotter
      line_params[:total] += Float(line_params[:variant].plotter.plotter_hourly_cost) * (Float(line_params[:variant].plotter.start_time_c)/60.to_f)
      # CUSTOM_LOGGER.info "totale dopo costo avviamento plotter: #{line_params[:total]} *********************************"

      # Calcolo il costo del plotter
      line_params[:total] += line_params[:print_color].cost_mq.to_f * (line_params[:area] / 10000.to_f) * line_params[:number_of_copy].presentation.to_i
      # CUSTOM_LOGGER.info "totale dopo costo stampa plotter: #{line_params[:total]} *********************************"
      
      # calcolo il costo degli occhielli
      if line_params[:eyelet]
        line_params[:total] += (line_params[:eyelet_accessory_quantity].presentation.to_i * line_params[:eyelet].eyelet.copy_cost.to_f) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo occhielli: #{line_params[:total]} *********************************"
      end
      
      # calcolo il costo dell'asola
      #if line_params[:buttonhole_accessory_disposition] && line_params[:buttonhole_accessory_disposition].presentation != ""
      #  disposition = get_disposition_perimeter(line_params[:buttonhole_accessory_disposition].presentation, line_params)
      #  # CUSTOM_LOGGER.info "disposition: #{disposition} *********************************"
      #  line_params[:total] += ((disposition/line_params[:buttonhole].available_meter.to_f).ceil * line_params[:buttonhole].buttonhole.copy_cost.to_f) * line_params[:number_of_copy].presentation.to_i
      #  # CUSTOM_LOGGER.info "totale dopo costo asole: #{line_params[:total]} *********************************"
      #end
      
      # calcolo il costo del rinforzo perimetrale
      if line_params[:reinforcement_perimeter]
        line_params[:total] += (line_params[:reinforcement_perimeter].available_meter * (line_params[:perimeter] / 100.to_f)) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo rinforzo perimetrale: #{line_params[:total]} *********************************"
      end
      
      # calcolo il costo della tasca se non esiste il rinforzo perimetrale
      if !line_params[:reinforcement_perimeter] && line_params[:pocket_disposition] && line_params[:pocket].presentation != ""
        disposition = get_disposition_perimeter(line_params[:pocket_disposition].presentation, line_params)
        # CUSTOM_LOGGER.info "disposition pocket: #{disposition} *********************************"
        line_params[:total] += ((disposition / 100.to_f) * line_params[:pocket].available_meter.to_f) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo tasca: #{line_params[:total]} *********************************"
      end
      
      # Recupero il peso delle copie
      line_params[:weight_line_item] = ((line_params[:area] / 10000.to_f) * (gr / 1000.to_f)) * Integer(line_params[:number_of_copy].presentation.to_i)
      # CUSTOM_LOGGER.info "weight_line_item: #{line_params[:weight_line_item]} *********************************"
      
      line_params
    end
    
    # Ricavo il costo di stampa e finitura (Laminatura e Sagomatura)
    def fetch_paper_pvc(line_params)
      # CUSTOM_LOGGER.info "SONO IN fetch_paper_pvc *********************************"
      #gr = Integer(get_weight_from_name(line_params[:paper].name))
      gr = line_params[:paper].weight
      # CUSTOM_LOGGER.info "gr: #{gr} *********************************"
      
      if line_params[:paper].waste.to_f > 0
        if line_params[:paper].waste.to_f >= ((line_params[:area] / 10000.to_f) * line_params[:number_of_copy].presentation.to_i)
          line_params[:total] += line_params[:paper].waste.to_f * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "totale dopo costo della carta con spreco: #{line_params[:total]} *********************************"
        else
          cost_for_one_copy = (line_params[:area] / 10000.to_f) * line_params[:paper].price.to_f
          line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
        end
      else
        cost_for_one_copy = (line_params[:area] / 10000.to_f) * line_params[:paper].price.to_f
        line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
      end
      
      # Calcolo il costo di avviamento del plotter
      line_params[:total] += Float(line_params[:variant].plotter.plotter_hourly_cost) * (Float(line_params[:variant].plotter.start_time_c)/60.to_f)
      # CUSTOM_LOGGER.info "totale dopo costo avviamento plotter: #{line_params[:total]} *********************************"

      # Calcolo il costo del plotter
      line_params[:total] += line_params[:print_color].cost_mq.to_f * (line_params[:area] / 10000.to_f) * line_params[:number_of_copy].presentation.to_i
      # CUSTOM_LOGGER.info "totale dopo costo stampa plotter: #{line_params[:total]} *********************************"
      
      # calcolo il costo della laminatura
      if line_params[:lamination_type]
        line_params[:total] += (line_params[:lamination_type].lamination.hourly_cost.to_f * (line_params[:lamination_type].lamination.start_time.to_f/60.to_f))
        line_params[:total] += (line_params[:lamination_type].available_meter.to_f * (line_params[:area] / 10000.to_f)) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo laminatura: #{line_params[:total]} *********************************"
      end
      # calcolo il costo del taglio in bobina
      if line_params[:cutting_coil_type]
        line_params[:total] += (line_params[:cutting_coil_type].hourly_cost.to_f * (line_params[:cutting_coil_type].start_time.to_f/60.to_f))
        line_params[:total] += (line_params[:cutting_coil_type].available_meter.to_f * (line_params[:perimeter] / 100.to_f)) * line_params[:number_of_copy].presentation.to_i
				line_params[:total] += line_params[:cutting_coil_type].plant.to_f
        # CUSTOM_LOGGER.info "totale dopo costo sagomatura: #{line_params[:total]} *********************************"
      end
      
      # Recupero il peso delle copie
      line_params[:weight_line_item] = ((line_params[:area] / 10000.to_f) * (gr / 1000.to_f)) * Integer(line_params[:number_of_copy].presentation.to_i)
      # CUSTOM_LOGGER.info "weight_line_item: #{line_params[:weight_line_item]} *********************************"
      
      line_params
    end
    
    # Ritorno i line_params per Canvas
    def fetch_values_canvas(params, variant, quantity)
      line_params = Hash.new()
      line_params[:variant] = variant
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      
      line_params[:paper] = Spree::CoilValue.find(params[:order][:paper])
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
      line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = 'front'").first
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      
      line_params[:frame] = Spree::Frame.find(params[:order][:frame]) if params[:order][:frame] && (params[:order][:frame] != "")
      line_params[:hanger_accessory] = line_params[:variant].hanger_accessories.find(params[:order][:hanger_accessory]) if params[:order][:hanger_accessory] && (params[:order][:hanger_accessory] != "")
      
      if params[:order][:custom]
        line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:order][:custom][:width])
        line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:order][:custom][:height])
      else
        line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:order][:format][:width])
        line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:order][:format][:height])
      end
      
      current_item.line_items_option_values.new :option_value => line_params[:custom_width]
      current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      current_item.line_items_option_values.new :option_value => line_params[:frame] if params[:order][:frame] && (params[:order][:frame] != "")
      current_item.line_items_option_values.new :option_value => line_params[:hanger_accessory] if params[:order][:hanger_accessory] && (params[:order][:hanger_accessory] != "")
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:orientation] = Spree::OptionValue.find_by_name('horizontal') # metto l'orientamento orizzontale fisso
      
      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params[:area] = (line_params[:custom_width].presentation.to_f + 10.to_f) * (line_params[:custom_height].presentation.to_f + 10.to_f)
      line_params[:perimeter] = (line_params[:custom_width].presentation.to_f * 2) + (line_params[:custom_height].presentation.to_f * 2)
      line_params[:base] = line_params[:custom_width].presentation.to_f
      # CUSTOM_LOGGER.info "perimeter: #{line_params[:perimeter]} *********************************"
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end
    
    # Ricavo il costo di stampa e finitura (Telaio e Appendino)
    def fetch_paper_canvas(line_params)
      # CUSTOM_LOGGER.info "SONO IN fetch_paper_canvas *********************************"
      #gr = Integer(get_weight_from_name(line_params[:paper].name))
      gr = line_params[:paper].weight
      # CUSTOM_LOGGER.info "gr: #{gr} *********************************"
      
      if line_params[:paper].waste.to_f > 0
        # CUSTOM_LOGGER.info "area per spreco: #{(line_params[:area] / 10000.to_f)} *********************************"
        if line_params[:paper].waste.to_f >= ((line_params[:area] / 10000.to_f) * line_params[:number_of_copy].presentation.to_i)
          line_params[:total] += line_params[:paper].waste.to_f * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "totale dopo costo della carta con spreco: #{line_params[:total]} *********************************"
        else
          cost_for_one_copy = (line_params[:area] / 10000.to_f) * line_params[:paper].price.to_f
          line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
        end
      else
        cost_for_one_copy = (line_params[:area] / 10000.to_f) * line_params[:paper].price.to_f
        line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
      end
      
      # Calcolo il costo di avviamento del plotter
      line_params[:total] += Float(line_params[:variant].plotter.plotter_hourly_cost) * (Float(line_params[:variant].plotter.start_time_c)/60.to_f)
      # CUSTOM_LOGGER.info "totale dopo costo avviamento plotter: #{line_params[:total]} *********************************"

      # Calcolo il costo del plotter
      line_params[:total] += line_params[:print_color].cost_mq.to_f * (line_params[:area] / 10000.to_f) * line_params[:number_of_copy].presentation.to_i
      # CUSTOM_LOGGER.info "totale dopo costo stampa plotter: #{line_params[:total]} *********************************"
              
      # calcolo il costo del telaio
      if line_params[:frame]
        line_params[:total] += ((line_params[:perimeter].to_i / 100.to_f) * line_params[:frame].copy_cost.to_f) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo telaio: #{line_params[:total]} *********************************"
      end
      # calcolo il costo degli appendini
      if line_params[:hanger_accessory]
        # CUSTOM_LOGGER.info "base: #{line_params[:base]} *********************************"
        # CUSTOM_LOGGER.info "hanger_accessory.available_meter: #{line_params[:hanger_accessory].available_meter.to_f} *********************************"
        # CUSTOM_LOGGER.info "hanger_accessory.hanger.copy_cost: #{line_params[:hanger_accessory].hanger.copy_cost} *********************************"
        line_params[:total] += ((line_params[:base].to_f/line_params[:hanger_accessory].available_meter.to_f).to_i * line_params[:hanger_accessory].hanger.copy_cost.to_f) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo appendini: #{line_params[:total]} *********************************"
      end
      
      # Recupero il peso delle copie
     line_params[:weight_line_item] = ((line_params[:area] / 10000.to_f) * (gr / 1000.to_f)) * line_params[:number_of_copy].presentation.to_i
     # CUSTOM_LOGGER.info "weight_line_item: #{line_params[:weight_line_item]} *********************************"
      
      line_params
    end
    
    # Ritorno i line_params per il rigido
    def fetch_values_rigid(params, variant, quantity)
      line_params = Hash.new()
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      
      line_params[:format] = variant.rigid_printer_formats(params[:order][:format].to_i)
      line_params[:paper] = Spree::RigidSupport.find(params[:order][:paper])
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      
      line_params[:variant] = variant.find_rigids_variants(line_params[:paper], line_params[:print_color])
      
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      
      if line_params[:format].sided && params[:order][:sided] == "si"
        line_params[:front_back] = Spree::OptionValue.create(:name => "sided", :presentation => params[:order][:sided])
      end
      
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      
      line_params[:lamination_type] = Spree::LaminationType.find(params[:order][:lamination]) rescue false
      if line_params[:format].cutting_coil
        line_params[:cutting_coil_type] = Spree::CuttingCoilType.find(params[:order][:cutting_coil]) rescue false
      end
      
      if line_params[:format].eyelet
        #line_params[:eyelet] = line_params[:variant].eyelet_accessories.find(params[:order][:eyelet]) rescue false
        line_params[:eyelet] = Spree::OptionValue.find(params[:order][:eyelet]) rescue false
        line_params[:eyelet_accessory_disposition] = Spree::OptionValue.create(:name => :eyelet_accessory_disposition, :presentation => params[:order][:eyelet_accessory]) if params[:order][:eyelet_accessory]
        line_params[:eyelet_accessory_quantity] = Spree::OptionValue.create(:name => :eyelet_accessory_quantity, :presentation => params[:order][:eyelet_quantity]) if params[:order][:eyelet_quantity]
      end
      
      # CUSTOM_LOGGER.info "Rigid: format.name: #{line_params[:format]}"
      # CUSTOM_LOGGER.info "Rigid: format.name: #{line_params[:format].name}"
      if line_params[:format].name == 'custom' && params[:order][:custom]
        line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:order][:custom][:width])
        line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:order][:custom][:height])
        current_item.line_items_option_values.new :option_value => line_params[:custom_width]
        current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      else
        current_item.line_items_option_values.new :option_value => line_params[:format]
      end
      
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:weight]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:sided] if line_params[:sided]
      current_item.line_items_option_values.new :option_value => line_params[:front_back] if line_params[:front_back]
      current_item.line_items_option_values.new :option_value => line_params[:lamination_type] if line_params[:lamination_type]
      current_item.line_items_option_values.new :option_value => line_params[:cutting_coil_type] if line_params[:cutting_coil_type]
      current_item.line_items_option_values.new :option_value => line_params[:eyelet] if line_params[:eyelet]
      current_item.line_items_option_values.new :option_value => line_params[:eyelet_accessory_disposition] if line_params[:eyelet_accessory_disposition]
      current_item.line_items_option_values.new :option_value => line_params[:eyelet_accessory_quantity] if line_params[:eyelet_accessory_quantity]
      
      # abbino il current_item ad un nuovo line_items
      current_item.variant = line_params[:variant]
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end
    
    def fetch_paper_rigid(line_params)
      #gr = Integer(get_weight_from_name(line_params[:paper].name))
      gr = line_params[:paper].weight.to_f
      if line_params[:format].name == "custom"
        h = {
            :w => (line_params[:custom_width].presentation.to_f / 100.to_f),
            :h => (line_params[:custom_height].presentation.to_f / 100.to_f)
            }
      else
        h = {
            :w => (line_params[:format].width.to_f / 100.to_f),
            :h => (line_params[:format].height.to_f / 100.to_f)
            }
      end
      #quantity = line_item.line_items_option_values.select { |ov| ov.option_value.name == 'quantity' }.first.option_value.presentation
      if line_params[:paper].waste > 0
        if line_params[:paper].waste.to_f >= ((h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i)
          line_params[:total] += line_params[:paper].waste.to_f * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "totale dopo costo della carta con spreco: #{line_params[:total]} *********************************"
        else
          cost_for_one_copy = (h[:w].to_f * h[:h].to_f) * line_params[:paper].price.to_f
          #if line_params[:front_back]
          #  cost_for_one_copy = cost_for_one_copy * 2
          #end
          # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
          line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
        end
      else
        cost_for_one_copy = (h[:w].to_f * h[:h].to_f) * line_params[:paper].price.to_f
        #if line_params[:front_back]
        #  cost_for_one_copy = cost_for_one_copy * 2
        #end
        # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
        line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
      end
      # Calcolo il costo di avviamento del plotter
      line_params[:total] += line_params[:print_color].plotter.plotter_hourly_cost.to_f * (line_params[:print_color].plotter.start_time_c.to_f/60.to_f)
      # Calcolo il costo del plotter
      line_params[:total] += line_params[:print_color].cost_mq.to_f * (h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i
			line_params[:total] += line_params[:print_color].plant.to_f

      if line_params[:front_back]
        # Calcolo il costo di avviamento del plotter per il retro
        line_params[:total] += line_params[:print_color].plotter.plotter_hourly_cost.to_f * (line_params[:print_color].plotter.start_time_c.to_f/60.to_f)
        # Calcolo il costo del plotter per il retro
        line_params[:total] += line_params[:print_color].cost_mq.to_f * (h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i
      end
      
      # calcolo il costo della laminatura
      if line_params[:lamination_type]
        start_lamination = (line_params[:lamination_type].lamination.hourly_cost.to_f * (line_params[:lamination_type].lamination.start_time.to_f/60.to_f))
        cost_lamination = (line_params[:lamination_type].available_meter.to_f * (h[:w].to_f * h[:h].to_f)) * line_params[:number_of_copy].presentation.to_i
        if line_params[:front_back]
          start_lamination += start_lamination
          cost_lamination += cost_lamination
        end
        line_params[:total] += start_lamination + cost_lamination
        # CUSTOM_LOGGER.info "totale dopo costo laminatura: #{line_params[:total]} *********************************"
      end
      
      # calcolo il costo del taglio in bobina
      if line_params[:cutting_coil_type]
        line_params[:total] += (line_params[:cutting_coil_type].hourly_cost.to_f * (line_params[:cutting_coil_type].start_time.to_f/60.to_f))
        line_params[:total] += (line_params[:cutting_coil_type].available_meter.to_f * ((h[:w].to_f * 2) + (h[:h].to_f * 2))) * line_params[:number_of_copy].presentation.to_i
				line_params[:total] += line_params[:cutting_coil_type].plant.to_f
        # CUSTOM_LOGGER.info "totale dopo costo sagomatura: #{line_params[:total]} *********************************"
      end
      
      # calcolo il costo degli occhielli
      if line_params[:eyelet]
        #line_params[:total] += (line_params[:eyelet_accessory_quantity].presentation.to_i * line_params[:eyelet].eyelet.copy_cost.to_f) * line_params[:number_of_copy].presentation.to_i
        line_params[:total] += (line_params[:eyelet_accessory_quantity].presentation.to_i * line_params[:eyelet].copy_cost.to_f) * line_params[:number_of_copy].presentation.to_i
        # CUSTOM_LOGGER.info "totale dopo costo occhielli: #{line_params[:total]} *********************************"
      end
      
      line_params[:weight_line_item] = (h[:w].to_f * h[:h].to_f) * (gr / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      
      line_params
    end
    
     # Ritorno i line_params per le Etichette
    def fetch_values_sticker(params, variant, quantity)
      line_params = Hash.new()
      # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
      current_item = Spree::LineItem.new(:quantity => quantity)
      
      line_params[:format] = variant.sticker_printer_formats.find(params[:order][:format].to_i)
      line_params[:paper] = Spree::CoilValue.find(params[:order][:paper])
      line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
      line_params[:variant] = variant.find_stickers_variants(line_params[:paper], line_params[:print_color])
      line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      
      #controllo che i processing plotter da eleborare siano validi
      if params[:order][:processing_plotter_combination]
        line_params[:processing_plotter_type] = []
        #tolgo le processing plotter combiantion NON selezionate
        processing_plotter_combination_passate = params[:order][:processing_plotter_combination].delete_if { |k, v| v.empty? }
        #controllo che il processing plotter selezionato sia incluso tra le opzioni della processing plotter combination associata
        processing_plotter_combination_passate.each do |ppcpkey, ppcpvalue|
          processing_plotter_valide = {}
          line_params[:variant].processing_plotter_combinations.each do |p|
              processing_plotter_valide.merge!(ppcpkey => ppcpvalue) if ppcpkey.to_i == p.id
          end
          
          processing_plotter_valide.each do |ppvkey, ppvvalue|
            line_params[:processing_plotter_type] << Spree::ProcessingPlotterType.find_by_id(ppcpvalue) if ppvvalue.to_i == ppcpvalue.to_i
          end
        end   
      end

      if line_params[:format].cutting_coil
        line_params[:cutting_coil_type] = Spree::CuttingCoilType.find(params[:order][:cutting_coil]) rescue false
      end
      
      if line_params[:format].name == 'custom' && params[:order][:custom]
        line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:order][:custom][:width])
        line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:order][:custom][:height])
        current_item.line_items_option_values.new :option_value => line_params[:custom_width]
        current_item.line_items_option_values.new :option_value => line_params[:custom_height]
      else
        current_item.line_items_option_values.new :option_value => line_params[:format]
      end
      
      current_item.line_items_option_values.new :option_value => line_params[:paper]
      current_item.line_items_option_values.new :option_value => line_params[:weight]
      current_item.line_items_option_values.new :option_value => line_params[:print_color]
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
      current_item.line_items_option_values.new :option_value => line_params[:cutting_coil_type] if line_params[:cutting_coil_type]
      #current_item.line_items_option_values.new :option_value => line_params[:processing_plotter_type] if line_params[:processing_plotter_type]
      if variant.finishing == "coil"
        current_item.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => "number_of_stickers", :presentation => params[:order][:number_of_stickers])
        current_item.line_items_option_values.new :option_value => Spree::OptionValue.find(params[:order][:orientation_coil])
      end
      
      if line_params[:processing_plotter_type]
        line_params[:processing_plotter_type].each do |st|
          current_item.line_items_option_values.new :option_value => st
        end
      end

      # abbino il current_item ad un nuovo line_items
      current_item.variant = line_params[:variant]
      self.line_items << current_item
      line_params[:current_item] = current_item
      
      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end

    def fetch_paper_sticker(line_params)
      #gr = Integer(get_weight_from_name(line_params[:paper].name))
      gr = line_params[:paper].weight.to_f
      if line_params[:format].name == "custom"
        h = {
            :w => (line_params[:custom_width].presentation.to_f / 100.to_f),
            :h => (line_params[:custom_height].presentation.to_f / 100.to_f)
            }
      else
        h = {
            :w => (line_params[:format].width.to_f / 100.to_f),
            :h => (line_params[:format].height.to_f / 100.to_f)
            }
      end
      #quantity = line_item.line_items_option_values.select { |ov| ov.option_value.name == 'quantity' }.first.option_value.presentation
      if line_params[:paper].waste > 0
        if line_params[:paper].waste.to_f >= ((h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i)
          line_params[:total] += line_params[:paper].waste.to_f * line_params[:paper].price.to_f
          # CUSTOM_LOGGER.info "totale dopo costo della carta con spreco: #{line_params[:total]} *********************************"
        else
          cost_for_one_copy = (h[:w].to_f * h[:h].to_f) * line_params[:paper].price.to_f
          if line_params[:front_back]
            cost_for_one_copy = cost_for_one_copy * 2
          end
          # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
          line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
        end
      else
        cost_for_one_copy = (h[:w].to_f * h[:h].to_f) * line_params[:paper].price.to_f
        if line_params[:front_back]
          cost_for_one_copy = cost_for_one_copy * 2
        end
        # CUSTOM_LOGGER.info "cost_for_one_copy: #{cost_for_one_copy} *********************************"
        line_params[:total] += cost_for_one_copy * line_params[:number_of_copy].presentation.to_i
      end
      # Calcolo il costo di avviamento del plotter
      line_params[:total] += line_params[:print_color].plotter.plotter_hourly_cost.to_f * (line_params[:print_color].plotter.start_time_c.to_f/60.to_f)
      # Calcolo il costo del plotter
      line_params[:total] += line_params[:print_color].cost_mq.to_f * (h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i
			# line_params[:total] += line_params[:print_color].plant.to_f
      
      if line_params[:variant].product.finishing == "coil"
        line_params[:total] += line_params[:print_color].plant.to_f
      end
      
      if line_params[:front_back]
        # Calcolo il costo di avviamento del plotter per il retro
        line_params[:total] += line_params[:print_color].plotter.plotter_hourly_cost.to_f * (line_params[:print_color].plotter.start_time_c.to_f/60.to_f)
        # Calcolo il costo del plotter per il retro
        line_params[:total] += line_params[:print_color].cost_mq.to_f * (h[:w].to_f * h[:h].to_f) * line_params[:number_of_copy].presentation.to_i
      end
      
      # calcolo il costo delle lavorazioni plotter
      if line_params[:processing_plotter_type]
        line_params[:processing_plotter_type].each do |st|
          # CUSTOM_LOGGER.info "totale prima costo lavorazione plotter: #{line_params[:total]}----[][][][][][]"
          start_processing_plotter = (st.hourly_cost.to_f * (st.start_time.to_f/60.to_f))
          cost_processing_plotter = (st.available_meter.to_f * (h[:w].to_f * h[:h].to_f)) * line_params[:number_of_copy].presentation.to_i
          line_params[:total] += start_processing_plotter + cost_processing_plotter
          if line_params[:variant].product.finishing == "coil"
            line_params[:total] += st.plant.to_f
          end
          # CUSTOM_LOGGER.info "totale dopo costo lavorazione plotter: #{line_params[:total]} *********************************"
        end
      end
      # calcolo il costo del taglio in bobina
      if line_params[:cutting_coil_type]
        # CUSTOM_LOGGER.info "[][][][][][][][][]--------------#{line_params[:cutting_coil_type]}"
        line_params[:total] += (line_params[:cutting_coil_type].hourly_cost.to_f * (line_params[:cutting_coil_type].start_time.to_f/60.to_f))
        line_params[:total] += (line_params[:cutting_coil_type].available_meter.to_f * ((h[:w].to_f * 2) + (h[:h].to_f * 2))) * line_params[:number_of_copy].presentation.to_i
        if line_params[:variant].product.finishing == "coil" && line_params[:format].name == "custom"
          line_params[:total] += line_params[:cutting_coil_type].plant.to_f
        end
        # CUSTOM_LOGGER.info "totale dopo costo taglio in bobina: #{line_params[:total]} *********************************"
      end
      line_params[:weight_line_item] = (h[:w].to_f * h[:h].to_f) * (gr / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      CUSTOM_LOGGER.info "---------------- WEIGHT LINE ITEM STICKER ----------------"
			CUSTOM_LOGGER.info "h[:w].to_f: #{h[:w].to_f}"
      CUSTOM_LOGGER.info "h[:h].to_f: #{h[:h].to_f}"
      CUSTOM_LOGGER.info "gr: #{gr}"
      CUSTOM_LOGGER.info "line_params[:number_of_copy].presentation: #{line_params[:number_of_copy].presentation.to_i}"
      CUSTOM_LOGGER.info "line_params[:weight_line_item]: #{line_params[:weight_line_item].to_f}"
      
      line_params
    end

    # ricavo il tipo di carta a seconda della stampante selezionata
    # Digitale  =>  Risma e nel caso Tagliato
    # Offset  =>  Solo Tagliato
    def fetch_paper(line_params, front_back = false)
      # CUSTOM_LOGGER.info "SONO IN fetch_paper *********************************"
      if line_params[:printer].digital?
        # CUSTOM_LOGGER.info "SONO IN DIGITALE *********************************"
        if line_params[:weight].ream?
          # CUSTOM_LOGGER.info "HO LA CARTA IN RISMA *********************************"
          line_params = get_paper_ream(line_params)
          line_params = get_printer_ream(line_params, front_back)
        else
          # CUSTOM_LOGGER.info "HO LA CARTA IN TAGLIATO *********************************"
          line_params = get_paper_cut(line_params)
          line_params = get_printer_cut(line_params, front_back)
        end
        if line_params[:printer].min_limit_waiting.blank?
          if line_params[:number_of_sheet] < line_params[:printer].min_limit_waiting.to_i
            line_params[:total] += line_params[:printer].cost_waiting.to_f
            # CUSTOM_LOGGER.info "totale dopo costo di attesa: #{line_params[:total]} *********************************"
          end
        end
      else
        # CUSTOM_LOGGER.info "SONO IN OFFSET *********************************"
        line_params = get_paper_cut(line_params)
        line_params = get_printer_cut(line_params, front_back)
      end
      
      # Aggiungo il prezzo per la plastificazione se presente
      line_params[:total] += plasticization_price(line_params) if line_params[:plastification]
      # CUSTOM_LOGGER.info "totale dopo la plastificazione: #{line_params[:total]} *********************************"

      # Aggiungo il costo di avviamento del tagliato
      line_params[:total] += (line_params[:variant].product.cut.hourly_cost * line_params[:variant].product.cut.start_time)/60
      # CUSTOM_LOGGER.info "totale dopo costo avviamento tagliato: #{line_params[:total]} *********************************"
      # Aggiungo il costo del tempo di taglio
      line_params[:total] += get_cutting_time(line_params)
      # CUSTOM_LOGGER.info "totale dopo costo del tagliato: #{line_params[:total]} *********************************"

      line_params
    end
    
    # Ricavo il costo e tutti i valori necessari per la carta in formato TAGLIATO
    def get_paper_cut(line_params)
      # CUSTOM_LOGGER.info "SONO IN get_paper_cut *********************************"
      # Calcolo il costo della carta
      line_params[:price_kg] = Float(line_params[:weight].cost_cut)
      # CUSTOM_LOGGER.info "price_kg: #{line_params[:price_kg]} *********************************"
      # Calcolo il prezzo per foglio è dato da:
      # supponendo di avere:
      #   - 64x88 cm
      #   - 150 gr
      #   - 0,90 €/kg
      # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
      line_params[:price_per_sheet] = (line_params[:paper_init_format][:master][0] / 100.to_f) * (line_params[:paper_init_format][:master][1] / 100.to_f) * (line_params[:price_kg] * (line_params[:weight].name.to_f / 1000.to_f))
      # CUSTOM_LOGGER.info "price_per_sheet: #{line_params[:price_per_sheet]} *********************************"
      # Ottengo i numero di fogli
      line_params[:number_of_sheet] = (Float(line_params[:number_of_copy].presentation) / Float(line_params[:pose].name).to_f).ceil
      # CUSTOM_LOGGER.info "number_of_sheet: #{line_params[:number_of_sheet]} *********************************"
      # Divido il numero di fogli per la quantità di fogli che ci stanno sul formato più grande
      line_params[:total] += (line_params[:price_per_sheet] * (line_params[:number_of_sheet]/line_params[:paper_init_format][:quantity].to_f).ceil)
      # CUSTOM_LOGGER.info "totale dopo il costo della carta: #{line_params[:total]} *********************************"
      # aggiungo il costo della passata
      line_params[:total] += line_params[:number_of_sheet] * line_params[:price_front_back]
      # CUSTOM_LOGGER.info "totale dopo il costo della passata: #{line_params[:total]} *********************************"

      return line_params
    end
    
    # Ricavo il costo e i valori necessari per la carta in formato RISMA
    def get_paper_ream(line_params)
      # CUSTOM_LOGGER.info "SONO IN get_paper_ream *********************************"
      # Non è formato tagliato quindi si calcola in risma
      line_params[:price_ream] = Float(line_params[:weight].cost_ream)
      # CUSTOM_LOGGER.info "price_ream: #{line_params[:price_ream]} *********************************"
      # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
      line_params[:price_per_sheet] = line_params[:price_ream] / 1000.to_f
      # CUSTOM_LOGGER.info "price_per_sheet: #{line_params[:price_per_sheet]} *********************************"
      # Ottengo i numeri di fogli
      line_params[:number_of_sheet] = (Float(line_params[:number_of_copy].presentation) / Float(line_params[:pose].name).to_f).ceil
      # CUSTOM_LOGGER.info "number_of_sheet: #{line_params[:number_of_sheet]} *********************************"
      # inizio a calcolare il totale
      line_params[:total] += line_params[:price_per_sheet] * line_params[:number_of_sheet]
      # CUSTOM_LOGGER.info "totale dopo il costo della carta: #{line_params[:total]} *********************************"
      # aggiungo il costo della carta
      line_params[:total] += line_params[:number_of_sheet] * line_params[:price_front_back]
      # CUSTOM_LOGGER.info "totale dopo il costo della passata: #{line_params[:total]} *********************************"

      return line_params
    end
    
    # Ricavo il costo per la stampante se TAGLIATO
    def get_printer_cut(line_params, front_back)
      # CUSTOM_LOGGER.info "SONO IN get_printer_cut *********************************"
      if line_params[:printer].color_4?
        # CUSTOM_LOGGER.info "SONO IN 4 COLORI *********************************"
        #Aggiungo avviamento se presente
        if line_params[:print_color].name == "true" && line_params[:printer].plant_color_c
          line_params[:total] += Float(line_params[:printer].plant_color_c)
          # CUSTOM_LOGGER.info "totale dopo costo a colori: #{line_params[:total]} *********************************"
        elsif line_params[:printer].plant_color_bw
          line_params[:total] += Float(line_params[:printer].plant_color_bw)
          # CUSTOM_LOGGER.info "totale dopo costo in biano e nero: #{line_params[:total]} *********************************"
        end
        # aggiungo costo per il tempo di attesa
        if line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back"
          if line_params[:number_of_sheet] < line_params[:printer].min_limit_waiting.to_i
            line_params[:total] += line_params[:printer].cost_waiting.to_f
            # CUSTOM_LOGGER.info "totale dopo costo di attesa: #{line_params[:total]} *********************************"
          end
        end
      else
        # CUSTOM_LOGGER.info "SONO IN 8 COLORI *********************************"
        #Aggiungo avviamento se presente
        if line_params[:print_color].name == "true" && line_params[:printer].plant_color_c
          if line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back"
            line_params[:total] += (Float(line_params[:printer].plant_color_c) * 2)
            # CUSTOM_LOGGER.info "totale dopo costo a colori per impianto: #{line_params[:total]} *********************************"
          else
            line_params[:total] += Float(line_params[:printer].plant_color_c)
          end
        elsif line_params[:printer].plant_color_bw
          if line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back"
            line_params[:total] += (Float(line_params[:printer].plant_color_bw) * 2)
            # CUSTOM_LOGGER.info "totale dopo costo in bianco e nero per impianto: #{line_params[:total]} *********************************"
          else
            line_params[:total] += Float(line_params[:printer].plant_color_bw)
            # CUSTOM_LOGGER.info "totale dopo costo in bianco e nero per impianto: #{line_params[:total]} *********************************"
          end
        end
      end
      
      # Devo moltiplicare per due il numero di fogli nel caso siano fronte e retro
      if (line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back")
        molt = 2
      else
        molt = 1
      end
      
      # Aggiungo costo di avviamento
      # Calcolandolo nel seguent modo:
      # (costo_orario * tempo_avviamento)/60
      # Calcolo il costo di finitura: che è dato dai seguenti valori:
      #   - printer_hourly_cost
      #   - average_hourly_print (colori o bw)
      #   - number_of_sheet
      #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      if line_params[:print_color].name == "true"
        line_params[:total] += Float(line_params[:printer].printer_hourly_cost_c) * Float(line_params[:printer].start_time_c)/60.to_f
        if line_params[:printer].average_hourly_print_c > 0
          line_params[:total] += Float(line_params[:printer].printer_hourly_cost_c) * ((line_params[:number_of_sheet] * molt) / Float(line_params[:printer].average_hourly_print_c))
        end
        # CUSTOM_LOGGER.info "totale dopo costo di avviamento a colori: #{line_params[:total]} *********************************"
        # calcolo il costo dello spreco di fogli
        line_params[:total] += line_params[:printer].start_up_waste_paper_cost((line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back") ? true : false, true, line_params[:price_per_sheet])
        # CUSTOM_LOGGER.info "totale dopo costo per lo spreco: #{line_params[:total]} *********************************"
      else
        line_params[:total] += Float(line_params[:printer].printer_hourly_cost_bw) * Float(line_params[:printer].start_time_bw)/60.to_f
        if line_params[:printer].average_hourly_print_bw > 0
          line_params[:total] += Float(line_params[:printer].printer_hourly_cost_bw) * ((line_params[:number_of_sheet] * molt) / Float(line_params[:printer].average_hourly_print_bw))
        end
        # CUSTOM_LOGGER.info "totale dopo costo di avviamento in bianco e nero: #{line_params[:total]} *********************************"
        # calcolo il costo dello spreco di fogli
        line_params[:total] += line_params[:printer].start_up_waste_paper_cost((line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back") ? true : false, false, line_params[:price_per_sheet])
        # CUSTOM_LOGGER.info "totale dopo costo per lo spreco: #{line_params[:total]} *********************************"
      end
      # NB: il peso è in grammi quindi lo devo trasformare in kg
      if line_params[:variant].product.is_a?(Spree::BusinessCardProduct)
        line_params[:weight_line_item] = (line_params[:format].base.to_f / 100.to_f) * (line_params[:format].height.to_f / 100.to_f) * (line_params[:weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      else
        line_params[:weight_line_item] = (line_params[:variant].width.to_f / 100.to_f) * (line_params[:variant].height.to_f / 100.to_f) * (line_params[:weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      end
      #line_params[:weight_line_item] = (line_params[:number_of_sheet]/line_params[:paper_init_format][:quantity].to_f) * (line_params[:weight].weight.to_f / 1000)
      # CUSTOM_LOGGER.info "calcolo il peso: #{line_params[:weight_line_item]} *********************************"      
      
      line_params
    end
    
    # Ricavo il costo per la stampante se TAGLIATO
    def get_printer_cover_cut(line_params, front_back)
      # CUSTOM_LOGGER.info "SONO IN get_printer_cover_cut *********************************"
      if line_params[:printer_cover].color_4?
        #Aggiungo avviamento se presente
        if line_params[:print_color_cover_bool] && line_params[:printer_cover].plant_color_c
          line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_c)
        elsif line_params[:printer_cover].plant_color_bw
          line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_bw)
        end
        if line_params[:printer_cover].digital?
          # aggiungo costo per il tempo di attesa
          if front_back
            if line_params[:number_of_sheet_cover] < line_params[:printer_cover].min_limit_waiting.to_i
              line_params[:total_cover] += line_params[:printer_cover].cost_waiting.to_f
            end
          end
        end
      else
        #Aggiungo avviamento se presente
        if line_params[:print_color_cover_bool] && line_params[:printer_cover].plant_color_c
          if front_back
            line_params[:total_cover] += (Float(line_params[:printer_cover].plant_color_c) * 2)
          else
            line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_c)
          end
        elsif line_params[:printer_cover].plant_color_bw
          if front_back
            line_params[:total_cover] += (Float(line_params[:printer_cover].plant_color_bw) * 2)
          else
            line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_bw)
          end
        end
      end
      # CUSTOM_LOGGER.info "totale dopo aggiunta costo avviamento stampa copertina: #{line_params[:total_cover]} *********************************"
      
      # Devo moltiplicare per due il numero di fogli nel caso siano fronte e retro
      if front_back
        molt = 2
      else
        molt = 1
      end
      
      # Aggiungo costo di avviamento
      # Calcolandolo nel seguent modo:
      # (costo_orario * tempo_avviamento)/60
      # Calcolo il costo di finitura: che è dato dai seguenti valori:
      #   - printer_hourly_cost
      #   - average_hourly_print (colori o bw)
      #   - number_of_sheet
      #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      if line_params[:print_color_cover_bool]
        line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_c) * Float(line_params[:printer_cover].start_time_c)/60.to_f
        if line_params[:printer_cover].average_hourly_print_c > 0
          line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_c) * ((line_params[:number_of_sheet_cover] * molt) / Float(line_params[:printer_cover].average_hourly_print_c))
        end
        # calcolo il costo dello spreco di fogli
        line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
      else
        line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_bw) * Float(line_params[:printer_cover].start_time_bw)/60.to_f
        if line_params[:printer_cover].average_hourly_print_bw > 0
          line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_bw) * ((line_params[:number_of_sheet_cover] * molt) / Float(line_params[:printer_cover].average_hourly_print_bw))
        end
        # calcolo il costo dello spreco di fogli
        line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, false, line_params[:price_per_sheet_cover])
      end
      # CUSTOM_LOGGER.info "totale dopo aggiunta costo stampa copertina: #{line_params[:total_cover]} *********************************"
      # NB: il peso è in grammi quindi lo devo trasformare in kg
      line_params[:weight_line_item_cover] = (line_params[:variant].cover_variant.width.to_f / 100.to_f) * (line_params[:variant].cover_variant.height.to_f / 100.to_f) * (line_params[:cover_weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      #line_params[:weight_line_item_cover] += (line_params[:number_of_sheet_cover]/line_params[:paper_init_format_cover][:quantity].to_f) * (line_params[:cover_weight].weight.to_f / 1000)
      # CUSTOM_LOGGER.info "weight_line_item_cover: #{line_params[:weight_line_item_cover]} *********************************"
      
      line_params
    end

    # Ricavo il costo per la stampante se RISMA
    def get_printer_ream(line_params, front_back)
      # Devo moltiplicare per due il numero di fogli nel caso siano fronte e retro
      if (line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back")
        molt = 2
      else
        molt = 1
      end
      
      # Aggiungo costo di avviamento
      # Calcolandolo nel seguente modo:
      # (costo_orario * tempo_avviamento)/60
      # Calcolo il costo di finitura: che è dato dai seguenti valori:
      #   - printer_hourly_cost
      #   - average_hourly_print (colori o bw)
      #   - number_of_sheet
      #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      if line_params[:print_color].name == "true"
        line_params[:total] += Float(line_params[:printer].printer_hourly_cost_c) * Float(line_params[:printer].start_time_c)/60.to_f
        if line_params[:printer].average_hourly_print_c > 0
          line_params[:total] += Float(line_params[:printer].printer_hourly_cost_c) * ((line_params[:number_of_sheet] * molt) /Float(line_params[:printer].average_hourly_print_c))
        end
        if front_back
          line_params[:total] += line_params[:printer].start_up_waste_paper_cost((line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back") ? true : false, true, line_params[:price_per_sheet])
        else
          line_params[:total] += line_params[:printer].start_up_waste_paper_cost((line_params[:front_back].name == "front_back") ? true : false, true, line_params[:price_per_sheet])
        end
      else
        line_params[:total] += Float(line_params[:printer].printer_hourly_cost_bw) * Float(line_params[:printer].start_time_bw)/60.to_f
        if line_params[:printer].average_hourly_print_bw > 0
          line_params[:total] += Float(line_params[:printer].printer_hourly_cost_bw) * ((line_params[:number_of_sheet] * molt)/Float(line_params[:printer].average_hourly_print_bw))
        end
        if front_back
          line_params[:total] += line_params[:printer].start_up_waste_paper_cost((line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back") ? true : false, true, line_params[:price_per_sheet])
        else
          line_params[:total] += line_params[:printer].start_up_waste_paper_cost((line_params[:front_back].name == "front_back") ? true : false, true, line_params[:price_per_sheet])
        end
      end
      # NB: il peso è in grammi quindi lo devo trasformare in kg
      if line_params[:variant].product.is_a?(Spree::BusinessCardProduct)
        line_params[:weight_line_item] = (line_params[:format].base.to_f / 100.to_f) * (line_params[:format].height.to_f / 100.to_f) * (line_params[:weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      else
        line_params[:weight_line_item] = (line_params[:variant].width.to_f / 100.to_f) * (line_params[:variant].height.to_f / 100.to_f) * (line_params[:weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      end
      #line_params[:weight_line_item] = (line_params[:number_of_sheet]) * (line_params[:weight].weight.to_f / 1000)
      return line_params
    end

    # Ricavo il costo per la stampante se RISMA
    def get_printer_cover_ream(line_params, front_back)
      # CUSTOM_LOGGER.info "SONO IN get_printer_cover_ream *********************************"
      # Devo moltiplicare per due il numero di fogli nel caso siano fronte e retro
      if front_back
        molt = 2
      else
        molt = 1
      end
      
      # Aggiungo costo di avviamento
      # Calcolandolo nel seguente modo:
      # (costo_orario * tempo_avviamento)/60
      # Calcolo il costo di finitura: che è dato dai seguenti valori:
      #   - printer_hourly_cost
      #   - average_hourly_print (colori o bw)
      #   - number_of_sheet
      #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      if line_params[:print_color_cover_bool]
        line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_c) * Float(line_params[:printer_cover].start_time_c)/60.to_f
        if line_params[:printer_cover].average_hourly_print_c > 0
          line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_c) *
              ((line_params[:number_of_sheet_cover] * molt)/Float(line_params[:printer_cover].average_hourly_print_c))
        end
        if front_back
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        else
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        end
      else
        line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_bw) * Float(line_params[:printer_cover].start_time_bw)/60.to_f
        if line_params[:printer_cover].average_hourly_print_bw > 0
          line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_bw) *
              ((line_params[:number_of_sheet_cover] * molt)/Float(line_params[:printer_cover].average_hourly_print_bw))
        end
        if front_back
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        else
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        end
      end
      # CUSTOM_LOGGER.info "totale dopo aggiunta costo stampa copertina: #{line_params[:total_cover]} *********************************"
      # NB: il peso è in grammi quindi lo devo trasformare in kg
      line_params[:weight_line_item_cover] = (line_params[:variant].cover_variant.width.to_f / 100.to_f) * (line_params[:variant].cover_variant.height.to_f / 100.to_f) * (line_params[:cover_weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      #line_params[:weight_line_item_cover] += (line_params[:number_of_sheet_cover]) * (line_params[:cover_weight].weight.to_f / 1000)
      # CUSTOM_LOGGER.info "weight_line_item: #{line_params[:weight_line_item_cover]} *********************************"

      line_params
    end

    # ricavo il tipo di carta a seconda della stampante selezionata
    # Digitale  =>  Risma e nel caso Tagliato
    # Offset  =>  Solo Tagliato
    def fetch_paper_cover(line_params, front_back = false)
      # CUSTOM_LOGGER.info "SONO IN fetch_paper_cover *********************************"
      line_params[:total_cover] = 0
      line_params[:weight_line_item_cover] = 0
      if line_params[:printer].digital?
        # CUSTOM_LOGGER.info "STAMPO COPERTINA IN DIGITALE *********************************"
        if line_params[:cover_weight].ream?
          line_params = get_cover_paper_ream(line_params)
          line_params = get_printer_cover_ream(line_params, front_back)
        else
          line_params = get_cover_paper_cut(line_params)
          line_params = get_printer_cover_cut(line_params, front_back)
        end
        if line_params[:number_of_sheet_cover] < line_params[:printer_cover].min_limit_waiting.to_i
          line_params[:total_cover] += line_params[:printer_cover].cost_waiting.to_f
        end
      else
        # CUSTOM_LOGGER.info "STAMPO COPERTINA IN OFFSET *********************************"
        line_params = get_cover_paper_cut(line_params)
        line_params = get_printer_cover_cut(line_params, front_back)
      end

      # Aggiungo il prezzo per la plastificazione se presente
      line_params[:total_cover] += plasticization_price_cover(line_params) if line_params[:plastification]
      # CUSTOM_LOGGER.info "totale dopo plastificazione della copertina: #{line_params[:total_cover]} *********************************" if line_params[:plastification]
      
      # Aggiungo il costo di avviamento del tagliato
      #line_params[:total_cover] += (line_params[:variant].cover_variant.product.cut.hourly_cost * line_params[:variant].cover_variant.product.cut.start_time)/60
      ## CUSTOM_LOGGER.info "totale dopo avviamento taglio copertina: #{line_params[:total_cover]} *********************************"
      # Aggiungo il costo del tempo di taglio
      #line_params[:total_cover] += cutting_time_cover(line_params[:variant].cover_variant.product.cut, line_params[:number_of_sheet_cover], line_params[:cover_weight], line_params[:pose_cover])
      ## CUSTOM_LOGGER.info "totale dopo taglio copertina: #{line_params[:total_cover]} *********************************"
      
      line_params
    end

    # Ricavo il costo e tutti i valori necessari per la carta in formato TAGLIATO
    def get_cover_paper_cut(line_params)
      # CUSTOM_LOGGER.info "SONO IN get_cover_paper_cut *********************************"
      # Calcolo il costo della carta
      line_params[:price_kg_cover] = Float(line_params[:cover_weight].cost_cut)
      # CUSTOM_LOGGER.info "price_kg_cover: #{line_params[:price_kg_cover]} *********************************"
      # Calcolo il prezzo per foglio è dato da:
      # supponendo di avere:
      #   - 64x88 cm
      #   - 150 gr
      #   - 0,90 €/kg
      # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
      line_params[:price_per_sheet_cover] = (line_params[:paper_init_format_cover][:master][0] / 100.to_f) * (line_params[:paper_init_format_cover][:master][1] / 100.to_f) * (line_params[:price_kg_cover] * (line_params[:cover_weight].name.to_f / 1000.to_f))
      # CUSTOM_LOGGER.info "price_per_sheet_cover: #{line_params[:price_per_sheet_cover]} *********************************"
      # Ottengo i numero di fogli
      line_params[:number_of_sheet_cover] = ((line_params[:number_of_copy].presentation.to_i) / line_params[:pose_cover].name.to_f).ceil
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation.to_f} *********************************"
      # CUSTOM_LOGGER.info "pose_cover: #{line_params[:pose_cover].name} *********************************"
      # CUSTOM_LOGGER.info "number_of_sheet_cover: #{line_params[:number_of_sheet_cover]} *********************************"
      # Divido il numero di fogli per la quantità di fogli che ci stanno sul formato più grande
      line_params[:total_cover] += (line_params[:price_per_sheet_cover] * (line_params[:number_of_sheet_cover]/line_params[:paper_init_format_cover][:quantity].to_f).ceil)
      # CUSTOM_LOGGER.info "totale dopo aggiunta costo fogli copertina: #{line_params[:total_cover]} *********************************"
      # aggiungo il costo della passata
      line_params[:total_cover] += line_params[:number_of_sheet_cover] * line_params[:price_front_back_cover]
      # CUSTOM_LOGGER.info "totale dopo aggiunta passate copertina: #{line_params[:total_cover]} *********************************"
      line_params[:number_of_sheet] +=line_params[:number_of_sheet_cover]
      line_params
    end

    # Ricavo il costo e i valori necessari per la carta in formato RISMA
    def get_cover_paper_ream(line_params)
      # CUSTOM_LOGGER.info "SONO IN get_cover_paper_ream *********************************"
      # Non è formato tagliato quindi si calcola in risma
      line_params[:price_ream_cover] = Float(line_params[:cover_weight].cost_ream)
      # CUSTOM_LOGGER.info "price_ream_cover: #{line_params[:price_ream_cover]} *********************************"
      # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
      line_params[:price_per_sheet_cover] = line_params[:price_ream_cover] / 1000.to_f
      # CUSTOM_LOGGER.info "price_per_sheet_cover: #{line_params[:price_per_sheet_cover]} *********************************"
      # Ottengo i numeri di fogli
      line_params[:number_of_sheet_cover] = (Float(line_params[:number_of_copy].presentation) / Float(line_params[:pose_cover].name).to_f).ceil
      # CUSTOM_LOGGER.info "number_of_sheet_cover: #{line_params[:number_of_sheet_cover]} *********************************"
      # inizio a calcolare il totale
      line_params[:total_cover] += line_params[:price_per_sheet_cover] * line_params[:number_of_sheet_cover]
      # CUSTOM_LOGGER.info "totale dopo aggiunta costo fogli copertina: #{line_params[:total_cover]} *********************************"
      # aggiungo il costo della carta
      line_params[:total_cover] += line_params[:number_of_sheet_cover] * line_params[:price_front_back_cover]
      # CUSTOM_LOGGER.info "totale dopo aggiunta passate copertina: #{line_params[:total_cover]} *********************************"
      line_params[:number_of_sheet] +=line_params[:number_of_sheet_cover]
      line_params
    end
    
    # Ritorno i line_params per la spirale
    def fetch_values_spiral(variant, params, quantity)
      # CUSTOM_LOGGER.info "SONO IN fetch_values *********************************"
      line_params = Hash.new()
      line_params[:variant] = variant
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      
      line_params[:orientation] = Spree::OptionValue.find_by_name('vertical') # metto l'orientamento veritcale
      line_params[:print_color_ot] = Spree::OptionType.find_by_name('printer_color')
      line_params[:print_color] = line_params[:print_color_ot].option_values.where("name = '#{params[:order][:print_color].to_s}'").first
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
      # Per la grammatura ho solo il valore(name) e devo cercare option_value appropiato
      line_params[:weight] = line_params[:paper].paper_weights.where("name = '#{params[:order][:weight].to_s}'").first
      line_params[:paper] = Spree::Paper.find(params[:order][:paper])
      # CUSTOM_LOGGER.info "paper: #{line_params[:paper].name} *********************************"
      
      # Informazioni per il numero di facciate
      line_params[:number_of_facades] = Spree::OptionValue.create(:name => :number_of_facades, :presentation => params[:order][:number_of_facades])
      # CUSTOM_LOGGER.info "number_of_facades: #{line_params[:number_of_facades].presentation} *********************************"
      # Informazioni per il numero di facciate
      
      # Informazioni per la plastificazione
      line_params[:plasticization_option] = params[:order][:plasticization_option]
      if params[:order][:plasticization_option] == "front_cover_plasticization"
        line_params[:plastification] = Spree::OptionValue.where(:name => "#{params[:order][:plasticization]}_front",
                                                                :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        # CUSTOM_LOGGER.info "plastification: #{line_params[:plastification].name} *********************************"
      elsif params[:order][:plasticization_option] == "front_back_cover_plasticization" || params[:order][:plasticization_option] == "all_plasticization"
        line_params[:plastification] = Spree::OptionValue.where(:name => "#{params[:order][:plasticization]}_front_back",
                                                                :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        # CUSTOM_LOGGER.info "plastification: #{line_params[:plastification].name} *********************************"
      end
      # Informazioni per la plastificazione
      
      # Informazioni per la copertina
      if params[:order][:paper_cover] == "-1"
        line_params[:cover] = false
        line_params[:paper_cover] = line_params[:paper]
        # CUSTOM_LOGGER.info "paper_cover: #{line_params[:paper_cover].name} *********************************"
        line_params[:cover_weight] = line_params[:weight]
        # CUSTOM_LOGGER.info "cover_weight: #{line_params[:cover_weight].name} *********************************"
      elsif params[:order][:paper_cover] == "-2"
        line_params[:cover] = false
        line_params[:default_cover] = params[:order][:default_cover]
      else
        line_params[:cover] = true
        line_params[:paper_cover] = Spree::Paper.find(params[:order][:paper_cover])
        # CUSTOM_LOGGER.info "paper_cover: #{line_params[:paper_cover].name} *********************************"
        line_params[:cover_weight] = line_params[:paper_cover].paper_weights.where("name = '#{params[:order][:paper_cover_weight].to_s}'").first
        # CUSTOM_LOGGER.info "cover_weight: #{line_params[:cover_weight].weight} *********************************"
      end
      # Informazioni per la copertina
      
      if params[:order][:instructions].blank?
        front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
        line_params[:front_back] = front_back_ot.option_values.where("name = 'different_front_back'").first
        # Serve per la formula del CoverFlyer (COPERTINA)
        line_params[:front_back_cover] = front_back_ot.option_values.where(:name => "different_front_back").first
        # CUSTOM_LOGGER.info "front_back_cover: #{line_params[:front_back_cover].presentation} *********************************"
      else
        front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
        line_params[:front_back] = front_back_ot.option_values.where("name = '#{params[:order][:instructions].to_s}'").first
        # Serve per la formula del CoverFlyer (COPERTINA)
        line_params[:front_back_cover] = front_back_ot.option_values.where(:name => "#{params[:order][:instructions].to_s}").first
        # CUSTOM_LOGGER.info "front_back_cover: #{line_params[:front_back_cover].presentation} *********************************"
      end
      
      # Informazioni per la tipologia di stampa
      if params[:order][:cover_bw]
        line_params[:print_color_cover] = print_color_ot.option_values.where(:name => params[:order][:cover_bw].to_s).first
        # CUSTOM_LOGGER.info "print_color_cover: #{line_params[:print_color_cover].name} *********************************"
      end
      
      # Informazioni per la spiralatura
      if params[:order][:spiraling]
        line_params[:spiraling] = Spree::SpiralingQuantity.find(params[:order][:spiraling])
      end
      
      if params[:order][:sleeve]
        line_params[:sleeve] = Spree::OptionValue.create(:name => :sleeve, :presentation => params[:order][:sleeve])
      end
      
      # Aggiungo i dati per la variante (l'interno del punto metallico)
      current_item.line_items_option_values.new :option_value => line_params[:paper], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:weight], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:print_color], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:front_back], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:number_of_facades], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:spiraling], :variant_parent_id => variant.id if params[:order][:spiraling]
      current_item.line_items_option_values.new :option_value => line_params[:sleeve], :variant_parent_id => variant.id if params[:order][:sleeve]
      current_item.line_items_option_values.new :option_value => line_params[:plastification], :variant_parent_id => variant.id if params[:order][:plasticization_option] == "all_plasticization"
      # Aggiungo i dati per la copertina
      current_item.line_items_option_values.new :option_value => line_params[:orientation], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:front_back_cover], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:paper_cover], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:cover_weight], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:plastification], :variant_parent_id => variant.cover_variant.id unless params[:order][:plasticization_option] == "no_plasticization"
      # la quantità sarà uno
      current_item.line_items_option_values.new :option_value => line_params[:print_color_cover], :variant_parent_id => variant.cover_variant.id

      # abbino il current_item ad un nuovo line_items
      self.line_items << current_item
      line_params[:current_item] = current_item

      # creo le variabili per il totale e il peso
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      
      line_params[:print_color_bool] = line_params[:print_color].name == "true" ? true : false
      line_params[:print_color_cover_bool] = line_params[:print_color].name == "true" ? true : line_params[:print_color_cover].name == "true" ? true : false
      
      # Calcolo il numero di facciate INTERNE
      if params[:order][:paper_cover] == "-1"
        # Se ho stessa grammatura non calcolo copertina
        # Recupero il numero delle copie reali per la stampa a FLYER
        line_params[:number_of_copy] = ((Integer(line_params[:number_of_copy].presentation) * line_params[:number_of_facades].presentation.to_i)/2).ceil
        if line_params[:print_color_cover_bool] != line_params[:print_color_bool]
          # Se ho stessa grammatura ma a colori calcolo copertina
          line_params[:cover] = true
          # Recupero il numero delle copie reali per la stampa a FLYER
          line_params[:number_of_copy] = ((Integer(line_params[:number_of_copy].presentation) * (line_params[:number_of_facades].presentation.to_i-4))/2).ceil
        end
      elsif params[:order][:paper_cover] == "-2"
        line_params[:cover] = false
      else
        # Recupero il numero delle copie reali per la stampa a FLYER
        line_params[:number_of_copy] = ((Integer(line_params[:number_of_copy].presentation) * (line_params[:number_of_facades].presentation.to_i-4))/2).ceil
      end
      
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation.to_i} *********************************"
      
      # Recupero le stampanti se bianco e nero o colori
      # line_params[:printer_multi] = current_item.variant.limit.printer_multi(line_params[:print_color_bool])
      # CUSTOM_LOGGER.info "printer: #{line_params[:printer_multi]} *********************************"
      
      # Ricavo i limiti per la copertina eventuale
      line_params[:limit_cover] = current_item.variant.cover_variant.limit
      
      # ricavo un nuovo hash per STAMPANTE, POSE e FORMATO DELLA CARTA
      line_params = get_printer_variant(line_params, line_params[:current_item].variant.limit)
      # Ricavo il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
      line_params[:paper_init_format] = get_start_format(line_params[:paper_format])
      
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]

      return line_params
    end

    # Ritorno i line_params per i MultiPagina
    def fetch_values(variant, params, quantity)
      # CUSTOM_LOGGER.info "SONO IN fetch_values *********************************"
      line_params = Hash.new
      line_params[:variant] = variant
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      line_params[:limit] = current_item.variant.limit
      line_params[:cut] = current_item.product.cut
      # CUSTOM_LOGGER.info "cut: #{line_params[:cut]} *********************************"
      
      line_params[:paper] = Spree::Paper.find(params[:order][:paper])
      # CUSTOM_LOGGER.info "paper: #{line_params[:paper].name} *********************************"
      line_params[:number_of_facades] = Spree::OptionValue.create(:name => :number_of_facades, :presentation => params[:order][:number_of_facades])
      # CUSTOM_LOGGER.info "number_of_facades: #{line_params[:number_of_facades].presentation} *********************************"
      print_color_ot = Spree::OptionType.find_by_name('printer_color')
      line_params[:print_color] = print_color_ot.option_values.where(:name => params[:order][:print_color].to_s).first
      # CUSTOM_LOGGER.info "print_color: #{line_params[:print_color].name} *********************************"
      # Informazioni per entrambi riguardo la plastificazione
      line_params[:plasticization_option] = params[:order][:plasticization_option]
      if params[:order][:plasticization_option] == "front_cover_plasticization"
        line_params[:plastification] = Spree::OptionValue.where(:name => "#{params[:order][:plasticization]}_front",
                                                                :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        # CUSTOM_LOGGER.info "plastification: #{line_params[:plastification].name} *********************************"
      elsif params[:order][:plasticization_option] == "front_back_cover_plasticization" || params[:order][:plasticization_option] == "all_plasticization"
        line_params[:plastification] = Spree::OptionValue.where(:name => "#{params[:order][:plasticization]}_front_back",
                                                                :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        # CUSTOM_LOGGER.info "plastification: #{line_params[:plastification].name} *********************************"
      end
      # Per la grammatura ho solo il valore(name) e devo cercare option_value appropiato
      line_params[:weight] = line_params[:paper].paper_weights.where("name = '#{params[:order][:weight].to_s}'").first
      # CUSTOM_LOGGER.info "weight: #{line_params[:weight].name} *********************************"
      line_params[:number_of_copy] = Spree::OptionValue.create(:name => :number_of_copy, :presentation => params[:order][:number_of_copy])
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation} *********************************"
      # Informazioni per la copertina
      if params[:order][:paper_cover] == "-1"
				line_params[:no_cover] = false
        line_params[:cover] = false
        line_params[:paper_cover] = line_params[:paper]
        # CUSTOM_LOGGER.info "paper_cover: #{line_params[:paper_cover].name} *********************************"
        line_params[:cover_weight] = line_params[:weight]
        # CUSTOM_LOGGER.info "cover_weight: #{line_params[:cover_weight].name} *********************************"
      elsif params[:order][:paper_cover] == "-2"
        line_params[:cover] = false
        line_params[:default_cover] = params[:order][:default_cover]
				line_params[:no_cover] = true if params[:order][:default_cover] != 0
      else
				line_params[:no_cover] = false
        line_params[:cover] = true
        line_params[:paper_cover] = Spree::Paper.find(params[:order][:paper_cover])
        # CUSTOM_LOGGER.info "paper_cover: #{line_params[:paper_cover].name} *********************************"
        line_params[:cover_weight] = line_params[:paper_cover].paper_weights.where("name = '#{params[:order][:paper_cover_weight].to_s}'").first
        # CUSTOM_LOGGER.info "cover_weight: #{line_params[:cover_weight].weight} *********************************"
      end
      line_params[:orientation] = Spree::OptionValue.find_by_name('vertical') # metto l'orientamento veritcale fisso
      if params[:order][:instructions].blank?
        front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
        line_params[:front_back] = front_back_ot.option_values.where("name = 'different_front_back'").first
        # Serve per la formula del CoverFlyer (COPERTINA)
        line_params[:front_back_cover] = front_back_ot.option_values.where(:name => "different_front_back").first
        # CUSTOM_LOGGER.info "front_back_cover: #{line_params[:front_back_cover].presentation} *********************************"
      else
        front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
        line_params[:front_back] = front_back_ot.option_values.where("name = '#{params[:order][:instructions].to_s}'").first
        # Serve per la formula del CoverFlyer (COPERTINA)
        line_params[:front_back_cover] = front_back_ot.option_values.where(:name => "#{params[:order][:instructions].to_s}").first
        # CUSTOM_LOGGER.info "front_back_cover: #{line_params[:front_back_cover].presentation} *********************************"
      end
      
      # CUSTOM_LOGGER.info "params[:order][:cover_bw]: #{params[:order][:cover_bw].to_s} *********************************"
      if params[:order][:cover_bw]
        line_params[:print_color_cover] = print_color_ot.option_values.where(:name => params[:order][:cover_bw].to_s).first
        # CUSTOM_LOGGER.info "print_color_cover: #{line_params[:print_color_cover].name} *********************************"
      end
      
      if params[:order][:spiraling]
        line_params[:spiraling] = Spree::SpiralingQuantity.find(params[:order][:spiraling])
      end
      
      if params[:order][:sleeve]
        line_params[:sleeve] = Spree::OptionValue.create(:name => :sleeve, :presentation => params[:order][:sleeve])
      end

      # Aggiungo i dati per la variante (l'interno del punto metallico)
      current_item.line_items_option_values.new :option_value => line_params[:paper], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:weight], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:print_color], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:front_back], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:number_of_facades], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:spiraling], :variant_parent_id => variant.id if params[:order][:spiraling]
      current_item.line_items_option_values.new :option_value => line_params[:sleeve], :variant_parent_id => variant.id if params[:order][:sleeve]
      current_item.line_items_option_values.new :option_value => line_params[:plastification], :variant_parent_id => variant.id if params[:order][:plasticization_option] == "all_plasticization"
      # Aggiungo i dati per la copertina
      current_item.line_items_option_values.new :option_value => line_params[:orientation], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:front_back_cover], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:paper_cover], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:cover_weight], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:plastification], :variant_parent_id => variant.cover_variant.id unless params[:order][:plasticization_option] == "no_plasticization"
      # la quantità sarà uno
      current_item.line_items_option_values.new :option_value => line_params[:print_color_cover], :variant_parent_id => variant.cover_variant.id

      self.line_items << current_item
      line_params[:current_item] = current_item
      
      line_params[:print_color_bool] = line_params[:print_color].name == "true" ? true : false
      line_params[:print_color_cover_bool] = line_params[:print_color].name == "true" ? true : line_params[:print_color_cover].name == "true" ? true : false
      
      ####  PROPIETA' COMUNI
      # Se ho stessa grammatura non calcolo copertina
      if params[:order][:paper_cover] == "-1"
        # Calcolo il numero di facciate
        line_params[:number_of_facades_internal] = Integer(line_params[:number_of_facades].presentation)
        # CUSTOM_LOGGER.info "number_of_facades_internal: #{line_params[:number_of_facades_internal]} *********************************"
        if line_params[:print_color_cover_bool] != line_params[:print_color_bool]
          line_params[:cover] = true
          line_params[:number_of_facades_internal] = Integer(line_params[:number_of_facades].presentation) - 4
        end
      elsif params[:order][:paper_cover] == "-2"
        # Calcolo il numero di facciate
        line_params[:number_of_facades_internal] = Integer(line_params[:number_of_facades].presentation)
        # CUSTOM_LOGGER.info "number_of_facades_internal: #{line_params[:number_of_facades_internal]} *********************************"
        line_params[:cover] = false
      else
        # Calcolo in numero di facciate togliendo quelle della copertina cioè - 4
        line_params[:number_of_facades_internal] = Integer(line_params[:number_of_facades].presentation) - 4
        # CUSTOM_LOGGER.info "number_of_facades_internal: #{line_params[:number_of_facades_internal]} *********************************"
      end
      line_params[:number_of_quarters] = (line_params[:number_of_facades_internal] / 4) * Integer(line_params[:number_of_copy].presentation)
      # CUSTOM_LOGGER.info "number_of_quarters: #{line_params[:number_of_quarters]} *********************************"
      # Recupero il totale delle copie
      line_params[:total_copy] = Integer(line_params[:number_of_copy].presentation) * (line_params[:number_of_facades_internal]/2).ceil
      # CUSTOM_LOGGER.info "total_copy: #{line_params[:total_copy]} *********************************"
      # Recupero le stampanti se bianco e nero o colori
      line_params[:printer_multi] = current_item.variant.limit.printer_multi(line_params[:print_color_bool])
      # CUSTOM_LOGGER.info "printer: #{line_params[:printer_multi]} *********************************"
      # Ricavo i limiti per la copertina eventuale
      line_params[:limit_cover] = current_item.variant.cover_variant.limit
      
      # Aggiungo la data di spedizione
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      
      line_params[:file_check] = params[:order][:file_check]
      
      line_params
    end

    # Ricavo il costo e i valori necessari per la carta in formato TAGLIATO per i formati MULTIPAGINA
    def get_multi_paper_cut(line_params, printer, step)
      # CUSTOM_LOGGER.info "SONO IN get_multi_paper_cut *********************************"
      # Recupero il prezzo al kg
      line_params[:price_kg] = Float(line_params[:weight].cost_cut)
      # CUSTOM_LOGGER.info "price_kg: #{line_params[:price_kg]} *********************************"
      # Calcolo il prezzo per foglio è dato da:
      # supponendo di avere:
      #   - 64x88 cm
      #   - 150 gr
      #   - 0,90 €/kg
      # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
      line_params["price_per_sheet_#{step}".to_sym] = (line_params["paper_init_format_#{step}".to_sym][:master][0] / 100.to_f) * (line_params["paper_init_format_#{step}".to_sym][:master][1] / 100.to_f) *
          (line_params[:price_kg] * (Float(line_params[:weight].name) / 1000.to_f))
      # CUSTOM_LOGGER.info "price_per_sheet: #{line_params["price_per_sheet_#{step}".to_sym]} *********************************"
      
      # Devo effettuare il doppio preventivo
      total = 0
      if printer.digital?
        # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
        plant_params_unic = Spree::Order.get_number_of_sheet(line_params, 3, step)
        #total += (line_params[:price_per_sheet] * (line_params[:number_of_copy].presentation.to_i * plant_params_unic[:number_of_sheet]*(1/line_params[:paper_init_format][:quantity].to_f).ceil))
        total += printer.print_cost(line_params, plant_params_unic, step)
        # CUSTOM_LOGGER.info "total: #{total} *********************************"
      else
        if printer.color_4?
          total_1 = 0
          # passo il valore 1 alla funzione get_number_of_sheet in quanto la considero come una 4 COLORI
          plant_params_1 = Spree::Order.get_number_of_sheet(line_params, 1, step)
          #total_1 += (line_params[:price_per_sheet] * (line_params[:number_of_copy].presentation.to_i * plant_params_1[:number_of_sheet]*(1/line_params[:paper_init_format][:quantity].to_f).ceil))
          total_1 += printer.print_cost(line_params, plant_params_1, step)
          # CUSTOM_LOGGER.info "plant_params_1: #{plant_params_1} *********************************"
          # CUSTOM_LOGGER.info "4 NORMALE total_1: #{total_1} *********************************"
          
          total_2 = 0
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.get_number_of_sheet(line_params, 3, step)
          #total_2 += (line_params[:price_per_sheet] * (line_params[:number_of_copy].presentation.to_i * plant_params_2[:number_of_sheet]*(1/line_params[:paper_init_format][:quantity].to_f).ceil))
          total_2 += printer.print_cost(line_params, plant_params_2, step)
          # CUSTOM_LOGGER.info "plant_params_2: #{plant_params_2} *********************************"
          # CUSTOM_LOGGER.info "8 NON SCARTO total_2: #{total_2} *********************************"

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        else
          # CALCOLO IL PREVENTIVO NORMALE
          total_1 = 0
          # passo il valore 2 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI CON SCARTO
          plant_params_1 = Spree::Order.get_number_of_sheet(line_params, 2, step)
          #total_1 += (line_params[:price_per_sheet] * (line_params[:number_of_copy].presentation.to_i * plant_params_1[:number_of_sheet]*(1/line_params[:paper_init_format][:quantity].to_f).ceil))
          total_1 += printer.print_cost(line_params, plant_params_1, step)
          # CUSTOM_LOGGER.info "plant_params_1: #{plant_params_1} *********************************"
          # CUSTOM_LOGGER.info "8 SCARTO total_1: #{total_1} *********************************"
          
          total_2 = 0
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.get_number_of_sheet(line_params, 3, step)
          #total_2 += (line_params[:price_per_sheet] * (line_params[:number_of_copy].presentation.to_i * plant_params_2[:number_of_sheet]*(1/line_params[:paper_init_format][:quantity].to_f).ceil))
          total_2 += printer.print_cost(line_params, plant_params_2, step)
          # CUSTOM_LOGGER.info "plant_params_2: #{plant_params_2} *********************************"
          # CUSTOM_LOGGER.info "8 NON SCARTO total_2: #{total_2} *********************************"

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        end # TERMINO IL CONFRONTO DELL'OFFSET
      end
      
      #line_params[:total] += total
      ## CUSTOM_LOGGER.info "totale definitivo per stampa: #{line_params[:total]} *********************************"
      #line_params.merge(plant_params_unic)
      line_params["total_#{step}".to_sym] = total
      line_params["print_option_#{step}".to_sym] = plant_params_unic
      line_params
    end

    # Ricavo il costo e i valori necessari per la carta in formato RISMA per i formati MULTIPAGINA
    def get_multi_paper_ream(line_params, printer, step)
      line_params["total_#{step}".to_sym] = 0
      # CUSTOM_LOGGER.info "SONO IN get_multi_paper_ream *********************************"
      # Sono in risma
      # TODO calcolare il costo di un foglio

      # Devo recuperare il numero dei fogli, # impianti, il peso del prodotto e aggiungerlo al totale
      #number_of_sheet = r[:number_of_sheet]
      #weight_line_item = r[:weight_line_item] #(number_of_sheet *Integer(number_of_copy.presentation)) * (weight.name.to_f / 1000)
      #plants = r[:plants]

      line_params = Spree::Order.paper_cost_in_risma_multipage(line_params, Float(line_params[:weight].cost_ream), Integer(line_params[:number_of_copy].presentation), Float(line_params["quarter_#{step}".to_sym].name), step)
      ## CUSTOM_LOGGER.info "price_per_sheet: #{line_params[:price_per_sheet]} *********************************"
      
      # Devo effettuare il doppio preventivo
      total = 0
      if printer.digital?
        # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
        plant_params_unic = Spree::Order.get_number_of_sheet(line_params, 3, step)
        # CUSTOM_LOGGER.info "price_per_sheet: #{line_params["price_per_sheet_#{step}".to_sym]} *********************************"
        # CUSTOM_LOGGER.info "plant_params_unic[:number_of_sheet]: #{plant_params_unic[:number_of_sheet]} *********************************"
        # CUSTOM_LOGGER.info "paper_init_format: #{line_params["paper_init_format_#{step}".to_sym][:quantity]} *********************************"
        total += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params_unic[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
        # CUSTOM_LOGGER.info "total: #{total} *********************************"
        total += printer.print_cost(line_params, plant_params_unic, step)
        # CUSTOM_LOGGER.info "total: #{total} *********************************"
      else
        if printer.color_4?
          total_1 = 0
          # passo il valore 1 alla funzione get_number_of_sheet in quanto la considero come una 4 COLORI
          plant_params_1 = Spree::Order.get_number_of_sheet(line_params, 1, step)
          total_1 += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params_1[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_1 += printer.print_cost(line_params, plant_params_1, step)
          # CUSTOM_LOGGER.info "total_1: #{total_1} *********************************"

          total_2 = 0
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.get_number_of_sheet(line_params, 3, step)
          total_2 += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params_2[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_2 += printer.print_cost(line_params, plant_params_2, step)
          # CUSTOM_LOGGER.info "total_2: #{total_2} *********************************"

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        else
          # CALCOLO IL PREVENTIVO NORMALE
          total_1 = 0
          # passo il valore 2 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI CON SCARTO
          plant_params_1 = Spree::Order.get_number_of_sheet(line_params, 2, step)
          total_1 += (line_params["price_per_sheet_#{step}".to_sym] * (line_params[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_1 += printer.print_cost(line_params, plant_params_1, step)
          # CUSTOM_LOGGER.info "total_1: #{total_1} *********************************"

          total_2 = 0
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.get_number_of_sheet(line_params, 3, step)
          total_2 += (line_params["price_per_sheet_#{step}".to_sym] * (line_params[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_2 += printer.print_cost(line_params, plant_params_2, step)
          # CUSTOM_LOGGER.info "total_2: #{total_2} *********************************"

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        end # TERMINO IL CONFRONTO DELL'OFFSET
      end
      
      # Non devo salvare subito il totale e i parametri ricavati perche` devo fare il confronto anche con la seconda stampante
      #line_params[:total] += total
      # CUSTOM_LOGGER.info "totale definitivo per stampa: #{total} *********************************"
      #line_params.merge(plant_params_unic)
      line_params["total_#{step}".to_sym] += total
      line_params["print_option_#{step}".to_sym] = plant_params_unic
      line_params
    end
    
    # Calcolo il costo per la piegha
    def folding_calculate_paper(line_params)
      # CUSTOM_LOGGER.info "SONO IN: folding_calculate *********************************"
      line_params[:folding_printer] = line_params[:folding_format].folding
      # CUSTOM_LOGGER.info "folding_printer: #{line_params[:folding_printer].name} *********************************"
      # Devo verificare cosa esiste come grammatura
      # Se esiste una grammatura maggiore e nessuna minore allora sara lei
      # Se esiste una grammatura minore e nessuna maggiore allora sbaglio
      # Se esiste una grammatura minore e una maggiore allora devo prendere quella maggiore
      max = line_params[:folding_printer].folding_weights.where("start_up_cost_bw >= #{line_params[:weight].name.to_i}")
      min = line_params[:folding_printer].folding_weights.where("start_up_cost_bw < #{line_params[:weight].name.to_i}")
      if !max.empty? && min.empty?
        line_params[:folding_weight] = max.first
      elsif !max.empty? && !min.empty?
        line_params[:folding_weight] = max.first
      end
      if !line_params[:folding_printer].external
        # CUSTOM_LOGGER.info "folding_weight: #{line_params[:folding_weight].weight_limit} *********************************"
        line_params[:total] += Float(line_params[:folding_printer].hourly_cost) * Float(line_params[:folding_printer].start_time)/60.to_f
        # CUSTOM_LOGGER.info "totale dopo costo avviamento piegha: #{line_params[:total]} *********************************"
        if (line_params[:plastification])
          line_params[:total] += Float(line_params[:folding_printer].hourly_cost) * (line_params[:number_of_sheet] / Float(line_params[:folding_printer].average_hourly_plastification))
        else
          line_params[:total] += Float(line_params[:folding_printer].hourly_cost) * (line_params[:number_of_sheet] / Float(line_params[:folding_weight].average_hourly))
        end
      else
        # CUSTOM_LOGGER.info "folding_weight: #{line_params[:folding_weight].weight_limit} *********************************"
        if line_params[:folding_printer].min_limit_folding > line_params[:number_of_copy].presentation.to_i
          line_params[:total] += line_params[:folding_printer].cost_folding.to_f
        end
        # CUSTOM_LOGGER.info "totale dopo costo avviamento piegha: #{line_params[:total]} *********************************"
        if (line_params[:plastification])
          line_params[:total] += line_params[:number_of_copy].presentation.to_i * line_params[:folding_printer].copy_cost_plastification.to_f
        else
          line_params[:total] += line_params[:number_of_copy].presentation.to_i * line_params[:folding_weight].copy_cost.to_f
        end
      end
      # CUSTOM_LOGGER.info "totale dopo costo pieghe: #{line_params[:total]} *********************************"
      line_params
    end
    
    # Controllo in che lato orientamento sono e se uno dei due lati oltrepassa il massimo o entrambi
    def calculate_heat_sealing(line_params)
      # CUSTOM_LOGGER.info "SONO IN calculate_heat_sealing *********************************"
      # CUSTOM_LOGGER.info "line_params[:custom_format][:width].to_f #{line_params[:custom_format][:width].to_f}"
      # CUSTOM_LOGGER.info "line_params[:custom_format][:height].to_f #{line_params[:custom_format][:height].to_f}"
      # CUSTOM_LOGGER.info "line_params[:custom_format][:width].to_f > line_params[:custom_format][:height].to_f) #{line_params[:custom_format][:width].to_f > line_params[:custom_format][:height].to_f}"
      # controllo se uno dei due valori oltrepassa l'altezza della bobina
      if ((line_params[:custom_format][:width].to_f > line_params[:paper].height.to_f) && (line_params[:custom_format][:height].to_f > line_params[:paper].height.to_f))
        # controllo se la larghezza oltrepassa l'altezza
        if (line_params[:custom_format][:width].to_f > line_params[:custom_format][:height].to_f)
          line_params[:heat_sealing_count] = (line_params[:custom_format][:width].to_f/line_params[:paper].height.to_f).ceil
          # CUSTOM_LOGGER.info "heat_sealing_count: #{line_params[:heat_sealing_count]} *********************************"
          line_params[:copy_heat_sealing] = (line_params[:paper].height.to_f * line_params[:heat_sealing_count].to_f) * line_params[:custom_format][:height].to_f
          # CUSTOM_LOGGER.info "copy_heat_sealing: #{line_params[:copy_heat_sealing]} *********************************"
          line_params[:total] += (line_params[:variant].heat_sealing.available_meter.to_f * (line_params[:custom_format][:height].to_f / 1000.to_f)) * (line_params[:heat_sealing_count]/2.0).ceil
          # CUSTOM_LOGGER.info "totale dopo termosaldatura: #{line_params[:total]} *********************************"
        else # sono nel caso opposto
          line_params[:heat_sealing_count] = (line_params[:custom_format][:height].to_f/line_params[:paper].height.to_f).ceil
          # CUSTOM_LOGGER.info "heat_sealing_count: #{line_params[:heat_sealing_count]} *********************************"
          line_params[:copy_heat_sealing] = (line_params[:paper].height.to_f * line_params[:heat_sealing_count].to_f) * line_params[:custom_format][:width].to_f
          # CUSTOM_LOGGER.info "copy_heat_sealing: #{line_params[:copy_heat_sealing]} *********************************"
          line_params[:total] += (line_params[:variant].heat_sealing.available_meter.to_f * (line_params[:custom_format][:width].to_f / 1000.to_f)) * (line_params[:heat_sealing_count]/2.0).ceil
          # CUSTOM_LOGGER.info "totale dopo termosaldatura: #{line_params[:total]} *********************************"
        end
      else
        line_params[:heat_sealing_count] = 1
        # CUSTOM_LOGGER.info "QUII"
        # CUSTOM_LOGGER.info "heat_sealing_count: #{line_params[:heat_sealing_count]} *********************************"
      end
      line_params
    end
    
    # Ricavo il formato di carta di partenza da quello indicato
    def get_start_format(format)
      if pfd = Spree::PrinterFormatDefault.find_by_name(format)
        if pfd.master.present?
          master = [pfd.master.base.to_f, pfd.master.height.to_f]
        else
          master = [pfd.base.to_f, pfd.height.to_f]
        end
        {:master => master, :quantity => pfd.quantity}
      else
        nil
      end
    end

    # Calcolo del Tempo di taglio:
    # da documentazione:
    #     Tempo di taglio= (fogli stampa/fogli risma)*(N° tagli/tagli all'ora)
    def get_cutting_time(line_params)
      if !line_params[:printer].digital? && !line_params[:weight].ream?
        line_params[:number_of_sheet] = (line_params[:number_of_sheet] / line_params[:paper_init_format][:quantity].to_f).ceil
      end
      cost_min = line_params[:variant].product.cut.hourly_cost.to_f / 60.to_f
      paper_per_ream = Spree::Weight.find_by_name(Integer(line_params[:weight].name))
      cut_in_min = line_params[:variant].product.cut.average_hourly_print / 60
      if line_params[:pose]
        (((Float(line_params[:number_of_sheet]) / Float(paper_per_ream.ream)).ceil) * (Float(line_params[:pose].cut_number) / Float(cut_in_min))) * cost_min.to_f
      else
        (((Float(line_params[:number_of_sheet]) / Float(paper_per_ream.ream)).ceil) * (Float(line_params[:variant].poses.cut_number) / Float(cut_in_min))) * cost_min.to_f
      end
    end

    # Per calcolare il prezzo della plastificazione mi serve sapere:
    #   - tipo plastificazione (Film Lucida o  Film Opaca)
    #   - numero di fogli da stampare
    #   - formato del foglio della stampante
    def plasticization_price(line_params)
      tot = 0
      # CUSTOM_LOGGER.info "SONO IN plasticization_price *********************************"
      if !Spree::Plasticization.first.max_quantity.blank? && line_params[:number_of_sheet] <= Spree::Plasticization.first.max_quantity
        tot += Spree::Plasticization.first.price_start_up.to_f
        # CUSTOM_LOGGER.info "aggiungo costo avvio plastificazione: #{tot} *********************************"
      end
      # t = line_params[:plastification].name.split('-')
      # price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(t[0].strip, line_params[:number_of_sheet], line_params[:paper_format])
      # r = price_for_sheet.price.to_f * line_params[:number_of_sheet]
      # if t[1].strip == "fronte e retro"
      #   tot += r * 2
      # else
      #   tot += r
      # end

			t = line_params[:plastification]
			plastification_type = (t.name.match(/^polished_film/).blank?) ? "matt_film" : "polished_film"
			price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(plastification_type, line_params[:number_of_sheet], line_params[:paper_format])
			r = price_for_sheet.price.to_f * line_params[:number_of_sheet]
			if !t.name.match(/front_back/).blank? # PLASTIFICO ENTRAMBI I LATI
				tot += r * 2
			else # PLASTIFICO SOLO IL FRONTE
				tot += r
			end
      # CUSTOM_LOGGER.info "aggiungo costo plastificazione: #{tot} *********************************"
      tot
    end

    # Recupera il peso dal nome, dovuto al fatto che il nome sarà: "nome componente 123gr"
    # valuto le seguenti possibilità:
    #   - nome componente 123gr
    #   - nome componente 123 gr
    #   - nome componente123gr
    #   - nome componente123 gr
    def get_weight_from_name(name)
      name.gsub(/[0-9]*(gr|gr)/).first.gsub("gr", '').strip!
    end

    # Dalla stringa 45x50cm recupera la larghezza e l'altezza, restituisce un hash di questo formato:
    # {:w => val, :h => val}
    # inoltre valuta se l'unità di misura è in metri o in centimetri. In questo ultimo caso esegue la conversione in
    # metri, così da essere più agevolati nel calcolo.
    def get_width_height(values)
      r = values.gsub(/[0-9]+/)
      unit = values.gsub(/(cm|m)$/).first
      if unit == "m"
        {:w => r.next, :h => r.next}
      else
        {:w => Float(r.next) / 100.to_f, :h => Float(r.next) / 100.to_f}
      end
    end

    # Ricavo il formato di carta di partenza da quello indicato
    def get_paper_format(line_params, limit)
      if line_params[:print_color].name == "true"
        if Integer(line_params[:number_of_copy].presentation) > limit.quantity_c
          limit.format_after_c.presentation
        else
          limit.format_before_c.presentation
        end
      else
        if Integer(line_params[:number_of_copy].presentation) > limit.quantity_bw
          limit.format_after_bw.presentation
        else
          limit.format_before_bw.presentation
        end
      end
    end

    # Ricavo il numero di pose
    def get_pose(line_params, limit)
      if line_params[:print_color].name == "true"
        if Integer(line_params[:number_of_copy].presentation) > limit.quantity_c
          limit.pose_after_c
        else
          limit.pose_before_c
        end
      else
        if Integer(line_params[:number_of_copy].presentation) > limit.quantity_bw
          limit.pose_after_bw
        else
          limit.pose_before_bw
        end
      end
    end

    # Ricavo la stampante utilizzata
    def get_printer(line_params, limit)
      if line_params[:print_color].name == "true"
        if Integer(line_params[:number_of_copy].presentation) > limit.quantity_c
          limit.printer_after_c
        else
          limit.printer_before_c
        end
      else
        if Integer(line_params[:number_of_copy].presentation) > limit.quantity_bw
          limit.printer_after_bw
        else
          limit.printer_before_c
        end
      end
    end

    # ricavo un nuovo hash per STAMPANTE, POSE e FORMATO DELLA CARTA
    def get_printer_variant(line_params, limit)
      if line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back"
        qt_c = limit.quantity_c / 2
        qt_bw = limit.quantity_bw / 2
      else
        qt_c = limit.quantity_c
        qt_bw = limit.quantity_bw
      end
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation} *********************************"
      # CUSTOM_LOGGER.info "qt_c: #{qt_c} *********************************"
      # CUSTOM_LOGGER.info "qt_bw: #{qt_bw} *********************************"
      
      if line_params[:print_color].name == "true"
        if Integer(line_params[:number_of_copy].presentation) > qt_c
          line_params[:printer] = limit.printer_after_c
          line_params[:pose] = limit.pose_after_c
          line_params[:paper_format] = limit.format_after_c.presentation
        else
          line_params[:printer] = limit.printer_before_c
          line_params[:pose] = limit.pose_before_c
          line_params[:paper_format] = limit.format_before_c.presentation
        end
        if line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back"
          line_params[:price_front_back] = line_params[:printer].price_front_and_back_c
        else
          line_params[:price_front_back] = line_params[:printer].price_front_c
        end
      else
        if Integer(line_params[:number_of_copy].presentation) > qt_bw
          line_params[:printer] = limit.printer_after_bw
          line_params[:pose] = limit.pose_after_bw
          line_params[:paper_format] = limit.format_after_bw.presentation
        else
          line_params[:printer] = limit.printer_before_bw
          line_params[:pose] = limit.pose_before_bw
          line_params[:paper_format] = limit.format_before_bw.presentation
        end
        if line_params[:front_back].name == "front_back" || line_params[:front_back].name == "equal_front_back" || line_params[:front_back].name == "different_front_back"
          line_params[:price_front_back] = line_params[:printer].price_front_and_back_bw
        else
          line_params[:price_front_back] = line_params[:printer].price_front_bw
        end
      end
      # CUSTOM_LOGGER.info "printer: #{line_params[:printer].name} *********************************"
      # CUSTOM_LOGGER.info "pose: #{line_params[:pose].name} *********************************"
      # CUSTOM_LOGGER.info "paper_format: #{line_params[:paper_format]} *********************************"
      # CUSTOM_LOGGER.info "price_front_back: #{line_params[:price_front_back]} *********************************"

      return line_params
    end
    
    # Ricavo la disposizione perimetrale a seconda del valore passato
    def get_disposition_perimeter(disposition, line_params)
      case disposition
    		when "all_perimeter"
    			tot = (line_params[:custom_width].presentation.to_f * 2) + (line_params[:custom_height].presentation.to_f * 2)
    		when "top_side"
    			tot = line_params[:custom_width].presentation.to_f
    		when "bottom_side"
    			tot = line_params[:custom_width].presentation.to_f
    		when "right_side"
    			tot = line_params[:custom_height].presentation.to_f
    		when "left_side"
    			tot = line_params[:custom_height].presentation.to_f
    		when "side_upper_lower"
    			tot = line_params[:custom_width].presentation.to_f * 2
    		when "right_left_side"
    			tot = line_params[:custom_height].presentation.to_f * 2
    	end
    	tot
    end
  end
end