module Spree
  class SpiralingCopyCost < OptionValue
    attr_accessible :copy_cost, :min_limit, :max_limit
    validates :copy_cost, :min_limit, :max_limit, :presence => true
    belongs_to :spiraling, :class_name => "Spree::Spiraling", :foreign_key => "option_type_id"
    
    def copy_cost
      self.price_front_bw
    end
    
    def min_limit
      self.average_hourly_print_bw
    end
    
    def max_limit
      self.average_hourly_print_c
    end
    
    def copy_cost=(value)
      self.price_front_bw = value
    end
    
    def min_limit=(value)
      self.average_hourly_print_bw = value
    end
    
    def max_limit=(value)
      self.average_hourly_print_c = value
    end
  end
end