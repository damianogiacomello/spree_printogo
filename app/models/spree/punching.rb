# PUNZONATURA O PUNTO METALLICO
module Spree
  class Punching < OptionValue
    after_save :associate_option_type
    belongs_to :bindery, :class_name => "Spree::Bindery", :foreign_key => "option_value_id"
    attr_accessible :type_quantity, :copy_cost, :start_time, :average_hourly_punching
    attr_accessor :_destroy
    
    def type_quantity
      self.presentation
    end
    
    def copy_cost
      self.start_time_bw
    end
    
    def start_time
      self.start_time_c
    end

    def average_hourly_punching
      self.average_hourly_print_c
    end
    
    def type_quantity=value
      self.presentation = value
    end
    
    def copy_cost=value
      self.start_time_bw = value
    end

    def start_time=value
      self.start_time_c = value
    end

    def average_hourly_punching=value
      self.average_hourly_print_c = value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('punching').id) if self.option_type.nil?
    end
  end
end