module Spree
  # TODO: aggiungere le validazioni in back end!!Da capire come fare in maniera appropiata.
  class BusinessCardVariant < Variant
    default_scope where(:is_master => false)
    after_create :set_active
    belongs_to :business_card_product, :class_name => Spree::BusinessCardProduct, :foreign_key => "product_id"
    has_one :limit, :class_name => "Spree::Limit"
    attr_accessible :pose, :numero_di_copie, :plastification, :print_color, :printer, :printer_format, :create, :sku,
                    :price, :weight, :height, :width, :depth, :deleted_at, :is_master, :product_id, :count_on_hand, :cost_price, :position,
                    :available_on, :limit
    
    scope :sold, lambda { where(:active => true) }

    def initialize(val={})

      tmp = nil
      if val && !val.empty?
        tmp = val.clone
        val.clear
        val = {}
      end
      super
      self.type = 'Spree::BusinessCardVariant'
      if tmp && tmp.count > 0
        if tmp.is_a? Array
          self.product = tmp[1][:product]
          self.print_color = tmp[1][:print_color]
          self.pose = tmp[1][:pose]
          self.plastification =tmp[1][:plastification]
          self.numero_di_copie = tmp[1][:numero_di_copie]
          self.printer = Printer.find tmp[1][:printer] if tmp[1][:printer]
        else
          self.product = tmp[:product]
          self.print_color=tmp[:print_color]
          self.pose = tmp[:pose]
          self.plastification_one_sided_glossy =tmp[:plastification][tmp[:plastification].keys[0]]
          self.plastification_single_sided_matte =tmp[:plastification][tmp[:plastification].keys[1]]
          self.plastification_front_back_glossy =tmp[:plastification][tmp[:plastification].keys[2]]
          self.plastification_front_back_matte =tmp[:plastification][tmp[:plastification].keys[3]]
          self.numero_di_copie = tmp[:numero_di_copie]
          self.printer = Printer.find tmp[:printer] if tmp[:printer]
        end
      end
    end

    #attr_accessible :pose=, :numero_di_copie=


    def poses_option_values
      opt = Spree::OptionType.find_by_name 'pose' # TODO da valutare di metterele in lib
      #self.option_values.select { |ot| ot.option_type == opt }.first
      self.option_values.where(:option_type_id => opt.id).first
    end

    alias :poses :poses_option_values

    def number_of_copy_option_values
      number_of_copy_ot = Spree::OptionType.find_by_name('number_of_copy')
      #self.option_values.select { |ot| ot.option_type == number_of_copy_ot }.first
      self.option_values.where(:option_type_id => number_of_copy_ot.id).first
    end

    alias :number_of_copy :number_of_copy_option_values

    def print_color_option_values
      opt = Spree::OptionType.find_by_name 'printer_color'
      #self.option_values.select { |ot| ot.option_type == opt }.first
      self.option_values.where(:option_type_id => opt.id).first
    end

    alias :print_color :print_color_option_values

    def printer_option_values
      opt = Spree::OptionType.find_by_name 'printer'
      #self.option_values.select { |ot| ot.option_type == opt }.first
      self.option_values.where(:option_type_id => opt.id).first
    end

    alias :printer :printer_option_values

    def pose=(value)
      old_poses = self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('pose').id)
      old_poses.each do |p|
        self.option_values.delete(p)
      end
      self.option_values << Spree::OptionValue.find(value)
    end

    def numero_di_copie=(value)
      set_option_value("numero_di_copie", value)
    end

    def plastification=(value)
      set_option_value("plastification", value)
    end

    def print_color=(value)
      print_color_option_type = Spree::OptionType.where(:name => "print_color").first
      self.option_values << print_color_option_type.option_values.where(:presentation => value).first
    end

    def printer=(value)
      #self.option_values.select { |ovt| ovt.option_type == value.option_type }.each { |o| self.option_values.delete o }
      self.option_values.where(:option_type_id => value.option_type.id).each { |o| self.option_values.delete o }
      self.option_values << value
    end

    def plastification_one_sided_glossy=(value)
      ot = self.product.plastification_option_type
      find_or_create_plastification 'one_sided_glossy', value, ot
    end

    def plastification_single_sided_matte=(value)
      ot = self.product.plastification_option_type
      find_or_create_plastification 'single_sided_matte', value, ot
    end

    def plastification_front_back_glossy=(value)
      ot = self.product.plastification_option_type
      find_or_create_plastification 'front_back_glossy', value, ot
    end

    def plastification_front_back_matte=(value)
      ot = self.product.plastification_option_type
      find_or_create_plastification 'front_back_matte', value, ot
    end


    def options_text
      values = self.option_values.sort_by(&:position)

      values.map! do |ov|
        "#{ov.option_type.presentation}: #{ov.presentation}"
      end

      values.to_sentence({:words_connector => ", ", :two_words_connector => ", "})
    end


    private
    def find_or_create_plastification(option_value_name, presentation, option_type)
      return if presentation.blank?
      ov = self.option_values.find_by_name option_value_name
      if ov
        ov.presentation=presentation
        ov.save
      else
        self.option_values.create :name => option_value_name,
                                  :presentation => presentation,
                                  :option_type_id => option_type.id
      end
    end

    def set_active
      self.active = true
      self.on_hand = 1
    end
  end
end