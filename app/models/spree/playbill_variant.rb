module Spree
  class PlaybillVariant < FlayerVariant
    belongs_to :playbill_product, :class_name => Spree::PlaybillProduct, :foreign_key => "product_id"
  end
end