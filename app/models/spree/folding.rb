# PIEGATURA
module Spree
  class Folding < OptionValue
    after_save :associate_option_type
    after_update :update_variant
    
    has_many :folding_weights, :class_name => "Spree::FoldingWeight", :foreign_key => "option_value_id", :dependent => :nullify
    has_many :folding_formats, :class_name => "Spree::FoldingFormat", :foreign_key => "option_value_id", :dependent => :delete_all

    attr_accessible :name, :hourly_cost, :folding_weights_attributes, :folding_weights, :start_time, :active,
                    :plastification, :average_hourly_plastification, :external, :min_limit_folding, :cost_folding,
                    :copy_cost_plastification

    validates :name, :presence => true

    accepts_nested_attributes_for :folding_weights, :allow_destroy => true
    
    scope :active, lambda { where("active IS NOT NULL AND active = 1") }
    
    def plastification
      self.is_cut
    end
    
    def external
      self.copy_cost_seam
    end
    
    def average_hourly_plastification
      self.average_hourly_print_c
    end
    
    def copy_cost_plastification
      self.average_hourly_print_bw
    end
    
    def hourly_cost
      self.start_up_cost_c
    end

    def start_time
      self.start_up_cost_bw
    end
    
    def cost_folding
      self.plant_color_bw
    end

    def min_limit_folding
      self.plant_color_c
    end
    
    def plastification= value
      self.is_cut = value
    end
    
    def external= value
      self.copy_cost_seam = value
    end
    
    def average_hourly_plastification= value
      self.average_hourly_print_c = value
    end
    
    def copy_cost_plastification= value
      self.average_hourly_print_bw = value
    end
    
    def hourly_cost= value
      self.start_up_cost_c = value
    end
    
    def start_time= value
      self.start_up_cost_bw = value
    end
    
    def cost_folding= value
      self.plant_color_bw = value
    end

    def min_limit_folding= value
      self.plant_color_c = value
    end
    
    def update_variant
      if !active
        tmp = Spree::FoldingFormat.find_all_by_option_value_id(self.id)
        if !tmp.empty?
          Spree::FoldingFormat.delete_all("option_value_id = #{self.id}")
        end
      else
        tmp = Spree::FoldingFormat.find_all_by_option_value_id(self.id)
        if tmp.empty?
          Spree::FoldingVariant.all.each do |fv|
            ff = Spree::FoldingFormat.create(:option_value_id => self.id, :variant_id => fv.id)
            ff.generate_paper_products
            fv.option_values << ff
          end
        end
      end
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('folding').id) if self.option_type.nil?
    end
  end
end