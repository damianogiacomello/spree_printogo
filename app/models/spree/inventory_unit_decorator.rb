Spree::InventoryUnit.class_eval do
  def self.increase(order, variant, quantity)
    back_order = determine_backorder(order, variant, quantity)
    sold = quantity - back_order
    #create units if configured
    if Spree::Config[:create_inventory_units]
      create_units(order, variant, sold, back_order)
    end
  end

  def self.decrease(order, variant, quantity)
    if Spree::Config[:create_inventory_units]
      destroy_units(order, variant, quantity)
    end
  end

  private
  def self.determine_backorder(order, variant, quantity)
    0
  end
  
  def self.destroy_units(order, variant, quantity)
    variant_units = order.inventory_units.group_by(&:variant_id)
    return unless variant_units.include?(variant.id)

    variant_units = variant_units[variant.id].reject do |variant_unit|
      variant_unit.state == 'shipped'
    end.sort_by(&:state)

    quantity.times do
      inventory_unit = variant_units.shift
      inventory_unit.destroy
    end
  end
  
  def self.create_units(order, variant, sold, back_order)
    return if back_order > 0 && !Spree::Config[:allow_backorders]
    shipment = order.shipments.detect { |shipment| !shipment.shipped? }
    attr_accessible :shipment
    sold.times { order.inventory_units.create({:variant => variant, :state => 'sold', :shipment => shipment}, :without_protection => true) }
    back_order.times { order.inventory_units.create({:variant => variant, :state => 'backordered', :shipment => shipment}, :without_protection => true) }
  end
  
  def restock_variant
    #variant.on_hand = (variant.on_hand + 1)
    #variant.save
  end
end