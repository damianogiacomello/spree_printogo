Spree::BaseHelper.module_eval do
  def meta_data_tags
    object = instance_variable_get('@'+controller_name.singularize)
    meta = { :keywords => Spree::Config[:default_meta_keywords], :description => Spree::Config[:default_meta_description] }

    if object.blank? && %w(banner canvas paperback staple business_card folding spiral flayer poster poster_flyer simple_product advanced_product sticker staple folding letterhead playbill spiral pvc_sticker).include?(controller_name.singularize)
      object = @product
    end
    
    if object.kind_of?(ActiveRecord::Base)
      meta[:keywords] = object.meta_keywords if object[:meta_keywords].present?
      meta[:description] = object.meta_description if object[:meta_description].present?
    end

    meta.map do |name, content|
      tag('meta', :name => name, :content => content)
    end.join("\n")
  end
  
  def retrive_option_calc(calc)
    calc.collect do |k, v|
      if !%w(total shipping_method shipping_date_adjustment promo total_whitout_date total_with_vat_and_shipping total_vat total_filecheck line_item_promotion).include?(k.to_s)
        content_tag(:p, "#{t(k.to_sym)}: #{v}", :class => "small").html_safe
      end
    end.join("")
  end
  
  def product_taxon_link(product)
    if product.class == Spree::Taxon
      seo_url(product)
    else
      case product.type
        when "Spree::BusinessCardProduct"
          business_card_path(product)
        when "Spree::FlayerProduct"
          flayer_product_path(product)
        when "Spree::LetterheadProduct"
          letterhead_path(product)
        when "Spree::PostcardProduct"
          postcard_path(product)
        when "Spree::PosterProduct"
          poster_path(product)
        when "Spree::PlaybillProduct"
          playbill_path(product)
        when "Spree::PosterFlyerProduct"
          poster_path(product)
        when "Spree::PosterHighQualityProduct"
          poster_path(product)
        when "Spree::PaperbackProduct"
          paperback_path(product)
        when "Spree::StapleProduct"
          staple_path(product)
        when "Spree::FoldingProduct"
          folding_path(product)
        when "Spree::BannerProduct"
          banner_path(product)
        when "Spree::PvcProduct"
          pvc_sticker_path(product)
        when "Spree::SpiralProduct"
          spiral_path(product)
        when "Spree::CanvasProduct"
          canvase_path(product)
        when "Spree::RigidProduct"
          rigid_path(product)
        when "Spree::StickerProduct"
          sticker_path(product)
        when "Spree::SimpleProduct"
          simple_product_path(product)
        when "Spree::AdvancedProduct"
          advanced_product_path(product)
      end
    end
  end
  
  def business_card_link
    business_card_path(Spree::BusinessCardProduct.last)
  end

  def flayer_link
    flayer_product_path(Spree::FlayerProduct.only_flyer.last)
  end
  
  def letterhead_link
    letterhead_product_path(Spree::LetterheadProduct.last)
  end
  
  def postcard_link
    postcard_product_path(Spree::PostcardProduct.last)
  end
  
  def playbill_link
    playbill_product_path(Spree::PlaybillProduct.last)
  end

  def poster_link
    posters_path()
  end

  def paperback_link
    paperbacks_path()
  end

  def staple_link
    staple_path(Spree::StapleProduct.last)
  end
  
  def folding_link
    folding_path(Spree::FoldingProduct.last)
  end
  
  def banner_link
    banner_path(Spree::BannerProduct.only_banner.last)
  end
  
  def pvc_link
    pvc_sticker_path(Spree::PvcProduct.last)
  end
  
  def spiral_link
    spiral_path(Spree::SpiralProduct.last)
  end
  
  def canvas_link
    canvase_path(Spree::CanvasProduct.last)
  end
  
  def rigid_link
    rigid_path(Spree::RigidProduct.last)
  end
  
  def sticker_link
    sticker_path(Spree::StickerProduct.last)
  end

  def tot_iva(price)
    ((price * 21) / 100.to_f)
  end

  def number_of_copy(product)
    a = []
    product.variants.sort { |c, b| c.number_of_copy.name.to_i <=> b.number_of_copy.name.to_i }.each do |v|
      tmp = a.select { |e| e[:name] == v.number_of_copy.presentation }
      if tmp.count == 1
        tmp[0][:values] << v.number_of_copy_option_values.id
      else
        a << {:name => v.number_of_copy.presentation, :values => [v.number_of_copy_option_values.id]}
      end
    end
    a
  end

  def delivery_date_from_today(shipping_data_adj)
    d = shipping_data_adj.days.business_day.from_now
    l(d, :format => '%A %d/%m/%Y')
  end
  
  # human readable list of variant options
  def variant_line_items_options(v)
    list = v.map do |lv|
      content_tag(:span, "( #{lv.option_value.option_type.presentation rescue ""}: #{lv.option_value.presentation rescue ""} )", :class => 'out-of-stock')
    end
    raw(list.join("<br />"))
  end

  def variant_line_items_options_presentation(v)
    list = v.map do |lv|
      content_tag(:span, "( #{lv.option_value.option_type.presentation rescue ""} #{lv.option_value.name rescue ""} #{lv.option_value.presentation rescue ""} )", :class => 'out-of-stock')
    end
    raw(list.join("<br />"))
  end

 # ======================= helper per homepage elements =======================

  def hp_link(homepage_element)
    if homepage_element.url.blank? && !homepage_element.homepageble.blank?
      if homepage_element.homepageble.class == Spree::BusinessCardProduct 
        return business_card_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::FlayerProduct
        return flayer_product_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::LetterheadProduct
        return letterhead_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PostcardProduct
        return postcard_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PosterProduct
        return poster_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PlaybillProduct
        return playbill_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PosterFlyerProduct
        return poster_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PosterHighQualityProduct
        return poster_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PaperbackProduct
        return paperback_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::StapleProduct
        return staple_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::FoldingProduct
        return folding_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::BannerProduct
        return banner_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::PvcProduct
        return pvc_sticker_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::SpiralProduct
        return spiral_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::CanvasProduct
        return canvase_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::RigidProduct
        return rigid_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::StickerProduct
        return stickers_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::SimpleProduct
        return simple_product_path(homepage_element.homepageble)
      elsif homepage_element.homepageble.class == Spree::AdvancedProduct
        return advanced_product_path(homepage_element.homepageble)
      end
      return seo_url(homepage_element.homepageble) if homepage_element.homepageble_type == "Spree::Taxon"
      return link_promo(homepage_element.homepageble.product, homepage_element.homepageble.promo_code) if homepage_element.homepageble_type == "Spree::LineItemPromotion"
    end
    return homepage_element.url
  end

  def hp_title(homepage_element)
    if homepage_element.title.blank?
      if homepage_element.homepageble_type == "Spree::Product"
        return homepage_element.homepageble.name
      elsif homepage_element.homepageble_type == "Spree::Taxon"
        return homepage_element.homepageble.name
      #elsif homepage_element.homepageble_type == "Spree::LineItemPromotion"
      #  return "Offerta #{homepage_element.homepageble.number_of_copy} #{homepage_element.homepageble.product.name}" 
      end
    end
    return homepage_element.title
  end

  def hp_description(homepage_element)
    if homepage_element.description.blank? 
      if homepage_element.homepageble_type == "Spree::Product"
        return homepage_element.homepageble.description
      elsif homepage_element.homepageble_type == "Spree::Taxon" 
        return homepage_element.homepageble.description
      #elsif homepage_element.homepageble_type == "Spree::LineItemPromotion" 
      #  return homepage_element.homepageble.product.description
      end         
    end
    return homepage_element.description 
  end

  def hp_image(homepage_element)
    if !homepage_element.attachment.exists?
      if homepage_element.homepageble_type == "Spree::Product"
        return homepage_element.homepageble.logo_image
      elsif homepage_element.homepageble_type == "Spree::LineItemPromotion" 
        return homepage_element.homepageble.product.logo_image
      elsif homepage_element.homepageble_type == "Spree::Taxon" && homepage_element.homepageble.icon.present?
        return homepage_element.homepageble.icon.url(:box)
      end
    end
    return homepage_element.attachment.url(:normal)
  end

 # =======================FINE helper per homepage elements =======================

  def flash_messages(opts = {})  
    opts[:ignore_types] = [:commerce_tracking, :conversion].concat(opts[:ignore_types] || [])

    inutile = true
    flash.each do |msg_type, text|
      inutile = false if !opts[:ignore_types].include?(msg_type)
    end
    return nil if inutile
    
    html = escape_javascript("<div class='notification_container'><div id='basic-template'><a class='ui-notify-cross ui-notify-close' href='#'>x</a><h1>\#{title}</h1><p>\#{text}</p></div></div>")
    js= "$('body').append(\"#{raw html}\");"
    js<< '$(".notification_container").notify();'
    flash.each do |msg_type, text|
      unless opts[:ignore_types].include?(msg_type)
        js<<"$('.notification_container').notify('create', { title: ' #{msg_type.to_s.titleize} ', text: ' #{text} '});"
      end
    end

    return javascript_tag(js)
  end

  def flash_messages_js(opts = {})  
    opts[:ignore_types] = [:commerce_tracking, :conversion].concat(opts[:ignore_types] || [])

    inutile = true
    flash.each do |msg_type, text|
      inutile = false if !opts[:ignore_types].include?(msg_type)
    end
    return nil if inutile
    
    html = escape_javascript("<div class='notification_container'><div id='basic-template'><a class='ui-notify-cross ui-notify-close' href='#'>x</a><h1>\#{title}</h1><p>\#{text}</p></div></div>")
    js = "$('body').append(\"#{raw html}\");"
    js << '$(".notification_container").notify();'
    flash.each do |msg_type, text|
      unless opts[:ignore_types].include?(msg_type)
        js << "$('.notification_container').notify('create', { title: ' #{msg_type.to_s.titleize} ', text: ' #{text} '});"
      end
    end

    return raw(js)
  end

  #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  def product_taxon_name(product)
    case product.type
      when "Spree::BusinessCardProduct"
        "Piccolo formato"
      when "Spree::FlayerProduct"
        "Piccolo formato"
      when "Spree::LetterheadProduct"
        "Piccolo formato"
      when "Spree::PostcardProduct"
        "Piccolo formato"
      when "Spree::PosterProduct"
        "Grande formato"
      when "Spree::PlaybillProduct"
        "Piccolo formato"
      when "Spree::PosterFlyerProduct"
        "Grande formato"
      when "Spree::PosterHighQualityProduct"
        "Grande Formato"
      when "Spree::PaperbackProduct"
        "Multipagina"
      when "Spree::StapleProduct"
        "Multipagina"
      when "Spree::FoldingProduct"
        "Piccolo formato"
      when "Spree::BannerProduct"
        "Grande formato"
      when "Spree::PvcProduct"
        "Grande formato"
      when "Spree::SpiralProduct"
        "Multipagina"
      when "Spree::CanvasProduct"
        "Grande formato"
      when "Spree::SimpleProduct"
        "Prodotti Semplici"
      when "Spree::AdvancedProduct"
        "Prodotti Avanzati"
    end
  end

  def product_taxon_parents_name(product)
    case product.type
      when "Spree::PosterProduct"
        "Manifesti"
      when "Spree::PosterFlyerProduct"
        "Manifesti"
      when "Spree::PosterHighQualityProduct"
        "Manifesti"
      when "Spree::PaperbackProduct"
        "Brossure"
    end
  end

  def product_taxon_parents_link(product)
    case product.type
      when "Spree::PosterProduct"
        posters_path()
      when "Spree::PosterFlyerProduct"
        posters_path()
      when "Spree::PosterHighQualityProduct"
        posters_path()
      when "Spree::PaperbackProduct"
        paperbacks_path()
    end
  end

  def breadcrumbs(taxon, product, products, separator="&nbsp;&raquo;&nbsp;")
    return "" if current_page?("/")
    separator = raw(separator)
    crumbs = [content_tag(:li, link_to(t(:home) , root_path))]
    if taxon
      crumbs << taxon.ancestors.collect { |ancestor| content_tag(:li, link_to(ancestor.name , seo_url(ancestor))) } unless taxon.ancestors.empty?
      #Nome della taxon in cui sono
      #crumbs << content_tag(:li, link_to(taxon.name , seo_url(taxon)))
    elsif (!@products.blank?)
        crumbs << content_tag(:li, product_taxon_name(@products.first()))
        crumbs << content_tag(:li, product_taxon_parents_name(@products.first()))
    elsif (!@product.blank?)
        crumbs << content_tag(:li, product_taxon_name(@product))
        crumbs << content_tag(:li, link_to(product_taxon_parents_name(@product), product_taxon_parents_link(@product))) if !product_taxon_parents_name(@product).blank?
        #crumbs << content_tag(:li, @product.name)
    end
    crumb_list = content_tag(:ul, raw(crumbs.flatten.map{|li| li.mb_chars}.join), :class => 'breadcrumbs')

  end

  #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  def delivery_date_from_today_promo(promo)
    l(promo.shipping_date.to_date, :format => '%A %d/%m/%Y') if !promo.shipping_date.blank?
  end

end

