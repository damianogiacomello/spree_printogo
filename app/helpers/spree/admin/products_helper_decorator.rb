Spree::Admin::ProductsHelper.module_eval do
  def product_link_edit(product)
    case product.type
      when "Spree::BusinessCardProduct"
        admin_business_cards_edit_path(product)
      when "Spree::PosterProduct"
        admin_posters_edit_path(product)
      when "Spree::PosterFlyerProduct"
        admin_poster_flyers_edit_path(product)
      when "Spree::PosterHighQualityProduct"
        admin_poster_high_qualities_edit_path(product)
      when "Spree::FlayerProduct"
        admin_flayers_edit_path(product)
      when "Spree::LetterheadProduct"
        admin_letterheads_edit_path(product)
      when "Spree::PlaybillProduct"
        admin_playbills_edit_path(product)
      when "Spree::PostcardProduct"
        admin_postcards_edit_path(product)
      when "Spree::PaperbackProduct"
        admin_paperbacks_edit_path(product)
      when "Spree::StapleProduct"
        admin_staples_edit_path(product)
      when "Spree::FoldingProduct"
        admin_foldings_edit_path(product)
      when "Spree::BannerProduct"
        admin_banners_edit_path(product)
      when "Spree::PvcProduct"
        admin_pvces_edit_path(product)
      when "Spree::SpiralProduct"
        admin_spirals_edit_path(product)
      when "Spree::CanvasProduct"
        admin_canvases_edit_path(product)
      when "Spree::RigidProduct"
        admin_rigids_edit_path(product)
      when "Spree::StickerProduct"
        admin_stickers_edit_path(product)
      else
        "javascript: void(0)"
    end
  end
end