Spree::Admin::NavigationHelper.module_eval do
  def flash_alert_html()
    js = []
    flash.each do |msg_type, text|
      unless opts[:ignore_types].include?(msg_type)
        js << "toastr.#{msg_type}(\"#{raw text}\");"
      end
    end
    javascript_tag(js.join())
  end
  
  def alert_html(target)
    if target && target.errors.any?
      html = content_tag(:div, content_tag(:button, "x", :class => "close", :type => "button", 'data-dismiss' => "alert") + content_tag(:strong, target.errors.full_messages.collect{|msg| msg}.join(", ").html_safe), :class => "target alert alert-block alert-error fade in").html_safe
      return javascript_tag("$('#pageContent .container').append(\"#{j(html)}\"); $('.target').alert();")
    end
  end
  
  def alert_js(target)
    if target && target.errors.any?
      html = content_tag(:div, content_tag(:button, "x", :class => "close", :type => "button", 'data-dismiss' => "alert") + content_tag(:strong, target.errors.full_messages.collect{|msg| msg}.join(", ").html_safe), :class => "target alert alert-block alert-error fade in").html_safe
      return "$('#pageContent .container').prepend(\"#{j(html)}\"); $('.target').alert();".html_safe
    end
  end
  
  def flash_alert_js()
    html = content_tag(:div, content_tag(:button, "x", :class => "close", :type => "button", 'data-dismiss' => "alert") + content_tag(:strong, flash.collect{|name, msg| msg}.join(", ").html_safe), :class => "flash alert alert-block alert-error fade in").html_safe
    return "$('#pageContent .container').prepend(\"#{j(html)}\"); $('.flash').alert();".html_safe
  end
  
  def tab(*args)
    options = {:label => args.first.to_s}
    if args.last.is_a?(Hash)
      options = options.merge(args.pop)
    end
    options[:route] ||=  "admin_#{args.first}"

    destination_url = options[:url] || spree.send("#{options[:route]}_path")

    titleized_label = t(options[:label], :default => options[:label]).titleize
    if options[:dropdown]
      titleized_label += '<b class="caret"></b>'
      link = link_to(titleized_label.html_safe, "#", :class => "dropdown-toggle", :data => {:toggle => "dropdown"})
      link += content_tag(:ul, (options[:dropdown].collect {|d| content_tag(:li, link_to(t(d, :default => d).titleize, spree.send("admin_#{d}_path")))}.join("")).html_safe, :class => "dropdown-menu pull-right")
    else
      link = link_to(titleized_label, destination_url)
    end

    css_classes = []

    selected = if options[:match_path]
      # TODO: `request.fullpath` for engines mounted at '/' returns '//'
      # which seems an issue with Rails routing.- revisit issue #910
      request.fullpath.gsub('//', '/').starts_with?("#{root_path}admin#{options[:match_path]}")
    else
      args.include?(controller.controller_name.to_sym)
    end
    css_classes << 'active' if selected

    if options[:css_class]
      css_classes << options[:css_class]
    end
    content_tag('li', link, :class => css_classes.join(' '))
  end
  
  def link_to_clone(resource, options={})
    link_to_with_icon('exclamation', t(:clone), clone_admin_product_url(resource), options)
  end

  def link_to_new(resource)
    link_to_with_icon('add', t(:new), edit_object_url(resource))
  end

  def link_to_edit(resource, options={})
    link_to("#{t(:edit)} <i class='icon-edit icon-white'></i>".html_safe, edit_object_url(resource), options)
  end
  
  def link_to_show_bootstrap_url(url, options={})
    link_to("<i class='icon-pencil'></i> #{t(:show)}".html_safe, url, options)
  end
  
  def link_to_edit_bootstrap_url(url, options={})
    link_to("<i class='icon-edit'></i> #{t(:edit)}".html_safe, url, options)
  end
  
  def link_to_personalized_bootstrap_url(url, options={})
    link_to("<i class='#{options[:icon]}'></i> #{options[:title]}".html_safe, url, :title => options[:title], :class => options[:class])
  end

  def link_to_edit_url(url, options={})
    link_to("#{t(:edit)} <i class='icon-edit icon-white'></i>".html_safe, url, options)
  end

  def link_to_clone(resource, options={})
    link_to_with_icon('exclamation', t(:clone), clone_admin_product_url(resource), options)
  end

  def link_to_delete(resource, options={})
    url = options[:url] || object_url(resource)
    name = options[:name] || t(:delete)
    link_to("#{name} <i class='icon-remove icon-white'></i>".html_safe, url, :class => "delete-resource btn btn-danger", :data => { :confirm => t(:are_you_sure) })
  end
  
  def link_to_delete_with_icon(resource, options={})
    url = options[:url] || object_url(resource)
    name = options[:name] || icon('cross')
    link_to name, url,
    :class => "delete-resource",
    :data => { :confirm => t(:are_you_sure) }
  end
  
  def link_to_add_fields(name, f, association, control = false)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\", true)")
  end

  def link_to_add_fields_with_limit(name, f, limit, association, control = false)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields_with_limit(this, \"#{association}\", \"#{limit}\", \"#{escape_javascript(fields)}\", true)")
    #link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\", true)")
  end

  def link_to_add_efields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_efields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end
  
  def link_to_confirm_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "confirm_remove_fields(this)")
  end

  def link_to_confirm_remove_fields_with_limit(name, f, limit)
    f.hidden_field(:_destroy) + link_to_function(name, "confirm_remove_fields_with_limit(this, \"#{limit}\")")
  end
  
  def default_link_promo(product, promo)
    case product.type
      when "Spree::BusinessCardProduct"
        "#{Spree::Config[:site_url]}/business_cards/#{product.permalink}/#{promo}"
      when "Spree::PosterProduct"
        "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
      when "Spree::PosterFlyerProduct"
        "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
      when "Spree::PosterHighQualityProduct"
        "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
      when "Spree::FlayerProduct"
        "#{Spree::Config[:site_url]}/flayers/#{product.permalink}/#{promo}"
      when "Spree::LetterheadProduct"
        "#{Spree::Config[:site_url]}/letterheads/#{product.permalink}/#{promo}"
      when "Spree::PostcardProduct"
        "#{Spree::Config[:site_url]}/postcards/#{product.permalink}/#{promo}"
      when "Spree::PlaybillProduct"
        "#{Spree::Config[:site_url]}/playbills/#{product.permalink}/#{promo}"
      when "Spree::PaperbackProduct"
        "#{Spree::Config[:site_url]}/paperbacks/#{product.permalink}/#{promo}"
      when "Spree::StapleProduct"
        "#{Spree::Config[:site_url]}/staples/#{product.permalink}/#{promo}"
      when "Spree::FoldingProduct"
        "#{Spree::Config[:site_url]}/foldings/#{product.permalink}/#{promo}"
      when "Spree::BannerProduct"
        "#{Spree::Config[:site_url]}/banners/#{product.permalink}/#{promo}"
      when "Spree::PvcProduct"
        "#{Spree::Config[:site_url]}/pvc_stickers/#{product.permalink}/#{promo}"
      when "Spree::SpiralProduct"
        "#{Spree::Config[:site_url]}/spirals/#{product.permalink}/#{promo}"
      when "Spree::CanvasProduct"
        "#{Spree::Config[:site_url]}/canvases/#{product.permalink}/#{promo}"
      when "Spree::RigidProduct"
        "#{Spree::Config[:site_url]}/rigids/#{product.permalink}/#{promo}"
      when "Spree::StickerProduct"
        "#{Spree::Config[:site_url]}/stickers/#{product.permalink}/#{promo}"
      else
        "javascript: void(0)"
    end
  end
end