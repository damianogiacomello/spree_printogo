module Spree
  module Admin
    module PaperTypesHelper
      def can_delete?(paper)
        can = false
        if Spree::LineItemsOptionValues.where(:option_value_id => paper.id).count == 0
          can = true
          paper.values.each do |v|
            unless Spree::LineItemsOptionValues.where(:option_value_id => paper.id).count == 0
              can = false
            end
          end
        end
        can
      end
      
      def print_paper_weight(paper)
        paper.paper_weights.collect {|pw| content_tag(:span, "#{pw.name} Gr.", :class => "badge #{(pw.paper_products.any?{|pp| !pp.product_id.blank?}) ? "badge-important" : "badge-inverse"}")}.join(" ")
      end
    end
  end
end
