# encoding: UTF-8
Spree::SlideshowTypesHelper.module_eval do
  def insert_slideshow(params={})
    params[:id] ||= "slides"
    params[:class] ||= "my_slide"
    params[:category] ||= "home"
    @@slideshow = Spree::SlideshowType.enable(params[:category]).try(:first)
    if @@slideshow.blank? || (!@@slideshow.blank? && @@slideshow.slides.empty?)
      return ''
    end
    params[:pagination_class] ||= "pagination"
    params[:slide_speed] ||= 200
    res = []

    res << content_tag(:div, content_tag(:div, slide_images(params, @@slideshow), :class => "slides_container"), :id => params[:id], :class => params[:class])
    res << "<script type='text/javascript'>
    $(function() {
      $('##{params[:id]}').slides({
        play: 20000,
        preload: true,
        generateNextPrev: #{@@slideshow.enable_navigation},
        generatePagination: #{@@slideshow.enable_pagination},
        paginationClass: '#{params[:pagination_class]}',
        slideSpeed: #{params[:slide_speed]}
        });
        });
        </script>"
    
    res.join.html_safe
  end
  
  def slide_images(params, slideshow)
    params[:style] ||= "custom"
    params[:show_content] ||= false
    max = slideshow.slides.enable.count
    slides = slideshow.slides.enable.limit(max).sort_by { |slide| slide.position }

    slides.map do |slide|
      content_tag(:div, :class => "slide_list") do
        divs = []

        divs << link_to(image_tag(slide.attachment.url(params[:style].to_sym)), (slide.url.blank? ? "javascript: void(0)" : slide.url), { :title => slide.title })
        if params[:show_content]
          if slide.promotion && slide.promotion.is_available?
            decrement_quantity = (slide.promotion.quantity > 0) ? content_tag(:div, "#{slide.promotion.quantity} pz. rimanenti", :class => "promo-pz") : ""
            timer_show = (slide.promotion.expires_at.blank?) ? "" : content_tag(:div, "", :id => "timer-#{slide.promotion.promo_code}") + content_tag(:div, content_tag(:ul, %w"giorni ore min sec".collect{|l| content_tag(:li, l)}.join().html_safe), :class => "label-time clearfix")
            title_promo = (slide.title.blank?) ? "<strong>OFFERTA #{slide.promotion.number_of_copy} #{slide.promotion.product.name.upcase}</strong>" : "<strong>#{slide.title.upcase}</strong>"
            divs << content_tag(:div, content_tag(:div, title_promo.html_safe, :class => "title-holder") + content_tag(:p, raw("#{number_to_currency(slide.promotion.price)} <em>#{number_to_currency(slide.promotion.original_price)}</em>")) + content_tag(:div, decrement_quantity.html_safe + timer_show.html_safe + link_to(content_tag(:span, "Vai all'offerta!"), link_promo(slide.promotion.product, slide.promotion.promo_code), :class => "btn-promo"), :class => "promo-content"), :class => "text-holder")
            if !slide.promotion.expires_at.blank?
            divs << "<script type='text/javascript'>
            $(function() {
          		var b = new Date(#{slide.promotion.expires_at.strftime("%Y")}, #{slide.promotion.expires_at.month - 1}, #{slide.promotion.expires_at.strftime("%d")}, 0, 0, 0);
          		$('#timer-#{slide.promotion.promo_code}').jCountdown({
          			timeText: b.getFullYear() + '/' + parseInt(b.getMonth() + 1) + '/' + b.getDate() + ' 23:00:00',
          			timeZone: new Date(),
          			style: 'slide',
          			color: 'black',
          			textSpace: 0,
          			reflection: false,
          			textGroupSpace: 0,
          			displayLabel: false
          		});
          	});
          	</script>"
            end
          else
            if !slide.title.blank? && !slide.content.blank?
              divs << content_tag(:div, content_tag(:strong, raw(slide.title.upcase), :class => "strong-title-holder") + content_tag(:p, raw(slide.content)), :class => "text-holder slider")
            end
          end
        end

        divs.join.html_safe
      end
    end.join.html_safe
  end

  def link_promo(product, promo)
    case product.type
    when "Spree::BusinessCardProduct"
      "#{Spree::Config[:site_url]}/business_cards/#{product.permalink}/#{promo}"
    when "Spree::PosterProduct"
      "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
    when "Spree::PosterFlyerProduct"
      "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
    when "Spree::PosterHighQualityProduct"
      "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
    when "Spree::FlayerProduct"
      "#{Spree::Config[:site_url]}/flayers/#{product.permalink}/#{promo}"
    when "Spree::LetterheadProduct"
      "#{Spree::Config[:site_url]}/letterheads/#{product.permalink}/#{promo}"
    when "Spree::PostcardProduct"
      "#{Spree::Config[:site_url]}/postcards/#{product.permalink}/#{promo}"
    when "Spree::PlaybillProduct"
      "#{Spree::Config[:site_url]}/playbills/#{product.permalink}/#{promo}"
    when "Spree::PaperbackProduct"
      "#{Spree::Config[:site_url]}/paperbacks/#{product.permalink}/#{promo}"
    when "Spree::StapleProduct"
      "#{Spree::Config[:site_url]}/staples/#{product.permalink}/#{promo}"
    when "Spree::FoldingProduct"
      "#{Spree::Config[:site_url]}/foldings/#{product.permalink}/#{promo}"
    when "Spree::BannerProduct"
      "#{Spree::Config[:site_url]}/banners/#{product.permalink}/#{promo}"
    when "Spree::PvcProduct"
      "#{Spree::Config[:site_url]}/pvc_stickers/#{product.permalink}/#{promo}"
    when "Spree::SpiralProduct"
      "#{Spree::Config[:site_url]}/spirals/#{product.permalink}/#{promo}"
    when "Spree::CanvasProduct"
      "#{Spree::Config[:site_url]}/canvases/#{product.permalink}/#{promo}"
    when "Spree::RigidProduct"
      "#{Spree::Config[:site_url]}/rigids/#{product.permalink}/#{promo}"
    when "Spree::StickerProduct"
      "#{Spree::Config[:site_url]}/stickers/#{product.permalink}/#{promo}"
    else
      "javascript: void(0)"
    end
  end
end