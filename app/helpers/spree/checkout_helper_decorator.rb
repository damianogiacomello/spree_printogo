Spree::CheckoutHelper.module_eval do

  def checkout_breadcrumbs(order)
    curretn_index = checkout_states.index(order.state)

    items = checkout_states.map do |step|
      css_classes = []
      state_index = checkout_states.index(step)
      if state_index < curretn_index
        text = link_to t(step), checkout_state_path(step)
      elsif state_index == curretn_index
        text = content_tag :span, t(step)
        css_classes << "current-state"
      else
        text = content_tag :span, t(step)
      end
      content_tag(:li, raw(text),:class => css_classes.join('-')) + (  step != checkout_states.last ? content_tag(:li,">") : ''  )
      #content_tag(:li, content_tag(:span, "#{" > " if step != checkout_states.last}" ),:class => css_classes.join('-'))
    end
    content_tag('ol', raw(items.join), :class => "breadcrumbs-ol clearfix")
  end

  def checkout_states
    %w(address upload_image payment)
  end

end