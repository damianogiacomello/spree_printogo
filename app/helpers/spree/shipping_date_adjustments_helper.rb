module Spree
  module ShippingDateAdjustmentsHelper
    def delivery_date(order, shipping_data_adj)
      d = shipping_data_adj.days.day
      l(d.business_days.after(order.updated_at), :format => '%A %d/%m/%Y')
    end
  end
end

