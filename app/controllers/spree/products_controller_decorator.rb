Spree::ProductsController.class_eval do
  def load_product
    @product = Spree::Product.active.find_by_permalink!(params[:id])
  end

  def show
    if @product.blank?
      flash[:notice] = t(:product_not_active)
      redirect_to root_path
      return false
    end
    
    case @product.type
      when "Spree::BusinessCardProduct"
        @variants = Spree::BusinessCardVariant.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PosterProduct"
        @variants = Spree::PosterProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PosterHighQualityProduct"
        @variants = Spree::PosterHighQualityProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::LetterheadProduct"
        @variants = Spree::LetterheadProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PostcardProduct"
        @variants = Spree::PostcardProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PlaybillProduct"
        @variants = Spree::PlaybillProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::FlayerProduct"
        @variants = Spree::FlayerProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PosterFlyerProduct"
        @variants = Spree::PosterFlyerProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::StapleProduct"
        @variants = Spree::StapleProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PaperbackProduct"
        @variants = Spree::PaperbackProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::FoldingProduct"
        @variants = Spree::FoldingProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::BannerProduct"
        @variants = Spree::BannerProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::PvcProduct"
        @variants = Spree::PvcProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::SpiralProduct"
        @variants = Spree::SpiralProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::CanvasProduct"
        @variants = Spree::CanvasProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::RigidProduct"
        @variants = Spree::RigidProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      when "Spree::StickerProduct"
        @variants = Spree::StickerProduct.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
    end

    @product_properties = Spree::ProductProperty.includes(:property).where(:product_id => @product.id)
    referer = request.env['HTTP_REFERER']
    if referer
      referer_path = URI.parse(request.env['HTTP_REFERER']).path
      if referer_path && referer_path.match(/\/t\/(.*)/)
        @taxon = Spree::Taxon.find_by_permalink($1)
      end
    end
    @title = @product.seo_title
    respond_with(@product)
  end
  
  def download_template
    begin
      variant = Spree::PosterPrinterFormat.find_by_id(params[:id]) || Spree::PosterHighQualityPrinterFormat.find_by_id(params[:id]) || Spree::RigidPrinterFormat.find_by_id(params[:id]) || Spree::StickerPrinterFormat.find_by_id(params[:id]) || Spree::Variant.find_by_id(params[:id]) || Spree::AdvancedVariant.find_by_id(params[:id]) || Spree::AdvancedProduct.find_by_permalink!(params[:id]) || Spree::Product.find_by_permalink!(params[:id])
      if variant.template.present?
        send_file(variant.template.attachment.path, :filename => variant.template.attachment_file_name, :type => variant.template.attachment_content_type)
      else
        if (variant.class == Spree::Variant) || (variant.class == Spree::FlayerVariant) || (variant.class == Spree::StapleVariant) || (variant.class == Spree::PaperbackVariant) || (variant.class == Spree::PosterFlyerVariant) || (variant.class == Spree::FoldingVariant) || (variant.class == Spree::PostcardVariant) || (variant.class == Spree::LetterheadVariant) || (variant.class == Spree::PlaybillVariant) || (variant.class == Spree::StickerVariant)
          if variant.product.template.present?
            send_file(variant.product.template.attachment.path, :filename => variant.product.template.attachment_file_name, :type => variant.product.template.attachment_content_type)
          else
            send_file(Spree::Template.find_by_default(true).attachment.path, :filename => "default_template", :type => 'application/pdf')
          end
        else
          send_file(Spree::Template.find_by_default(true).attachment.path, :filename => "default_template", :type => 'application/pdf')
        end
      end
    rescue Exception => e
      logger.info "+=+=+=+=+=+=+=+=+=+=+=+=+=+= #{e} +=+=+=+=+=+=+=+=+=+=+=+=+=+="
      send_file(Spree::Template.find_by_default(true).attachment.path, :filename => "default_template", :type => 'application/pdf')
    end
  end
end