module Spree
  module Admin
    class FoldingVariantsController < Spree::Admin::BaseController
      respond_to :html, :js

      def index
        @product = Spree::FoldingProduct.find_by_permalink(params[:folding_id])
        @variants = @product.folding_variants
      end

      def new
        @product = Spree::FoldingProduct.find_by_permalink params[:folding_id]
        @variant = @product.folding_variants.create()
      end
      
      def create
        @product = Spree::FoldingProduct.find_by_permalink params[:folding_id]
        @variant = @product.folding_variants.find(params[:variant_id])
        @variant.update_attribute(:sku, params[:variant][:format])
        limit = params[:variant].delete(:limit)
        folding_formats = params[:variant].delete(:folding_formats_attributes)

        if @variant.limit.update_attributes(limit) && @variant.update_attributes(params[:variant]) && update_folding_formats(folding_formats)
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def edit
        @product = Spree::FoldingProduct.find_by_permalink params[:folding_id]
        @variant = @product.folding_variants.find(params[:id])
      end

      def update
        @product = Spree::FoldingProduct.find_by_permalink params[:folding_id]
        @variant = @product.folding_variants.find(params[:variant_id])
        limit = params[:variant].delete(:limit)
        folding_formats = params[:variant].delete(:folding_formats_attributes)
        img = params[:variant].delete(:image_attachment)
        viewable = params[:variant].delete(:image_viewable_id)
        if @variant.limit.update_attributes(limit) && @variant.update_attributes(params[:variant]) && update_folding_formats(folding_formats)
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def destroy
        @product = Spree::FoldingProduct.find_by_permalink params[:folding_id]
        @variant = @product.folding_variants.find params[:id]
        @variant.paper_products.clear
        @variant.destroy
        flash[:notice] = t(:delete_success)
        redirect_to :action => :index
      end
      
      private
      def update_folding_formats(folding_formats)
        folding_formats.each_with_index do |ff, i|
          sff = Spree::FoldingFormat.find(ff[1]["id"])
					sff.paper_products.destroy_all if !sff.paper_products.blank?
          
          if !ff[1]["paper_weights"].blank? && !ff[1]["papers"].blank?
            ff[1]["paper_weights"].each do |weight|
              ff[1]["papers"].each do |paper|
                paper_weight = Spree::PaperWeight.find_by_name_and_option_value_id(weight.to_s, Spree::Paper.find(paper).id)
                if paper_weight
                  sff.paper_products.create(:paper_id => paper_weight.paper.id, :weight_id => paper_weight.id, :product_id => sff.folding_variant.id)
                end
              end
            end
          end
          sff.update_attribute(:active, ff[1]["active"])
          sff.update_attribute(:one_orientation, ff[1]["one_orientation"])
        end
        true
      end
    end
  end
end
