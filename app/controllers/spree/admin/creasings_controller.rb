module Spree
  module Admin
    class CreasingsController < Spree::Admin::BaseController
      def edit
        @creasing = Spree::Creasing.find_or_initialize_by_name 'creasing'
      end

      def update
        @creasing = Spree::Creasing.find_or_initialize_by_name 'creasing'
        @creasing.update_attributes(params[:creasing])
        redirect_to :action => :edit
      end

    end
  end
end