module Spree
  module Admin
    class SimpleAttributesController < Spree::Admin::BaseController
      respond_to :html, :js, :json
      before_filter :load_data, :on => :index
      
      def index
        respond_with(@collection)
      end
      
      def new
        @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @attribute = @product.simple_attributes.build
      end
      
      def create
        @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @attribute = @product.simple_attributes.create(params[:simple_attribute])
        if @attribute.valid?
          redirect_to :action => :index
        else
          respond_with(@attribute) { |format| format.js { render :action => :new } }
        end
      end
      
      def edit
        @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @attribute = @product.simple_attributes.find(params[:id])
        respond_with(@product)
      end
      
      def update
        @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @attribute = @product.simple_attributes.find(params[:id])
        if @attribute.update_attributes(params[:simple_attribute])
          redirect_to :action => :index
        else
          respond_with(@attribute) { |format| format.js { render :action => :edit } }
        end
      end
      
      def destroy
        @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @attribute = @product.simple_attributes.find(params[:id])
        @attribute.destroy
      end
      
      protected
      def load_data
        @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @collection = @product.simple_attributes
      end
    end
  end
end