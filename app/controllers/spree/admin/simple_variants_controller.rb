module Spree
  module Admin
    class SimpleVariantsController < ResourceController
      respond_to :html, :js, :json
      
      before_filter :find_resource
      
      def index
        respond_with(@collection)
      end
      
      def new
        @variant = @simple_product.simple_variants.build
      end
      
      def create
        @variant = @simple_product.simple_variants.create(params[:simple_variant])
        if @variant.valid?
          redirect_to :action => :index
        else
          respond_with(@variant) { |format| format.js { render :action => :new } }
        end
      end
      
      def edit
        @variant = @simple_product.simple_variants.find(params[:id])
        respond_with(@simple_product)
      end
      
      def update
        @variant = @simple_product.simple_variants.find(params[:id])
        
        if @variant.update_attributes(params[:simple_variant])
          redirect_to :action => :index
        else
          respond_with(@variant) { |format| format.js { render :action => :new } }
        end
      end
      
      def destroy
        @simple_product = Spree::SimpleProduct.where(:permalink => params[:simple_product_id]).first!
        @variant = @simple_product.simple_variants.find params[:id]
        @variant.destroy
        flash[:notice] = t(:delete_success)
        
        respond_with(@simple_product) do |format|
          format.html { redirect_to collection_url }
          format.js  { render :layout => false }
        end
      end
      
      protected

      def find_resource
        @simple_product = SimpleProduct.find_by_permalink!(params[:simple_product_id])
      end

      def location_after_save
        edit_admin_simple_product_url(@simple_product)
      end
        
      def collection
        return @collection if @collection.present?
        
        @simple_product = Spree::SimpleProduct.find_by_permalink(params[:simple_product_id])
        @collection = @simple_product.simple_variants
      end
    end
  end
end