module Spree
  module Admin
    class FoldingsController < Spree::Admin::BaseController
      def index
        @foldings = Spree::Folding.all
      end

      def update
        @folding = Spree::Folding.find(params[:id])
        @folding.update_attributes(params[:folding])
        if @folding.folding_weights.empty?
          @folding.update_attribute(:active, false)
        end
        
        flash[:notice] = t(:edit_success)
        redirect_to :action => :edit
      end

      def edit
        @folding = Spree::Folding.find(params[:id])
      end

      def destroy
        @folding = Spree::Folding.find(params[:id])
        @folding.destroy
        #flash[:notice] = t(:delete_success)
        #redirect_to :action => :index
        respond_to do |format|
          format.html {redirect_to :action => :index}
          format.js {render :layout => false}
        end
      end

      def new
        @folding = Spree::Folding.new
      end

      def create
        @folding = Spree::Folding.new(params[:folding])
        @folding.save
        
        flash[:notice] = t(:create_success)
        redirect_to :action => :edit
      end
    end
  end
end