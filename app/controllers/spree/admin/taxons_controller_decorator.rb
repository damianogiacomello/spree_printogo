Spree::Admin::TaxonsController.class_eval do
  def destroy
    @taxon = Spree::Taxon.find(params[:id])
    @taxon.destroy
    respond_with(@taxon) do |format|
      format.json { render :json => @taxon.to_json }
    end
  end
end