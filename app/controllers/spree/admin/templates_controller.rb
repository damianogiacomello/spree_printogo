module Spree
  module Admin
    class TemplatesController < Spree::Admin::BaseController
      respond_to :html, :js
      
      def destroy
        @template = Spree::Template.find(params[:id])
        @template.delete
      end
      
      def new_variant
        @variant = Spree::Variant.find(params[:variant_id])
        @template = @variant.template.present? ? @variant.template : @variant.build_template()
      end
      
      def new_option_value
        @option = Spree::OptionValue.find(params[:option_value_id])
        @template = @option.template.present? ? @option.template : @option.build_template()
      end
      
      def create_variant
        @variant = Spree::Variant.find(params[:variant_id])
        @template = @variant.create_template(params[:template])
        respond_to do |format|
          format.html {
            render :json => {:files => [@template.to_jq_upload]},
            :content_type => 'text/html',
            :layout => false
          }
          format.json { render :json => {:files => [@template.to_jq_upload]}, :status =>  :created }
        end
      end
      
      def create_option_value
        @option = Spree::OptionValue.find(params[:option_value_id])
        @template = @option.create_template(params[:template])
        respond_to do |format|
          format.html {
            render :json => {:files => [@template.to_jq_upload]},
            :content_type => 'text/html',
            :layout => false
          }
          format.json { render :json => {:files => [@template.to_jq_upload]}, :status => :created }
        end
      end
    end
  end
end