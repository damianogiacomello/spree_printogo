module Spree
  module Admin
    class PresentationPacksController < ResourceController

      def show
        @presentation_pack = Spree::PresentationPack.all
      end

      def index

        #@cms_events = CmsEvent.page(params[:page]).per(Spree::Config[:admin_products_per_page]).order('\'from\'')#per(Spree::Config[:admin_products_per_page])
        params[:search] ||= {}
        params[:search][:meta_sort] ||= "name.asc"

        @presentation_pack = Spree::PresentationPack.all
        #render :layout => 'admin'
      end

      def collection
        @search = super.ransack(params[:search])
      end

      def new

        @presentation_pack = @object
      end

      #def edit
      #  @presentation_pack = Spree::PresentationPack.find params[:format]
      #end

      #def update
      #  @presentation_pack = Spree::PresentationPack.find params[:format]
      #  @presentation_pack.update_attributes(params[:presentation_pack])
      #  redirect_to location_after_save
      #end


    end
  end
end