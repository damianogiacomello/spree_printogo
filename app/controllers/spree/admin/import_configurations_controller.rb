module Spree
  module Admin
    class ImportConfigurationsController < Spree::Admin::BaseController
      #require 'fastercsv'
      respond_to :html, :js, :json
      
      def index
        @imports = Spree::ImportConfiguration.all
      end
      
      def new
        @import = Spree::ImportConfiguration.new
      end
      
      def create
        @import = Spree::ImportConfiguration.create(params[:import_configuration])
        @import.delay.configuration_csv
      end
    end
  end
end