Spree::Admin::StoreSettingsController.class_eval do
  def update
    params.each do |name, value|
      next unless Spree::Config.has_preference? name
      Spree::Config[name] = value
    end

    redirect_to edit_admin_store_settings_url
  end
end