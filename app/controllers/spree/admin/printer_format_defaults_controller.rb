module Spree
  module Admin
    class PrinterFormatDefaultsController < Spree::Admin::BaseController
      
      def index
        @printer_format_defaults = Spree::PrinterFormatDefault.all
      end

      def new
        @printer_format_default = Spree::PrinterFormatDefault.new()
        @masters = Spree::PrinterFormatDefault.masters
      end
      
      def edit
        @printer_format_default = Spree::PrinterFormatDefault.find(params[:id])
        @masters = Spree::PrinterFormatDefault.masters
      end
      
      def create
        @printer_format_default = Spree::PrinterFormatDefault.create(params[:printer_format_default])
        flash[:notice] = t(:create_success)
        redirect_to :action => :index
      end
      
      def update
        @printer_format_default = Spree::PrinterFormatDefault.find(params[:id])
        @printer_format_default.update_attributes(params[:printer_format_default])

        flash[:notice] = t(:edit_success)
        redirect_to :action => :index
      end
      
      def destroy
        @printer_format_default = Spree::PrinterFormatDefault.find(params[:id])
        @printer_format_default.destroy
        
        respond_to do |format|
          format.html {redirect_to :action => :index}
          format.js {render :layout => false}
        end
      end      
    end
  end
end