module Spree
  module Admin
    class ProcessingPlotterController < Spree::Admin::BaseController
      
      def edit
        @processing_plotter = Spree::ProcessingPlotter.last
      end

      def update
        @processing_plotter = Spree::ProcessingPlotter.last
        @processing_plotter.update_attributes(params[:processing_plotter])
        flash[:notice] = "#{@processing_plotter.presentation} modificato con successo"
        redirect_to :action => :edit
      end
    end
  end
end