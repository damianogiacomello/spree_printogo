Spree::Admin::UsersController.class_eval do  
	def edit
		@user = Spree::User.find(params[:id])
		@user.user_promotions.build if @user.user_promotions.empty?
		@user.bill_address ||= Spree::Address.default
		@user.ship_address ||= Spree::Address.default
	end

	# handling raise from Admin::ResourceController#destroy
	def user_destroy_with_orders_error
		invoke_callbacks(:destroy, :fails)
		flash[:error] = t(:error_user_destroy_with_orders)
		respond_to do |format|
			format.html {render :status => :forbidden, :text => t(:error_user_destroy_with_orders)}
			format.js {render :status => :forbidden}
		end
	end

	def update
		@user = Spree::User.find(params[:id])
		@user.update_with_password(params[:user])
		redirect_to admin_users_path
	end
end