module Spree
  module Admin
    class HollowPunchController < Spree::Admin::BaseController
      def edit
        @hollow_punch = Spree::HollowPunch.find_or_initialize_by_name 'hollowpunch'
        if params[:hollow_punch]
          if @hollow_punch.update_attributes(params[:hollow_punch])
            flash[:notice] = t(:edit_success)
          else
            flash[:error] = t(:edit_fail)
          end
        end
      end
    end
  end
end

