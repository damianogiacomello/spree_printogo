module Spree
  module Admin
    class PlottersController < Spree::Admin::BaseController
      
      def edit
        @plotter = Spree::Plotter.find params[:id]
      end

      def update
        @plotter = Spree::Plotter.find params[:id]
        @plotter.update_attributes params[:plotter]
        if @plotter.save
          flash[:notice] = t(:edit_success)
          redirect_to :action => :index
        else
          flash[:notice] = t(:edit_fail)
        end
      end
      
      def show
      end

      def destroy
        ov = Spree::Plotter.find params[:id]
        ov.destroy
        flash[:notice] = t(:delete_success)
        redirect_to :action => :index
      end

      def new
        @plotter = Spree::Plotter.new
      end

      def index
        @plotters = Spree::Plotter.all
      end

      def create
        p = Spree::Plotter.new params[:plotter]
        p.name = p.presentation.downcase.gsub(' ', '_')
        if p.save
          # Associo i formati di default di stampa
          flash[:notice] = t(:create_success)
          redirect_to :action => :index
        else
          flash[:error] = t(:create_error)
          redirect_to :action => :new
        end
      end
    end
  end
end