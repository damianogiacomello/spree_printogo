module Spree
  module Admin
    class ButtonholesController < Spree::Admin::BaseController
      
      def index
        @buttonholes = Spree::Buttonhole.all
      end

      def destroy
        @buttonhole = Spree::Buttonhole.find params[:id]
        @buttonhole.destroy
      end

      def edit
        @buttonhole = Spree::Buttonhole.find params[:id]
      end

      def new
        @buttonhole = Spree::Buttonhole.new
      end

      def update
        @buttonhole = Spree::Buttonhole.find(params[:id])
        @buttonhole.update_attributes(params[:buttonhole])
        flash[:notice] = "#{@buttonhole.name} modificata con successo"
        redirect_to :action => :index
      end

      def create
        if params[:buttonhole][:name].empty?
          flash[:error] = "Dati incompleti"
          redirect_to :action => :new
        else
          @buttonhole = Spree::Buttonhole.new(params[:buttonhole])
          @buttonhole.save
          flash[:notice] = "#{@buttonhole.name} creata con successo."
          redirect_to :action => :index
        end
      end
    end
  end
end


