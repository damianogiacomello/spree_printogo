Spree::Admin::ProductsController.class_eval do
  before_filter :initialize_percent, :only => :index
  
  def new_business_cards
    @product = Spree::BusinessCardProduct.new
  end

  def create_business_cards
    # params[:id]
    @product = Spree::BusinessCardProduct.new params[:business_card_product]
    @product.save!
  end

  def create_before
    return if params[:product][:prototype_id].blank?
    @prototype = Spree::Prototype.find(params[:product][:prototype_id])
  end

  def edit_business_cards
    @product = Spree::BusinessCardProduct.find_by_permalink params[:id]
    @product.create_template() if @product.template.blank?
  end

  def update_business_cards
    p = Spree::BusinessCardProduct.find_by_permalink(params[:id])
    weights = params[:business_card_product].delete :paper_weight
    papers = params[:business_card_product].delete :paper
    limit = params[:business_card_product].delete :limit
    printer_formats = params[:business_card_product].delete :printer_formats

    p.update_attributes(params[:business_card_product])
    p.master.limit.update_attributes(limit)

		p.paper_products.destroy_all if !p.paper_products.blank?
    if !weights.blank? && !papers.blank?
      weights.each do |weight|
        papers.each do |paper|
          paper_weight = Spree::PaperWeight.find_by_name_and_option_value_id(weight.to_s, Spree::Paper.find(paper).id)
          if paper_weight
            Spree::PaperProduct.create :product_id => p.id, :paper_id => paper_weight.paper.id, :weight_id => paper_weight.id
          end
        end
      end
    end

    p.save
    flash[:notice] = t(:update_success)
    redirect_to admin_business_cards_edit_url()
  end
  
  def delete_template_business_cards
    @product = Spree::BusinessCardProduct.find_by_permalink params[:id]
    @product.template.destroy()
    flash[:notice] = t(:template_removed)
    redirect_to admin_business_cards_edit_url()
  end

  def edit_flayers
    @product = Spree::FlayerProduct.find_by_permalink params[:id]
  end

  def update_flayers
    @product = Spree::FlayerProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:flayer_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_flayers
    end
  end
  
  def edit_letterheads
    @product = Spree::LetterheadProduct.find_by_permalink params[:id]
  end
  
  def update_letterheads
    @product = Spree::LetterheadProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:letterhead_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_letterheads
    else
      flash[:error] = t(:updated_error)
      redirect_to :action => :edit_letterheads
    end
  end
  
  def edit_postcards
    @product = Spree::PostcardProduct.find_by_permalink params[:id]
  end
  
  def update_postcards
    @product = Spree::PostcardProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:postcard_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_postcards
    end
  end
  
  def edit_playbills
    @product = Spree::PlaybillProduct.find_by_permalink params[:id]
  end
  
  def update_playbills
    @product = Spree::PlaybillProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:playbill_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_playbills
    end
  end

  def edit_posters
    @product = Spree::PosterProduct.find_by_permalink(params[:id])
  end

  def update_posters
    p = Spree::PosterProduct.find_by_permalink params[:id]
    if p.update_attributes(params[:poster_product])
      flash[:notice] = t(:update_success)
      redirect_to :action => :edit_posters
    else
      flash[:error] = t(:edit_fail)
      redirect_to :action => :edit_posters
    end
  end
  
  def edit_poster_high_qualities
    @product = Spree::PosterHighQualityProduct.find_by_permalink(params[:id])
  end

  def update_poster_high_qualities
    p = Spree::PosterHighQualityProduct.find_by_permalink params[:id]
    if p.update_attributes(params[:poster_high_quality_product])
      flash[:notice] = t(:update_success)
      redirect_to :action => :edit_poster_high_qualities
    else
      flash[:error] = t(:edit_fail)
      redirect_to :action => :edit_poster_high_qualities
    end
  end
  
  def edit_rigids
    @product = Spree::RigidProduct.find_by_permalink(params[:id])
  end

  def update_rigids
    p = Spree::RigidProduct.find_by_permalink params[:id]
    if p.update_attributes(params[:rigid_product])
      flash[:notice] = t(:update_success)
      redirect_to :action => :edit_rigids
    else
      flash[:error] = t(:edit_fail)
      redirect_to :action => :edit_rigids
    end
  end
  
  def edit_stickers
    @product = Spree::StickerProduct.find_by_permalink(params[:id])
  end

  def update_stickers
    p = Spree::StickerProduct.find_by_permalink params[:id]
    if p.update_attributes(params[:sticker_product])
      flash[:notice] = t(:update_success)
      redirect_to :action => :edit_stickers
    else
      flash[:error] = t(:edit_fail)
      redirect_to :action => :edit_stickers
    end
  end

  def edit_poster_flyers
    @product = Spree::PosterFlyerProduct.find_by_permalink params[:id]
  end

  def update_poster_flyers
    @product = Spree::PosterFlyerProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:poster_flyer_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_poster_flyers
    end
  end

  def add_image
    @product = Spree::Product.find params[:product_id]
  end
  
  def edit_paperbacks
    @product = Spree::PaperbackProduct.find_by_permalink params[:id]
  end
  
  def update_paperbacks
    @product = Spree::PaperbackProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:paperback_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_paperbacks
    end
  end
  
  def edit_staples
    @product = Spree::StapleProduct.find_by_permalink params[:id]
  end
  
  def update_staples
    @product = Spree::StapleProduct.find_by_permalink params[:id]
    params[:staple_product][:max_facades_weight_cover] = params[:staple_product][:max_weight_cover].to_i * params[:staple_product][:max_facades_cover].to_i
    if @product.update_attributes params[:staple_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_staples
    end
  end
  
  def edit_spirals
    @product = Spree::SpiralProduct.find_by_permalink params[:id]
  end
  
  def update_spirals
    @product = Spree::SpiralProduct.find_by_permalink params[:id]
    params[:spiral_product][:max_back] = params[:spiral_product][:max_thickness_spiraling].to_f * (params[:spiral_product][:max_facades_spiraling].to_i/2)
    if @product.update_attributes params[:spiral_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_spirals
    end
  end
  
  def edit_foldings
    @product = Spree::FoldingProduct.find_by_permalink params[:id]
  end
  
  def update_foldings
    @product = Spree::FoldingProduct.find_by_permalink params[:id]
    if @product.update_attributes params[:folding_product]
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_foldings
    end
  end
  
  def edit_banners
    @product = Spree::BannerProduct.find_by_permalink params[:id]
    @variant = @product.banner_variant
    #@variant.create_buttonhole_accessory() if !@variant.buttonhole_accessory.present?
    @variant.create_pocket(:available_meter => 0) if !@variant.pocket.present?
    @variant.create_heat_sealing(:available_meter => 0) if !@variant.heat_sealing.present?
    @variant.create_reinforcement_perimeter(:available_meter => 0) if !@variant.reinforcement_perimeter.present?
  end
  
  def update_banners
    @product = Spree::BannerProduct.find_by_permalink params[:id]
    if @product.update_attributes(params[:banner_product])
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_banners
    end
  end
  
  def edit_pvces
    @product = Spree::PvcProduct.find_by_permalink params[:id]
    @variant = @product.pvc_variant
  end
  
  def update_pvces
    @product = Spree::PvcProduct.find_by_permalink params[:id]
    if @product.update_attributes(params[:pvc_product])
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_pvces
    end
  end
  
  def edit_canvases
    @product = Spree::CanvasProduct.find_by_permalink(params[:id])
    @variant = @product.canvas_variant
  end
  
  def update_canvases
    @product = Spree::CanvasProduct.find_by_permalink(params[:id])
    if @product.update_attributes(params[:canvas_product])
      flash[:notice] = t(:updated_successfully)
      redirect_to :action => :edit_canvases
    end
  end
  
  def deactive
    @product = Spree::Product.find_by_permalink(params[:id])
    @product.available_on = Time.now + 2.years
    @product.save
    Spree::HomepageElement.find_all_by_homepageble_id(@product.id).each do |h|
      h.update_attribute(:enabled, false) if (!h.blank? && h.homepageble_type == "Spree::Product")
    end
    Spree::Menu.find_all_by_linkable_id(@product.id).each do |m|
      m.update_attribute(:visible, false) if (!m.blank? && m.linkable.class == "Spree::Product")
    end
    redirect_to :action => :index
  end
  
  def active
    @product = Spree::Product.find_by_permalink(params[:id])
    @product.available_on = Time.now - 2.days
    @product.save
    redirect_to :action => :index
  end
  
  def initialize_percent
    @all = Spree::Product.only_printer
    @active = @all.active
    @completed = (100/@all.count) * @active.count
    
    @all_promo = Spree::LineItemPromotion.all.count
    @promo_a = Spree::LineItemPromotion.available.count
    @promop = (100/@all_promo) * @promo_a rescue 0
  end
  
  protected
  def collection
    return @collection.where("type != 'Spree::CoverFlyerProduct' AND type != 'Spree::SimpleProduct' AND type != 'Spree::AdvancedProduct'") if @collection.present?

    unless request.xhr?
      params[:q] ||= {}
      params[:q][:deleted_at_null] ||= "1"

      params[:q][:s] ||= "name asc"

      @search = super.ransack(params[:q])
      @collection = @search.result.
      group_by_products_id.
      includes([:master, {:variants => [:images, :option_values]}]).
      page(params[:page]).
      per(5)

      if params[:q][:s].include?("master_price")
        # By applying the group in the main query we get an undefined method gsub for Arel::Nodes::Descending
        # It seems to only work when the price is actually being sorted in the query
        # To be investigated later.
        @collection = @collection.group("spree_variants.price")
      end
    else
      includes = [{:variants => [:images,  {:option_values => :option_type}]}, {:master => :images}]

      @collection = super.where(["name #{LIKE} ?", "%#{params[:q]}%"])
      @collection = @collection.includes(includes).limit(params[:limit] || 5)

      tmp = super.where(["#{Variant.table_name}.sku #{LIKE} ?", "%#{params[:q]}%"])
      tmp = tmp.includes(:variants_including_master).limit(params[:limit] || 5)
      @collection.concat(tmp)
    end
    @collection = @collection.where("type != 'Spree::CoverFlyerProduct' AND type != 'Spree::SimpleProduct' AND type != 'Spree::AdvancedProduct'")
  end
end