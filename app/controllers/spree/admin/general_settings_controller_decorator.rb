Spree::Admin::GeneralSettingsController.class_eval do
  def show
    @preferences = ['site_name', 'default_seo_title', 'default_meta_keywords',
      'default_meta_description', 'paper_weight', 
      'file_check', 'file_check_cost', 'file_check_desc', 
      "homepage_new_version_enabled", "homepage_elements_number", "privacy_description", "order_notes", 'cookie_law']
  end

  def edit
    @preferences = [
			:site_name, :default_seo_title, :default_meta_keywords, :default_meta_description,
			:privacy_description, :allow_ssl_in_production, :allow_ssl_in_staging,
			:allow_ssl_in_development_and_test, :check_for_spree_alerts, :paper_weight, :file_check,
			:file_check_cost, :file_check_desc, :homepage_new_version_enabled, :homepage_elements_number,
      :invoice_on_confirm_email, :invoice_on_order_complete, :invoice_seller_details, :order_notes,
			:cookie_law, :notify_bill_user_change, :notify_email
		]
  end
  
  def update
    params.each do |name, value|
      next unless Spree::Config.has_preference? name
      Spree::Config[name] = value
    end
    # TODO devo cancellare o creare i PaperWeight e le Weight nel caso venga aggiornato il campo di configurazione
    redirect_to admin_configurations_path
  end
end