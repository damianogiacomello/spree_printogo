module Spree
  module Admin
    class QuartersController < Spree::Admin::BaseController
      def index
        @quarters = Spree::Quarter.page(params[:page]).per(Spree::Config[:admin_products_per_page])
      end

      def new
        @quarter = Spree::Quarter.new
      end
      
      def create
        @quarter = Spree::Quarter.new(params[:quarter])
        @quarter.save
        
        flash[:notice] = t(:create_success)
        redirect_to :action => :index
      end
      
      def edit
        @quarter = Spree::Quarter.find(params[:id])
      end
      
      def update
        @quarter = Spree::Quarter.find(params[:id])
        @quarter.update_attributes(params[:quarter])
        
        flash[:notice] = t(:edit_success)
        redirect_to :action => :index
      end
    end
  end
end