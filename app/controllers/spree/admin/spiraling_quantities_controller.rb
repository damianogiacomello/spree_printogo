module Spree
  module Admin
    class SpiralingQuantitiesController < Spree::Admin::BaseController
      def create
        @spiraling = Spree::Spiraling.find params[:spiraling_id]
        @spiraling_q = @spiraling.spiraling_quantities.create params[:spiraling_quantity]
        flash[:notice] = "Formato #{@spiraling_q.color} creato correttamente"
        redirect_to admin_spiraling_quantities_index_path(@spiraling)
      end

      def destroy
        @spiraling = Spree::Spiraling.find params[:spiraling_id]
        @spiraling_q = @spiraling.spiraling_quantities.find params[:id]
        @spiraling_q.destroy
        redirect_to admin_spiraling_quantities_index_path(@spiraling)
      end

      def index
        @spiraling = Spree::Spiraling.find params[:spiraling_id]
        @spiraling_quantities = @spiraling.spiraling_quantities
      end

      def new
        @spiraling = Spree::Spiraling.find params[:spiraling_id]
        @spiraling_quantity = @spiraling.spiraling_quantities.new
      end

      def edit
        @spiraling = Spree::Spiraling.find params[:spiraling_id]
        @spiraling_quantity = @spiraling.spiraling_quantities.find params[:id]
      end

      def update
        @spiraling = Spree::Spiraling.find params[:spiraling_id]
        @spiraling_quantity = @spiraling.spiraling_quantities.find params[:id]
        if @spiraling_quantity.update_attributes(params[:spiraling_quantity])
          flash[:notice] = t(:edit_success)
          redirect_to admin_spiraling_quantities_index_path(@spiraling)
        else
          flash[:error] = t(:edit_fail)
          redirect_to admin_spiraling_quantities_edit_path(@spiraling, @spiraling_quantity.id)
        end

      end

    end
  end
end