module Spree
  module Admin
    class AdvancedVariantsController < ResourceController
      respond_to :html, :js, :json
      before_filter :find_resource
      
      def index
        respond_with(@collection)
      end
      
      def new
        @variant = @advanced_product.advanced_variants.build
        @entities = []
      end
      
      def create
				@advanced_product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        if params[:entity] && params[:entity][:attributes]
          @variant = @advanced_product.advanced_variants.find_or_create_by_sku(params[:advanced_variant][:format])
					@variant.update_attributes(params[:advanced_variant])
          @entity = @variant.entities.create({:price => params[:entity][:price], :weight => params[:entity][:weight]})
          params[:entity][:attributes].each do |k, v|
            @entity.entity_attribute_values.create({:attribute_id => k, :value_id => v})
          end
          @entities = @entity = @variant.entities
        else
          @variant = @advanced_product.advanced_variants.build(params[:advanced_variant])
          @entities = []
          flash[:alert] = "Impossibile salvare il formato desiderato. Mancano le configurazioni delle opzioni!"
        end
      end

      def edit
        @variant = @advanced_product.advanced_variants.find(params[:id])
        @entities = @variant.entities
        respond_with(@advanced_product)
      end

      def update
				@variant = @advanced_product.advanced_variants.find(params[:id])
				if params[:entity] && !params[:entity][:attributes].blank? && !params[:entity][:price].blank? && !params[:entity][:weight].blank?
	        @variant.update_attributes(params[:advanced_variant])
	        @entity = @variant.entities.create({:price => params[:entity][:price], :weight => params[:entity][:weight]})
	        params[:entity][:attributes].each do |k, v|
	          @entity.entity_attribute_values.create({:attribute_id => k, :value_id => v})
	        end
					@entities = @entity = @variant.entities
				else
					flash[:alert] = "Impossibile salvare il formato desiderato. Mancano le configurazioni delle opzioni!"
				end
        @entities = @variant.entities
      end

      def destroy
        @advanced_product = Spree::AdvancedProduct.where(:permalink => params[:advanced_product_id]).first!
        @variant = @advanced_product.advanced_variants.find params[:id]
        @variant.destroy
        flash[:notice] = t(:delete_success)
        
        respond_with(@advanced_product) do |format|
          format.html { redirect_to collection_url }
          format.js  { render :layout => false }
        end
      end

      def remove_entity
        @advanced_product = Spree::AdvancedProduct.find_by_permalink params[:advanced_product_id]
        @variant = @advanced_product.advanced_variants.find params[:advanced_variant_id]
        @entity = @variant.entities.find(params[:entity_id])
        @entity.destroy
      end
      
      protected

      def find_resource
        @advanced_product = AdvancedProduct.find_by_permalink(params[:advanced_product_id])
      end

      def location_after_save
        edit_admin_advanced_product_url(@advanced_product)
      end
        
      def collection
        return @collection if @collection.present?
        
        @advanced_product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
        @collection = @advanced_product.advanced_variants
      end
    end
  end
end