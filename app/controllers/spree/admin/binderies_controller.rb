module Spree
  module Admin
    class BinderiesController < Spree::Admin::BaseController
      def index
        @binderies = Spree::OptionType.find_by_name('bindery').binderies
      end

      def update
        @bindery = Spree::Bindery.find(params[:id])
        @bindery.update_attributes(params[:bindery])

        flash[:notice] = t(:edit_success)
        redirect_to :action => :edit
      end

      def edit
        @bindery = Spree::Bindery.find(params[:id])
        @bindery.build_seam if !@bindery.seam.present?
        @bindery.build_sleeve if !@bindery.sleeve.present?
        @bindery.build_milling if !@bindery.milling.present?
      end

      def destroy
        @bindery = Spree::Bindery.find(params[:id])
        @bindery.destroy
        #flash[:notice] = t(:delete_success)
        #redirect_to :action => :index
        respond_to do |format|
          format.html {redirect_to :action => :index}
          format.js {render :layout => false}
        end
      end

      def new
        @bindery = Spree::Bindery.new
        @bindery.build_seam
        @bindery.punchings.build
        @bindery.bendings.build
        @bindery.build_sleeve
        @bindery.build_milling
      end

      def create
        @bindery = Spree::Bindery.new(params[:bindery])
        @bindery.save
        
        flash[:notice] = t(:create_success)
        redirect_to :action => :edit
      end
    end
  end
end