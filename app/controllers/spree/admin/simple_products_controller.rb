module Spree
  module Admin
    class SimpleProductsController < ResourceController
      respond_to :html, :js
      
      def index
        respond_with(@collection)
      end
      
      def show
        redirect_to( :action => :edit )
      end
      
      def new
        @simple_product = Spree::SimpleProduct.new(:price => 0, :max_width => 1)
      end
      
      def create
        @simple_product = Spree::SimpleProduct.new(params[:simple_product])
        @simple_product.price = 0
        @simple_product.save
        redirect_to :action => :index
      end
      
      def destroy
        @simple_product = Spree::SimpleProduct.where(:permalink => params[:id]).first!
        @simple_product.destroy

        flash.notice = I18n.t('notice_messages.simple_product_deleted')

        respond_with(@simple_product) do |format|
          format.html { redirect_to collection_url }
          format.js  { render_js_for_destroy }
        end
      end
      
      def add_image
        @image = @simple_product.images.first if !@simple_product.images.blank?
        @image = @simple_product.images.new if @simple_product.images.blank?
      end
      
      def deactive
        @simple_product.available_on = Time.now + 2.years
        @simple_product.save
        Spree::HomepageElement.find_all_by_homepageble_id(@simple_product.id).each do |h|
          h.update_attribute(:enabled, false) if (!h.blank? && h.homepageble_type == "Spree::Product")
        end
        Spree::Menu.find_all_by_linkable_id(@simple_product.id).each do |m|
          m.update_attribute(:visible, false) if (!m.blank? && m.linkable.class == "Spree::Product")
        end
        redirect_to :action => :index
      end

      def active
        if @simple_product.simple_variants.any? && @simple_product.simple_variants.any?{|sv| sv.format_simple_variants.any?}
          @simple_product.available_on = Time.now - 2.days
          @simple_product.save
          flash.notice = "#{@simple_product.name.titleize} attivato con successo!"
        else
          flash[:error] = "Impossibile attivare il prodotto #{@simple_product.name.titleize} in quanto mancano alcune configurazioni!"
        end
        redirect_to :action => :index
      end
      
      protected
      def find_resource
        SimpleProduct.find_by_permalink!(params[:id])
      end

      def location_after_save
        edit_admin_simple_product_url(@simple_product)
      end
      
      def collection
        return @collection if @collection.present?

        unless request.xhr?
          params[:q] ||= {}

          params[:q][:s] ||= "name asc"

          @search = Spree::SimpleProduct.ransack(params[:q])
          @collection = @search.result.
            group_by_products_id.
            includes([:master, {:variants => [:images, :option_values]}]).
            page(params[:page]).
            per(Spree::Config[:admin_products_per_page])
        else
          includes = [{:variants => [:images,  {:option_values => :option_type}]}, {:master => :images}]

          @collection = super.where(["name #{LIKE} ?", "%#{params[:q]}%"])
          @collection = @collection.includes(includes).limit(params[:limit] || 10)
        end
        @collection
      end
    end
  end
end