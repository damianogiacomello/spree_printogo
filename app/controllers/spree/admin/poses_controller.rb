module Spree
  module Admin
    class PosesController < Spree::Admin::BaseController
      
      def index
        #@poses = Spree::OptionType.find_by_name('pose').option_values.page(params[:page]).
        #    per(Spree::Config[:admin_products_per_page])
        @poses = Spree::Pose.page(params[:page]).per(Spree::Config[:admin_products_per_page])
      end

      def new
        @pose = Spree::Pose.new
      end

      def create
        pose = Spree::Pose.new params[:pose]
        if pose.save
          flash[:notice] = t(:created_successfully)
          redirect_to :action => :index
        else
          flash[:error] = t(:create_error)
        end
      end

      def edit
        @pose = Spree::Pose.find params[:id]
      end

      def update
        @pose = Spree::Pose.find params[:id]
        if @pose.update_attributes params[:pose]
          flash[:notice] = t(:edit_success)
          redirect_to :action => :index
        else
          flash[:error] = t(:edit_fail)
        end
      end

      def destroy
        @pose = Spree::Pose.find params[:id]
        if @pose.destroy
          flash[:notice] = t(:delete_success)
        else
          flash[:error] = t(:deleted_error)
        end
        render :partial => "spree/admin/shared/destroy"
      end
    end
  end
end