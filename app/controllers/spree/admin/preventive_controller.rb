module Spree
  module Admin
    class PreventiveController < Spree::Admin::BaseController
      respond_to :html, :js, :json

      def retrive_folding_format
        result = []
        @product = Spree::Product.find(params[:product_id])
        if @product.type == 'Spree::FoldingProduct'
          @product.folding_variants.find(params[:id]).folding_formats.each do |f| 
            opt = Spree::OptionValue.find(f.option_value_id)
            result << {:id => opt.id, :name => opt.name} if f.active
          end
        end
        render :json => result.to_json
      end

      def retrive_paper_weight
        result = []
        @product = Spree::Product.find(params[:product_id])
        @paper = Spree::Paper.find(params[:paper_id]) rescue nil
        case @product.type
          when 'Spree::BusinessCardProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::PosterProduct'
            result = nil
          when 'Spree::PosterHighQualityProduct'
            result = nil
          when 'Spree::CanvasProduct'
            result = nil
          when 'Spree::PvcProduct'
            result = nil
          when 'Spree::RigidProduct'
            result = nil
          when 'Spree::StickerProduct'
            result = nil
          when 'Spree::FlayerProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.flayer_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::LetterheadProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.letterhead_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::PlaybillProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.playbill_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::PostcardProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.postcard_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::PosterFlyerProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.poster_flyer_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::PaperbackProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.paperback_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::StapleProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.staple_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::BannerProduct'
            result = nil
          when 'Spree::SpiralProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.spiral_variants.find(params[:id]).id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::FoldingProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id_and_folding_format_id(@product.folding_variants.find(params[:id]).id, @paper.id, @product.folding_variants.find(params[:id]).folding_formats.last).collect{|pp| pp.paper_weight.name}
        end
        render :json => result.to_json
      end
      
      def retrive_paper
        tmp = []
        result = []
        paper_products = []
        @product = Spree::Product.find(params[:product_id])
        case @product.type
          when 'Spree::FlayerProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.flayer_variants.find(params[:id]).id)
          when 'Spree::LetterheadProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.letterhead_variants.find(params[:id]).id)
          when 'Spree::PlaybillProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.playbill_variants.find(params[:id]).id)
          when 'Spree::PostcardProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.postcard_variants.find(params[:id]).id)
          when 'Spree::PosterFlyerProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.poster_flyer_variants.find(params[:id]).id)
          when 'Spree::PaperbackProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.paperback_variants.find(params[:id]).id)
          when 'Spree::StapleProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.staple_variants.find(params[:id]).id)
          when 'Spree::SpiralProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.spiral_variants.find(params[:id]).id)
          when 'Spree::FoldingProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.folding_variants.find(params[:id]).id)
        end
        paper_products.each do |pp|
          if tmp.empty?
            tmp << pp.paper_id
            result << {:id => pp.paper.id, :presentation => pp.paper.presentation}
          elsif !tmp.include?(pp.paper_id)
            tmp << pp.paper_id
            result << {:id => pp.paper.id, :presentation => pp.paper.presentation}
          end
        end
        render :json => result.to_json
      end
      
      def retrive_paper_weight_cover
        @product = Spree::Product.find(params[:product_id])
        @paper = Spree::Paper.find(params[:paper_id]) rescue nil
        case @product.type
          when 'Spree::PaperbackProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.paperback_variants.find(params[:id]).cover_variant.id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::StapleProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.staple_variants.find(params[:id]).cover_variant.id, @paper.id).collect{|pp| pp.paper_weight.name}
          when 'Spree::SpiralProduct'
            result = Spree::PaperProduct.find_all_by_product_id_and_paper_id(@product.spiral_variants.find(params[:id]).cover_variant.id, @paper.id).collect{|pp| pp.paper_weight.name}
        end
        render :json => result.to_json
      end
      
      def retrive_paper_cover
        tmp = []
        result = []
        paper_products = []
        @product = Spree::Product.find(params[:product_id])
        case @product.type
          when 'Spree::PaperbackProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.paperback_variants.find(params[:id]).cover_variant.id)
          when 'Spree::StapleProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.staple_variants.find(params[:id]).cover_variant.id)
          when 'Spree::SpiralProduct'
            paper_products = Spree::PaperProduct.find_all_by_product_id(@product.spiral_variants.find(params[:id]).cover_variant.id)
        end
        paper_products.each do |pp|
          if tmp.empty?
            tmp << pp.paper_id
            result << {:id => pp.paper.id, :presentation => pp.paper.presentation}
          elsif !tmp.include?(pp.paper_id)
            tmp << pp.paper_id
            result << {:id => pp.paper.id, :presentation => pp.paper.presentation}
          end
        end
        render :json => result.to_json
      end
      
      def precalculate
        @error = false
        begin
          @product = Spree::Product.find(params[:line_item_promotion][:product_id])
          params[:order] = params[:line_item_promotion]
          params[:order_calculator] = {}
          params[:order_calculator][:data] = "#{params[:line_item_promotion][:shipping_date].split("/")[2]}/#{params[:line_item_promotion][:shipping_date].split("/")[1]}/#{params[:line_item_promotion][:shipping_date].split("/")[0]}"
          case @product.type
          when 'Spree::BusinessCardProduct'
            business_card_variant = Spree::BusinessCardProduct.find(params[:line_item_promotion][:product_id]).master
            @calc = current_order(true).add_variant(business_card_variant, params, nil, 1, true, false)
          when 'Spree::PosterProduct'
            @calc = current_order(true).add_variant(Spree::PosterProduct.find(params[:line_item_promotion][:product_id]), params, nil, 1, true, false)
          when 'Spree::PosterHighQualityProduct'
            @calc = current_order(true).add_variant(Spree::PosterHighQualityProduct.find(params[:line_item_promotion][:product_id]), params, nil, 1, true, false)
          when 'Spree::PosterFlyerProduct'
            @calc = current_order(true).add_variant(Spree::PosterFlyerVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::FlayerProduct'
            @calc = current_order(true).add_variant(Spree::FlayerVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::LetterheadProduct'
            @calc = current_order(true).add_variant(Spree::LetterheadVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::PostcardProduct'
            @calc = current_order(true).add_variant(Spree::PostcardVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::PlaybillProduct'
            @calc = current_order(true).add_variant(Spree::PlaybillVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::StapleProduct'
            @calc = current_order(true).add_variant(Spree::StapleVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::PaperbackProduct'
            @calc = current_order(true).add_variant(Spree::PaperbackVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::FoldingProduct'
            @calc = current_order(true).add_variant(Spree::FoldingVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::BannerProduct'
            @calc = current_order(true).add_variant(Spree::BannerProduct.find(params[:line_item_promotion][:product_id]).banner_variant, params, nil, 1, true, false)
          when 'Spree::SpiralProduct'
            @calc = current_order(true).add_variant(Spree::SpiralVariant.find(params[:order][:format]), params, nil, 1, true, false)
          when 'Spree::CanvasProduct'
            @calc = current_order(true).add_variant(Spree::CanvasProduct.find(params[:line_item_promotion][:product_id]).canvas_variant, params, nil, 1, true, false)
          when 'Spree::PvcProduct'
            @calc = current_order(true).add_variant(Spree::PvcProduct.find(params[:line_item_promotion][:product_id]).pvc_variant, params, nil, 1, true, false)
          end
          respond_to do |format|
            format.js {render :layout => false}
          end
        rescue Exception => e
          logger.info "+=+=+=+=+=+=+=+=+=+=+=+=+=+= #{e} +=+=+=+=+=+=+=+=+=+=+=+=+=+="
          @error = true
          respond_to do |format|
            format.js {render :layout => false}
          end
        end
      end
    end
  end
end