Spree::Admin::ImagesController.class_eval do

  def set_viewable
    if params[:image].has_key? :viewable_id
      if params[:image][:viewable_id] == 'All'
        @image.viewable = @product.master
      else
        @image.viewable_type = 'Spree::Variant'
        @image.viewable_id = params[:image][:viewable_id]
      end
    else
      @image.viewable = @product.master
    end
  end

  private

  def load_data
    if params[:simple_product_id].blank? && params[:advanced_product_id].blank?
      @product = Spree::Product.find_by_permalink(params[:product_id])
      @variants = @product.variants.collect do |variant|
        [variant.options_text, variant.id]
      end
      @variants.insert(0, [I18n.t(:all), 'All'])
    elsif params[:advanced_product_id].blank?
      @product = Spree::SimpleProduct.find_by_permalink(params[:simple_product_id])
      @variants = @product.simple_variants.collect do |variant|
        [variant.options_text, variant.id]
      end
      @variants.insert(0, [I18n.t(:all), 'All'])
    else
      @product = Spree::AdvancedProduct.find_by_permalink(params[:advanced_product_id])
      @variants = @product.advanced_variants.collect do |variant|
        [variant.options_text, variant.id]
      end
      @variants.insert(0, [I18n.t(:all), 'All'])
    end
  end

  def location_after_save
    if @product.is_a? Spree::BusinessCardProduct
      admin_business_cards_edit_path(@product)
    elsif (@product.is_a?(Spree::PosterProduct) && !@product.is_a?(Spree::PosterFlyerProduct))
      admin_posters_edit_path(@product)
    elsif @product.is_a? Spree::FlayerProduct
      admin_flayers_edit_path(@product)
    elsif @product.is_a? Spree::LetterheadProduct
      admin_letterheads_edit_path(@product)
    elsif @product.is_a? Spree::PostcardProduct
      admin_postcards_edit_path(@product)
    elsif @product.is_a? Spree::PlaybillProduct
      admin_playbills_edit_path(@product)
    elsif @product.is_a? Spree::PosterFlyerProduct
      admin_poster_flyers_edit_path(@product)
    elsif @product.is_a? Spree::PaperbackProduct
      admin_paperbacks_edit_path(@product)
    elsif @product.is_a? Spree::StapleProduct
      admin_staples_edit_path(@product)
    elsif @product.is_a? Spree::FoldingProduct
      admin_foldings_edit_path(@product)
    elsif (@product.is_a?(Spree::BannerProduct) && !@product.is_a?(Spree::PvcProduct))
      admin_banners_edit_path(@product)
    elsif @product.is_a? Spree::PvcProduct
      admin_pvces_edit_path(@product)
    elsif @product.is_a? Spree::SpiralProduct
      admin_spirals_edit_path(@product)
    elsif @product.is_a? Spree::CanvasProduct
      admin_canvases_edit_path(@product)
    elsif @product.is_a? Spree::RigidProduct
      admin_rigids_edit_path(@product)
    elsif @product.is_a? Spree::StickerProduct
      admin_stickers_edit_path(@product)
    elsif @product.is_a? Spree::SimpleProduct
      edit_admin_simple_product_path(@product)
    elsif @product.is_a? Spree::AdvancedProduct
      edit_admin_advanced_product_path(@product)
    end
  end
end