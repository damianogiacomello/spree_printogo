module Spree
  module Admin
    class RigidVariantsController < Spree::Admin::BaseController
      respond_to :html, :js

      def index
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @variants = @product.rigid_variants
      end

      def new
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @variant = @product.rigid_variants.create()
      end
      
      def create
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @variant = @product.rigid_variants.find(params[:variant_id])
        if @variant.update_attributes(params[:rigid_variant])
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def edit
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @variant = @product.rigid_variants.find(params[:id])
      end

      def update
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @variant = @product.rigid_variants.find(params[:variant_id])
          if @variant.update_attributes(params[:rigid_variant])
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def destroy
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @variant = @product.rigid_variants.find params[:id]
        @variant.destroy
        flash[:notice] = t(:delete_success)
        redirect_to :action => :index
      end
      
    end
  end
end
