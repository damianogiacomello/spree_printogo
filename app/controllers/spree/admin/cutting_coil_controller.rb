module Spree
  module Admin
    class CuttingCoilController < Spree::Admin::BaseController
      
      def edit
        @cutting_coil = Spree::CuttingCoil.last
      end

      def update
        @cutting_coil = Spree::CuttingCoil.last
        @cutting_coil.update_attributes(params[:cutting_coil])
        flash[:notice] = "#{@cutting_coil.presentation} modificato con successo"
        redirect_to :action => :edit
      end
    end
  end
end
