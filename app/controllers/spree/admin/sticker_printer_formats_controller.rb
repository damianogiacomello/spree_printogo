module Spree
  module Admin
    class StickerPrinterFormatsController < Spree::Admin::BaseController
      def create
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @product.sticker_printer_formats.create(params[:sticker_printer_format])
        #@sticker_printer_format = Spree::StickerPrinterFormat.create(params[:sticker_printer_format])
        #@sticker_printer_format.save
        #@product.master.option_values << Spree::OptionValue.find(@sticker_printer_format.id)
        flash[:notice] = "Quantita' creata correttamente"
        redirect_to admin_sticker_printer_formats_index_path(@product)
      end

      def destroy
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @sticker_printer_format = Spree::StickerPrinterFormat.find params[:id]
        @sticker_printer_format.destroy
        redirect_to admin_sticker_printer_formats_index_path(@product)
      end

      def index
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @sticker_printer_formats = @product.sticker_printer_formats
      end

      def new
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @sticker_printer_format = @product.sticker_printer_formats.new()
      end

      def edit
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @sticker_printer_format = @product.sticker_printer_formats.find params[:id]
        @sticker_printer_format.build_template_format if @sticker_printer_format.template_format.blank?
      end

      def update
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @sticker_printer_format = @product.sticker_printer_formats.find params[:id]
        if @sticker_printer_format.update_attributes(params[:sticker_printer_format])
          @sticker_printer_format.save
          flash[:notice] = t(:edit_success)
          respond_to do |format|
            format.html { redirect_to admin_sticker_printer_formats_index_path(@product) }
            format.js {
              @sticker_printer_formats = @product.sticker_printer_formats
              render :action => :index
            }
          end
        else
          flash[:error] = t(:edit_fail)
          redirect_to admin_sticker_printer_formats_edit_path(@product, @sticker_printer_format.id)
        end

      end

    end
  end
end