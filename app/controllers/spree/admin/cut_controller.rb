module Spree
  module Admin
    class CutController < Spree::Admin::BaseController
      def edit
        @cut = Spree::Cut.find_or_initialize_by_name('cut')
        if params[:cut]
          if @cut.update_attributes(params[:cut])
            flash[:notice] = t(:edit_success)
            redirect_to :action => :edit
          else
            flash[:error] = t(:edit_fail)
          end
        end
      end
    end
  end
end

