module Spree
  module Admin
    class PosterHighQualityPrinterFormatsController < Spree::Admin::BaseController
      def create
        @product = Spree::PosterHighQualityProduct.find_by_permalink params[:product_id]
        @poster_high_quality_printer_formats = Spree::PosterHighQualityPrinterFormat.create(params[:poster_high_quality_printer_formats])
        @poster_high_quality_printer_formats.save
        @product.master.option_values << Spree::OptionValue.find(@poster_high_quality_printer_formats.id)
        flash[:notice] = "Quantita' creata correttamente"
        redirect_to admin_poster_high_quality_printer_formats_index_path(@product)
      end

      def destroy
        @product = Spree::PosterHighQualityProduct.find_by_permalink params[:product_id]
        @poster_high_quality_printer_formats = Spree::PosterHighQualityPrinterFormat.find params[:id]
        @poster_high_quality_printer_formats.destroy
        redirect_to admin_poster_high_quality_printer_formats_index_path(@product)
      end

      def index
        @product = Spree::PosterHighQualityProduct.find_by_permalink params[:product_id]
        @poster_high_quality_printer_formats = @product.poster_high_quality_printer_formats
      end

      def new
        @product = Spree::PosterHighQualityProduct.find_by_permalink params[:product_id]
        @poster_high_quality_printer_formats = Spree::PosterHighQualityPrinterFormat.new
      end

      def edit
        @product = Spree::PosterHighQualityProduct.find_by_permalink params[:product_id]
        @poster_high_quality_printer_formats = Spree::PosterHighQualityPrinterFormat.find params[:id]
      end

      def update
        @product = Spree::PosterHighQualityProduct.find_by_permalink params[:product_id]
        @poster_high_quality_printer_formats = Spree::PosterHighQualityPrinterFormat.find params[:id]
        if @poster_high_quality_printer_formats.update_attributes(params[:poster_high_quality_printer_formats])
          @poster_high_quality_printer_formats.save
          flash[:notice] = t(:edit_success)
          redirect_to admin_poster_high_quality_printer_formats_index_path(@product)
        else
          flash[:error] = t(:edit_fail)
          redirect_to admin_poster_high_quality_printer_formats_edit_path(@product, poster_high_quality_printer_formats.id)
        end

      end

    end
  end
end