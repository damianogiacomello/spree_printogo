module Spree
  module Admin
    class LineItemPromotionsController < BaseController
      respond_to :html, :js, :json
      
      def index
        @product = Spree::Product.find_by_permalink!(params[:id])
        @line_item_promotions = Spree::LineItemPromotion.find_all_by_product_id(@product.id)
        respond_with(@line_item_promotions)
      end
      
      def new
        @product = Spree::Product.find_by_permalink!(params[:product_id])
        get_form_values(@product)
        @line_item_promotion = Spree::LineItemPromotion.new(:product_id => @product.id, :quantity => 0, :price => 0)
        respond_with(@line_item_promotion)
      end
      
      def edit
        @line_item_promotion = Spree::LineItemPromotion.find(params[:id])
        @product = @line_item_promotion.product
        get_form_values(@product)
        respond_with(@line_item_promotion)
      end
      
      def create
        @product = Spree::Product.find(params[:line_item_promotion][:product_id])
        @line_item_promotion = Spree::LineItemPromotion.create(:product_id => @product.id, :quantity => params[:line_item_promotion][:quantity], :active => params[:line_item_promotion][:active], :unlimited => params[:line_item_promotion][:unlimited], :expires_at => Date.new(params[:line_item_promotion][:expires_at].split("/")[2].to_i, params[:line_item_promotion][:expires_at].split("/")[1].to_i, params[:line_item_promotion][:expires_at].split("/")[0].to_i), :price => params[:line_item_promotion][:price], :original_price => params[:line_item_promotion][:original_price], :original_total => params[:line_item_promotion][:original_total], :weight_item => params[:line_item_promotion][:weight_item], :shipping_date => Date.new(params[:line_item_promotion][:shipping_date].split("/")[2].to_i, params[:line_item_promotion][:shipping_date].split("/")[1].to_i, params[:line_item_promotion][:shipping_date].split("/")[0].to_i))
        @line_item_promotion.generate_configuration(params)
        flash[:notice] = t(:create_success)
        redirect_to admin_products_line_item_promotions_path(@product)
      end
      
      def update
        @line_item_promotion = Spree::LineItemPromotion.find(params[:id])
        @line_item_promotion.line_items_option_values.clear
        @line_item_promotion.update_attributes({:quantity => params[:line_item_promotion][:quantity], :active => params[:line_item_promotion][:active], :unlimited => params[:line_item_promotion][:unlimited], :expires_at => Date.new(params[:line_item_promotion][:expires_at].split("/")[2].to_i, params[:line_item_promotion][:expires_at].split("/")[1].to_i, params[:line_item_promotion][:expires_at].split("/")[0].to_i), :price => params[:line_item_promotion][:price], :original_price => params[:line_item_promotion][:original_price], :original_total => params[:line_item_promotion][:original_total], :weight_item => params[:line_item_promotion][:weight_item], :shipping_date => Date.new(params[:line_item_promotion][:shipping_date].split("/")[2].to_i, params[:line_item_promotion][:shipping_date].split("/")[1].to_i, params[:line_item_promotion][:shipping_date].split("/")[0].to_i)})
        if params[:line_item_promotion][:active] == "0"
          Spree::HomepageElement.find_all_by_homepageble_id(@line_item_promotion.id).each do |h|
            h.update_attribute(:enabled, false) if (!h.blank? && h.homepageble_type == "Spree::LineItemPromotion")
          end
        end  
        @line_item_promotion.generate_configuration(params)
        @product = @line_item_promotion.product
        flash[:notice] = t(:create_success)
        redirect_to admin_products_line_item_promotions_path(@product)
      end
      
      def destroy
        @line_item_promotion = Spree::LineItemPromotion.find(params[:id])
        @product = @line_item_promotion.product
        Spree::HomepageElement.find_all_by_homepageble_id(@line_item_promotion.id).each do |h|
          h.destroy if (!h.blank? && h.homepageble_type == "Spree::LineItemPromotion")
        end
        @line_item_promotion.destroy
        flash[:notice] = t(:delete_success)
        redirect_to admin_products_line_item_promotions_path(@product)

      end
      
      def get_form_values(product)
        @format = product.retrive_formats
        @paper = product.retrive_papers
        @cover = product.retrive_covers
        @spiral = (product.type == "Spree::SpiralProduct") ? true : false
        @folding = (product.type == "Spree::FoldingProduct") ? true : false
        @canvas = (product.type == "Spree::CanvasProduct") ? true : false
        @banner = (product.type == "Spree::BannerProduct") ? true : false
        @facades = product.retrive_facades
        @plasticization = product.retrive_plasticization
        @paper_cover = product.retrive_paper_cover
        @printer_instruction = product.retrive_printer_instruction
      end

    end
  end
end