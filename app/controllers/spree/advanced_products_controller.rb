module Spree
  class AdvancedProductsController < BaseController
    before_filter :load_product, :only => :show
    rescue_from ActiveRecord::RecordNotFound, :with => :render_root
    helper 'spree/taxons'
    respond_to :html
    
    def index
      @searcher = Config.searcher_class.new(params)
      @products = @searcher.retrieve_advanced_products
      if @products.active.blank?
        redirect_to root_path
        return false
      end
      
      @title = @products.first.seo_title
      respond_with(@products)
    end

    def load_product
      @product = Spree::AdvancedProduct.active.find_by_permalink(params[:id])
    end

    def show
      if @product.blank?
        flash[:notice] = t(:product_not_active)
        redirect_to root_path
        return false
      end
      
      return unless @product
      @title = @product.seo_title
      respond_with(@product)
    end

  end
end