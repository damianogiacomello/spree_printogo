module Spree
  class PaperbacksController < BaseController
    before_filter :load_product, :only => :show
    rescue_from ActiveRecord::RecordNotFound, :with => :render_root
    helper 'spree/taxons'
    respond_to :html
    
    def load_product
      @product = Spree::PaperbackProduct.active.find_by_permalink!(params[:id])
      @promotions = Spree::LineItemPromotion.active.available.find_all_by_product_id(@product.id)
    end
    
    def show
      return unless @product
      @title = @product.seo_title
      @promo = getPromotion(@product, params[:promo_code])
      @variants = @product.paperback_variants.active.includes([:option_values]).where(:product_id => @product.id, :active => true)
      
      respond_with(@product, @promo)
    end
    
    def index
      @products = Spree::PaperbackProduct.active
      @title = @products.first.seo_title
    end
  end
end