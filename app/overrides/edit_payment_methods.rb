Deface::Override.new(:virtual_path => "spree/admin/payment_methods/_form",
                     :name => "icon",
                     :insert_bottom => "[data-hook='payment_method']",
                     :partial => "spree/admin/shared/icon_payment_methods")