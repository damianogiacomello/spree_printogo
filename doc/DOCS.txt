#################################################################################################################
REQUISITI PER MULTI-PAGINA:

Tipologia di stampante [4/8 Colori]
Facciate da stampare
Costo Impianto
Formato della carta di stampa
Numero di copie
Grammatura
Plastificazione
Costi Stampa e Legatorie

1. Devo calcolare il numero di quartini di base da stampare tramite il calcolo: (facciate / 4) * numero di copie
2. Controllo il punto di convenienza per i quartini così da decidere se andare in Digitale o in Offset
3. Effettuo il calcolo per ritornarmi il costo per foglio
4. Se sono in digitale so che stampo un quartino e il calcolo mi viene facile in quanto non scarto mai e devo andare a piegare sempre con 1 segnatura e di conseguenza sia cucitura/punzonatura
5. Se sono in offset devo andare a vedere
	- se si tratta di una 4 Colori
		- vado ad effettuare il doppio preventivo [normale e come 8 Colori senza scarto]
	- se si tratta di una 8 Colori
		- vado ad effettuare il doppio preventivo [con scarto e senza scarto?]
6. Mi ricavo il numero di impianti e i vari costi
	- Costi di avviamento
	- Costo dei fogli
	- Costo della stampa
7. Mi calcolo il costo della plastificazione nel caso ci sia
8. Calcolo il costo della piegatura a seconda delle tipologie di impianti e del numero di fogli effettivi
9. Calcolo il costo della punzonatura o della cucitura come per la piegatura
10. Calcolo il costo della copertina a parte e lo aggiungo

NOTE:
Nel caso stia valutando di stampare su un formato di quartini dispari allora faccio prima -1 e poi divido per due
Si potrebbe circoscrivere il DOPPIO PREVENTIVO in:
	- calcolo il numero di impianti e relativo costo
	- calcolo il numero di fogli e relativo costo
	- calcolo il numero di avviamenti e nel caso le attese

#################################################################################################################
CALCOLO SPIRALE:

1. Verifico i dati inseriti (copi, facciate, plastificazione, copertina, carta, spirale/punto metallico/cucitura/fresatura, tipo stampa, formato, )
2. Effettuo il doppio preventivo e calcolo sia con la prima stampate che con la seconda (o B/N o COLORI)
3. Tengo solo il preventivo piu` basso e mi salvo il tipo di stampante, i quartini di stampa, il numero di foglio, il costo dei fogli, il peso dei fogli
4. Mi calcolo il costo della plastificazione di tutto nel caso ci sia
5. Calcolo il costo della copertina separata nel caso ci sia (cartoncino standard o grammatura differente)
6. Calcolo il costo della plastificazione della copertina se era stato impostato solo quello
7. Calcolo il costo della piegha nel caso ci sia (la copertina se ha grammatura alta viene cordonata)
8. Calcolo il costo della punzonatura o della cucitura/fresatura o della spiralatura (costo avviamento e costo per numero di copie o se in esterna costo fisso per basso numero copie e costo per tipologia o per copia)
9. Aggiungo il costo della copertina con o senza alette nel caso sia stata inserita la variabile
10. Aggiungo il costo della marginalita`

#################################################################################################################
REQUISITI PER PICCOLO FORMATO:

Tipologia di stampante [4/8 Colori]
Facciate da stampare
Costo Impianto
Formato della carta di stampa
Costi Stampa
Numero di copie
Grammatura
Plastificazione

1. Controllo il punto di convenienza per scegliere la stampante tramite il numero di copie
2. Effettuo il calcolo per ritornarmi il costo per foglio
4. Se sono in digitale so che stampo senza impianti e devo fare il calcolo semplice per un avviamento immediato al massimo sommando il costo del foglio per due nel caso di fronte e retro
5. Se sono in offset devo andare a vedere
	- se si tratta di una 4 Colori
		- vado a vedere se fronte e retro e nel caso considero un solo impianto
	- se si tratta di una 8 Colori
		- considero sempre due impianti altrimenti vado in perdita e quindi considero sempre un solo avviamento
6. Mi ricavo il numero di impianti e i vari costi
	- Costi di avviamento
	- Costo dei fogli
	- Costo della stampa
7. Mi calcolo il costo della plastificazione nel caso ci sia
8. Calcolo il costo del taglio

NOTE:


CALCOLO PUNTO DI CONVENIENZA:

(x/npose1)*(costo inchiostro1 + costo passaggio1) + (avviamento1 + impianti1) = (x/npose2)*(costo inchisotro2 + costo passaggio2) + (avviamento2 + impianti2)

(x/npose1)*(costo inchiostro1 + costo passaggio1) = (x/npose2)*(costo inchisotro2 + costo passaggio2) + (avviamento2 + impianti2) - (avviamento1 + impianti1)

(x/npose1) = ((x/npose2)*(costo inchisotro2 + costo passaggio2) + (avviamento2 + impianti2) - (avviamento1 + impianti1)) / (costo inchiostro1 + costo passaggio1)


TODO:
- scollegare la bobina dal plotter ????

- vorrei aggiungere i seguenti formati carta: ????
	1) 25x17,5
	2) 25x35
	3) 22x32
	4) 33x70 (un terzo del 70x100)

- impostare reparto piegatrici anche per lavorazione esterna (TIPO DI PIEGA - PIEGA ESTERNA SI - COSTO AVVIAMENTO - COSTO A COPIA CON POSSIBILITA' DIVERSIFICARE GRAMMATURA)

- sistemazione intercalatura ????

- calcolo miglior preventivo nel punto multipagina ????

- sistemare sul fronteend la scelta del formato cercherei di farne stare più possibile sulla stessa riga... [OK]
- sul frontend dello striscione il formato è impostato male [OK]

