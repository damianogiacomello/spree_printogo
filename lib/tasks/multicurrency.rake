namespace :spree_print2go do
  desc "Call spree_multicurrency with key, run only if spree_multicurrency is installed"
  task :refresh_currency_rate => :environment do
    exec 'bundle exec rake spree_multicurrency:refresh_currency_rate APP_ID=3dfafbbc9169404a9d0df6eb3c5d14e9'
  end
end