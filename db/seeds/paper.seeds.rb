# Aggiungo i Formati Carta
puts "Aggiungo i Formati Carta"
pf = Spree::OptionType.create(:name => 'paper', :presentation => 'Formati Carta')
papers = %w(usomano patinata_opaca patinata_lucida)
prices = {
    :usomano => {80 => 0.88, 90 => 0.88, 100 => 0.88, 110 => 0.88, 115 => 0.88, 120 => 0.88, 130 => 0.88, 140 => 0.88, 150 => 0.88, 160 => 0.88, 170 => 0.88, 200 => 0.88, 215 => 0.88, 230 => 0.88, 240 => 0.88, 250 => 0.88, 280 => 0.88, 300 => 0.88, 350 => 0.88},
    :patinata_opaca => {80 => 0.89, 90 => 0.865, 100 => 0.84, 110 => 0.84, 115 => 0.84, 120 => 0.84, 130 => 0.84, 140 => 0.84, 150 => 0.84, 160 => 0.84, 170 => 0.84, 200 => 0.84, 250 => 0.865, 300 => 0.89, 350 => 0.915},
    :patinata_lucida => {80 => 0.89, 90 => 0.865, 100 => 0.84, 110 => 0.84, 115 => 0.84, 120 => 0.84, 130 => 0.84, 140 => 0.84, 150 => 0.84, 160 => 0.84, 170 => 0.84, 200 => 0.84, 250 => 0.865, 300 => 0.89, 350 => 0.915}
}
papers.each do |p|
  sp = Spree::Paper.create!(:name => p.downcase.titleize, :presentation => p.downcase.titleize, :active => true)
  Spree::Config[:paper_weight].split(",").each do |weight|
    sp.paper_weights.create!(:weight => weight.to_s, :presentation => weight.to_s, :cost_cut => prices[p.to_sym][weight.to_i].to_f, :cost_ream => prices[p.to_sym][weight.to_i].to_f)
  end
end

# Aggiungo le grammature numero di fogli taglio
puts "Aggiungo le grammature numero di fogli taglio"
Spree::OptionType.create(:name => 'weight_cut_ream', :presentation => 'Grammatura numero di fogli taglio')

weight = %(80,90,100,110,115,120,130,140,150,160,170,200,215,230,240,250,280,300,350)
cut_weight = %(1100,1000,900,800,700,600,500,500,500,400,400,400,400,300,300,300,200,200,100)
weight.split(",").count.times do |wt|
  Spree::Weight.create(:name => weight.split(",")[wt], :presentation => cut_weight.split(",")[wt])
end
