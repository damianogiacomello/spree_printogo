# Aggiungo il Biglietto da Visita
puts "Aggiungo il Biglietto da Visita"
tp_pfu = Spree::OptionType.create(:name => "printer_format_card", :presentation => "Formati di stampa per i Biglietti")
pfca = Spree::OptionValue.create(:name => "a", :presentation => "8,5x5 cm", :option_type_id => tp_pfu.id)
pfca.create_template_format(:attachment => File.open('public/default/template_format/bc.jpg'))
#pfca.create_template(:attachment => File.open('public/default/template/8-5x5_orizzontale.pdf'))
pfcb = Spree::OptionValue.create(:name => "b", :presentation => "9x5 cm", :option_type_id => tp_pfu.id)
pfcb.create_template_format(:attachment => File.open('public/default/template_format/bc.jpg'))
#pfcb.create_template(:attachment => File.open('public/default/template/9x5_orizzontale.pdf'))
pfcc = Spree::OptionValue.create(:name => "c", :presentation => "8,5x5,5 cm", :option_type_id => tp_pfu.id)
pfcc.create_template_format(:attachment => File.open('public/default/template_format/bc.jpg'))
#pfcc.create_template(:attachment => File.open('public/default/template/8-5x5-5_orizzontale.pdf'))

bcp = Spree::BusinessCardProduct.new()
bcp.save
bcp.create_template(:attachment => File.open('public/default/template.pdf'))
bcp.master.images.create(:attachment => File.open('public/default/business_card.gif'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => bcp.id)

# Aggiungo il formato di stampa di default per i manifesti
puts "Aggiungo il formato di stampa di default per i manifesti"
Spree::OptionType.create(:name => 'poster_printer_format', :presentation => 'Formato di stampa dei manifesti')
Spree::PosterPrinterFormat.create(:name => 'personalizzato', :presentation => 'Personalizzato')
Spree::PosterPrinterFormat.create(:name => '21x33cm', :presentation => '21x33cm')
# Aggiungo il Manifesto
puts "Aggiungo il Manifesto"
pp = Spree::PosterProduct.new()
pp.save
pp.create_template(:attachment => File.open('public/default/template.pdf'))
#pp.create_template(:attachment => File.open('public/default/template/poster_template.pdf'))
pp.master.images.create(:attachment => File.open('public/default/poster.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pp.id)
# Aggiungo le configurazione per stampante e carta
pp.plotter = Spree::Plotter.find_by_presentation("Epson 9880").id
pp.papers << Spree::CoilValue.find_by_presentation("Blueback 150 gr").id
pp.papers << Spree::CoilValue.find_by_presentation("Canvas 300 gr").id
pp.available_on = Time.now - 2.days
pp.save

# Aggiungo il Flyer
puts "Aggiungo il Flyer"
fp = Spree::FlayerProduct.new(:label_name => "Flyer")
fp.save
fp.create_template(:attachment => File.open('public/default/template.pdf'))
#fp.create_template(:attachment => File.open('public/default/template/flyer_template.pdf'))
fp.master.images.create(:attachment => File.open('public/default/flyer.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => fp.id)

# Aggiungo il Manifesto Classico
puts "Aggiungo il Manifesto Classico"
pfp = Spree::PosterFlyerProduct.new(:label_name => "Manifesto Classico")
pfp.save
pfp.create_template(:attachment => File.open('public/default/template.pdf'))
#pfp.create_template(:attachment => File.open('public/default/template/poster_classic_template.pdf'))
pfp.master.images.create(:attachment => File.open('public/default/flyer.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pfp.id)

# Aggiungo il Punto Metallico
puts "Aggiungo il Punto Metallico"
sp = Spree::StapleProduct.new()
sp.save
sp.create_template(:attachment => File.open('public/default/template.pdf'))
#sp.create_template(:attachment => File.open('public/default/template/staple_template.pdf'))
cfp_sp = Spree::CoverFlyerProduct.new(:label_name => "Staple", :parent_id => sp.id)
cfp_sp.save
sp.master.images.create(:attachment => File.open('public/default/staple.gif'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => sp.id)

# Aggiungo la Brossura Filo Refe
puts "Aggiungo la Brossura Filo Refe"
pbp = Spree::PaperbackProduct.new(:label_name => "Filo Refe", :finishing => "seam")
pbp.save
pbp.create_template(:attachment => File.open('public/default/template.pdf'))
#pbp.create_template(:attachment => File.open('public/default/template/paperback_seam_template.pdf'))
cfp_pbp = Spree::CoverFlyerProduct.new(:label_name => "PaperbackSeam", :parent_id => pbp.id)
cfp_pbp.save
pbp.master.images.create(:attachment => File.open('public/default/paperback_seam.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pbp.id)

# Aggiungo la Brossura Fresata
puts "Aggiungo la Brossura Fresata"
pbp = Spree::PaperbackProduct.new(:label_name => "Fresata", :finishing => "milling")
pbp.save
pbp.create_template(:attachment => File.open('public/default/template.pdf'))
#pbp.create_template(:attachment => File.open('public/default/template/paperback_milling_template.pdf'))
cfp_pbp = Spree::CoverFlyerProduct.new(:label_name => "PaperbackMilled", :parent_id => pbp.id)
cfp_pbp.save
pbp.master.images.create(:attachment => File.open('public/default/paperback_milling.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pbp.id)

# Aggiungo il Pieghevole
puts "Aggiungo il Pieghevole"
fdp = Spree::FoldingProduct.new()
fdp.save
fdp.create_template(:attachment => File.open('public/default/template.pdf'))
#fdp.create_template(:attachment => File.open('public/default/template/folding_template.pdf'))
fdp.master.images.create(:attachment => File.open('public/default/folding.gif'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => fdp.id)

# Aggiungo Termosaldatura e Rinforzo Perimetrale
puts "Aggiungo Termosaldatura e Rinforzo Perimetrale"
Spree::OptionType.create(:name => 'heat_sealing', :presentation => 'Termosaldatura')
Spree::OptionType.create(:name => 'reinforcement_perimeter', :presentation => 'Rinforzo Perimetrale')
# Aggiungo lo Striscione
puts "Aggiungo lo Striscione"
bp = Spree::BannerProduct.new()
bp.save
bp.create_template(:attachment => File.open('public/default/template.pdf'))
#bp.create_template(:attachment => File.open('public/default/template/banner_template.pdf'))
bp.create_banner_variant
bp.master.images.create(:attachment => File.open('public/default/banner.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => bp.id)

# Aggiungo la Spirale
puts "Aggiungo la Spirale"
sp = Spree::SpiralProduct.new()
sp.save
sp.create_template(:attachment => File.open('public/default/template.pdf'))
#sp.create_template(:attachment => File.open('public/default/template/spiral_template.pdf'))
cfp_sp = Spree::CoverFlyerProduct.new(:label_name => "Spiral", :parent_id => sp.id)
cfp_sp.save
sp.master.images.create(:attachment => File.open('public/default/spiral.png'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => sp.id)

# Aggiungo il Canvas
puts "Aggiungo il Canvas"
cp = Spree::CanvasProduct.new()
cp.save
cp.create_template(:attachment => File.open('public/default/template.pdf'))
#cp.create_template(:attachment => File.open('public/default/template/canvas_template.pdf'))
cp.create_canvas_variant
bp.master.images.create(:attachment => File.open('public/default/canvas.gif'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => cp.id)

# Aggiungo la Carta Intestata
puts "Aggiungo la Carta Intestata"
lh = Spree::LetterheadProduct.new(:label_name => "Carta Intestata")
lh.save
lh.create_template(:attachment => File.open('public/default/template.pdf'))
#lh.create_template(:attachment => File.open('public/default/template/letterhead_template.pdf'))
lh.master.images.create(:attachment => File.open('public/default/letterhead.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => lh.id)

# Aggiungo le Cartoline e Inviti
puts "Aggiungo le Cartoline e Inviti"
pc = Spree::PostcardProduct.new(:label_name => "Cartoline e Inviti")
pc.save
pc.create_template(:attachment => File.open('public/default/template.pdf'))
#pc.create_template(:attachment => File.open('public/default/template/postcard_template.pdf'))
pc.master.images.create(:attachment => File.open('public/default/postcard.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pc.id)

# Aggiungo le Locandine
puts "Aggiungo le Locandine"
pb = Spree::PlaybillProduct.new(:label_name => "Locandine")
pb.save
pb.create_template(:attachment => File.open('public/default/template.pdf'))
#pb.create_template(:attachment => File.open('public/default/template/playbill_template.pdf'))
pb.master.images.create(:attachment => File.open('public/default/playbill.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pb.id)

# Aggiungo il PVC Adesivo
puts "Aggiungo il PVC Adesivo"
pp = Spree::PvcProduct.new()
pp.save
pp.create_template(:attachment => File.open('public/default/template.pdf'))
#pp.create_template(:attachment => File.open('public/default/template/pvc_template.pdf'))
pp.create_pvc_variant
pp.master.images.create(:attachment => File.open('public/default/pvc_adhesive.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pp.id)
