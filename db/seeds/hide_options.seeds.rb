after :default do
  # Aggiungo l'orientatamento come option type
  puts "Aggiunto l'orientamento"
  d = Spree::OptionType.create :name => 'printer_orientation', :presentation => 'Orientamento'
  d.option_values.create :name => 'horizontal', :presentation => 'Orizzontale'
  d.option_values.create :name => 'vertical', :presentation => 'Verticale'

  # Aggiungo il fronte e retro
  puts "Aggiunto stampa fronte e retro"
  fb = Spree::OptionType.create :name => "printer_instructions", :presentation => 'Stampa Fronte e retro'
  fb.option_values.create :name => 'front_back', :presentation => 'Fronte e retro'
  fb.option_values.create :name => 'equal_front_back', :presentation => 'Fronte e retro uguali'
  fb.option_values.create :name => 'different_front_back', :presentation => 'Fronte e retro differenti'
  fb.option_values.create :name => 'front', :presentation => 'Solo fronte'

  # Aggiungo se a colori o bianco e nero
  puts "Aggiungo Tipologia di stampa"
  pc = Spree::OptionType.create :name => "printer_color", :presentation => 'Tipologia di stampa'
  pc.option_values.create :name => 'true', :presentation => 'Colori'
  pc.option_values.create :name => 'false', :presentation => 'Bianco e Nero'

  # Aggiungo il costo orario come in OptionType che avrà un unico OptionValue
  hourly_cost = Spree::OptionType.create :name => 'hourly_cost', :presentation => 'Costo orario'
  hourly_cost.option_values.create :name => "60 minuti", :presentation => '50'
  puts "Aggiunto costo orario"

  # Aggiungo le pose
  puts "Aggiungo le Pose"
  Spree::OptionType.create(:name => 'pose', :presentation => 'Pose')
  Spree::Pose.create(:name => 1, :presentation => 4)
  Spree::Pose.create(:name => 2, :presentation => 6)
  Spree::Pose.create(:name => 3, :presentation => 8)
  Spree::Pose.create(:name => 4, :presentation => 8)
  Spree::Pose.create(:name => 5, :presentation => 12)
  Spree::Pose.create(:name => 6, :presentation => 10)
  Spree::Pose.create(:name => 7, :presentation => 16)
  Spree::Pose.create(:name => 8, :presentation => 12)
  Spree::Pose.create(:name => 9, :presentation => 12)
  Spree::Pose.create(:name => 10, :presentation => 14)
  Spree::Pose.create(:name => 12, :presentation => 14)
  Spree::Pose.create(:name => 14, :presentation => 18)
  Spree::Pose.create(:name => 16, :presentation => 16)
  Spree::Pose.create(:name => 24, :presentation => 12)
  Spree::Pose.create(:name => 25, :presentation => 20)
  Spree::Pose.create(:name => 32, :presentation => 14)

  # Aggiungo i quartini
  puts "Aggiungo i Quartini"
  Spree::OptionType.create(:name => 'quarter', :presentation => 'Taglio Quartini')
  Spree::Quarter.create(:name => 1, :presentation => 4)
  Spree::Quarter.create(:name => 2, :presentation => 8)
  Spree::Quarter.create(:name => 3, :presentation => 12)
  Spree::Quarter.create(:name => 4, :presentation => 16)
  Spree::Quarter.create(:name => 6, :presentation => 24)
  Spree::Quarter.create(:name => 8, :presentation => 32)
  Spree::Quarter.create(:name => 12, :presentation => 48)
  Spree::Quarter.create(:name => 16, :presentation => 64)
end