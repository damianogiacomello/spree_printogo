class AddDaysToShippingDateAdjustment < ActiveRecord::Migration
  def change
    add_column :spree_shipping_date_adjustments, :days, :integer
  end
end
