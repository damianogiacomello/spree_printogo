class AddShippingDateAdjustmentToLineItem < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :shipping_date_adjustment_id, :integer
  end
end
