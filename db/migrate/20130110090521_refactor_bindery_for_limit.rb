class RefactorBinderyForLimit < ActiveRecord::Migration
  def up
    rename_column :spree_limits, :bindery_before_min_c_id, :bindery_before_id
    rename_column :spree_limits, :bindery_after_c_id, :bindery_after_id
    remove_column :spree_limits, :bindery_before_max_c_id
    remove_column :spree_limits, :bindery_before_min_bw_id
    remove_column :spree_limits, :bindery_before_max_bw_id
    remove_column :spree_limits, :bindery_after_bw_id
    remove_column :spree_limits, :bindery_bw
    remove_column :spree_limits, :bindery_c
  end

  def down
    rename_column :spree_limits, :bindery_before_id, :bindery_before_min_c_id
    rename_column :spree_limits, :bindery_after_id, :bindery_after_c_id
    add_column :spree_limits, :bindery_before_max_c_id, :integer, :after => :bindery_before_min_c_id
    add_column :spree_limits, :bindery_before_min_bw_id, :integer, :after => :bindery_after_c_id
    add_column :spree_limits, :bindery_before_max_bw_id, :integer, :after => :bindery_before_min_bw_id
    add_column :spree_limits, :bindery_after_bw_id, :integer, :after => :bindery_before_max_bw_id
    add_column :spree_limits, :bindery_bw, :integer, :after => :bindery_after_bw_id
    add_column :spree_limits, :bindery_c, :integer, :after => :bindery_bw
  end
end
