class AddPriceToPresentationPack < ActiveRecord::Migration
  def change
    add_column :spree_presentation_packs, :price, :decimal
  end
end
