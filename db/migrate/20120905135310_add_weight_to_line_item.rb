class AddWeightToLineItem < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :weight, :float
  end
end
