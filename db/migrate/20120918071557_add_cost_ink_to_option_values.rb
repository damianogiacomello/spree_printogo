class AddCostInkToOptionValues < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :cost_ink_c, :decimal
    add_column :spree_option_values, :cost_ink_bw, :decimal
  end
end
