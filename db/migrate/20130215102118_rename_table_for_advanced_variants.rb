class RenameTableForAdvancedVariants < ActiveRecord::Migration
  def change
    rename_table :spree_attributes, :spree_simple_attributes
    rename_table :spree_values, :spree_simple_values
  end
end
