class AddHideToOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_types, :is_hide, :boolean
  end
end
