class AddTypeToOptionValues < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :type, :string
  end
end
