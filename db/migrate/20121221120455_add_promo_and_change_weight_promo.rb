class AddPromoAndChangeWeightPromo < ActiveRecord::Migration
  def up
    add_column :spree_line_items, :promo, :boolean, :default => false, :after => :price
    rename_column :spree_line_item_promotions, :weight, :weight_item
  end

  def down
    remove_column :spree_line_items, :promo
    rename_column :spree_line_item_promotions, :weight_item, :weight
  end
end
