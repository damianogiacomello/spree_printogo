class AddOptionValueIdToOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_types, :option_value_id, :integer
  end
end
