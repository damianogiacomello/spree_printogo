class AddFieldToCover < ActiveRecord::Migration
  def up
    add_column :spree_variants, :parent_id, :integer
    add_index :spree_variants, :parent_id
    add_column :spree_line_items_option_values, :variant_parent_id, :integer
    add_index :spree_line_items_option_values, :variant_parent_id
  end
  
  def down
    remove_column :spree_variants, :parent_id
    remove_index :spree_variants, :parent_id
    remove_column :spree_line_items_option_values, :variant_parent_id
    remove_index :spree_line_items_option_values, :variant_parent_id
  end
end
