class CreatePaperProduct < ActiveRecord::Migration
  def up
    create_table :spree_paper_products do |t|
      t.integer :product_id
      t.integer :paper_id
      t.integer :weight_id
    end
  end

  def down
    drop_table :spree_paper_products
  end
end
