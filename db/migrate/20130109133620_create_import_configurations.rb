class CreateImportConfigurations < ActiveRecord::Migration
  def up
    create_table :import_configurations do |t|
      t.string :csv_file_name, :csv_content_type
      t.integer :csv_file_size
      t.datetime :csv_updated_at
      
      t.boolean :imported
      t.text :error_log
            
      t.timestamps
    end
  end

  def down
    drop_table :import_configurations
  end
end
