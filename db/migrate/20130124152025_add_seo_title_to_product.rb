class AddSeoTitleToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :seo_title, :string, :after => :name
  end
end
