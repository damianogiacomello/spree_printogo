class PlotterCombinationPlotterType < ActiveRecord::Migration
  def change
    create_table :spree_processing_plotter_combination_processing_plotter_types, :id => false, :force => true do |t|
      t.integer  :processing_plotter_combination_id
      t.integer  :processing_plotter_type_id
    end
  end
end
