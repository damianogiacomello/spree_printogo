class AddPresentationItemPackToOrder < ActiveRecord::Migration
  def change
    add_column :spree_orders, :presentation_pack_id, :integer
  end
end
