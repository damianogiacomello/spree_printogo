class AddNumberOfCopyToProduct < ActiveRecord::Migration
  def up
    add_column :spree_products, :min_number_of_copies, :integer, :default => 1
    add_column :spree_products, :number_of_copies, :string
  end
  
  def down
    remove_column :spree_products, :min_number_of_copies
    remove_column :spree_products, :number_of_copies
  end
end
