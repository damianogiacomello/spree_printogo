class CreateSpreeUserPromotions < ActiveRecord::Migration
  def up
    create_table :spree_user_promotions do |t|
      t.decimal :amount, :scale => 5, :precision => 8
      t.integer :user_id, :product_id
      t.integer :number_of_copy
      t.boolean :included_in_price

      t.timestamps
    end
  end

  def down
    drop_table :spree_user_promotions
  end
end
