class RemoveQuarterForWasteConvenience < ActiveRecord::Migration
  def up
    remove_column :spree_limits, :quarter_bw
    remove_column :spree_limits, :quarter_c
  end

  def down
    add_column :spree_limits, :quarter_bw, :integer
    add_column :spree_limits, :quarter_c, :integer
  end
end
