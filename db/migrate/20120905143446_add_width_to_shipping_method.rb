class AddWidthToShippingMethod < ActiveRecord::Migration
  def change
    add_column :spree_shipping_methods, :min_width, :float
    add_column :spree_shipping_methods, :max_width, :float
  end
end
