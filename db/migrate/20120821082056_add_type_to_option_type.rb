class AddTypeToOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_types, :type, :string
    add_column :spree_option_values, :option_value_id, :integer

  end
end
