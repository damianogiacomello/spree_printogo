class ChangeDecimalForPrinterAndAddIndex < ActiveRecord::Migration
  def up
    change_column :spree_option_values, :start_up_cost_bw, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :plant_color_bw, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :price_front_bw, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :price_front_and_back_bw, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :start_up_cost_c, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :plant_color_c, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :price_front_c, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :price_front_and_back_c, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :cost_ink_c, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_values, :cost_ink_bw, :decimal, {:precision => 10, :scale => 5}
    
    add_index :spree_option_values, :option_type_id
    add_index :spree_option_values, :option_value_id
    
    add_index :spree_option_types, :option_value_id
    
    add_index :spree_limits, :printer_before_c_id
    add_index :spree_limits, :pose_before_c_id
    add_index :spree_limits, :format_before_c_id
    add_index :spree_limits, :printer_after_c_id
    add_index :spree_limits, :pose_after_c_id
    add_index :spree_limits, :format_after_c_id
    add_index :spree_limits, :printer_before_bw_id
    add_index :spree_limits, :pose_before_bw_id
    add_index :spree_limits, :format_before_bw_id
    add_index :spree_limits, :printer_after_bw_id
    add_index :spree_limits, :pose_after_bw_id
    add_index :spree_limits, :format_after_bw_id
    add_index :spree_limits, :variant_id
    add_index :spree_limits, :quarter_before_c_id
    add_index :spree_limits, :quarter_after_c_id
    add_index :spree_limits, :quarter_before_bw_id
    add_index :spree_limits, :quarter_after_bw_id
    add_index :spree_limits, :bindery_before_min_c_id
    add_index :spree_limits, :bindery_before_max_c_id
    add_index :spree_limits, :bindery_after_c_id
    add_index :spree_limits, :bindery_before_min_bw_id
    add_index :spree_limits, :bindery_before_max_bw_id
    add_index :spree_limits, :bindery_after_bw_id
    
    add_index :spree_line_items, :shipping_method_id
    add_index :spree_line_items, :shipping_date_adjustment_id
    
    add_index :spree_line_items_option_values, :option_value_id
    add_index :spree_line_items_option_values, :line_item_id
    
    add_index :spree_paper_products, :product_id
    add_index :spree_paper_products, :paper_id
    add_index :spree_paper_products, :weight_id
    
    add_index :spree_user_promotions, :user_id
    add_index :spree_user_promotions, :product_id
    
    add_index :spree_product_option_types, :product_id
    add_index :spree_product_option_types, :option_type_id
  end

  def down
    change_column :spree_option_values, :start_up_cost_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :plant_color_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_and_back_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :start_up_cost_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :plant_color_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_and_back_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :cost_ink_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :cost_ink_bw, :decimal, {:precision => 10, :scale => 2}
    
    remove_index :spree_option_values, :option_type_id
    remove_index :spree_option_values, :option_value_id
    
    remove_index :spree_option_types, :option_value_id
    
    remove_index :spree_limits, :printer_before_c_id
    remove_index :spree_limits, :pose_before_c_id
    remove_index :spree_limits, :format_before_c_id
    remove_index :spree_limits, :printer_after_c_id
    remove_index :spree_limits, :pose_after_c_id
    remove_index :spree_limits, :format_after_c_id
    remove_index :spree_limits, :printer_before_bw_id
    remove_index :spree_limits, :pose_before_bw_id
    remove_index :spree_limits, :format_before_bw_id
    remove_index :spree_limits, :printer_after_bw_id
    remove_index :spree_limits, :pose_after_bw_id
    remove_index :spree_limits, :format_after_bw_id
    remove_index :spree_limits, :variant_id
    remove_index :spree_limits, :quarter_before_c_id
    remove_index :spree_limits, :quarter_after_c_id
    remove_index :spree_limits, :quarter_before_bw_id
    remove_index :spree_limits, :quarter_after_bw_id
    remove_index :spree_limits, :bindery_before_min_c_id
    remove_index :spree_limits, :bindery_before_max_c_id
    remove_index :spree_limits, :bindery_after_c_id
    remove_index :spree_limits, :bindery_before_min_bw_id
    remove_index :spree_limits, :bindery_before_max_bw_id
    remove_index :spree_limits, :bindery_after_bw_id
    
    remove_index :spree_line_items, :shipping_method_id
    remove_index :spree_line_items, :shipping_date_adjustment_id
    
    remove_index :spree_line_items_option_values, :option_value_id
    remove_index :spree_line_items_option_values, :line_item_id
    
    remove_index :spree_paper_products, :product_id
    remove_index :spree_paper_products, :paper_id
    remove_index :spree_paper_products, :weight_id
    
    remove_index :spree_user_promotions, :user_id
    remove_index :spree_user_promotions, :product_id
    
    remove_index :spree_product_option_types, :product_id
    remove_index :spree_product_option_types, :option_type_id
  end
end
