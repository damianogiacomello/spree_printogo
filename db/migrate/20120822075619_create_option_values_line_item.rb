class CreateOptionValuesLineItem < ActiveRecord::Migration
  def up
    create_table :spree_line_items_option_values do |t|
      t.integer :option_value_id
      t.integer :line_item_id

      t.timestamps
    end
  end

  def down
    drop_table :spree_line_items_option_values
  end
end
