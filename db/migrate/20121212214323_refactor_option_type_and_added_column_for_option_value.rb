class RefactorOptionTypeAndAddedColumnForOptionValue < ActiveRecord::Migration
  def up
    add_column :spree_products, :finishing, :string, :after => :type
    add_column :spree_option_values, :copy_cost_seam, :boolean
    add_column :spree_option_values, :copy_cost_milling, :boolean
    add_column :spree_option_values, :hourly_cost_milling, :decimal, {:precision => 10, :scale => 5}
    add_column :spree_option_values, :min_limit_milling, :float
    add_column :spree_option_values, :cost_milling, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_types, :hourly_cost, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_option_types, :start_time, :decimal, {:precision => 10, :scale => 5}
  end

  def down
    remove_column :spree_products, :finishing
    remove_column :spree_option_values, :copy_cost_seam
    remove_column :spree_option_values, :copy_cost_milling
    remove_column :spree_option_values, :hourly_cost_milling
    remove_column :spree_option_values, :min_limit_milling
    remove_column :spree_option_values, :cost_milling
    change_column :spree_option_types, :hourly_cost, :decimal, {:precision => 10, :scale => 0}
    change_column :spree_option_types, :start_time, :decimal, {:precision => 10, :scale => 0}
  end
end
