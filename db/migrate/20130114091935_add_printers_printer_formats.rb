class AddPrintersPrinterFormats < ActiveRecord::Migration
  def up
    create_table :printer_format_defaults_printers, :id => false do |t|
      t.integer :printer_id
      t.integer :printer_format_default_id
    end
  end

  def down
    drop_table :printer_format_defaults_printers
  end
end
