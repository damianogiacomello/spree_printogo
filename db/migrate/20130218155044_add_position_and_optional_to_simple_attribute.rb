class AddPositionAndOptionalToSimpleAttribute < ActiveRecord::Migration
  def change
    add_column :spree_simple_values, :position, :integer, :after => :presentation
    add_column :spree_simple_attributes, :position, :integer, :after => :presentation
    add_column :spree_simple_attributes, :optional, :boolean, :default => false, :after => :position
  end
end
