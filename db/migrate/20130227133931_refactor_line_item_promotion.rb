class RefactorLineItemPromotion < ActiveRecord::Migration
  def change
    remove_column :spree_line_item_promotions, :shipping_method_id
    add_column :spree_line_item_promotions, :shipping_date, :date, :after => :product_id
    add_column :spree_line_item_promotions, :active, :boolean, :default => false, :after => :expires_at
    add_column :spree_line_items, :shipping_date, :date, :after => :shipping_date_adjustment_id
  end
end
