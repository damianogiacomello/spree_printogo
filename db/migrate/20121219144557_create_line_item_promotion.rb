class CreateLineItemPromotion < ActiveRecord::Migration
  def up
    create_table :spree_line_item_promotions do |t|
      t.integer :variant_id, :product_id, :shipping_method_id, :shipping_date_adjustment_id
      t.integer :quantity
      t.float :weight
      t.decimal :price, {:precision => 10, :scale => 5}
      
      t.timestamps
    end
  end

  def down
    drop_table :spree_line_item_promotions
  end
end
