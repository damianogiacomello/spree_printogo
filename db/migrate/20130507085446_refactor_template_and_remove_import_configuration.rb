class RefactorTemplateAndRemoveImportConfiguration < ActiveRecord::Migration
  def change
    add_column :spree_templates, :default, :boolean, :after => :attachment_updated_at, :default => false
    drop_table :import_configurations
  end
end
