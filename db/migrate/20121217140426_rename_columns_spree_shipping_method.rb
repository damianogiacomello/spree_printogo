class RenameColumnsSpreeShippingMethod < ActiveRecord::Migration
  def up
    rename_column :spree_shipping_methods, :min_width, :min_weight
    rename_column :spree_shipping_methods, :max_width, :max_weight
    add_column :spree_templates, :type, :string, :after => :template_updated_at
    rename_column :spree_templates, :template_file_name, :attachment_file_name
    rename_column :spree_templates, :template_content_type, :attachment_content_type
    rename_column :spree_templates, :template_file_size, :attachment_file_size
    rename_column :spree_templates, :template_updated_at, :attachment_updated_at
  end

  def down
    rename_column :spree_shipping_methods, :min_weight, :min_width
    rename_column :spree_shipping_methods, :max_weight, :max_width
    remove_column :spree_templates, :type
    rename_column :spree_templates, :attachment_updated_at, :template_updated_at
    rename_column :spree_templates, :attachment_file_size, :template_file_size
    rename_column :spree_templates, :attachment_content_type, :template_content_type
    rename_column :spree_templates, :attachment_file_name, :template_file_name
  end
end
