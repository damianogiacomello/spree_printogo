class AddDefaultToLayout < ActiveRecord::Migration
  def change
    add_column :spree_taxonomies, :default, :boolean, :after => :name, :default => false
    add_column :spree_taxons, :default, :boolean, :default => false
    add_column :spree_menus, :default, :boolean, :default => false
    add_column :spree_banners, :default, :boolean, :after => :bannerable_type, :default => false
  end
end
