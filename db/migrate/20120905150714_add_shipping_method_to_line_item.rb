class AddShippingMethodToLineItem < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :shipping_method_id, :integer
  end
end
