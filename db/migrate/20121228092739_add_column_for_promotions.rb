class AddColumnForPromotions < ActiveRecord::Migration
  def up
    add_column :spree_line_item_promotions, :unlimited, :boolean, :default => false, :after => :price
    add_column :spree_line_item_promotions, :expires_at, :date, :default => nil, :after => :unlimited
    add_column :spree_line_items, :line_item_promotion_id, :integer, :after => :promo
    rename_column :spree_line_items, :image_file_name, :file_attachment_file_name
    rename_column :spree_line_items, :image_content_type, :file_attachment_content_type
    rename_column :spree_line_items, :image_file_size, :file_attachment_file_size
    rename_column :spree_line_items, :image_updated_at, :file_attachment_updated_at
    add_column :spree_line_items, :file_attachment_url, :string, :after => :updated_at
  end

  def down
    remove_column :spree_line_item_promotions, :unlimited
    remove_column :spree_line_item_promotions, :expires_at
    remove_column :spree_line_items, :line_item_promotion_id
    rename_column :spree_line_items, :file_attachment_updated_at, :image_updated_at
    rename_column :spree_line_items, :file_attachment_file_size, :image_file_size
    rename_column :spree_line_items, :file_attachment_content_type, :image_content_type
    rename_column :spree_line_items, :file_attachment_file_name, :image_file_name
    remove_column :spree_line_items, :file_attachment_url
  end
end
