class AddPolymorphicToSlide < ActiveRecord::Migration
  def up
    add_column :spree_slides, :slideable_id, :integer, :after => :type
    add_column :spree_slides, :slideable_type, :string, :after => :slideable_id
    add_column :spree_banners, :bannerable_id, :integer, :after => :attachment_size
    add_column :spree_banners, :bannerable_type, :string, :after => :bannerable_id
  end

  def down
    remove_column :spree_slides, :slideable_id
    remove_column :spree_slides, :slideable_type
    remove_column :spree_banners, :bannerable_id
    remove_column :spree_banners, :bannerable_type
  end
end
