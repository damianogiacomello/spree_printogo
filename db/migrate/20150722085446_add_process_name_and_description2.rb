class AddProcessNameAndDescription2 < ActiveRecord::Migration
	def change
		add_column :spree_line_items, :process_name, :string, :after => :notes
		add_column :spree_products, :description2, :text, :after => :description
	end
end
